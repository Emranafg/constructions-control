<?php

namespace App\Http\Controllers;

use App\DataTables\DocumentDataTable;
use App\Document;
use App\DocumentCase;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Transformers\DocumentTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\DocumentRequest;
use Validator;
class DocumentController extends Controller
{
    public function __construct(Document $document)
    {
        $this->document = $document;

    }

    /**
     * @return mixed
     */
    public function index()
    {
        $infCount = $this->document->all()->count();
        return view('admin.document.document_search', compact('infCount'));
    }

    /**
     * @return mixed
     */
    public function createDocument()
    {
        return view('admin.document.document_create');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function details($id)
    {
        $documentDetails = $this->document->where('id', $id)->with('DocumentUpload')->first();
        return view('admin.document.details', compact('documentDetails'));
    }

    /**
     * @param Request $request
     * @param DocumentRequest $validator
     * @return mixed
     */
    public function documentStore(Request $request, DocumentRequest $validator)
    {
        $document = new Document;
        $document->progress = $request->input('progress');
        $document->title = $request->input('title');
        $document->source = $request->input('source');
       $document->destination = $request->input('destination');
        $document->start_date = $request->input('start_date');
        $document->file = $request->input('file');
        $document->case = $request->input('case');
        $document->expiration_date = $request->input('expiration_date');
        $document->priority = $request->input('priority');
        $document->category = $request->input('category');
        $document->version = $request->input('version');
        $document->save();
        return redirect('/documents')->with('message_title', trans('message.d_c_success_title'))->with('message_description', trans('message.d_c_success_desc'))->with('message_class', 'alert-success');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function createCase($id)
    {
        return view('admin.document.document_case', compact('id'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function storeCase(Request $request)
    {
		
	
		
		
		
        $case = new DocumentCase;
        $case->documents_id = $request->input('document_id');
      
        $case->description = $request->input('case');
		
			$files = $request->file('images');

        foreach($files as $image){

          $random_name = str_random(16);
          $destinationPath = 'standard/assets/documents/';
          $extension = $image->getClientOriginalExtension();
          $img_url = $random_name . "." . $extension;
          $image->move($destinationPath, $img_url);
		  
		  
		}
		  $case->file = $destinationPath.$img_url;
		
        $case->save();
        return redirect('/document/details/' . $request->input('document_id'))->with('message_title', trans('message.c_success_title'))->with('message_description', trans('message.c_success_desc'))->with('message_class', 'alert-success');
    }

    /**
     *
     */
    public function getExcelFile()
    {
        $this->document->setDataForExcelExport($this->document->all());
        return $this->document->generatExcelReport();
    }

    /**
     * @param $progress
     * @param $priority
     * @param $version
     * @param $date
     */
    public function getCustomizeDataForExcel($progress, $priority, $version, $date)
    {
        $data = $this->document->getCustomizedDataBaseOnUrlParameters($progress, $priority, $version, $date);
        $this->document->setDataForExcelExport($data);
        $this->document->generatExcelReport();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function searchDocument(Request $request)
    {
        return $this->document->getCustomeFilteredDataForDataTable($request);
    }

    /**
     * show data intry blade for project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @param Request $request
     * @return string
     */
    public function upload(Request $request)
    {
        $rules = array(
            'file' => 'required|mimes:jpg,jpeg,pdf,doc,docx,png|max:6000'
            );
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return response()->json('error', 400);
        }
        $image = $request->file('file');
        $random_name = str_random(16);
        $destinationPath = 'standard/assets/documents';
        $extension = $image->getClientOriginalExtension();
        $img_url = $random_name . "." . $extension;
        $image->move($destinationPath, $img_url);

        // resizing image
        $img = 'standard/assets/documents/' . $img_url;
        if ($image)
        {
            return response()->json($img, 200);
        } else
        {
            return response()->json('error', 400);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function editDocument($id)
    {
        $document = Document::findOrFail($id);
        return view('admin.document.document_edit', compact('document'));
    }

    /**
     * @param Request $request
     * @param DocumentRequest $validator
     * @return mixed
     */
    public function updateDocument(Request $request, DocumentRequest $validator)
    {
        $document = Document::findOrFail($request->input('id'));
        $document->progress = $request->input('progress');
        $document->title = $request->input('title');
        $document->source = $request->input('source');
        $document->destination = $request->input('destination');
        $document->start_date = $request->input('start_date');
        $document->file            = $request->input('file');
        $document->expiration_date = $request->input('expiration_date');
        $document->case            = $request->input('case');
        $document->priority = $request->input('priority');
        $document->category = $request->input('category');
        $document->version = $request->input('version');
        $document->save();
        return redirect('/documents')->with('message_title', trans('message.d_u_success_title'))->with('message_description', trans('message.d_u_success_desc'))->with('message_class', 'alert-success');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteDocument($id)
    {
        Document::findOrFail($id)->delete();
        return redirect()->back()->with('message_title', trans('message.d_d_success_title'))->with('message_description', trans('message.d_d_success_desc'))->with('message_class', 'alert-success');
    }


}
