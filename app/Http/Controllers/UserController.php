<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\District;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Hash;
use DB;
use JsValidator;
use Validator;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
class UserController extends Controller
{

	protected $validationRules=[
		'first_name' => 'required|string|max:255',
	    	'last_name' => 'required|string|max:255',
		'email' => 'required|email|max:255',
		'role' => 'required|alpha|max:255',
		'phone' => 'required|numeric|digits:10',
		// 'password' => 'required|strong_password|min:6|max:50|confirmed',
	];

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$users = User::all();
		return view('admin.users.index',compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$district = District::all();
		return view('admin.users.create', compact('district'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request, UserRequest $validator)
	{
		$password = $request->input('password');
		$name = $request->input('first_name').$request->input('last_name');
		$user = new User;
		$user->first_name = $request->input('first_name');
		$user->last_name = $request->input('last_name');
		$user->email = $request->input('email');
		$user->phone = $request->input('phone');
		$user->user_district = $request->input('user_district');
		$user->password = bcrypt($request->input('password'));
		$user->save();

		$role = Role::firstOrCreate(['name' => $request->input('role')]);
		$user->assignRole($request->input('role'));

		$count = count($request->input('permission'));
		for ($index = 0; $index < $count; $index ++)
		{
			$permission = $request->input('permission')[ $index ];
			$permissions = Permission::firstOrCreate(['name' => $permission]);
			$user->givePermissionTo($permission);
		}

	    $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
	    $beautymail->send('emails.account', ['user' => $user, 'password' => $password], function($message) use ($user)
	    {
	        $message
	            ->from('edanish@cyberaan.com')
	            ->to($user->email, $user->first_name." ".$user->last_name)
	            ->subject('Account Information!');
	    });
		return redirect('/users')
			->with('message_title', trans('message.u_c_success_title'))
			->with('message_description', trans('message.u_c_success_desc'))
			->with('message_class', 'alert-success');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$user = User::findOrFail($id);
		return view('admin.users.show', compact('user'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function permission($id)
	{
		$users= Permission::findOrFail($id)->users()->get();
		$permission = Permission::findOrFail($id);
		return view('admin.users.user', compact('users', 'permission'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$validator = JsValidator::make($this->validationRules);
		
		$user = User::findOrfail($id);
		$district = District::all();
		return view('admin.users.edit', compact('validator','user', 'district'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		$validator = Validator::make($request->all(), $this->validationRules);
		if($validator->fails()){
			return redirect()->back()
				->with('message_title', trans('message.query_error_title'))
				->with('message_description', trans('message.query_error_desc'))
				->with('message_class', 'alert-error');
		}
		$user = User::findOrfail($request->input('id'));
		$user->first_name = $request->input('first_name');
		$user->last_name = $request->input('last_name');
		$user->email = $request->input('email');
		$user->phone = $request->input('phone');
		$user->user_district = $request->input('user_district');
		if($request->input('password'))
		{
			$user->password = bcrypt($request->input('password'));
		}
		$user->save();

		$role = Role::firstOrCreate(['name' => $request->input('role')]);
		$user->syncRoles([$request->input('role')]);

		$count = count($request->input('permission'));
		for ($index = 0; $index < $count; $index ++)
		{
			$permission = $request->input('permission')[ $index ];
			$permissions = Permission::firstOrCreate(['name' => $permission]);
		}
		$data = $request->input('permission');
		$user->syncPermissions($data);
		return redirect('/users')
			->with('message_title', trans('message.u_u_success_title'))
			->with('message_description', trans('message.u_u_success_desc'))
			->with('message_class', 'alert-success');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{

		$user = User::findOrfail($id);
		$role = $user->roles;
		if(count($role))
		{
			$role = $user->roles[0]['name'];
			$user->removeRole($role);
		}
		$count = count($user->permissions);
		for ($index = 0; $index < $count; $index ++)
		{
			$permission = $user->permissions[$index]['name'];
			$user->revokePermissionTo($permission);
		}
		$user->delete();
		return redirect('/users')
			->with('message_title', trans('message.u_d_success_title'))
			->with('message_description', trans('message.u_d_success_desc'))
			->with('message_class', 'alert-success');
	}

	/**
	 * Display the the current user resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function profile()
	{
		$user = Auth::user();
		return view('admin.users.profile', compact('user'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function profileUpdate(Request $request)
	{
		$user = Auth::user();
		$user = User::findOrfail($user->id);
		$user->first_name = $request->input('first_name');
		$user->last_name = $request->input('last_name');
		$user->email = $request->input('email');
		$user->phone = $request->input('phone');
		$user->save();
		return redirect('/user/profile')
			->with('message_title', trans('message.profile_u_success_title'))
			->with('message_description', trans('message.profile_u_success_desc'))
			->with('message_class', 'alert-success');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function resetPassword(Request $request)
	{
		$user = Auth::user();
		$user = User::findOrfail($user->id);
		$password = $user->password;
		$current_password = $request->input('current_password');
		if(Hash::check($current_password, $password))
		{
			$user->password = bcrypt($request->input('new_password'));
			$user->save();
			return redirect('/user/profile')
				->with('message_title', trans('message.p_success_title'))
				->with('message_description', trans('message.p_success_desc'))
				->with('message_class', 'alert-success');
		}
		return redirect('/user/profile')
			->with('message_title', trans('message.p_success_title'))
			->with('message_description', trans('message.p_success_desc'))
			->with('message_class', 'alert-danger');
	}
}
