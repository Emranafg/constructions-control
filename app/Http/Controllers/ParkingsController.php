<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parking;
use App\District;

class ParkingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parking = Parking::all();
        // return $building;
       return view("Parking.index",compact("parking"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parking = Parking::all();
    $district = District::all();
        // $user = Auth::user();
        // $district = District::all();
        // $violationType = ViolationType::all();
        return view("parking.create", compact("district"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  dd($request->all());
        $gul_building = new Parking;
        
        $gul_building->District = $request->district;
        $gul_building->Guzar = $request->Guzar;
        $gul_building->Block = $request->Block;
        $gul_building->Parcel = $request->Parcel;
        $gul_building->Unit = $request->Unit;
        $gul_building->Name = $request->name;
        $gul_building->Father_name = $request->father_name;
        $gul_building->grand_father_name = $request->G_father_name;
        $gul_building->Tazkira = $request->Tazkira;
       
        $gul_building->Area_size = $request->Area_size;
        $gul_building->Latitude = $request->Latitude;
        $gul_building->Longtitude = $request->Area_size;
        $gul_building->molkiyat = $request->Molkiyat;
        $gul_building->Contact = $request->Contact;
        $gul_building->land_catagory = $request->land_catagory;
        $gul_building->land_type = $request->type_of_activity;
        $gul_building->Comment = $request->comment;
        if($request->hasFile("parking_image")) {
            
            $gul_building->building_image = $request->parking_image;
         }
        

    







         $gul_building->save();
         return redirect('/Parkings')->with('message_title', trans('message.i_c_success_title'))->with('message_description', trans('message.i_c_success_desc'))->with('message_class', 'alert-success');
  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $gul_habib =  Parking::find($id);
        $gul_building->District = $request->district;
        $gul_building->Guzar = $request->Guzar;
        $gul_building->Block = $request->Block;
        $gul_building->Parcel = $request->Parcel;
        $gul_building->Unit = $request->Unit;
        $gul_building->Name = $request->name;
        $gul_building->Father_name = $request->father_name;
        $gul_building->grand_father_name = $request->G_father_name;
        $gul_building->Tazkira = $request->Tazkira;
        $gul_building->Area_size = $request->Area_size;
        $gul_building->Latitude = $request->Latitude;
        $gul_building->Longtitude = $request->Area_size;
        $gul_building->molkiyat = $request->molkiyat;
        $gul_building->Contact = $request->Contact;
        $gul_building->land_catagory = $request->land_catagory;
        $gul_building->land_type = $request->land_type;
        $gul_building->contruction_certificate = $request->contruction_certificate;
        $gul_building->Comment = $request->Comment;
        if($request->hasFile("parking_image")) {
            
            $gul_building->building_image = $request->parking_image;
         }
        

    







         $gul_building->save();
         return redirect('/Parkings')->with('message_title', trans('message.i_c_success_title'))->with('message_description', trans('message.i_c_success_desc'))->with('message_class', 'alert-success');
  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $gul_habib =  Parking::find($id);
        $gul_habib->delete();
      
        return redirect('/Parkings')->with('message_title', trans('message.i_c_success_title'))->with('your parking have been deleted', trans('message.i_c_success_desc'))->with('message_class', 'alert-success');
  
    }
}
