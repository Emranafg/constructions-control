<?php

namespace App\Http\Controllers;


use App\Infraction;
use App\Transformers\ViolationTransformer;
use Illuminate\Http\Request;
use App\ViolationUpload;
use App\District;
use App\Supervisor;
use App\InfractionUser;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\ViolationRequest;
use App\Http\Requests\ViolationFileRequest;
use File;
use Validator;
use App\ViolationType;
use App\ViolationTypeInfraction;
use App\ViolationDocument;
use App\ViolationDocumentFile;
use Carbon\Carbon;
class InfractionController extends Controller
{

    public function __construct(Infraction $infraction)
    {
        $this->infraction = $infraction;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::all();
        $infCount = $this->infraction->all()->count();
        $district = District::all();
        $violationType = ViolationType::all();
        return view('admin.infractions.infractions', compact('users', 'infCount', 'district','violationType'));
    }

    /**
     * make a filterd dataTable base on request parameter
     * @param Request $request
     * @return mixed
     */
    public function getCustomFilterData(Request $request)
    {
        return $this->infraction->getCustomeFilteredDataForDataTable($request);
    }

    /**
     * @param $district
     * @param $certification
     * @param $regionStatus
     * @param $activityType
     * @param $cunstractionSt
     * @param $infractionsSt
     * @param $proceedings
     * @param $violationType
     * @return mixed
     */
    public function generateCustomizedExcelSheet($district, $certification, $regionStatus, $activityType, $cunstractionSt, $infractionsSt, $proceedings, $violationType)
    {
        return $this->infraction->getCustomizedDataBaseOnUrlParametersForExcel($district, $certification, $regionStatus, $activityType, $cunstractionSt, $infractionsSt, $proceedings, $violationType);
    }

    /**
     *
     */
    public function getExcel()
    {
        return $this->infraction->setExcelData($this->infraction->all())->generateExcelBaseOnUrlData();
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewViolationDetails($id)
    {
        $infractionWithoutTranformer = $this->infraction->find($id);
        //google map cordinates
        $coordinates = ViolationTransformer::transformGMapCoordintes($infractionWithoutTranformer);

        //creating supervisor for this Infraction
        $supervisor = new  Supervisor();
        //check whether we have supervisor for this Infraction or not
        if ($infractionWithoutTranformer->members()->count() > 0)
        {
            $supervisor = $infractionWithoutTranformer->members()->get()->last();
        }
        //transform Infraction table data to readable viwable data
        $infraction = fractal()->item($infractionWithoutTranformer)->transformWith(new ViolationTransformer())->toArray();
        // convert from array to Infraction Object that we can refere to it's property in view
        $infraction = new Infraction($infraction['data']);
        $infractionUploads = $infractionWithoutTranformer->uploads;

        // retrive violation type from violation_type table
        $infvt = DB::table('violation_type')
            ->join('violation_type_infraction', 'violation_type_infraction.vt_id', '=', 'violation_type.id')
            ->select('violation_type.*')->where('violation_type_infraction.in_id',$id)
            ->get();

        // Violation documents
        // $violationDouments = ViolationDocument::all()->where('infraction_id',$id);
        $violationDouments = Infraction::find($id)->ViolationDocuments()->get();


        return view('admin.infractions.infraction_details', compact('infraction', 'infractionUploads', 'coordinates', 'supervisor','infvt'))->with('id',$id)->with('violationDouments',$violationDouments);
    }

    /**
     * @return mixed
     */
    public function create() 
    {
        $user = Auth::user();
        $district = District::all();
        $violationType = ViolationType::all();
        return view('admin.infractions.create', compact('user', 'district','violationType'));
    }

    /**
     * @param Request $request
     * @return string
     */
    public function store(Request $request, ViolationRequest $validator)
    {

        //dd($request->all());
       // return $request->images;
        $user_id = Auth::user()->id;
        $infraction = new Infraction;
        $infraction->serial_number = $request->input('serial_number');
        $infraction->name = $request->input('name');
        $infraction->father_name = $request->input('father_name');
        $infraction->tazkira_number = $request->input('tazkira_number');
        $infraction->job = $request->input('job');
        $infraction->property_area = $request->input('property_area');
        $infraction->district = $request->input('district');
        $infraction->street = $request->input('street');
        $infraction->property_number = $request->input('property_number');
        $infraction->contruction_certificate = $request->input('contruction_certificate');
        $infraction->comment = $request->input('comment');
        $infraction->region_status = $request->input('region_status');
        $infraction->type_of_activity = $request->input('type_of_activity');
        $infraction->status_of_cunstraction = $request->input('status_of_cunstraction');
        $infraction->start_date = $request->input('start_date');
        $infraction->monitor_date = $request->input('monitor_date');
        $infraction->proceedings = $request->input('proceedings');
        $infraction->status_of_infractions = $request->input('status_of_infractions');
        // $infraction->violation_type = $request->input('violation_type');
        $latitude = ($request->input('latitude_second') / 60 + $request->input('latitude_minute')) / 60 + $request->input('latitude_degree');
        $longitude = ($request->input('longitude_second') / 60 + $request->input('longitude_minute')) / 60 + $request->input('longitude_degree');
        $infraction->latitude = $latitude;
        $infraction->longitude = $longitude;
        $infraction->save();
        $infraction_user = InfractionUser::firstOrCreate(['infraction_id' => $infraction->id, 'user_id' => $user_id,]);
if($request->hasFile("images")) {
    $files = $request->file('images');

    foreach($files as $image){

      $random_name = str_random(16);
      $destinationPath = 'standard/assets/images/';
      $extension = $image->getClientOriginalExtension();
      $img_url = $random_name . "." . $extension;
      $image->move($destinationPath, $img_url);

      $violation_image = new ViolationUpload;
      $violation_image->infraction_id = $infraction->id;
      $violation_image->name = $image->getClientOriginalName();
      $violation_image->path = $destinationPath.$img_url;
      $violation_image->save();

    }
}
        

        $countb = count($request->input('violation_type'));
        for ($index = 0; $index < $countb; $index++)
        {
            $vti = new ViolationTypeInfraction;
            $vti->in_id = $infraction->id;
            $vti->vt_id = $request->input('violation_type')[$index];
            $vti->save();
        }

        return redirect('/km/infractions')->with('message_title', trans('message.i_c_success_title'))->with('message_description', trans('message.i_c_success_desc'))->with('message_class', 'alert-success');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $infraction_user = InfractionUser::where('infraction_id', $id)->with('user')->first();
        // dd($infraction_user);
        $district = District::all();
        $violationType = ViolationType::all();
        $infraction = Infraction::findOrFail($id);
        $infractionUploads = $infraction->uploads;
        $result_lat = $infraction->latitude;
        $result_lng = $infraction->longitude;
        $latitude_degree = $result_lat - fmod($result_lat, 1);
        $latitude_minute1 = 60 * fmod($result_lat, 1);
        $latitude_minute = $latitude_minute1 - fmod($latitude_minute1, 1);
        $latitude_second = round(60 * fmod($latitude_minute1, 1), 2);
        $longitude_degree = $result_lng - fmod($result_lng, 1);
        $longitude_minute1 = 60 * fmod($result_lng, 1);
        $longitude_minute = $longitude_minute1 - fmod($longitude_minute1, 1);
        $longitude_second = round(60 * fmod($longitude_minute1, 1), 2);

        // get the violation types
        $infvt = ViolationTypeInfraction::all()->where('in_id',$id);

        // return for example violation type 1 2 5





        return view('admin.infractions.infraction_edit', compact('infraction_user', 'infraction', 'district' ,'infractionUploads', 'latitude_degree', 'latitude_minute', 'latitude_second', 'longitude_degree', 'longitude_minute', 'longitude_second','violationType','infvt'));
    }

    /**
     * @param Request $request
     * @param ViolationRequest $validator
     * @return mixed
     */
    public function update(Request $request, ViolationRequest $validator)
    {
        $id = $request->input('infraction_id');
        $infraction = Infraction::findOrFail($id);
        $infraction->serial_number = $request->input('serial_number');
        $infraction->name = $request->input('name');
        $infraction->father_name = $request->input('father_name');
        $infraction->tazkira_number = $request->input('tazkira_number');
        $infraction->job = $request->input('job');
        $infraction->property_area = $request->input('property_area');
        $infraction->district = $request->input('district');
        $infraction->street = $request->input('street');
        $infraction->property_number = $request->input('property_number');
        $infraction->contruction_certificate = $request->input('contruction_certificate');
        $infraction->comment = $request->input('comment');
        $infraction->region_status = $request->input('region_status');
        $infraction->type_of_activity = $request->input('type_of_activity');
        $infraction->status_of_cunstraction = $request->input('status_of_cunstraction');
        $infraction->start_date = $request->input('start_date');
        $infraction->monitor_date = $request->input('monitor_date');
        $infraction->proceedings = $request->input('proceedings');
        $infraction->status_of_infractions = $request->input('status_of_infractions');
        // $infraction->violation_type = $request->input('violation_type');
        $latitude = ($request->input('latitude_second') / 60 + $request->input('latitude_minute')) / 60 + $request->input('latitude_degree');
        $longitude = ($request->input('longitude_second') / 60 + $request->input('longitude_minute')) / 60 + $request->input('longitude_degree');
        $infraction->latitude = $latitude;
        $infraction->longitude = $longitude;
        $infraction->save();

    //    // $count = count($request->input('file'));
    //     //for ($index = 0; $index < $count; $index++)
    //     //{

    //         if($request->hasFile("images")) {
    //             $files = $request->file('images');
            
    //             foreach($files as $image){
            
    //               $random_name = str_random(16);
    //               $destinationPath = 'standard/assets/images/';
    //               $extension = $image->getClientOriginalExtension();
    //               $img_url = $random_name . "." . $extension;
    //               $image->move($destinationPath, $img_url);
            
    //               $violation_image = new ViolationUpload;
    //               $violation_image->infraction_id = $infraction->id;
    //               $violation_image->name = $image->getClientOriginalName();
    //               $violation_image->path = $destinationPath.$img_url;
    //               $violation_image->save();
            
    //             }
    //         }
                 
	// 		 $files = $request->file('images');

	// 		foreach($files as $image){

    //       $random_name = str_random(16);
    //       $destinationPath = 'standard/assets/images/';
    //       $extension = $image->getClientOriginalExtension();
    //       $img_url = $random_name . "." . $extension;
    //       $image->move($destinationPath, $img_url);

    //         $violation_image = new ViolationUpload;
    //         $violation_image->infraction_id = $infraction->id;
    //         $violation_image->name = $img_url;
    //         $violation_image->path = $destinationPath.$img_url;
    //         $violation_image->save();
    //     }

    //     $uploads = ViolationTypeInfraction::where('in_id', $id)->delete();
    //     $countb = count($request->input('violation_type'));
    //     for ($index = 0; $index < $countb; $index++)
    //     {
    //         $vti = new ViolationTypeInfraction;
    //         $vti->in_id = $infraction->id;
    //         $vti->vt_id = $request->input('violation_type')[$index];
    //         $vti->save();
    //     }

        return redirect('/km/infractions')->with('message_title', trans('message.i_u_success_title'))->with('message_description', trans('message.i_u_success_desc'))->with('message_class', 'alert-success');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteInfraction($id)
    {
        $this->infraction->find($id)->delete();
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return string
     */
    public function assign(Request $request)
    {
        $infraction_user = InfractionUser::firstOrCreate(['infraction_id' => $request->input('infraction_id'), 'user_id' => $request->input('user_id'),]);


        return redirect()->back()->with('message_title', trans('message.i_a_success_title'))->with('message_description', trans('message.i_a_success_desc'))->with('message_class', 'alert-success');
    }

    public function removeImage(Request $request)
    {
        $file = $request->input('path');

        $uploads = ViolationUpload::where('path', $file)->delete();

        return response()->json('success');
    }

    // Google map | $id searches for deistrict
    public function map($id)
    {
        $district = District::all();
        //the last district id
        $dis = District::all()->max('id');
        // check prameter ID from /km/infraction/map/id route
        if (!($id == 0 || $id > $dis || $id < 0 ))
        {
            // Infractions Data for window info of markers
            $locations =  Infraction::where('district',$id)
                                        ->get(['id','name','father_name','latitude','longitude','property_area','district','street','property_number','contruction_certificate']);


            return view('admin/infractions/maps')
                    ->with('locations', $locations)
                    ->with('district',$district)
                    ->with('sid',$id);
        }

        // Infractions Data for window info of markers if parameter is /all
        $locations =  Infraction::get(['id','name','father_name','latitude','longitude','property_area','district','street','property_number','contruction_certificate']);
            // Transelate contruction certificate
            foreach ($locations as $locationss)
            {
                $locationss->contruction_certificate = $this->localizePermitInMap($locationss->contruction_certificate);

            }

        // Google map with the id of seleted district
        return view('admin/infractions/maps')
                ->with('locations', $locations)
                ->with('district',$district)
                ->with('sid',$id);
    }

    // switch contruction_certificate to yes / no
    private function localizePermitInMap($locations)
    {
        switch ($locations) {
            case '1':
                return trans('violation.Yes');
                break;
            case '2':
                return trans('violation.No');
                break;
        }
    }

    // upload document
    public function uploadDocuments(Request $request,ViolationFileRequest $validator){
       // return "success";

        $violation_document = new ViolationDocument;
        $violation_document->infraction_id = $request->infraction_id;
        $violation_document->doc_number = $request->doc_number;
        $violation_document->doc_name = $request->doc_name;
        $violation_document->doc_description = $request->doc_description;
        $violation_document->doc_date = $request->doc_date;

        $violation_document->save();

        // insert to document files table
        //$count = count($request->input('file'));
        //for ($index = 0; $index < $count; $index++)
        //{
			 $files = $request->file('images');

			foreach($files as $image){

          $random_name = str_random(16);
          $destinationPath = 'standard/assets/violations/';
          $extension = $image->getClientOriginalExtension();
          $img_url = $random_name . "." . $extension;
          $image->move($destinationPath, $img_url);
          $violation_document_files = new ViolationDocumentFile;
          $violation_document_files->document_id = $violation_document->id;
		  $violation_document_files->doc_path = $destinationPath.$img_url;

         // $violation_document_files->doc_path = $request->input('file');
            $violation_document_files->save();
        }

        return redirect()->back();

    }
    public function uploadFile(Request $request)
    {
        $rules = array(
            'file' => 'required|mimes:jpg,jpeg,pdf,doc,docx,png|max:6000'
            );
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return response()->json('error', 400);
        }
        $image = $request->file('file');
        $random_name = str_random(16);
        $destinationPath = 'standard/assets/violations';
        $extension = $image->getClientOriginalExtension();
        $img_url = $random_name . "." . $extension;
        $image->move($destinationPath, $img_url);

        // resizing image
        $img = 'standard/assets/violations/' . $img_url;
        if ($image)
        {
            return response()->json($img, 200);
        } else
        {
            return response()->json('error', 400);
        }
    }

    public function removeFile(Request $request)
    {
        $file = $request->input('file');
        File::delete($file);
        return response()->json('success');
    }

    // To delete Violation document
    public function violationDocumentDelete($id){

        $ViolationDocumentFiles = ViolationDocumentFile::where('document_id',$id)->get();
        foreach ($ViolationDocumentFiles as $ViolationDocumentFile) {
            File::delete($ViolationDocumentFile->doc_path);
        }
        ViolationDocument::where('id', $id)->delete();
         return redirect()->back();
    }

    // return edit view
    public function ViolationDocumentEdit($id)
    {

        $violationDoument = ViolationDocument::where('id',$id)->get();
        $documentpath = ViolationDocumentFile::where('document_id',$id)->get();


        return view('admin/infractions/violation_document_edit',compact('violationDoument'))->with('documentpaths',$documentpath);
    }

    // to edit violation document
    public function editDocuments(Request $request,ViolationFileRequest $validator){
        $doc_id = $request->doc_id;
        $violationDouments = ViolationDocument::findOrFail($doc_id);
        $violationDouments->doc_number = $request->doc_number;
        $violationDouments->doc_name = $request->doc_name;
        $violationDouments->doc_date = $request->doc_date;
        $violationDouments->doc_description = $request->doc_description;
        $violationDouments->save();

        // insert to document files table
        $count = count($request->input('file'));
        for ($index = 0; $index < $count; $index++)
        {
            $violation_document_files = new ViolationDocumentFile;
            $violation_document_files->document_id = $request->doc_id;
            $violation_document_files->doc_path = $request->input('file')[$index];
            $violation_document_files->save();
        }
        return redirect()->back();
    }

    public function DocumentDelete($id){
        $ViolationDocumentFiles = ViolationDocumentFile::where('id',$id)->get();
        foreach ($ViolationDocumentFiles as $ViolationDocumentFile) {
            File::delete($ViolationDocumentFile->doc_path);
        }
        ViolationDocumentFile::where('id', $id)->delete();
         return redirect()->back();
    }

}
