<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Building;
use App\District;


class BuildingsController extends Controller
{

    public function __construct(Building $building)
    {
        $this->building = $building;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    $building = Building::all();

    
        // return $building;
       return view("building.index",compact("building","district"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    $building = Building::all();
    $district = District::all();
        // $user = Auth::user();
        // $district = District::all();
        // $violationType = ViolationType::all();
        return view("building.create", compact("district"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  dd($request->all());
         $gul_building = new Building;
        


         $gul_building->District = $request->district;
         $gul_building->Guzar = $request->Guzar;
         $gul_building->Block = $request->Block;
         $gul_building->Parcel = $request->Parcel;
         $gul_building->Unit = $request->Unit;
         $gul_building->Name = $request->name;
         $gul_building->Father_name = $request->father_name;
         $gul_building->grand_father_name = $request->G_father_name;
         $gul_building->Tazkira = $request->Tazkira;
         $gul_building->Floors = $request->floor;
         $gul_building->Area_size = $request->Area_size;
         $gul_building->Latitude = $request->Latitude;
         $gul_building->Longtitude = $request->Area_size;
         $gul_building->molkiyat = $request->molkiyat;
         $gul_building->Contact = $request->Contact;
         $gul_building->land_catagory = $request->land_catagory;
         $gul_building->land_type = $request->land_type;
         $gul_building->contruction_certificate = $request->contruction_certificate;
         $gul_building->Comment = $request->Comment;
         $gul_building->Building_Volume = $request->Building_Volume;
         

         if($request->hasFile("building_sketch")) {
            $gul_building->building_sketch = $request->building_sketch;

         }

         if($request->hasFile("building_image")) {
            
            $gul_building->building_image = $request->building_image;
         }







         $gul_building->save();
         return redirect('/Buildings')->with('message_title', trans('message.i_c_success_title'))->with('message_description', trans('message.i_c_success_desc'))->with('message_class', 'alert-success');
  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gul_habib =  Building::find($id);
        return $building;
    }
    public function getExcel()
    {
        return $this->building->setExcelData($this->building->all())->generateExcelBaseOnUrlData();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       

        $gul_habib =  Building::find($id);
        return $gul_habib;

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gul_habib =  Building::find($id);
        
        $gul_building->District = $request->district;
         $gul_building->Guzar = $request->Guzar;
         $gul_building->Block = $request->Block;
         $gul_building->Parcel = $request->Parcel;
         $gul_building->Unit = $request->Unit;
         $gul_building->Name = $request->name;
         $gul_building->Father_name = $request->father_name;
         $gul_building->grand_father_name = $request->G_father_name;
         $gul_building->Tazkira = $request->Tazkira;
         $gul_building->Floors = $request->floor;
         $gul_building->Area_size = $request->Area_size;
         $gul_building->Latitude = $request->Latitude;
         $gul_building->Longtitude = $request->Area_size;
         $gul_building->molkiyat = $request->molkiyat;
         $gul_building->Contact = $request->Contact;
         $gul_building->land_catagory = $request->land_catagory;
         $gul_building->land_type = $request->land_type;
         $gul_building->contruction_certificate = $request->contruction_certificate;
         $gul_building->Comment = $request->Comment;
         $gul_building->Building_Volume = $request->Building_Volume;

    







         $gul_building->save();
         return redirect('/Buildings')->with('message_title', trans('message.i_c_success_title'))->with('message_description', trans('message.i_c_success_desc'))->with('message_class', 'alert-success');
  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gul_habib =  Building::find($id);
        $gul_habib->delete();
        
        return redirect('/Buildings')->with('message_title', trans('message.i_u_success_title'))->with('message_description', trans('message.i_u_success_desc'))->with('message_class', 'alert-success');
   
    }
}
