<?php

namespace App\Http\Controllers;

use App\DataTables\ProjectsDataTable;
use App\DataTables\ReportDataTable;
use App\Project;
use App\Report;
use App\SafetyMeasur;
use App\Supervisor;
use App\BuildingEquipment;
use App\BuildingMaterialTest;
use App\District;
use App\Equipment;
use App\ProjectPartialList;
use App\ProjectUser;
use App\ProjectWorker;
use App\Resize;
use App\Supervision;
use App\SupervisorProject;
use App\SupervisorUser;
use App\User;
use App\WorkActivity;
use App\WorkActivityUpload;
use App\WorkDefact;
use App\WorkDefactsUpload;
use App\MaterialTestUplaod;
use Illuminate\Http\Request;
use App\Transformers\ProjectTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\ProjectRequest;
use App\Http\Requests\ReportRequest;
use PDF;
use File;
use Validator;
use App;
use DB;
use App\ProjectStatus;
use App\Transformers\ProjectStatusTransformer;
use App\InvioceFile;
class ProjectController extends Controller
{

    public $supervisorForReport;

    public function __construct(Project $project, Report $report, Manager $manager, Request $request, ProjectStatus $proStatus)
    {
        $this->project = $project;
        $this->report = $report;
        $this->manager = $manager;
        $this->request = $request;
        $this->proStatus = $proStatus;
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index()
    {
 
        $users = User::all();
        $proCount = $this->project->all()->count();
        return view('admin.projects.project_search', compact('proCount', 'users'));
    }

    /**
     * @param ProjectsDataTable $dataTable
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchProject(ProjectsDataTable $dataTable)
    {
        return $dataTable->ajax();
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details($id)
    {
        $projectDetails = $this->project->findOrFail($id);
        //coordinates of project on google map
        $projectCoordinates = ProjectsDataTable::transformGMapCoordintes($projectDetails);
        //creating supervisor for this project
        $projectSupervisor = new  Supervisor();
        //check whether we have supervisor for this project or not
        if ($projectDetails->members()->count() > 0)
        {
            $projectSupervisor = $projectDetails->members()->get()->last();
            $this->supervisorForReport = $projectSupervisor;

        }
        $daylyRepCount = $projectDetails->reports()->count();
        // get the executed till now percentage
        $status = $this->handleDevideByZero($projectDetails->exp_rec_budg_dep , $projectDetails->expected_exe_in_year) * 100;
        $sumOfActualAmount = ProjectStatus::where('project_id',$id)->sum('actual_amount');
        return view('admin.projects.details', compact('projectDetails', 'projectSupervisor', 'daylyRepCount', 'projectCoordinates','project_status', 'status','sumOfActualAmount'));
    }

    /**
     * @param $id
     * @return string
     */
    public function getDailyReports($id)
    {
        return view('admin.projects.project_daliyReport');
    }

    /**
     * @param $id
     * @return string
     */
    public function getMpnthlyReports($id)
    {
        return view('admin.projects.project_monthlyReport');
    }

    /**
     * @param $id
     * @param ReportDataTable $dataTable
     * @return \Illuminate\Http\JsonResponse
     */
    public function dailyReport($id, ReportDataTable $dataTable)
    {
        //set project_id key for forther use to generate generic excel report from daily reports
        $this->setProjectId($id);
        $dataTable->setId($id);
        return $dataTable->ajax();
    }

    /**
     * @param $proId
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reportDetails($proId, $id)
    {
        $projecDefacts = $this->project->find($proId)->deficiencies;
        $reportDetails = $this->report->find($id);
        $supervisor = User::find($reportDetails->supervisor_id);
        $workActivities = $reportDetails->workActivities;
        $workDefacts = $reportDetails->workDefacts;
        if ($reportDetails->safetyMeasures()->first())
        {
            $safetyMeasures = $reportDetails->safetyMeasures()->first();
        } else
        {
            $safetyMeasures = new SafetyMeasur();
        }
        $buildingMaterialTests = $reportDetails->buildingMaterialTests;
        $projectWorkers = $reportDetails->projectWorkers;
        $equipments = $reportDetails->equipments;
        $buildingEquipments = $reportDetails->buildingEquipments;
        return view('admin.projects.project_report_detail', compact('workActivities', 'workDefacts', 'safetyMeasures', 'buildingMaterialTests', 'projectWorkers', 'equipments', 'buildingEquipments', 'reportDetails', 'supervisor', 'projecDefacts'));
    }

    /**
     * compose view for excel reports
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getReports()
    {
        return view('admin.projects.ecxcel_report');
    }

    /**
     * Generating Excel formated Reports base on Project Table
     */
    public function getExcelFile()
    {
        $this->project->setDataForExcelExport($this->project->all());
        return $this->project->generatExcelReport();
    }

    public function getCustomizeDataForExcel($status, $progress, $Poperetor, $value, $dateType, $dateOperator, $date)
    {
        $data = $this->project->getCustomizedDataBaseOnUrlParameters($status, $progress, $Poperetor, $value, $dateType, $dateOperator, $date);

        $this->project->setDataForExcelExport($data);
        $this->project->generatExcelReport();

    }

    /**
     * Generates Excel sheet for budget report
     */
    public function getProjectBudgetExcelFile() {
        $this->proStatus->generateProjectBudgetReport();
    }


    /**
     * show data intry blade for project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @param Request $request
     * @return string
     */
    public function upload(Request $request)
    {
        $rules = array(
            'file' => 'required|image|mimes:jpg,jpeg,pdf,png|max:6000'
            );
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return response()->json('error', 400);
        }
        $image = $request->file('file');
        $random_name = str_random(16);
        $destinationPath = 'standard/assets/images';
        $extension = $image->getClientOriginalExtension();
        $img_url = $random_name . "." . $extension;
        $image->move($destinationPath, $img_url);

        // resizing image
        $img = 'standard/assets/images/' . $img_url;
        $image = new Resize($img);
        $image->resizeImage(1400, 800, 'crop');
        $image->saveImage("standard/assets/images/$img_url", 60);
        if ($image)
        {
            return response()->json($img, 200);
        } else
        {
            return response()->json('error', 400);
        }
    }

    public function remove(Request $request)
    {
        $file = $request->input('file');
        File::delete($file);
        return response()->json('success');
    }

    /**
     * @return mixed
     */
    public function createProject()
    {
        $district = District::all();
        return view('admin.projects.project_create', compact('district'));
    }

    /**
     * @param Request $request
     * @return string
     */
    public function storeProject(Request $request, ProjectRequest $validator)
    {
        $project                            = new Project;
        $project->name                      = $request->input('name');
        $project->code                      = $request->input('code');
        $project->project_type              = $request->input('project_type');
        $project->available_fund            = $request->input('available_fund');
        $project->expected_exe_in_year      = $request->input('expected_exe_in_year');
        $project->year                      = $request->input('year');
        $project->exp_rec_budg_dep          = $request->input('exp_rec_budg_dep');
        $project->contractor                = $request->input('contractor');
        $project->comment                   = $request->input('comment');
        // $project->contract               = $request->input('contract');
        $project->owner                     = $request->input('owner');
        $project->area                      = $request->input('area');
        $project->district                  = $request->input('district');
        $project->passway                   = $request->input('passway');
        $project->contract_date             = $request->input('contract_date');
        $project->start_date                = $request->input('start_date');
        $project->actual_date               = $request->input('actual_date');
        $project->planned_date              = $request->input('planned_date');
        $project->status                    = $request->input('status'); // last updated value
        $project->days_remain               = $request->input('days_remain');
        $project->percentage_remain         = $request->input('percentage_remain');
        // $project->status_value           = $request->input('status_value'); // last updated value
        // $project->reasson_why_stop       = $request->input('reason_why_stop'); // last updated value
        $latitude                           = ($request->input('latitude_second') / 60 + $request->input('latitude_minute')) / 60 + $request->input('latitude_degree');
        $longitude                          = ($request->input('longitude_second') / 60 + $request->input('longitude_minute')) / 60 + $request->input('longitude_degree');
        $project->latitude                  = $latitude;
        $project->gratitude                 = $longitude;
        $project->project_budget            = $request->input('project_budget');
        $project->addational_budget         = $request->input('additional_budget');
        $project->save();

        // assign the project to creator of the project
        $project_id = $project->id;
        $projectUser = new ProjectUser;
        $projectUser->project_id = $project_id;
        $projectUser->user_id = Auth::user()->id;
        $projectUser->save();
        return redirect('/projects')->with('message_title', trans('message.p_c_success_title'))->with('message_description', trans('message.p_c_success_desc'))->with('message_class', 'alert-success');
    }

    /**
     * @param Request $request
     * @param ProjectRequest $validator
     * @return mixed
     */
    public function updateProject(Request $request, ProjectRequest $validator)
    {
        $id = $request->input('project_id');
        $project = Project::find($id);
        $project->name = $request->input('name');
        $project->code                      = $request->input('code');
        $project->project_type              = $request->input('project_type');
        $project->available_fund            = $request->input('available_fund');
        $project->expected_exe_in_year      = $request->input('expected_exe_in_year');
        $project->year                      = $request->input('year');
        $project->exp_rec_budg_dep          = $request->input('exp_rec_budg_dep');
        $project->contractor = $request->input('contractor');
        $project->comment  = $request->input('comment');
        // $project->contract = $request->input('contract');
        $project->owner = $request->input('owner');
        $project->area = $request->input('area');
        $project->district = $request->input('district');
        $project->passway = $request->input('passway');
        $project->contract_date = $request->input('contract_date');
        $project->start_date = $request->input('start_date');
        $project->actual_date = $request->input('actual_date');
        $project->planned_date = $request->input('planned_date');
        // $project->status = $request->input('status'); // last updated value
        // $project->status_value = $request->input('status_value'); // last updated value
        // $project->reasson_why_stop = $request->input('reason_why_stop'); // last updated value
        $latitude = ($request->input('latitude_second') / 60 + $request->input('latitude_minute')) / 60 + $request->input('latitude_degree');
        $longitude = ($request->input('longitude_second') / 60 + $request->input('longitude_minute')) / 60 + $request->input('longitude_degree');
        $project->latitude = $latitude;
        $project->gratitude = $longitude;
        $project->project_budget = $request->input('project_budget');
        $project->addational_budget = $request->input('additional_budget');
        $project->days_remain               = $request->input('days_remain');
        $project->percentage_remain         = $request->input('percentage_remain');
        $project->status = $request->input('status');
        $project->save();

        return redirect('/projects')->with('message_title', trans('message.p_u_success_title'))->with('message_description', trans('message.p_u_success_desc'))->with('message_class', 'alert-success');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function createReport($id)
    {
        $user = Auth::user();
        $project = Project::find($id);
        $activity_type = ProjectPartialList::where('type', 'activity')->get();
        $measure_type = ProjectPartialList::where('type', 'measure')->get();
        $material_test_type = ProjectPartialList::where('type', 'material_test')->get();
        $equipment_type = ProjectPartialList::where('type', 'equipment')->get();
        $building_equipment_type = ProjectPartialList::where('type', 'building_equipment')->get();
        $district = District::all();
        return view('admin.projects.report_create', compact('user', 'project', 'activity_type', 'measure_type', 'material_test_type', 'equipment_type', 'building_equipment_type', 'district'));
    }


    /**
     * @param $p_id
     * @param $r_id
     * @return mixed
     */
    public function editReport($p_id, $r_id)
    {
        $project = Project::find($p_id);
        $activity_type = ProjectPartialList::where('type', 'activity')->get();
        $measure_type = ProjectPartialList::where('type', 'measure')->get();
        $material_test_type = ProjectPartialList::where('type', 'material_test')->get();
        $equipment_type = ProjectPartialList::where('type', 'equipment')->get();
        $building_equipment_type = ProjectPartialList::where('type', 'building_equipment')->get();
        $district = District::all();
        $report = $this->report->find($r_id);
        $user = User::findOrFail($report->supervisor_id);
        $work_activities = $report->workActivities;
        $work_defects = $report->workDefacts;
        $safety_measure = $report->safetyMeasures()->first();
        $building_material_tests = $report->buildingMaterialTests;
        $project_worker = $report->projectWorkers->first();
        if (is_null($project_worker))
        {
            $project_worker = new ProjectWorker;
            $project_worker->report_id = $r_id;
            $project_worker->projectmanager = 0;
            $project_worker->control_engineer = 0;
            $project_worker->safety_engineer = 0;
            $project_worker->worker_leader = 0;
            $project_worker->smart_worker = 0;
            $project_worker->ordinary_worker = 0;
            $project_worker->gard = 0;
            $project_worker->visitor = 0;
            $project_worker->other = 0;
            $project_worker->save();
        }
        $equipments = $report->equipments;
        $building_equipments = $report->buildingEquipments;
        return view('admin.projects.report_edit', compact('user', 'project', 'activity_type', 'measure_type', 'material_test_type', 'equipment_type', 'building_equipment_type', 'district', 'report', 'user', 'work_activities', 'work_defects', 'safety_measure', 'building_material_tests', 'project_worker', 'equipments', 'building_equipments'));
    }

    /**
     * generate filterd data table base on url parameters
     * @param $status
     * @param $progress
     * @param $Poperetor
     * @param $value
     * @param $dateType
     * @param $dateOperator
     * @param $date
     * @return mixed
     */
    public function getCustomizeDataForDataTable($status, $progress, $Poperetor, $value, $dateType, $dateOperator, $date)
    {
        $data = $this->project->getCustomizedDataBaseOnUrlParameters($status, $progress, $Poperetor, $value, $dateType, $dateOperator, $date);

        return $this->project->generateDataTable($data);

    }

    /*
     * @param Request $request
     * @return string
     */
    /**
     * @param Request $request
     * @param ReportRequest $validator
     * @return mixed
     */
    public function storeReport(Request $request, ReportRequest $validator)
    {
        $project = Project::find($request->input('project_id'));
        $user_id = Auth::user()->id;
//        $project->district = $request->input('district'); // last updated value
        // $project->passway = $request->input('passway'); // last updated value
        $project->daily_status = $request->input('status'); // last updated value
        $project->status_value = $request->input('status_value'); // last updated value
        $project->reasson_why_stop = $request->input('reason_why_stop'); // last updated value
        $project->update();
        ProjectUser::firstOrCreate(['project_id' => $request->input('project_id'), 'user_id' => $user_id,]);
        $report = new Report;
        $report->project_id = $request->input('project_id');
        $report->supervisor_id = $user_id;
        $report->status = $request->input('status');
        $report->status_value = $request->input('status_value');
        $report->district = $request->input('district');
        $report->gozar = $request->input('passway');
        $report->date = $request->input('supervision_date');
        $report->time = date("H:i", strtotime($request->input('supervision_time'))) . ":30";
        $report->weather = $request->input('weather');
        $report->temprature = $request->input('temperature');
        $report->save();

        /** @var TYPE_NAME $count */
        $count = count($request->input('activity_type'));
        for ($index = 0; $index < $count; $index++)
        {
            $work_activity = new WorkActivity;
            $work_activity->report_id = $report->id;
            $work_activity->type = $request->input('activity_type')[$index];
            $work_activity->description = $request->input('activity_description')[$index];
            $work_activity->save();

            $activity_image = $request->input("activity_image");
            /** @var TYPE_NAME $image */
            $image = count($activity_image[$index]['image']);
            for ($in = 0; $in < $image; $in++)
            {
                $work_activity_upload = new WorkActivityUpload;
                $work_activity_upload->work_activitie_id = $work_activity->id;
                $work_activity_upload->name = "something";
                $work_activity_upload->path = $activity_image[$index]['image'][$in];
                $work_activity_upload->save();
            }
        }


        /** @var TYPE_NAME $count */
        $count = count($request->input('defect_type'));
        for ($index = 0; $index < $count; $index++)
        {
            $work_defect = new WorkDefact;
            $work_defect->report_id = $report->id;
            $work_defect->type = $request->input('defect_type')[$index];
            $work_defect->description = $request->input('defect_description')[$index];
            $work_defect->save();

            $defect_image = $request->input("defect_image");
            /** @var TYPE_NAME $image */
            $image = count($defect_image[$index]['image']);
            for ($in = 0; $in < $image; $in++)
            {
                $work_defect_upload = new WorkDefactsUpload;
                $work_defect_upload->work_defact_id = $work_defect->id;
                $work_defect_upload->name = "something";
                $work_defect_upload->path = $defect_image[$index]['image'][$in];
                $work_defect_upload->save();
            }
        }
        $count = count($request->input('material_test_type'));
        for ($index = 0; $index < $count; $index++)
        {
            $building_material_test = new BuildingMaterialTest;
            $building_material_test->report_id = $report->id;
            $building_material_test->type = $request->input('material_test_type')[$index];
            $building_material_test->result = $request->input('material_test_result')[$index];
            $building_material_test->description = $request->input('material_test_description')[$index];
            $building_material_test->save();

            $material_image = $request->input("material_image");
            $image = count($material_image[$index]['image']);
            for ($in = 0; $in < $image; $in++)
            {
                $material_test_upload = new MaterialTestUplaod;
                $material_test_upload->material_test_id = $building_material_test->id;
                $material_test_upload->name = "something";
                $material_test_upload->path = $material_image[$index]['image'][$in];
                $material_test_upload->save();
            }
        }


        $count = count($request->input('material_test_type'));
        for ($index = 0; $index < $count; $index ++)
        {
            $building_material_test = new BuildingMaterialTest;
            $building_material_test->report_id = $report->id;
            $building_material_test->type = $request->input('material_test_type')[ $index ];
            $building_material_test->result = $request->input('material_test_result')[ $index ];
            $building_material_test->description = $request->input('material_test_description')[ $index ];
            $building_material_test->save();

            $material_image = $request->input("material_image");
            /** @var TYPE_NAME $image */
            $image = count($material_image[ $index ]['image']);
            for ($in = 0; $in < $image; $in ++)
            {
                $material_test_upload = new MaterialTestUplaod;
                $material_test_upload->material_test_id = $building_material_test->id;
                $material_test_upload->name = "something";
                $material_test_upload->path = $material_image[ $index ]['image'][ $in ];
                $material_test_upload->save();
            }
        }

        $safety_measure = new SafetyMeasur;
        $safety_measure->report_id = $report->id;
        $safety_measure->eye_protection = $request->input('eye_protection') ? 1 : 0;
        $safety_measure->helmet = $request->input('helmet') ? 1 : 0;
        $safety_measure->ear_protection = $request->input('ear_protection') ? 1 : 0;
        $safety_measure->respiratory_protection = $request->input('respiratory_protection') ? 1 : 0;
        $safety_measure->safety_boots = $request->input('safety_boots') ? 1 : 0;
        $safety_measure->safety_gloves = $request->input('safety_gloves') ? 1 : 0;
        $safety_measure->safety_uniforms = $request->input('safety_uniforms') ? 1 : 0;
        $safety_measure->safe_face_on = $request->input('safe_face_on') ? 1 : 0;
        $safety_measure->tape_barriers = $request->input('tape_barriers') ? 1 : 0;
        $safety_measure->symptoms_of_obstacles = $request->input('symptoms_of_obstacles') ? 1 : 0;
        $safety_measure->warning_signs = $request->input('warning_signs') ? 1 : 0;
        $safety_measure->mandatory_signs = $request->input('mandatory_signs') ? 1 : 0;
        $safety_measure->first_aid_sign = $request->input('first_aid_sign') ? 1 : 0;
        $safety_measure->risks_of_chemicals = $request->input('risks_of_chemicals') ? 1 : 0;
        $safety_measure->noise_pollution = $request->input('noise_pollution') ? 1 : 0;
        $safety_measure->asphalting_disadvantages = $request->input('asphalting_disadvantages') ? 1 : 0;
        $safety_measure->concrete_disadvantages = $request->input('concrete_disadvantages') ? 1 : 0;
        $safety_measure->save();

        $project_worker = new ProjectWorker;
        $project_worker->report_id = $report->id;
        $project_worker->projectmanager = $request->input('project_manager');
        $project_worker->control_engineer = $request->input('control_manager');
        $project_worker->safety_engineer = $request->input('safety_manager');
        $project_worker->worker_leader = $request->input('worker_leader');
        $project_worker->smart_worker = $request->input('smart_worker');
        $project_worker->ordinary_worker = $request->input('ordinary_worker');
        $project_worker->gard = $request->input('guard');
        $project_worker->visitor = $request->input('visitor');
        $project_worker->other = $request->input('other');
        $project_worker->save();

        /** @var TYPE_NAME $count */
        $count = count($request->input('equipment_type'));
        for ($index = 0; $index < $count; $index++)
        {
            $equipment = new Equipment;
            $equipment->report_id = $report->id;
            $equipment->type = $request->input('equipment_type')[$index];
            $equipment->quantity = $request->input('equipment_quantity')[$index];
            $equipment->comment = $request->input('equipment_description')[$index];
            $equipment->save();
        }
        /** @var TYPE_NAME $count */
        $count = count($request->input('building_equipment_type'));
        for ($index = 0; $index < $count; $index++)
        {
            $building_equipment = new BuildingEquipment;
            $building_equipment->report_id = $report->id;
            $building_equipment->type = $request->input('building_equipment_type')[$index];
            $building_equipment->estim_value = $request->input('building_equipment_quantity')[$index];
            $building_equipment->quality = $request->input('building_equipment_quality')[$index];
            $building_equipment->comment = $request->input('building_equipment_description')[$index];
            $building_equipment->save();
        }
        return redirect('/project/details/' . $request->input('project_id'))->with('message_title', trans('message.r_c_success_title'))->with('message_description', trans('message.r_c_success_desc'))->with('message_class', 'alert-success');

    }

    /*
     * @param Request $request
     * @return string
     */
    /**
     * @param Request $request
     * @param ReportRequest $validator
     * @return mixed
     */
    public function updateReport(Request $request, ReportRequest $validator)
    {
        $project = Project::findOrFail($request->input('project_id'));
        $user_id = Auth::user()->id;
        $project->daily_status = $request->input('status'); // last updated value
        $project->status_value = $request->input('status_value'); // last updated value
        $project->reasson_why_stop = $request->input('reason_why_stop'); // last updated value
        $project->update();

        $report = Report::findOrFail($request->input('report_id'));
        $report->status = $request->input('status');
        $report->status_value = $request->input('status_value');
        $report->district = $request->input('district');
        $report->gozar = $request->input('passway');
        $report->date = $request->input('supervision_date');
        $report->time = date("H:i", strtotime($request->input('supervision_time'))) . ":30";
        $report->weather = $request->input('weather');
        $report->temprature = $request->input('temperature');
        $report->save();

        $count = count($request->input('activity_type'));
        for ($index = 0; $index < $count; $index++)
        {
            $work_activity = WorkActivity::findOrFail($request->input('activity_id')[$index]);
            $work_activity->type = $request->input('activity_type')[$index];
            $work_activity->description = $request->input('activity_description')[$index];
            $work_activity->save();
        }
        $count = count($request->input('defect_type'));
        for ($index = 0; $index < $count; $index++)
        {
            $work_defect = WorkDefact::findOrFail($request->input('defect_id')[$index]);
            $work_defect->type = $request->input('defect_type')[$index];
            $work_defect->description = $request->input('defect_description')[$index];
            $work_defect->save();
        }

        $count = count($request->input('material_test_type'));
        for ($index = 0; $index < $count; $index++)
        {
            $building_material_test = BuildingMaterialTest::findOrFail($request->input('material_test_id')[$index]);
            $building_material_test->type = $request->input('material_test_type')[$index];
            $building_material_test->result = $request->input('material_test_result')[$index];
            $building_material_test->description = $request->input('material_test_description')[$index];
            $building_material_test->save();
        }
        $safety_measure = SafetyMeasur::findOrFail($request->input('report_id'));
        $safety_measure->eye_protection = $request->input('eye_protection') ? 1 : 0;
        $safety_measure->helmet = $request->input('helmet') ? 1 : 0;
        $safety_measure->ear_protection = $request->input('ear_protection') ? 1 : 0;
        $safety_measure->respiratory_protection = $request->input('respiratory_protection') ? 1 : 0;
        $safety_measure->safety_boots = $request->input('safety_boots') ? 1 : 0;
        $safety_measure->safety_gloves = $request->input('safety_gloves') ? 1 : 0;
        $safety_measure->safety_uniforms = $request->input('safety_uniforms') ? 1 : 0;
        $safety_measure->safe_face_on = $request->input('safe_face_on') ? 1 : 0;
        $safety_measure->tape_barriers = $request->input('tape_barriers') ? 1 : 0;
        $safety_measure->symptoms_of_obstacles = $request->input('symptoms_of_obstacles') ? 1 : 0;
        $safety_measure->warning_signs = $request->input('warning_signs') ? 1 : 0;
        $safety_measure->mandatory_signs = $request->input('mandatory_signs') ? 1 : 0;
        $safety_measure->first_aid_sign = $request->input('first_aid_sign') ? 1 : 0;
        $safety_measure->risks_of_chemicals = $request->input('risks_of_chemicals') ? 1 : 0;
        $safety_measure->noise_pollution = $request->input('noise_pollution') ? 1 : 0;
        $safety_measure->asphalting_disadvantages = $request->input('asphalting_disadvantages') ? 1 : 0;
        $safety_measure->concrete_disadvantages = $request->input('concrete_disadvantages') ? 1 : 0;
        $safety_measure->save();

        $project_worker = ProjectWorker::findOrFail($request->input('project_worker_id'));
        $project_worker->projectmanager = $request->input('project_manager');
        $project_worker->control_engineer = $request->input('control_manager');
        $project_worker->safety_engineer = $request->input('safety_manager');
        $project_worker->worker_leader = $request->input('worker_leader');
        $project_worker->smart_worker = $request->input('smart_worker');
        $project_worker->ordinary_worker = $request->input('ordinary_worker');
        $project_worker->gard = $request->input('guard');
        $project_worker->visitor = $request->input('visitor');
        $project_worker->other = $request->input('other');
        $project_worker->save();
        $count = count($request->input('equipment_type'));
        for ($index = 0; $index < $count; $index++)
        {
            $equipment = Equipment::findOrFail($request->input('equipment_id')[$index]);
            $equipment->type = $request->input('equipment_type')[$index];
            $equipment->quantity = $request->input('equipment_quantity')[$index];
            $equipment->comment = $request->input('equipment_description')[$index];
            $equipment->save();
        }
        $count = count($request->input('building_equipment_type'));
        for ($index = 0; $index < $count; $index++)
        {
            $building_equipment = BuildingEquipment::findOrFail($request->input('building_equipment_id')[$index]);
            $building_equipment->report_id = $report->id;
            $building_equipment->type = $request->input('building_equipment_type')[$index];
            $building_equipment->estim_value = $request->input('building_equipment_quantity')[$index];
            $building_equipment->quality = $request->input('building_equipment_quality')[$index];
            $building_equipment->comment = $request->input('building_equipment_description')[$index];
            $building_equipment->save();
        }

        return redirect('/project/details/' . $request->input('project_id'))->with('message_title', trans('message.r_u_success_title'))->with('message_description', trans('message.r_u_success_desc'))->with('message_class', 'alert-success');
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function listProject(Request $request)
    {
        $id = $request->id;
        $project = Project::find($id);

        return $project;
    }

    /**
     * @param $type
     * @return mixed
     */
    public function partialList($type)
    {
        $list = ProjectPartialList::where('type', $type)->get();
        return $list;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function partialListCreate(Request $request)
    {
        $list = ProjectPartialList::firstOrCreate(['name' => $request->name, 'type' => $request->type]);
        return $list;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function supervisorAutocomplete(Request $request)
    {
        $keyword = $request->input('q');
        return Supervisor::where('name', 'like', '%' . $keyword . '%')->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function editProject($id)
    {
        $project = Project::findOrFail($id);
        $district = District::all();
        $result_lat = $project->latitude;
        $result_lng = $project->gratitude;
        $latitude_degree = $result_lat - fmod($result_lat, 1);
        $latitude_minute1 = 60 * fmod($result_lat, 1);
        $latitude_minute = $latitude_minute1 - fmod($latitude_minute1, 1);
        $latitude_second = round(60 * fmod($latitude_minute1, 1), 2);
        $longitude_degree = $result_lng - fmod($result_lng, 1);
        $longitude_minute1 = 60 * fmod($result_lng, 1);
        $longitude_minute = $longitude_minute1 - fmod($longitude_minute1, 1);
        $longitude_second = round(60 * fmod($longitude_minute1, 1), 2);

        // pass the current month number and name
        $month_number = getdate()['mon'];
        $month_name = getdate()['month'];
        return view('admin.projects.project_edit', compact('project', 'district', 'latitude_degree', 'latitude_minute', 'latitude_second', 'longitude_degree', 'longitude_minute', 'longitude_second','month_number','month_name'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deletProject($id)
    {
        if (Auth::user()->can('delete_project'))
        {
            $this->project->find($id)->delete();
            return redirect()->back();
        }else
        {
            return redirect()->back();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteDailyReport($id)
    {
        $this->report->find($id)->delete();
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return string
     */
    public function assign(Request $request)
    {
        ProjectUser::firstOrCreate(['project_id' => $request->input('project_id'), 'user_id' => $request->input('user_id'),]);
        return redirect()->back()->with('message_title', trans('message.p_a_success_title'))->with('message_description', trans('message.p_a_success_desc'))->with('message_class', 'alert-success');
    }

    public function getExcelFileForDailyReport()
    {
        $project = $this->project->findOrFail($this->getProjectId());

        return $project->setprojectDailyReportData($project->reports)->generatExcelGeneralDailyReport();

    }

    /**
     * @return mixed
     */
    public function getProjectId()
    {
        return Session::get('project_id');
    }

    /**
     * @param $id
     * @internal param mixed $projectId
     */
    public function setProjectId($id)
    {
        Session::put('project_id', $id);
        Session::save();
    }

    /**
     * @param Request $request
     * @param $p_id
     * @param $r_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param $proId
     * @internal param $id
     */
    public function printAsPdf(Request $request, $p_id, $r_id)
    {
        $projecDefacts = $this->project->find($p_id)->deficiencies;
        $reportDetails = $this->report->find($r_id);
        $supervisor = User::find($reportDetails->supervisor_id);
        $workActivities = $reportDetails->workActivities;
        $workDefacts = $reportDetails->workDefacts;
        if ($reportDetails->safetyMeasures()->first())
        {
            $safetyMeasures = $reportDetails->safetyMeasures()->first();
        } else
        {
            $safetyMeasures = new SafetyMeasur();
        }
        $buildingMaterialTests = $reportDetails->buildingMaterialTests;
        $projectWorker = $reportDetails->projectWorkers->first();
        $equipments = $reportDetails->equipments;
        $buildingEquipments = $reportDetails->buildingEquipments;

        view()->share(['workActivities' => $workActivities, 'workDefacts' => $workDefacts, 'safetyMeasures' => $safetyMeasures, 'buildingMaterialTests' => $buildingMaterialTests, 'projectWorker' => $projectWorker, 'equipments' => $equipments, 'buildingEquipments' => $buildingEquipments, 'reportDetails' => $reportDetails, 'supervisor' => $supervisor, 'projecDefacts' => $projecDefacts]);
        $file_name = "daily_report_".$reportDetails->date.".pdf";
        if(App::isLocale('fa') || App::isLocale('pa'))
        {
            $lg = Array();
            $lg['a_meta_charset'] = 'UTF-8';
            $lg['a_meta_dir'] = 'rtl';
            $lg['a_meta_language'] = 'fa';
            $lg['w_page'] = 'page';
            PDF::setLanguageArray($lg);
        }
        PDF::SetFont('dejavusans', '', 12);
        PDF::AddPage();
        PDF::writeHTML(view('admin.projects.print')->render(), true, 0, true, 0);
        PDF::Output($file_name, 'D');
    }

    public function changeStatus($id,$status){
        $project = Project::find($id);
        $project_status = $project->status;
        $isPlannedAmount = false;
       if ($project->getStatus->count('month_number') < 12){
            $isPlannedAmount = false;
       }else{
           $isPlannedAmount = true;
       }
        return view('admin.projects.project_status',compact('project_status','project','isPlannedAmount'));
    }

    public function storeStatus(Request $request,$id){
        $project            = Project::find($id);
        if ($project->status == 1 || $project->status == 2){
            $project->days_remain           = $request->days_remain;
            $project->percentage_remain     = $request->percentage_remain;
            $project->save();
            return redirect('/project/details/'.$id);

        }elseif ($project->status == 3){
            // insert the months planned amount, percentage and actual amount
            $janStatus                          = new ProjectStatus();
            $janStatus->project_id              = $id;
            $janStatus->month_number            = 1;
            $janStatus->planned_amount          = $request->jan_planned_amount;
            $janStatus->planned_percentage      = $request->jan_percentage;
            $janStatus->save();

            $febStatus                          = new ProjectStatus();
            $febStatus->project_id              = $id;
            $febStatus->month_number            = 2;
            $febStatus->planned_amount          = $request->feb_planned_amount;
            $febStatus->planned_percentage      = $request->feb_percentage;
            $febStatus->save();

            $marStatus                          = new ProjectStatus();
            $marStatus->project_id              = $id;
            $marStatus->month_number            = 3;
            $marStatus->planned_amount          = $request->mar_planned_amount;
            $marStatus->planned_percentage      = $request->mar_percentage;
            $marStatus->save();

            $aprStatus                          = new ProjectStatus();
            $aprStatus->project_id              = $id;
            $aprStatus->month_number            = 4;
            $aprStatus->planned_amount          = $request->apr_planned_amount;
            $aprStatus->planned_percentage      = $request->apr_percentage;
            $aprStatus->save();

            $mayStatus                          = new ProjectStatus();
            $mayStatus->project_id              = $id;
            $mayStatus->month_number            = 5;
            $mayStatus->planned_amount          = $request->may_planned_amount;
            $mayStatus->planned_percentage      = $request->may_percentage;
            $mayStatus->save();

            $junStatus                          = new ProjectStatus();
            $junStatus->project_id              = $id;
            $junStatus->month_number            = 6;
            $junStatus->planned_amount          = $request->jun_planned_amount;
            $junStatus->planned_percentage      = $request->jun_percentage;
            $junStatus->save();

            $julStatus                          = new ProjectStatus();
            $julStatus->project_id              = $id;
            $julStatus->month_number            = 7;
            $julStatus->planned_amount          = $request->jul_planned_amount;
            $julStatus->planned_percentage      = $request->jul_percentage;
            $julStatus->save();

            $augStatus                          = new ProjectStatus();
            $augStatus->project_id              = $id;
            $augStatus->month_number            = 8;
            $augStatus->planned_amount          = $request->aug_planned_amount;
            $augStatus->planned_percentage      = $request->aug_percentage;
            $augStatus->save();

            $sepStatus                          = new ProjectStatus();
            $sepStatus->project_id              = $id;
            $sepStatus->month_number            = 9;
            $sepStatus->planned_amount          = $request->sep_planned_amount;
            $sepStatus->planned_percentage      = $request->sep_percentage;
            $sepStatus->save();

            $octStatus                          = new ProjectStatus();
            $octStatus->project_id              = $id;
            $octStatus->month_number            = 10;
            $octStatus->planned_amount          = $request->oct_planned_amount;
            $octStatus->planned_percentage      = $request->oct_percentage;
            $octStatus->save();

            $novStatus                          = new ProjectStatus();
            $novStatus->project_id              = $id;
            $novStatus->month_number            = 11;
            $novStatus->planned_amount          = $request->nov_planned_amount;
            $novStatus->planned_percentage      = $request->nov_percentage;
            $novStatus->save();

            $decStatus                          = new ProjectStatus();
            $decStatus->project_id              = $id;
            $decStatus->month_number            = 12;
            $decStatus->planned_amount          = $request->dec_planned_amount;
            $decStatus->planned_percentage      = $request->dec_percentage;
            $decStatus->save();
            return redirect('/project/details/'.$id);
        }elseif ($request->status == 4){
            $project->complete_date         = $request->complete_date;
            $project->save();
            return redirect('/project/details/'.$id);

        }
    }

    // export project status excel report
    public function getExcelFileForProjectStatus()
    {
//        $data = Project::selectRaw('*,available_fund / (SELECT sum(projects.available_fund) FROM projects) * 100 as project_weightage, expected_exe_in_year / (SELECT sum(projects.available_fund) FROM projects) * 100 ecpected_exe, exp_rec_budg_dep / expected_exe_in_year * 100 as expected_till_now')
//                ->with('getStatus')
//                ->get();

        $project = Project::all();
        return current(fractal()->collection($project, new ProjectStatusTransformer())->toArray());
    }


    // This function is for the ajax request to update the
    // of a project
    public function updateStatus(Request $request, $id){
        $project            = Project::find($id);
        if($request->project_status == 1){
            $project->status = 1;
            $project->days_remain = $request->days_remain;
            $project->percentage_remain = $request->percentage_remain;
            $project->save();
        }elseif ($request->project_status == 2){
            $project->status = 2;
            $project->procurement_due_date = $request->procurement_due_date;
            $project->procurement_type     = $request->procurement_type;
            $project->save();
        }elseif ($request->project_status == 3){
            $project->status = 3;
            $project->save();

        }else{
            $project->status = 4;
            $project->complete_date = $request->completed_date;
            $project->save();
        }
    }

    public function updateActualAmount(Request $request){


        $projectStatus                          = ProjectStatus::where('project_id',$request->project_id)->where('month_number',$request->month_number)->first();
        $projectStatus->project_id              = $request->project_id;
        $projectStatus->month_number            = $request->month_number;
        $projectStatus->actual_amount           = $request->actual_amount;
        $projectStatus->actual_perccentage      = $request->actual_amount_percentage;
        $projectStatus->save();


        $count = count($request->file);
        for ($index = 0; $index < $count; $index++)
        {
            $actual_amount_files = new InvioceFile();
            $actual_amount_files->project_status_id = $projectStatus->id;
            $actual_amount_files->file_path = $request->file[$index];
            $actual_amount_files->save();
        }
        return redirect()->back();

    }

    // upload invoice file
    public function uploadInvoiceFile(Request $request)
    {
        $rules = array(
            'file' => 'required|mimes:jpg,jpeg,pdf,doc,docx,png|max:6000'
        );
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return response()->json('error', 400);
        }
        $file = $request->file('file');
        $random_name = str_random(16);
        $destinationPath = 'actualdoc/';
        $extension = $file->getClientOriginalExtension();
        $file_url = $random_name . "." . $extension;
        $file->move($destinationPath, $file_url);

        // resizing image
        $f = 'actualdoc/' . $file_url;
        if ($file)
        {
            return response()->json($f, 200);
        } else
        {
            return response()->json('error', 400);
        }
    }

    // remove the invioce file
    public function removeInvoiceFile(Request $request)
    {
        $file = $request->input('file');
        File::delete($file);
        return response()->json('success');
    }

    public  function getActualAmountOfMonth(Request $request)
    {

        $projectStatus = ProjectStatus::where('project_id',$request->project_id)->where('month_number',$request->month_number)->first();
        $response = ['actual_amount'=>$projectStatus->actual_amount,'actual_percentage'=>$projectStatus->actual_perccentage];
        return $response;
    }

    public function getPlannedAmountOfMonth(Request $request){
        $projectStatus = ProjectStatus::where('project_id',$request->project_id)->where('month_number',$request->month_number)->first();
        $response = ['planned_amount'=>$projectStatus->planned_amount,'planned_percentage'=>$projectStatus->planned_percentage];
        return $response;
    }

    public function updatePlannedAmount(Request $request){
        $projectStatus = ProjectStatus::where('project_id',$request->project_id)->where('month_number',$request->month_number)->first();
        $projectStatus->planned_amount = $request->planned_amount;
        $projectStatus->planned_percentage = $request->planned_amount_percentage;
        $projectStatus->save();
        return redirect()->back();
    }




    /**
     * @param $project_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *  project supervisor dropdown in assign project to supervisor
     */
    public function dropdown($project_id){

        $users = User::all();
        $project = Project::find($project_id);
        $projectManagers = $project->members;

        return view('admin.projects.dropdown',compact('projectManagers','users'));
    }

    private function handleDevideByZero($value, $value2)
    {
        if ($value == 0 || $value2 == 0)
        {
            return 0;
        } else
        {
            return $value / $value2;
        }
    }
}
