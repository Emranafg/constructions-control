<?php

namespace App\Http\Controllers;


use App\District;
use App\Infraction;
use FontLib\TrueType\Collection;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Yajra\Datatables\Datatables;

class PublicReportViewController extends Controller
{

    /**
     * DashboardController constructor.
     * @param Project $project
     * @param Infraction $infraction
     */
    public function __construct(Project $project, Infraction $infraction)
    {
        $this->project = $project;
        $this->infraction = $infraction;
    }

    public function index()
    {
        $district = District::all();

        return view('public.public_reports', compact('distVioBchar', 'district'));
    }

    public function showViolationGraph() {

        $distVioBchar = $this->makeBarChartForViolationBaseOnDistrict($this->makeDistrictCatagoryArray(),
            $this->getDataArrayOfDistrictsForBChart(), 'violationBarCh');

        $district = District::all();

        return view('public.violation_graph', compact('distVioBchar', 'district'));
    }

    /**
     * this method generate highchart script for bar chart
     * that shows culomn of violation chart base on districts
     * @param $catagoryArray
     * @param $dataArray
     * @param $id
     * @return string
     */
    public function makeBarChartForViolationBaseOnDistrict($catagoryArray, $dataArray, $id)
    {
        $script = "<script>";
        $script .= "$(function () {Highcharts.chart('" . $id . "', {chart: {type: 'column'},title: {text: '" . trans('pmis.bar_violation') . "'},";
        $script .= "subtitle: {text: '" . trans('pmis.linchart_source') . "'},xAxis: {categories: " . $catagoryArray . ",title: {text: null}},";
        $script .= "yAxis: {min: 0,title: {text: '" . trans('pmis.vio_count') . "',align: 'high'},labels: {overflow: 'justify'}},tooltip: {valueSuffix: '";
        $script .= " violation'},colors: ['#815D8D', '#BF453D', '#C9DBE4', '#286198', '#7CB5EC'],plotOptions: {bar: {dataLabels: {enabled: true}},";
        $script .= "column: {colorByPoint: true}}, credits: {enabled: false},series: [{name: '" . trans('pmis.vio_count') . "',data: ";
        $script .= $dataArray . "}]});}); </script>";

        return $script;

    }

    /**
     * make catagory array of districts for violation bar chart
     * @return string
     */
    public function makeDistrictCatagoryArray()
    {
        $catArrayStr = '[';
        for ($i = 1; $i <= 22; $i ++)
        {
            if (App::isLocale('fa') || App::isLocale('pa'))
            {
                $catArrayStr .= "'" . "ناحیه " . "-" . $i . "', ";
            } else
            {
                $catArrayStr .= "'" . "Dis" . "-" . $i . "', ";
            }

        }

        $catArrayStr .= "]";

        return $catArrayStr;
    }

    /**
     * generates array of district counts
     * @return array
     */
    public function getDataArrayOfDistrictsForBChart()
    {
        $distCountArray = array();
        for ($i = 1; $i <= 22; $i ++)
        {
            $result = $this->infraction->where('district', $i)->count();
            array_push($distCountArray, $result);
        }

        $arrayString = "[";

        for ($i = 0; $i <= 21; $i ++)
        {
            if ($i != 21)
            {
                $arrayString .= $distCountArray[ $i ] . ", ";
            } else
            {
                $arrayString .= $distCountArray[ $i ];
            }
        }
        $arrayString .= "]";

        return $arrayString;
    }

    public function getMultiFilterSelectData(Request $request)
    {
//        dd($request->get('district'));
        $projects = Project::select('projects.*')->get();

        return Datatables::of($projects)
            ->addColumn('project_status_percentage', function (Project $project) {
                return $this->getExecutedTellNowBudget($project);
            })
            ->editColumn('status', function (Project $project) {
                return $this->getFormattedStatusColumnData($project);
            })
            ->editColumn('name', function (Project $project) {
                return $this->getFormattedNameColumn($project);
            })
            ->editColumn('district', function (Project $project) {
                return $this->getFormattedDistrictColumn($project);
            })
            ->filter(function ($instance) use ($request) {
                if ($request->has('district'))
                {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        if($request->get('district') == 'all')
                        {
                            return true;
                        }

                        return $row['district'] == $request->get('district') ? true : false;
                    });
                }

                if ($request->has('status_n'))
                {

                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        if($request->get('status_n') == 'all')
                        {
                            return true;
                        }

                        return $row['status'] == $request->get('status_n') ? true : false;
                    });
                }
            })->addIndexColumn()
            ->make(true);
    }

    private function getFormattedStatusColumnData($project)
    {
        switch ($project->status)
        {
            case '1':
                return trans('pmis.design');
            case '2':
                return trans('pmis.procurement');
            case '3':
                return trans('pmis.construction');
            case '4':
                return trans('pmis.completed');
        }
    }

    private function getFormattedNameColumn($project)
    {
        return '<span style="color: #4183C4; ">' . $project->name . '</span>';
    }

    private function getFormattedDistrictColumn($project)
    {
        return District::where('id', $project->district)->value('fa_name');
    }

    private function getExecutedTellNowBudget($project)
    {
        $executedTellNow = $this->roundOf(($this->handleDevideByZero($project->exp_rec_budg_dep, $project->expected_exe_in_year)) * 100);
        if ($executedTellNow == 0)
        {
            return '0%';
        } else
        {
            return $executedTellNow . '%';
        }
    }

    private function roundOf($value)
    {
        if ($value == 0)
        {
            return '';
        } else
        {
            return round($value);
        }
    }

    private function handleDevideByZero($value, $value2)
    {
        if ($value == 0 || $value2 == 0)
        {
            return 0;
        } else
        {
            return $value / $value2;
        }
    }
}
