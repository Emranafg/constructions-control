<?php

namespace App\Http\Controllers;


use App\Infraction;
use App\Project;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class DashboardController extends Controller
{

    /**
     * DashboardController constructor.
     * @param Project $project
     * @param Infraction $infraction
     */
    public function __construct(Project $project, Infraction $infraction)
    {
        $this->project = $project;
        $this->infraction = $infraction;
    }

    public function index()
    {
        $building =  DB::table('buildings')
        ->select('District', DB::raw('count(*) as total'))
        ->groupBy('District')
        ->get();
        $parking =  DB::table('parkings')
        ->select('District', DB::raw('count(*) as total'))
        ->groupBy('District')
        ->get();
        // return $building;
        $linearChartScript = $this->makeLinearChartScript();
        $pieCountChart = $this->makeCountPieChartOfProjectsAndViolations();
        $barChartBaseOnPercantage = $this->makeBarChartForProjectBaseOnCount();
        $projectpiePercantage = $this->makePieChartForProjectsAndViolations($this->getAssocValueForProjectPercantage(), 'ProjectPiePerc');
        $distVioBchar = $this->makeBarChartForViolationBaseOnDistrict($this->makeDistrictCatagoryArray(),
        $this->getDataArrayOfDistrictsForBChart(), 'violationBarCh');
        $typeOfActChart = $this->makeActivityChart();
        $regionstatus = $this->makeRegionStatusPieChart();
        $statusOfConstraction = $this->makestatusOfConstractionPieChart();
        $constructionPermit = $this->makeConstructionPermitPieChart();
        $projCOunt = $this->project->all()->count();
        $violationCount = $this->infraction->all()->count();
        $projectStatusChart = $this->makePieChartForProjectsAndViolations($this->getAssocValueForProjectStatus(),'proj_pie');

        $available_fund = $this->getProjectsAvailableFund();
        $project_weightage = $this->getProjectWeightedToKmBudget();
        $expected_exe_in_year = $this->getExpectedExecutionInYear();
        $exp_weightage_to_total_budget = $this->getExpectedExecutionToTotalBudget();
        $exp_rec_budg_dep = $this->getExpenseRecordWithBudgetDepartment();
        $executed_tell_now = $this->getExecutedTellNow();

        return view('admin.dashboard', compact('linearChartScript', 'pieCountChart',
            'barChartBaseOnPercantage', 'projectpiePercantage', 'distVioBchar', 'typeOfActChart',
            'regionstatus', 'statusOfConstraction', 'constructionPermit', 'projCOunt','projectStatusChart',
            'violationCount','available_fund','project_weightage','expected_exe_in_year','exp_weightage_to_total_budget',
            'exp_rec_budg_dep','executed_tell_now'
            ,"building","parking"));
    }

    /**
     * generating array of projects status
     * @return array
     */
    public function makeProjectOverviewArrayForPieChart(){
        $dataArray = array();

    }


    /**
     * generating array of how much projects created
     * each month in this year
     * @return array
     */
    public function makeProjectsDataArrayForLinearChart()
    {
        $dataArray = array();

        for ($i = 1; $i <= 12; $i ++)
        {
            $dayOfMonths = cal_days_in_month(CAL_GREGORIAN, $i, date("Y"));
            $coutn = 0;

            for ($j = 1; $j <= $dayOfMonths; $j ++)
            {
                $tempDate = $this->getChosenDateBaseOnLoopVariables($i, $j);

                if ($this->dateCountForProjects($tempDate) > 0)
                {
                    $coutn ++;
                }
            }

            array_push($dataArray, $coutn);
        }

        return $dataArray;
    }

    /**
     * generating array of how much Violation created
     * each month in this year
     * @return array
     */
    public function makeViolationDataArrayForLinearChart()
    {
        $dataArray = array();

        for ($i = 1; $i <= 12; $i ++)
        {
            $dayOfMonths = cal_days_in_month(CAL_GREGORIAN, $i, date("Y"));
            $coutn = 0;

            for ($j = 1; $j <= $dayOfMonths; $j ++)
            {
                $tempDate = $this->getChosenDateBaseOnLoopVariables($i, $j);

                if ($this->dateCountForViolations($tempDate) > 0)
                {
                    $coutn ++;
                }
            }

            array_push($dataArray, $coutn);
        }

        return $dataArray;
    }

    /**
     * for specific mysql date format we need to add 0 for
     * numbers less than 9 before number
     *
     * @param $number
     * @return string
     */
    private function formatNumberLessThan10($number)
    {
        if ($number <= 9)
        {
            return "0" . $number;
        } else
        {
            return $number;
        }
    }

    /**
     * returns the result count of where cluse
     * @param $tempDate
     * @return mixed
     */
    private function dateCountForProjects($tempDate)
    {
        return $this->project->where('start_date', $tempDate)->count();
    }

    /**
     * returns chossen datate base on tow loops variables
     * @param $i
     * @param $j
     * @return string
     */
    private function getChosenDateBaseOnLoopVariables($i, $j)
    {
        return date("Y") . "-" . $this->formatNumberLessThan10($i) . "-" . $this->formatNumberLessThan10($j);
    }

    /**
     * returns the result count of where cluse for violation table
     * @param $tempDate
     * @return mixed
     */
    private function dateCountForViolations($tempDate)
    {
        return $this->infraction->where('start_date', $tempDate)->count();
    }

    /**
     * generate script of liniar chart for violation and projects
     * @return string
     */
    public function makeLinearChartScript()
    {
        $script = "<script>";
        $script .= "$(function () { Highcharts.chart('containerrtr', {chart: {type: 'line'},";
        $script .= "title: {text: '" . trans('pmis.linechart') . "'},subtitle: {text: '" . trans('pmis.linchart_source') . "'},";
        $script .= "xAxis: { categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']},";
        $script .= "yAxis: {title: {text: ''}},plotOptions: {line: {dataLabels: {enabled: true},enableMouseTracking: false }},";
        $script .= "series: [";
        $script .= "{name: '" . trans('pmis.violation') . "',data: " . $this->makeJavaScriptArrayString($this->makeViolationDataArrayForLinearChart()) . "}]";
        $script .= "});});</script>";

        return $script;
    }

    /**
     * make javascript array
     * @param $array
     * @return string
     */
    private function makeJavaScriptArrayString($array)
    {
        $arrayString = "[";
        for ($i = 0; $i <= count($array) - 1; $i ++)
        {
            if ($i != 11)
            {
                $arrayString .= $array[ $i ] . ", ";
            } else
            {
                $arrayString .= $array[ $i ];
            }
        }
        $arrayString .= "]";

        return $arrayString;
    }

    /**
     * generate pie chart for violation and projects report related fields
     * @param $assocArray
     * @param $id
     * @return string
     */
    public function makePieChartForProjectsAndViolations($assocArray, $id)
    {
        $script = "<script>";
        $script .= "$(function () {Highcharts.getOptions().plotOptions.pie.colors = (function () {";
        $script .= "var colors = [],base = Highcharts.getOptions().colors[0],i;for (i = 0; i < 10; i += 1) {";
        $script .= "colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());}return colors;}());";
        $script .= "Highcharts.chart('" . $id . "', {chart: {plotBackgroundColor: null,plotBorderWidth: null,plotShadow: false,type: 'pie'},";
        $script .= "title: {text: '" . trans('pmis.vnp_title') . "'},subtitle: {text: '" . trans('pmis.linchart_source') . "'},";
        $script .= "tooltip: {pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},";
        $script .= "plotOptions: {pie: {allowPointSelect: true,cursor: 'pointer',dataLabels: { enabled: true,format: '<b>{point.name}</b>:";
        $script .= " {point.percentage:.1f} ',style: {color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'}}}},";
        $script .= "series: [{name: 'Percentage',data: [" . $this->makeJavascriptArrayOfObjForPieChart($assocArray) . "]";

        $script .= "}]});}); </script>";

        return $script;
    }

    /**
     * this function make array object of javascript
     * so that can we use it in pie chart
     * @param $nameDataPair
     * @return string
     */
    public function makeJavascriptArrayOfObjForPieChart($nameDataPair)
    {
        $stringifyArray = '';
        foreach ($nameDataPair as $key => $value)
        {
            $stringifyArray .= "{ name: '" . $key . "'," . "y:" . $value . "},";
        }

        return $stringifyArray;
    }

    /**
     * generate count and push it to array for project Barchart
     * @return array
     */
    public function makeCountArrayForProjectsBaseOnProgressPercantage()
    {
        $valArray = [20, 40, 60, 80, 100];
        $scriptArray = array();
        $lastval = 0;
        for ($i = 0; $i <= count($valArray) - 1; $i ++)
        {
            if ($i == 0)
            {
                $result = $this->project->where('status', true)->where('status_value', '<=', $valArray[ $i ])->count();
                array_push($scriptArray, $result);
            } else
            {
                $result = $this->project->where('status', true)->where('status_value', '>=', $lastval)->where('status_value', '<=', $valArray[ $i ])->count();
                array_push($scriptArray, $result);
            }

            $lastval = $valArray[ $i ];
        }

        return $scriptArray;
    }

    /**
     * this method generate highchart script for bar chart
     * that shows count of project base on set of percentage's
     * @return string
     */
    public function makeBarChartForProjectBaseOnCount()
    {
        $script = "<script>";
        $script .= "$(function () {Highcharts.chart('projectBarcontainer', {chart: {type: 'column'},title: {text: '" . trans('pmis.pro_bar_title') . "'},";
        $script .= "subtitle: {text: '" . trans('pmis.linchart_source') . "'},xAxis: {categories: ['10-20%', '20-40%', '40-60%', '60-80%', '80-100%'],title: {text: null}},";
        $script .= "yAxis: {min: 0,title: {text: '" . trans('pmis.proj_count') . "',align: 'high'},labels: {overflow: 'justify'}},tooltip: {valueSuffix: '";
        $script .= " Projects'},colors: ['#815D8D', '#BF453D', '#C9DBE4', '#286198', '#7CB5EC'],plotOptions: {bar: {dataLabels: {enabled: true}},";
        $script .= "column: {colorByPoint: true}}, credits: {enabled: false},series: [{name: '".trans('pmis.projects')."',data: ";
        $script .= $this->makeJavaScriptArrayString($this->makeCountArrayForProjectsBaseOnProgressPercantage()) . "}]});}); </script>";

        return $script;

    }

    /**
     * this method generates script of pie chart for amount of pro
     * ject and violations that we allready have in MIS
     * @return string
     */
    public function makeCountPieChartOfProjectsAndViolations()
    {
        $script = "<script>";
        $script .= "$(function () {Highcharts.chart('pieChContainer', {chart: {type: 'pie',options3d: {enabled: true,alpha: 45,beta: 0}},";
        $script .= "title: {text: '" . trans('pmis.count_pie_title') . "'}, subtitle: {text: '" . trans('pmis.linchart_source') . "'},";
        $script .= "plotOptions: {pie: {allowPointSelect: true,cursor: 'pointer',depth: 35,dataLabels: {enabled: true,format: '{point.name}'}}},";
        $script .= "series: [{type: 'pie',name: 'Count',data: [['" . trans('pmis.projects') . " (" . $this->project->count() . ")" . "', " . $this->project->count() . "]";
        $script .= ",['" . trans('pmis.violation') . " (" . $this->infraction->count() . ")" . "', " . $this->infraction->count() . "],]}]";
        $script .= "});}); </script>";

        return $script;
    }

    /**
     * generate associative array for pie chart
     * to present percentage
     * @return array
     */
    public function getAssocValueForProjectPercantage()
    {
        $all = $this->project->all()->count();
        if($all == 0)$all = 1;
        $stoped = $this->project->where('status', false)->count();
        $underProgress = $this->project->where('status', true)->count();

        $underProgressPer = $underProgress * 100 / $all;
        $stopedPer = $stoped * 100 / $all;

        return [trans('pmis.under_progress') => $underProgressPer, trans('pmis.Stopped') => $stopedPer];
    }

    /**
     * this method generate highchart script for bar chart
     * that shows culomn of violation chart base on districts
     * @param $catagoryArray
     * @param $dataArray
     * @param $id
     * @return string
     */
    public function makeBarChartForViolationBaseOnDistrict($catagoryArray, $dataArray, $id)
    {
        $script = "<script>";
        $script .= "$(function () {Highcharts.chart('" . $id . "', {chart: {type: 'column'},title: {text: '" . trans('pmis.bar_violation') . "'},";
        $script .= "subtitle: {text: '" . trans('pmis.linchart_source') . "'},xAxis: {categories: " . $catagoryArray . ",title: {text: null}},";
        $script .= "yAxis: {min: 0,title: {text: '" . trans('pmis.vio_count') . "',align: 'high'},labels: {overflow: 'justify'}},tooltip: {valueSuffix: '";
        $script .= " violation'},colors: ['#815D8D', '#BF453D', '#C9DBE4', '#286198', '#7CB5EC'],plotOptions: {bar: {dataLabels: {enabled: true}},";
        $script .= "column: {colorByPoint: true}}, credits: {enabled: false},series: [{name: '" . trans('pmis.vio_count') . "',data: ";
        $script .= $dataArray . "}]});}); </script>";

        return $script;

    }

    /**
     * make catagory array of districts for violation bar chart
     * @return string
     */
    public function makeDistrictCatagoryArray()
    {
        $catArrayStr = '[';
        for ($i = 1; $i <= 22; $i ++)
        {
            if(App::isLocale('fa') || App::isLocale('pa'))
            {
              $catArrayStr .= "'" . "ناحیه " . "-" . $i . "', ";
            }
            else
            {
              $catArrayStr .= "'" . "Dis" . "-" . $i . "', ";
            }

        }

        $catArrayStr .= "]";

        return $catArrayStr;
    }

    /**
     * generates array of district counts
     * @return array
     */
    public function getDataArrayOfDistrictsForBChart()
    {
        $distCountArray = array();
        for ($i = 1; $i <= 22; $i ++)
        {
            $result = $this->infraction->where('district', $i)->count();
            array_push($distCountArray, $result);
        }

        $arrayString = "[";

        for ($i = 0; $i <= 21; $i ++)
        {
            if ($i != 21)
            {
                $arrayString .= $distCountArray[ $i ] . ", ";
            } else
            {
                $arrayString .= $distCountArray[ $i ];
            }
        }
        $arrayString .= "]";

        return $arrayString;
    }

    /**
     * generate pie chart for violation and projects report related fields
     * @param $assocArray
     * @param $id
     * @param $title
     * @return string
     */
    public function makeDynamicPieChartForProjectsAndViolations($assocArray, $id, $title)
    {
        $script = "<script>";
        $script .= "$(function () {Highcharts.getOptions().plotOptions.pie.colors = (function () {";
        $script .= "var colors = [],base = Highcharts.getOptions().colors[0],i;for (i = 0; i < 10; i += 1) {";
        $script .= "colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());}return colors;}());";
        $script .= "Highcharts.chart('" . $id . "', {chart: {plotBackgroundColor: null,plotBorderWidth: null,plotShadow: false,type: 'pie'},";
        $script .= "title: {text: '" . $title . "'},subtitle: {text: '" . trans('pmis.linchart_source') . "'},";
        $script .= "tooltip: {pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},";
        $script .= "plotOptions: {pie: {allowPointSelect: true,cursor: 'pointer',dataLabels: { enabled: true,format: '<b>{point.name}</b>:";
        $script .= " {point.percentage:.1f} ',style: {color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'}}}},";
        $script .= "series: [{name: 'Percentage',data: [" . $this->makeJavascriptArrayOfObjForPieChart($assocArray) . "]";

        $script .= "}]});}); </script>";

        return $script;
    }

    /**
     * @param $keyArray
     * @param $id
     * @param $title
     * @param $preperty
     * @param $count
     * @return string
     */
    public function makePieChartForInfractionProperties($keyArray, $id, $title, $preperty, $count)
    {
        $all = $this->infraction->all()->count();
        $percantageArray = array();
        if($all == 0)$all = 1;
        for ($i = 1; $i <= $count; $i ++)
        {
            $resul = $this->infraction->where($preperty, $i)->count();
            $percantageArray[ $keyArray[ $i ] ] = ($resul * 100 / $all);
        }

        return $this->makeDynamicPieChartForProjectsAndViolations($percantageArray, $id, $title);
    }

    /**
     * this is creating script for pie chart that shows type of activity in infraction
     * @return string
     */
    public function makeActivityChart()
    {
        $assocArray = ['', trans('pmis.residential'), trans('pmis.commercial'), trans('pmis.industrial'), trans('pmis.services')];
        $id = "typeActivityPie";
        $title = trans("pmis.type_Of_activity");
        $preperty = "type_of_activity";
        $count = 4;

        return $this->makePieChartForInfractionProperties($assocArray, $id, $title, $preperty, $count);
    }

    /**
     * this is creating script for pie chart that shows type of activity in infraction
     * @return string
     */
    public function makeRegionStatusPieChart()
    {
        $assocArray = ['', trans('pmis.planned'), trans('pmis.nplanned'), trans('pmis.overconf')];
        $id = "regionStatus";
        $title = trans("pmis.region_status");
        $preperty = "region_status";
        $count = 3;

        return $this->makePieChartForInfractionProperties($assocArray, $id, $title, $preperty, $count);
    }

    /**
     * this is creating script for pie chart that shows status Of Constraction in infraction
     * @return string
     */
    public function makestatusOfConstractionPieChart()
    {
        $assocArray = ['', trans('pmis.und_construction'), trans('pmis.stopped'), trans('pmis.Completed')];
        $id = "constStatus";
        $title = trans("pmis.const_status");
        $preperty = "status_of_cunstraction";
        $count = 3;

        return $this->makePieChartForInfractionProperties($assocArray, $id, $title, $preperty, $count);
    }

    /**
     * this is creating script for pie chart that shows Construction Permit in infraction
     * @return string
     */
    public function makeConstructionPermitPieChart()
    {
        $assocArray = ['', trans("pmis.yes"), trans('pmis.no')];
        $id = "constPermit";
        $title = trans("pmis.permits");
        $preperty = "contruction_certificate";
        $count = 2;

        return $this->makePieChartForInfractionProperties($assocArray, $id, $title, $preperty, $count);
    }

    public function test()
    {
        return $this->project->where('status', 1)->where('status_value', '>=', 65)->get();
    }

     /**
     * generate pie chart for violation and projects report related fields
     * @param $assocArray
     * @param $id
     * @return string
     */
    public function makePieChartForProjectsDetails($assocArray, $id)
    {
        $script = "<script>";
        $script .= "$(function () {Highcharts.getOptions().plotOptions.pie.colors = (function () {";
        $script .= "var colors = [],base = Highcharts.getOptions().colors[0],i;for (i = 0; i < 10; i += 1) {";
        $script .= "colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());}return colors;}());";
        $script .= "Highcharts.chart('" . $id . "', {chart: {plotBackgroundColor: null,plotBorderWidth: null,plotShadow: false,type: 'pie'},";
        $script .= "title: {text: '" . trans('pmis.vnp_title') . "'},subtitle: {text: '" . trans('pmis.linchart_source') . "'},";
        $script .= "tooltip: {pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},";
        $script .= "plotOptions: {pie: {allowPointSelect: true,cursor: 'pointer',dataLabels: { enabled: true,format: '<b>{point.name}</b>:";
        $script .= " {point.percentage:.1f} ',style: {color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'}}}},";
        $script .= "series: [{name: 'Percentage',data: [" . $this->makeJavascriptArrayOfObjForPieChart($assocArray) . "]";

        $script .= "}]});}); </script>";

        return $script;
    }

    /**
     * generate associative array for pie chart
     * to present percentage
     * @return array
     */
    public function getAssocValueForProjectStatus()
    {

        $count_design_project = Project::where('status',1)->count();
        $count_procurement_project = Project::where('status',2)->count();
        $count_construction_project = Project::where('status',3)->count();
        $count_completed_project = Project::where('status',4)->count();
        return [
            trans('pmis.design')  => $count_design_project,
            trans('pmis.procurement') => $count_procurement_project,
            trans('pmis.construction') =>$count_construction_project,
            trans('pmis.completed') =>$count_completed_project
        ];
    }


    /**
     * return the total projects Weighted to KM Project
     */
    public function getProjectWeightedToKmBudget()
    {
        $projects = Project::all();
        $weightage = 0;
        foreach ($projects as $project){
            $weightage += (
                $this->handleDividedByZero($project->available_fund,$this->getProjectsAvailableFund()
                )*100);
        }
        return $weightage;
    }

    /**
     * @return float|int
     */
    public function getExpectedExecutionToTotalBudget(){

        return round($this->handleDividedByZero(
            $this->getExpectedExecutionInYear(),
            $this->getProjectsAvailableFund()
            )*100);
    }

    /**
     * @return mixed
     * get the total of expense record with budget department
     */
    public function getExpenseRecordWithBudgetDepartment(){
        return Project::all()->sum('exp_rec_budg_dep');
    }

    /**
     * return the total available funds of project
     */
    public function getProjectsAvailableFund(){


        return  Project::all()->sum('available_fund');
    }


    /**
     * return the total of expected execution in year 1396
     */
    public function getExpectedExecutionInYear(){


        return  Project::all()->sum('expected_exe_in_year');
    }

    /**
     * @return float
     * get the executed tell now of total project
     */
    public function getExecutedTellNow(){
        return round($this->handleDividedByZero(
            $this->getExpenseRecordWithBudgetDepartment(),
            $this->getExpectedExecutionInYear())*100);
    }

    /**
     * @param $value1
     * @param $value2
     * @return float|int
     */
    public function handleDividedByZero($value1, $value2)
    {
        if($value2 == 0){
            return 0;
        }else{
            return $value1 / $value2;
        }
    }



}
