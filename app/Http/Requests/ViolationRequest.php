<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class ViolationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'serial_number' => 'required|numeric',
            'name' => 'required|string|max:255',
            'father_name' => 'required|string|max:255',
            'tazkira_number' => 'required|numeric',
            //'job' => 'required|string|max:255',
            //'property_area' => 'required|string|max:255',
             'district' => 'required|max:255',
            'street' => 'required|string|max:255',
            'property_number' => 'required|numeric',
            'contruction_certificate' => 'required|max:255',

            'region_status' => 'required|max:255',
            'type_of_activity' => 'required|max:255',
            'status_of_cunstraction' => 'required|max:255',
            'status_of_infractions' => 'required|max:255',
            'violation_type' => 'required|max:255',
            // 'start_date' => 'required|date',
            // 'monitor_date' => 'required|date|after:start_date',
            'proceedings' => 'required|string|max:255',
            'latitude_degree' => 'required|numeric',
            'latitude_minute' => 'required|numeric',
            'latitude_second' => 'required|numeric',
            'longitude_degree' => 'required|numeric',
            'longitude_minute' => 'required|numeric',
            'longitude_second' => 'required|numeric',
            'comment' => 'required|string',
        ];
    }

}
