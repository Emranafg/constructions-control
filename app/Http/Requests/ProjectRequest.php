<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
//            'contractor' => 'required|string|max:255',
//            'code' => 'required|numeric',
           'available_fund'=>'numeric',
           'expected_exe_in_year' => 'numeric',
            'code' => 'numeric',
            'project_type' => 'numeric',
            'year' => 'numeric',
            'exp_rec_budg_dep' => 'numeric',
            'actual_date' => 'date',
            'planned_date' => 'date',
            'status' => 'numeric',
            'days_remain' => 'date',
            'percentage_remain' => 'numeric',
//            'year' => 'required|string',
            // 'contract' => 'required|string|max:255',
            'owner' => 'required|string|max:255',
            'area' => 'required|string|max:255',
            'district' => 'required|string|max:255',
            'passway' => 'required|string|max:255',
            'contract_date' => 'required|date',
            'start_date' => 'required|date|after:contract_date',
            'actual_date' => 'required|date|after:start_date',
            'planned_date' => 'required|date|after:start_date',
            'project_budget' => 'required|alpha_num|max:255',
            'additional_budget' => 'required|alpha_num|max:255',
            'latitude_degree' => 'required|numeric',
            'latitude_minute' => 'required|numeric',
            'latitude_second' => 'required|numeric',
            'longitude_degree' => 'required|numeric',
            'longitude_minute' => 'required|numeric',
            'longitude_second' => 'required|numeric',
        ];
    }

}
