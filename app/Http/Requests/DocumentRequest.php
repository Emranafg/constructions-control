<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class DocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'source' => 'required|string|max:255',
            //'destination' => 'required|string|max:255',
             'start_date' => 'required|date',
           //'expiration_date' => 'required|date|after:start_date',
           // 'category' => 'required|numeric',
            //'priority' => 'required|numeric',
           // 'progress' => 'required|numeric',
           // 'version' => 'required|numeric',
            'case' => 'required|string|max:255',
        ];
    }

}
