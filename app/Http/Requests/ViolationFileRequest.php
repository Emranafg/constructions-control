<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class ViolationFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'doc_number' => 'required|string',
            'doc_name' => 'required|string|max:255',
            'doc_date' => 'required|date',
            'doc_description' => 'required',
            //'job' => 'required|string|max:255',
            //'property_area' => 'required|string|max:255',
        ];
    }

}
