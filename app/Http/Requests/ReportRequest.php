<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class ReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'supervision_date' => 'required|max:255',
            'supervision_time' => 'required|max:255',
            'weather' => 'required|alpha|max:255',
            'temperature' => 'required|max:255',
            'project_manager' => 'required|numeric',
            'control_manager' => 'required|numeric',
            'safety_manager' => 'required|numeric',
            'worker_leader' => 'required|numeric',
            'smart_worker' => 'required|numeric',
            'ordinary_worker' => 'required|numeric',
            'guard' => 'required|numeric',
            'visitor' => 'required|numeric',
        ];
    }

}
