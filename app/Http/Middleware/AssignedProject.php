<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\ProjectUser;

class AssignedProject
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $userId = Auth::id();
        $project_id = $request->id;
        $projectUser = ProjectUser::where('user_id',$userId)->where('project_id',$project_id)->first();
        if($projectUser || Auth::user()->hasRole('admin') || Auth::user()->can('edit_project')){
            return $next($request);
        }
        else{
        abort('404');
    }

    }
}
