<?php

namespace App\Http\Middleware;


use Closure;
use App\InfractionUser;
use Illuminate\Support\Facades\Auth;

class AssignedInfraction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = Auth::id();
        $infraction_id = $request->id;
        $projectUser = InfractionUser::where('user_id',$userId)->where('infraction_id',$infraction_id)->first();
        if($projectUser || Auth::user()->hasRole('admin') || Auth::User()->can('edit_violation')){
            return $next($request);
        }
        else{
            abort('404');
        }
    }
}
