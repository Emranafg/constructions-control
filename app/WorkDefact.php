<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class WorkDefact extends Model
{

    /**
     * The database table name used by this model.
     * @var String
     */
    protected $table = 'work_defacts';

    /**
     * Relationship to reports
     */
    public function reports()
    {
        return $this->belongsTo('App\Report');
    }

    /**
     * work defacts composed of many uploads
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function defactsUpload()
    {
        return $this->hasMany('App\WorkDefactsUpload');
    }
}
