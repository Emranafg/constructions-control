<?php

namespace App\Transformers;


use App\Project;
use DB;
use League\Fractal\TransformerAbstract;

class ProjectStatusTransformer extends TransformerAbstract
{

//    protected $defaultIncludes = [
//        'GetStatus'
//    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Project $project)
    {

        $projectData = [
            "id"                        => $project->id,
            "name"                      => $project->name,
            "code"                      => $project->code,
            "available_fund"            => $project->available_fund,
            "project_weightage"         => $this->getProjectWeightage($project->available_fund),
            "expected_exe_in_year"      => $project->expected_exe_in_year,
            "ecp_exe_weig_to_total_Bud" => $this->getExepectedExecutionToTotalBudget($project->expected_exe_in_year),
            "exp_rec_budg_dep"          => $project->exp_rec_budg_dep,
            "expected_till_now"         => $this->roundOf(($this->handleDevideByZero($project->exp_rec_budg_dep, $project->expected_exe_in_year)) * 100),
            "status"                    => $this->getStatus($project->status),
            // important for operation not report
            "year"                      => $project->year,
            "complete_date"             => $project->complete_date,
            "days_remain"               => $project->days_remain,
            "percentage_remain"         => $this->roundOf($project->percentage_remain),
            "complete_date"             => $project->complete_date,
            "project_type"              => $project->project_type,
        ];

        $result = $project->getStatus;

        if ( ! $result->isEmpty())
        {
            $budgetCollect = $this->includeGetStatus($project);
            $projectData = array_merge($projectData, $budgetCollect);
        }

        return $projectData;
    }

    public function includeGetStatus($project)
    {
        $budgetStatus = $project->getStatus;

        $budgetCollection = $this->MakeBudgetCollection($budgetStatus);

        return $budgetCollection;
    }

    private function MakeBudgetCollection($budgetStatus)
    {
        $projects = array();

        foreach ($budgetStatus as $status)
        {
            $projects [] = $this->transformProjectStatus($status->toArray());
        }

        $budgetSchedule = $this->getProjectBudgetSchedule($projects);

        return $budgetSchedule;
    }

    private function transformProjectStatus($statusArr)
    {
        return [

            'planned_amount'  => $statusArr['planned_amount'],
            'plan_percentage' => $statusArr['planned_percentage'],
            'actual_amount'   => $statusArr['actual_amount'],
            'act_percentage'  => $statusArr['actual_perccentage'],
            'month'           => $statusArr['month_number'],
            'year'            => $statusArr['year'],
            'id'              => $statusArr['id']
        ];
    }

    private function getProjectBudgetSchedule($budgetCollection)
    {
        $singletonArr = array();

        for ($i = 0; $i < count($budgetCollection); $i ++)
        {
            $singletonArr[ 'planned_amount_' . $i ] = $budgetCollection[ $i ]['planned_amount'];
            $singletonArr[ 'plan_percentage_' . $i ] = $budgetCollection[ $i ]['plan_percentage'];
            $singletonArr[ 'actual_amount_' . $i ] = $budgetCollection[ $i ]['actual_amount'];
            $singletonArr[ 'act_percentage_' . $i ] = $budgetCollection[ $i ]['act_percentage'];
        }

        return $singletonArr;
    }


    private function getStatus($status)
    {
        switch ($status)
        {
            case 1 :
                return 'Design';
            case 2 :
                return 'Procurement';
            case 3 :
                return 'Construction';
            case 4 :
                return 'Complete';
        }
    }

    private function getProjectWeightage($available_fund)
    {
        $sum_avaiable_fund = Project::sum('available_fund');
        $projectWeightage = ($this->handleDevideByZero($available_fund, $sum_avaiable_fund)) * 100;

        return $this->roundOf($projectWeightage);
    }

    private function getExepectedExecutionToTotalBudget($expected_exe_in_year)
    {
        $sum_avaiable_fund = Project::sum('available_fund');
        $exepected_execution_to_total_budget = ($this->handleDevideByZero($expected_exe_in_year, $sum_avaiable_fund)) * 100;

        return $this->roundOf($exepected_execution_to_total_budget);
    }

    private function roundOf($value)
    {
        if ($value == 0)
        {
            return '';
        } else
        {
            return round($value);
        }
    }

    private function handleDevideByZero($value, $value2)
    {
        if ($value == 0 || $value2 == 0)
        {
            return 0;
        } else
        {
            return $value / $value2;
        }
    }

}
