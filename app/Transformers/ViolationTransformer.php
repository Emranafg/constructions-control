<?php
/**
 * Created by PhpStorm.
 * User: zaki
 * Date: 11/26/16
 * Time: 1:48 PM
 */

namespace App\Transformers;


use App\Infraction;
use Illuminate\Support\Facades\App;
use League\Fractal\TransformerAbstract;

class ViolationTransformer extends TransformerAbstract
{

    /**
     *
     * @param Infraction $infraction
     * @return array
     */
    public function transform(Infraction $infraction)
    {
        // check the localization base on decide which aray should be returned
        if (App::isLocale('en'))
        {
            return [

                'serial_number'           => $infraction->serial_number,
                'name'                    => $infraction->name,
                'father_name'             => $infraction->father_name,
                'tazkira_number'          => $infraction->tazkira_number,
                'job'                     => $infraction->job,
                'coordinate'              => $this->getCoordinates($infraction),
                'property_area'           => $infraction->property_area,
                'district'                => $infraction->district,
                'street'                  => $infraction->street,
                'property_number'         => $infraction->property_number,
                'contruction_certificate' => $this->getPermits($infraction),
                'comment'                 => $infraction->comment,
                'region_status'           => $this->getAreaStatus($infraction),
                'type_of_activity'        => $this->getTypeOfActivity($infraction),
                'status_of_cunstraction'  => $this->getStatusOfConstruction($infraction),
                'start_date'              => $infraction->start_date,
                'monitor_date'            => $infraction->monitor_date,
                'proceedings'             => $this->transformViolationProceedings($infraction),
                'status_of_infractions'   => $this->transformStatusOfViolation($infraction),
                'violation_type'          => $this->transformViolationType($infraction),
                'garbbing_area'          => $infraction->garbbing_area,

            ];
        } else
        {
            return [
                'garbbing_area'          => $infraction->garbbing_area,
                'violation_type'          => $this->transformViolationType($infraction),
                'status_of_infractions'   => $this->transformStatusOfViolation($infraction),
                'proceedings'             => $this->transformViolationProceedings($infraction),
                'monitor_date'            => $infraction->monitor_date,
                'start_date'              => $infraction->start_date,
                'status_of_cunstraction'  => $this->getStatusOfConstruction($infraction),
                'type_of_activity'        => $this->getTypeOfActivity($infraction),
                'region_status'           => $this->getAreaStatus($infraction),
                'comment'                 => $infraction->comment,
                'contruction_certificate' => $this->getPermits($infraction),
                'property_number'         => $infraction->property_number,
                'street'                  => $infraction->street,
                'district'                => $infraction->district,
                'property_area'           => $infraction->property_area,
                'coordinate'              => $this->getCoordinates($infraction),
                'job'                     => $infraction->job,
                'tazkira_number'          => $infraction->tazkira_number,
                'father_name'             => $infraction->father_name,
                'name'                    => $infraction->name,
                'serial_number'           => $infraction->serial_number,
            ];
        }

        //if lang is Pashto and Dari return this
    }

    private function getCoordinates($data)
    {
        return $data->latitude . ' ' . $data->longitude;
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function getPermits($data)
    {
        switch ($data->contruction_certificate)
        {
            case 1:
                return trans('pmis.yes');
            case 2:
                return trans('pmis.no');
        }
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function getAreaStatus($data)
    {
        switch ($data->region_status)
        {
            case 1:
                return trans('violation.Planned');
            case 2:
                return trans('violation.Unplanned');
            case 3:
                return trans('violation.Irregular');
        }
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function getTypeOfActivity($data)
    {
        switch ($data->type_of_activity)
        {
            case 1:
                return trans('pmis.residential');
            case 2:
                return trans('pmis.commercial');
            case 3:
                return trans('pmis.industrial');
            case 4:
                return trans('pmis.services');
        }
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function getStatusOfConstruction($data)
    {
        switch ($data->status_of_cunstraction)
        {
            case 1:
                return trans('violation.ongoing');
            case 2:
                return trans('violation.stopped');
            case 3:
                return trans('violation.completed');
        }
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function transformStatusOfViolation($data)
    {
        switch ($data->status_of_infractions)
        {
            case 1:
                return trans('violation.ongoing');
            case 2:
                return trans('violation.stopped');
            case 3:
                return trans('violation.completed');
            case 4:
                return trans('violation.demolished');
            case 5:
                return trans('violation.resolved');

        }
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function transformViolationProceedings($data)
    {
        for ($i = 1; $i <= 8; $i ++)
        {
            if ($i == $data->proceedings)
            {
                return trans('violation.proceeding_' . $i);
            }
        }
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function transformViolationType($data)
    {
        for ($i = 1; $i <= 24; $i ++)
        {
            if ($i == $data->violation_type)
            {
                return trans('violation.Type_of_Violation_' . $i);
            }
        }
    }

    /**
     * transform the extact geven coordinate location
     * @param $infraction
     * @return string
     */
    public static function transformGMapCoordintes($infraction)
    {
        return "<a target = '_blank' href = 'http://maps.google.com/maps?z=13&t=m&q=loc:" . $infraction->latitude . "+" . $infraction->longitude . "'> " . trans('violation.see_on_go') . "</a>";
    }
}