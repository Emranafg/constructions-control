<?php
namespace App\Transformers;


use App\Project;
use Illuminate\Support\Facades\App;
use League\Fractal\TransformerAbstract;

class ProjectTransformer extends TransformerAbstract
{

    /**
     * this transformer is for Allergy Model ;)
     *
     * @param Project $project
     * @return array
     */
    public function transform(Project $project)
    {
        // check the localization base on decide which aray should be returned
        if (App::isLocale('en'))
        {
            return [

                'name'              => $project->name,
                'contract'          => $project->contract,
                'contractor'        => $project->contractor,
                'owner'             => $project->owner,
                'area'              => $project->area,
                'passway'           => $project->passway,
                'status'            => $this->getStatus($project->status),
                'status_value'      => $project->status_value,
                'latitude'          => $project->latitude,
                'gratitude'         => $project->gratitude,
                'contract_date'     => $project->contract_date,
                'start_date'        => $project->start_date,
                'compilation_date'  => $project->compilation_date,
                'project_budget'    => $project->project_budget,
                'addational_budget' => $project->addational_budget

            ];
        } else
        {
            return [

                'addational_budget' => $project->addational_budget,
                'project_budget'    => $project->project_budget,
                'compilation_date'  => $project->compilation_date,
                'start_date'        => $project->start_date,
                'contract_date'     => $project->contract_date,
                'gratitude'         => $project->gratitude,
                'latitude'          => $project->latitude,
                'status_value'      => $project->status_value,
                'status'            => $this->getStatus($project->status),
                'passway'           => $project->passway,
                'area'              => $project->area,
                'owner'             => $project->owner,
                'contractor'        => $project->contractor,
                'contract'          => $project->contract,
                'name'              => $project->name,
            ];
        }


        //if lang is Pashto and Dari return this

    }

    /**
     * @param $data
     * @return string
     */
    private function getStatus($data)
    {
        switch ($data)
        {
            case 1:
                return trans('pmis.design');
            case 2:
                return trans('pmis.procurement');
            case 3:
                return trans('pmis.construction');
            case 4:
                return trans('pmis.completed');
        }
    }

}