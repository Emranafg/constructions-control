<?php
namespace App\Transformers;


use App\Document;
use Illuminate\Support\Facades\App;
use League\Fractal\TransformerAbstract;

class DocumentTransformer extends TransformerAbstract
{

    /**
     * this transformer is for Allergy Model ;)
     *
     * @param Document $document
     * @return array
     * @internal param Project $project
     */
    public function transform(Document $document)
    {
        // check the localization base on decide which aray should be returned


            return [

                'id '                => $document->id,
                'title'              => $document->title,
                'source'             => $document->source,
                'destination '       => $document->destination,
                'expiration_date'    => $document->expiration_date,
                'progress'           => $this->getprogress($document->progress),
                'priority'           => $this->getperiority($document->priority),
                'version'            => $this->getversin($document->version),
                'case'                => $document->case
            ];


        //if lang is Pashto and Dari return this

    }

    /**
     * @param $data
     * @return string
     */
    private function getprogress($data)
    {
        switch ($data)
        {
            case 1:
                return 'start';
            case 2:
                return 'under progress';
            case 3:
                return 'completed';
        }
    }


    private function getperiority($data)
    {
        switch ($data)
        {
            case 1:
                return trans('Emergency mesaures');
            case 2:
                return trans('People Petition');
            case 3:
                return trans('Instractions');
            case 4:
                return trans('meetings');
        }
    }
    private function getversin($data)
    {
        switch ($data)
        {
            case 1:
                return trans('First version');
            case 2:
                return trans('secand version');
            case 3:
                return trans('third version');

        }
    }


}