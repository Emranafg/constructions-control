<?php

namespace App\Transformers;

use App\ProjectStatus;
use League\Fractal\TransformerAbstract;

class ProjectGetStatusTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ProjectStatus $status)
    {
        return [
            'planned_amount'  => $status->planned_amount,
            'plan_percentage' => $status->percentage,
            'actual_amount'   => $status->actual_amount,
            'act_percentage'  => '1',
            'month'           => $status->month_number,
            'year'            => $status->year
        ];

    }

}
