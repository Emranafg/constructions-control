<?php

namespace App\Transformers;


use App\Report;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use League\Fractal\TransformerAbstract;

class DailyReportTransformer extends TransformerAbstract
{

    /**
     * this transformer is for Allergy Model ;)
     *
     * @param Report $report
     * @return array
     */
    public function transform(Report $report)
    {
        // check the localization base on decide which aray should be returned
        if (App::isLocale('en'))
        {
            return [

                'status'   => $this->getReportStatus($report),
                'progress' => $this->getRreportProgress($report),
                'address'  => $this->getAddress($report),
                'date'     => $this->getDate($report),
                'time'     => $this->getTime($report),
                'weather'  => $report->weather,
                'temp'     => $report->temprature,

            ];
        } else
        {
            return [

                'temp'     => $report->temprature,
                'weather'  => $report->weather,
                'time'     => $this->getTime($report),
                'date'     => $this->getDate($report),
                'address'  => $this->getAddress($report),
                'progress' => $this->getRreportProgress($report),
                'status'   => $this->getReportStatus($report),
            ];
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    private function getTime($data)
    {
        return Carbon::parse($data->time)->format('l jS \\of F Y h:i:s A');
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function getReportStatus($data)
    {
        switch ($data->status)
        {
            case 0:
                return trans('pmis.stoped');
            case 1:
                return trans('pmis.under_progress');
        }
    }

    /**
     * @param $data
     * @return string
     */
    private function getRreportProgress($data)
    {
        if ($data->status == 1)
        {
            if (App::isLocale('en'))
            {
                return $data->status_value . " %";
            } else
            {
                return  $data->status_value . " فیصد";
            }
        } else
        {
            return $data->status_value;
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    private function getDate($data)
    {
        return Carbon::parse($data->date)->toFormattedDateString();
    }

    /**
     * @param $data
     * @return string
     */
    private function getAddress($data)
    {
        return $data->district . " " . $data->gozar;
    }
}