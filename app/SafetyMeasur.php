<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class SafetyMeasur extends Model
{

    /**
     * The database table name used by this model.
     * @var String
     */
    protected $table = 'safety_measures';

    /**
     * Relationship to reports
     */
    public function reports()
    {
        return $this->belongsTo('App\Report');
    }
}
