<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class ViolationUpload extends Model
{

    /**
     * The database table name used by this model.
     * @var String
     */
    protected $table = 'violation_upload';

    /**
     * Relationship to infraction table
     */
    public function infractions()
    {
        return $this->belongsTo('App\Infraction');
    }
}
