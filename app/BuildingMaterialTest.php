<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class BuildingMaterialTest extends Model
{

    /**
     * The database table name used by this model.
     * @var String
     */
    protected $table = 'building_material_tests';

    /**
     * Relationship to reports
     */
    public function reports()
    {
        return $this->belongsTo('App\Report');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function uploads()
    {
        return $this->hasMany('App\MaterialTestUplaod', 'material_test_id');
    }
}
