<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentCase extends Model
{
    protected $table = "case";
}
