<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class WorkActivityUpload extends Model
{

    /*
    * Relationship to work defacts
    */
    public function activity()
    {
        return $this->belongsTo('App\WorkActivity', 'work_activitie_id');
    }
}
