<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class WorkDefactsUpload extends Model
{

    /**
     * Relationship to work defacts
     */
    public function defacts()
    {
        return $this->belongsTo('App\WorkDefact');
    }
}
