<?php

namespace App;


use App\Transformers\DailyReportTransformer;
use App\Transformers\ProjectStatusTransformer;
use App\Transformers\ProjectTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use Yajra\Datatables\Datatables;

class Project extends Model
{

    /**
     * The database table name used by this model.
     * @var String
     */
    protected $table = 'projects';

    /**
     * The Excel report export data
     * @var Object
     */
    public $excelData;

    /**
     * The Excel project daily report export data
     * @var Object
     */
    private $projectDailyReportData;

    /**
     * project composed of many uploads
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function uploads()
    {
        return $this->hasMany('App\Upload');
    }

    /**
     * project composed of many status
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getStatus()
    {
        return $this->hasMany('App\ProjectStatus');
    }



    /**
     * project composed of many Reports
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports()
    {
        return $this->hasMany('App\Report');
    }

    /**
     * project composed of many Deficiency
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deficiencies()
    {
        return $this->hasMany('App\Deficiency', 'project_id');
    }

    /**
     * project composed of many supervisors
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function supervisors()
    {
        return $this->hasMany('App\Supervisor');
    }

    /**
     * projects belongs to many users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        return $this->belongsToMany('App\User', 'project_user');
    }

    /**
     * belongs to Supervisor
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Supervisor');
    }

    /**
     * Generating Excel formated Reports base on Project Table
     * @return downloadable Excel File
     */
    public function generatExcelReport()
    {
        Excel::create('KM-Project-report', function ($excel)
        {
            // here we are making all rows align center
            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            // here we are adding border to all rows
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
            $excel->getDefaultStyle()->applyFromArray($styleArray);
            // here we are making sheet

            $excel->sheet('KM-Project-report', function ($sheet)
            {
                //Array that replace non readable database columns with a readable columns name for excel sheet
                $data = $this->getLocalizedExcelSheetTitles();

                $sheet->setHeight(1, 35);


                //generating the sheet title from array
                $sheet->fromArray(array($data), null, 'A1', false, false);
                //generating excel sheet by help of Transformer class of Projec
                $sheet->cell('A1:O1', function ($cell)
                {
                    // manipulate the cell
                    $cell->setBackground('#C00000');
                    // Set with font color
                    $cell->setFontColor('#ffffff');
                    // Set font family
                    $cell->setFontFamily('Calibri');
                    // Set font size
                    $cell->setFontSize(16);
                    // Set font weight to bold
                    $cell->setFontWeight('bold');
                    //Set alignment to center
                    $cell->setAlignment('center');
                    //Set vertical alignment to middle
                    $cell->setValignment('center');
                    // Set all borders (top, right, bottom, left)
                    $cell->setBorder('solid', 'solid', 'solid', 'solid');


                });
                $transformedData = $this->getTransformedProjects();

                $sheet->fromArray($transformedData["data"], null, 'A1', false, false);
            });

        })->download('xls');
    }

    /**
     * prepare localized base order array for excel sheet titles
     * @return array
     */
    private function getLocalizedExcelSheetTitles()
    {
        if (App::isLocale('en'))
        {
            return [

                trans('pmis.p_name'), trans('pmis.contract'), trans('pmis.contractor'),
                trans('pmis.owner'), trans('pmis.area'), trans('pmis.passway'),
                trans('pmis.status'), trans('pmis.status_value'), trans('pmis.latitude'),
                trans('pmis.longitude'), trans('pmis.contract_date'), trans('pmis.Start_Date'),
                trans('pmis.compilation_date'), trans('pmis.project_budget'), trans('pmis.addational_budget')
            ];
        } else
        {
            return [

                trans('pmis.addational_budget'), trans('pmis.project_budget'), trans('pmis.compilation_date'),
                trans('pmis.start_date'), trans('pmis.contract_date'), trans('pmis.longitude'),
                trans('pmis.latitude'), trans('pmis.status_value'), trans('pmis.status'),
                trans('pmis.passway'), trans('pmis.area'), trans('pmis.owner'), trans('pmis.contractor'),
                trans('pmis.contract'), trans('pmis.p_name')
            ];
        }
    }

    /**
     * generating the excel files for
     * @return array
     */
    private function getTransformedProjects()
    {
        return fractal()
            ->collection($this->getDataForExcelExport())
            ->transformWith(new ProjectTransformer())
            ->toArray();
    }

    /**
     * generate filterd data which passed with get request
     * @param $status
     * @param $progress
     * @param $Poperetor
     * @param $pValue
     * @param $dateType
     * @param $dateOperator
     * @param $date
     * @return mixed
     */
    public function getCustomizedDataBaseOnUrlParameters($status, $progress, $Poperetor, $pValue, $dateType, $dateOperator, $date)
    {
        $Varfields = [
            'status'       => $status,
            'status_value' => $pValue,
            'date'         => $date,
        ];

        $result = new self;

        foreach ($Varfields as $attr => $value)
        {

            if ( ! empty($value))
            {
                if ($attr == 'status')
                {
                    $value = $this->castBoolean($value);
                    $result = $result->where($attr, (int) $value);
                }
                if ($attr == 'status_value' && $value != 'unknown')
                {
                    $result = $result->where($attr, $Poperetor, (int) $value);
                }
                if ($attr == 'date' && $value != 'unknown' && $dateType != '')
                {
                    $result = $result->where($dateType, $dateOperator, $value);
                }
            }
        }

        return $result->get();
    }

    /**
     * setter function for data that is used for excel export
     * @param $data
     */
    public function setDataForExcelExport($data)
    {
        $this->excelData = $data;
    }

    /**
     * setter function for data that is used for excel export
     * @return Object
     */
    public function getDataForExcelExport()
    {
        return $this->excelData;
    }

    /**
     * generate data table
     * @param $data
     * @return mixed
     */
    public function generateDataTable($data)
    {
        return Datatables::of($data)
            ->editColumn('status', function ($data)
            {
                return $this->transformProjectStatus($data);
            })
            ->editColumn('status_value', function ($data)
            {
                return $this->transformProjectStatusValue($data);
            })
            ->editColumn('project_budget', function ($data)
            {
                return $this->transformProjectBudget($data);
            })
            ->make(true);
    }

    /**
     * transform project status to wanted format
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function transformProjectStatus($data)
    {
        switch ($data->status)
        {
            case true:
                return trans('pmis.under_progress');
            case false:
                return trans('pmis.stoped');
        }
    }

    /**
     * transform status_value to wanted format
     * @param $data
     * @return string
     */
    private function transformProjectStatusValue($data)
    {
        if (is_numeric($data->status_value))
        {
            return $data->status_value . ' %';
        }

        return '(Stopped) ' . $data->status_value;
    }

    /**
     * transform project budget to wanted format
     * @param $data
     * @return string
     */
    private function transformProjectBudget($data)
    {
        return $data->project_budget . ' $';
    }

    /**
     * @param $value
     * @return bool
     */
    private function castBoolean($value)
    {
        if ($value == "TRUE")
        {
            return true;
        } else
        {
            return false;
        }
    }
    public function district()
    {
        return $this->belongsTo('App\District');
    }
    /**
     * generate general excel sheet report on project daily reports
     * @return downloadable Excel File
     */
    public function generatExcelGeneralDailyReport()
    {
        Excel::create('KM-Project-Daily-Report', function ($excel)
        {
            // here we are making all rows align center
            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            // here we are adding border to all rows
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
            $excel->getDefaultStyle()->applyFromArray($styleArray);
            // here we are making sheet

            $excel->sheet('KM-Project-Daily-report', function ($sheet)
            {
                //Array that replace non readable database columns with a readable columns name for excel sheet
                $data = $this->getLocalizedExcelSheetTitlesForDailyReport();

                $sheet->setHeight(1, 35);


                //generating the sheet title from array
                $sheet->fromArray(array($data), null, 'A1', false, false);
                //generating excel sheet by help of Transformer class of Projec
                $sheet->cell('A1:G1', function ($cell)
                {
                    // manipulate the cell
                    $cell->setBackground('#C00000');
                    // Set with font color
                    $cell->setFontColor('#ffffff');
                    // Set font family
                    $cell->setFontFamily('Calibri');
                    // Set font size
                    $cell->setFontSize(16);
                    // Set font weight to bold
                    $cell->setFontWeight('bold');
                    //Set alignment to center
                    $cell->setAlignment('center');
                    //Set vertical alignment to middle
                    $cell->setValignment('center');
                    // Set all borders (top, right, bottom, left)
                    $cell->setBorder('solid', 'solid', 'solid', 'solid');


                });
                $transformedData = $this->getTransformedProjectsDailyReport();

                $sheet->fromArray($transformedData["data"], null, 'A1', false, false);
            });

        })->download('xls');
    }

    /**
     * generate localized array of project daily report titles
     * @return array
     */
    private function getLocalizedExcelSheetTitlesForDailyReport()
    {
        if (App::isLocale('en'))
        {
            return [

                trans('pmis.status'), trans('pmis.Progress'), trans('pmis.location'),
                trans('pmis.date'), trans('pmis.time'), trans('pmis.Weather'),
                trans('pmis.Temprature')
            ];
        } else
        {
            return [

                trans('pmis.Temprature'), trans('pmis.Weather'), trans('pmis.time'),
                trans('pmis.date'), trans('pmis.location'), trans('pmis.Progress'),
                trans('pmis.status')
            ];
        }
    }

    /**
     * generating the excel files for
     * @return array
     */
    public function getTransformedProjectsDailyReport()
    {
        return fractal()
            ->collection($this->getprojectDailyReportData())
            ->transformWith(new DailyReportTransformer())
            ->toArray();
    }

    /**
     * @param Object $projectDailyGeneralData
     * @return $this
     */
    public function setprojectDailyReportData($projectDailyGeneralData)
    {
        $this->projectDailyReportData = $projectDailyGeneralData;

        return $this;
    }

    /**
     * @return Object
     */
    public function getprojectDailyReportData()
    {
        return $this->projectDailyReportData;
    }



}
