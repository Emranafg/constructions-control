<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Report extends Model
{

    /**
     * Relationship to project
     */
    public function project()
    {
        return $this->belongsTo('App\Project', 'project_id');
    }

    /**
     * project composed of many Supervision
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function supervisions()
    {
        return $this->hasMany('App\Supervision');
    }

    /**
     * project composed of many WorkActivity
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workActivities()
    {
        return $this->hasMany('App\WorkActivity');
    }

    /**
     * project composed of many WorkDefact
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workDefacts()
    {
        return $this->hasMany('App\WorkDefact');
    }

    /**
     * project composed of many SafetyMeasur
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function safetyMeasures()
    {
        return $this->hasMany('App\SafetyMeasur');
    }

    /**
     * project composed of many BuildingMaterialTest
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function buildingMaterialTests()
    {
        return $this->hasMany('App\BuildingMaterialTest');
    }

    /**
     * project composed of many ProjectWorker
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projectWorkers()
    {
        return $this->hasMany('App\ProjectWorker');
    }

    /**
     * project composed of many Equipment
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function equipments()
    {
        return $this->hasMany('App\Equipment');
    }

    /**
     * project composed of many BuildingEquipment
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function buildingEquipments()
    {
        return $this->hasMany('App\BuildingEquipment');
    }
}
