<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Supervisor extends Model
{

    /**
     * The database table name used by this model.
     * @var String
     */
    protected $table = 'supervisors';
    protected $fillable = ['name', 'email', 'phone', 'project_id'];

    /**
     * belongs to project
     */
    public function project()
    {
        return $this->belongsTo('App\Project', 'project_id');
    }

    /**
     * belongs to Infractions
     */
    public function infraction()
    {
        return $this->belongsTo('App\Infraction', 'infraction_id');
    }
}
