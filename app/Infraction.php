<?php

namespace App;


use App\Transformers\ViolationTransformer;
use FontLib\TrueType\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use Yajra\Datatables\Datatables;
use Maatwebsite\Excel\Facades\Excel;
use App\ViolationTypeInfraction;
use Log;

class Infraction extends Model
{

    protected $guarded = ['id'];

    private $excelData;

    /**
     * Infraction composed of many Uploads
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function uploads()
    {
        return $this->hasMany('App\ViolationUpload', 'infraction_id');
    }

    /**
     * Infraction composed of many supervisors
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function supervisors()
    {
        return $this->hasMany('App\Supervisor');
    }

    /**
     * projects belongs to many users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * Infraction composed of many Uploads
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ViolationDocuments()
    {
        return $this->hasMany('App\ViolationDocument', 'infraction_id');
    }

    /**
     * Filters Data came from Request also Transform data to specific wanted format
     * @param $request
     * @return mixed
     */
    public function getCustomeFilteredDataForDataTable($request)
    {
        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor')  || Auth::user()->hasRole('user') )
        {
            if(Auth::user()->hasPermission('view_violation'))
            {
                $infraction = Infraction::select('infractions.*')->get();
            }
        } else
        {
            $infraction = Auth::user()->infractions()->select('infractions.*')->get();
        }

        return Datatables::of($infraction)
            ->editColumn('name', function ($data) {
                return "<a href = '/violation/details/violationId/" . $data->id . " '> " . $data->name . '</a>';
            })
            ->addColumn('action', function ($data) {
                return '<a href="/infraction/' . $data->id . '/edit"><i class="icon edit"></i></a>';
            })
            ->editColumn('latitude', function ($data) {
                return "<a target = '_blank' href = 'http://maps.google.com/maps?z=13&t=m&q=loc:" . $data->latitude . "+" . $data->longitude . "'> " . trans('violation.see_on_go') . "</a>";
            })
            ->editColumn('contruction_certificate', function ($data) {
                return $this->getPermits($data);
            })
            ->editColumn('region_status', function ($data) {
                return $this->getAreaStatus($data);
            })
            ->editColumn('type_of_activity', function ($data) {
                return $this->getTypeOfActivity($data);
            })
            ->editColumn('status_of_cunstraction', function ($data) {
                return $this->getStatusOfConstruction($data);
            })
            ->editColumn('status_of_infractions', function ($data) {
                return $this->transformStatusOfViolation($data);
            })
            ->editColumn('proceedings', function ($data) {
                return $this->transformViolationProceedings($data);
            })
            ->editColumn('violation_type', function ($data) {
                return $this->transformViolationType($data);
            })
            ->addColumn('assign', function ($data) {
                return '<a href = "#" class = "assign-infraction" infraction-id = "' . $data->id . '"><i class = "icon tasks"></i></a>';
            })
            ->addColumn('edite', function ($data) {
                return $this->editeInfraction($data);
            })
            ->addColumn('delete', function ($data) {
                return $this->deleteInfraction($data);
            })
            ->filter(function ($instance) use ($request) {

                if ($request->has('district'))
                {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['district'] == $request->get('district') ? true : false;
                    });
                }
                if ($request->has('contruction_certificate'))
                {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['contruction_certificate'] == $request->get('contruction_certificate') ? true : false;
                    });
                }
                if ($request->has('region_status'))
                {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['region_status'] == $request->get('region_status') ? true : false;
                    });
                }
                if ($request->has('type_of_activity'))
                {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['type_of_activity'] == $request->get('type_of_activity') ? true : false;
                    });
                }
                if ($request->has('status_of_cunstraction'))
                {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['status_of_cunstraction'] == $request->get('status_of_cunstraction') ? true : false;
                    });
                }

                if ($request->has('status_of_infractions'))
                {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['status_of_infractions'] == $request->get('status_of_infractions') ? true : false;
                    });
                }
                if ($request->has('proceedings'))
                {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['proceedings'] == $request->get('proceedings') ? true : false;
                    });
                }
                if ($request->has('violation_type'))
                {
                    $vttype = ViolationTypeInfraction::where('vt_id', '=', $request->get('violation_type'))->get();
                    $rrow = $this->getMatchedViolationType($vttype);
                    $idArr = array();
                    foreach ($rrow as $ro) {
                        if (isset($ro[0])) {
                            $idArr[] = $ro[0]['id'];
                        }
                    }

                    $instance->collection = $instance->collection->filter(function ($row) use ($idArr) {
                        return $row['id'] == in_array($row['id'], $idArr) ? true : false;
                    });
                }
            })
            ->make(true);
    }

    private function getMatchedViolationType($vtType)
    {
        $result = array();
        foreach ($vtType as $vt)
        {
            $result[] = Infraction::where('id', $vt->in_id)->get();
        }

        return $result;
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function getPermits($data)
    {
        switch ($data->contruction_certificate)
        {
            case 1:
                return trans('pmis.yes');
            case 2:
                return trans('pmis.no');
        }
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function getAreaStatus($data)
    {
        switch ($data->region_status)
        {
            case 1:
                return trans('violation.Planned');
            case 2:
                return trans('violation.Unplanned');
            case 3:
                return trans('violation.Irregular');
        }
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function getTypeOfActivity($data)
    {
        switch ($data->type_of_activity)
        {
            case 1:
                return trans('pmis.residential');
            case 2:
                return trans('pmis.commercial');
            case 3:
                return trans('pmis.industrial');
            case 4:
                return trans('pmis.services');
        }
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function getStatusOfConstruction($data)
    {
        switch ($data->status_of_cunstraction)
        {
            case 1:
                return trans('violation.ongoing');
            case 2:
                return trans('violation.stopped');
            case 3:
                return trans('violation.completed');
        }
    }

    private function transformStatusOfViolation($data)
    {
        switch ($data->status_of_infractions)
        {
            case 1:
                return trans('violation.ongoing');
            case 2:
                return trans('violation.stopped');
            case 3:
                return trans('violation.completed');
            case 4:
                return trans('violation.demolished');
            case 5:
                return trans('violation.resolved');

        }
    }

    /**
     * generate filterd data which passed with get request
     * @param $district
     * @param $certification
     * @param $regionStatus
     * @param $activityType
     * @param $cunstractionSt
     * @param $infractionsSt
     * @param $proceedings
     * @param $violationType
     * @return mixed
     */
    public function getCustomizedDataBaseOnUrlParametersForExcel($district, $certification, $regionStatus, $activityType,
                                                                 $cunstractionSt, $infractionsSt, $proceedings, $violationType)
    {
        $Varfields = [
            'district'                => $district,
            'contruction_certificate' => $certification,
            'region_status'           => $regionStatus,
            'type_of_activity'        => $activityType,
            'status_of_cunstraction'  => $cunstractionSt,
            'status_of_infractions'   => $infractionsSt,
            'proceedings'             => $proceedings,
            'violation_type'          => $violationType
        ];

        $result = new self;

        foreach ($Varfields as $attr => $value)
        {
            if ( ! empty($value) && $value != 'unknown')
            {
                $result = $result->where($attr, "=", $value);
            }
        }

        $this->excelData = $result->get();

        return $this->generateExcelBaseOnUrlData();
    }


    /**
     *
     */
    public function generateExcelBaseOnUrlData()
    {
        Excel::create('KM-Project-report', function ($excel) {
            // here we are making all rows align center
            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            // here we are adding border to all rows
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
            $excel->getDefaultStyle()->applyFromArray($styleArray);
            // here we are making sheet

            $excel->sheet('KM-violation-report', function ($sheet) {
                //Array that replace non readable database columns with a readable columns name for excel sheet
                $data = $this->getLocalizedExcelSheetTitles();

                $sheet->setHeight(1, 35);


                //generating the sheet title from array
                $sheet->fromArray(array($data), null, 'A1', false, false);
                //generating excel sheet by help of Transformer class of Projec
                $sheet->cell('A1:U1', function ($cell) {
                    // manipulate the cell
                    $cell->setBackground('#C00000');
                    // Set with font color
                    $cell->setFontColor('#ffffff');
                    // Set font family
                    $cell->setFontFamily('Calibri');
                    // Set font size
                    $cell->setFontSize(16);
                    // Set font weight to bold
                    $cell->setFontWeight('bold');
                    //Set alignment to center
                    $cell->setAlignment('center');
                    //Set vertical alignment to middle
                    $cell->setValignment('center');
                    // Set all borders (top, right, bottom, left)
                    $cell->setBorder('solid', 'solid', 'solid', 'solid');


                });
                $transformedData = $this->getTransformedViolation();

                $sheet->fromArray($transformedData["data"], null, 'A1', false, false);
            });

        })->download('xls');
    }

    /**
     * prepare localized base order array for excel sheet titles
     * @return array
     */
    private function getLocalizedExcelSheetTitles()
    {
        if (App::isLocale('en'))
        {
            return [

                trans('violation.serial_number'), trans('violation.name'), trans('violation.father_name'),
                trans('violation.tazkira_number'), trans('violation.job'), trans('violation.Violation_Coordinates'),
                trans('violation.property_area'), trans('violation.district'), trans('violation.street'),
                trans('violation.property_number'), trans('violation.contruction_certificate'), trans('violation.comment'),
                trans('violation.Category_of_Land'), trans('violation.Type_Of_Activity'), trans('violation.Construction_Status'),
                trans('violation.start_date'), trans('violation.monitor_date'), trans('violation.proceedings'),
                trans('violation.Violation_Status'), trans('violation.Type_of_Violation'),trans('violation.garbbing_land_area'),
            ];
        } else
        {
            return [

                trans('violation.garbbing_land_area'),trans('violation.Type_of_Violation'), trans('violation.Violation_Status'), trans('violation.proceedings'),
                trans('violation.monitor_date'), trans('violation.start_date'), trans('violation.Construction_Status'),
                trans('violation.Type_Of_Activity'), trans('violation.Category_of_Land'), trans('violation.comment'),
                trans('violation.contruction_certificate'), trans('violation.property_number'), trans('violation.street'),
                trans('violation.district'), trans('violation.property_area'), trans('violation.Violation_Coordinates'),
                trans('violation.job'), trans('violation.tazkira_number'), trans('violation.father_name'),
                trans('violation.name'), trans('violation.serial_number'),
            ];
        }
    }

    /**
     * generating excel transformed array for export
     * @return array
     */
    private function getTransformedViolation()
    {

        return fractal()
            ->collection($this->excelData)
            ->transformWith(new ViolationTransformer())
            ->toArray();
    }

    /**
     * @param mixed $excelData
     * @return Infraction
     */
    public function setExcelData($excelData)
    {
        $this->excelData = $excelData;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExcelData()
    {
        return $this->excelData;
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function transformViolationProceedings($data)
    {
        for ($i = 1; $i <= 8; $i ++)
        {
            if ($i == $data->proceedings)
            {
                return trans('violation.proceeding_' . $i);
            }
        }
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function transformViolationType($data)
    {
        for ($i = 1; $i <= 24; $i ++)
        {
            if ($i == $data->violation_type)
            {
                return trans('violation.Type_of_Violation_' . $i);
            }
        }
    }

    /**
     * @param $data
     * @return string
     */
    private function editeInfraction($data)
    {
        $string = '<a href="/infraction/' . $data->id . '/edit"><i class="icon edit"></i></a>';

        return $string;
    }

    /**
     * @param $data
     * @return string
     */
    private function deleteInfraction($data)
    {
        $string = '<div class="visible-md visible-lg hidden-sm hidden-xs">';
        $string .= '<a onclick=confirmDelete(' . $data->id . ') class="btn btn-transparent btn-xs tooltips" id="confirmDelete" tooltip-placement="top" tooltip="Remove"><i class="fa fa-times fa fa-white" style="color: #c82e29;"></i></a>';
        $string .= '</div>';

        return $string;
    }


}
