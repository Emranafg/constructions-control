<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class MaterialTestUplaod extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function materialTest()
    {
        return $this->belongsTo('App\BuildingMaterialTest');
    }
}
