<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeficiencyUpload extends Model
{
    protected $table = 'deficiency_uploads';
    /**
     * Relationship to project
     */
    public function deficiency()
    {
        return $this->belongsTo('App\Deficiency');
    }

}
