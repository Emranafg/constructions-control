<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViolationDocument extends Model
{
    protected $table = 'violation_documents';
    

     /**
     * Relationship to infraction table
     */
    public function infractions()
    {
        return $this->belongsTo('App\Infraction');
    }

    /**
     * Violation Documents composed of many Document Files
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ViolationFiles()
    {
        return $this->hasMany('App\ViolationDocumentFile', 'document_id');
    }
}
