<?php

namespace App\DataTables;


use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Services\DataTable;

class ProjectsDataTable extends DataTable
{

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('name', function ($data)
            {
                return '<a href="/project/details/' . $data->id . '">' . $data->name . "</a>";
            })
            ->editColumn('status', function ($data)
            {
                return $this->getProjectStatus($data);
            })
            ->editColumn('created_at', function ($data)
            {
                return '<a href="/prjects/dailyReports/' . $data->id . '">' . "Daily</a>" . ' | ' . '<a href="/prjects/monthlyReports/' . $data->id . '">' . "Monthly</a>";
            })
            ->addColumn('assign', function ($data)
            {
                return '<a href = "#" class = "assign-project" project-id = "' . $data->id . '"><i class = "icon tasks"></i></a>';
            })
            ->editColumn('status_value', function ($data)
            {
                return $this->getProjectProgress($data);
            })
            ->addColumn('edite', function ($user)
            {
                return $this->editeProject($user);
            })
            ->addColumn('delete', function ($user)
            {
                return $this->deleteProject($user);
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('supervisor'))
        {
            if(Auth::user()->hasPermission('view_project'))
            {
                $query = \App\Project::select();
            }
        } else
        {
            $query = Auth::user()->projects()->select('projects.*');
        }

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {

        return $this->builder()
            ->columns([
                'id',
                'name',
                'code',
                'available_fund',
                'owner',
                'area',
                'status',
                'reports'
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            // add your columns
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'projectsdatatables_' . time();
    }

    /**
     * @param $data
     * @return string
     */
    private function getProjectStatus($data)
    {
        switch ($data->status   )
        {
            case 1:
                return trans('pmis.design');
            case 2:
                return trans('pmis.procurement');
            case 3:
                return trans('pmis.construction');
            case 4:
                return trans('pmis.completed');
        }
    }

    /**
     * transform the extact geven coordinate location
     * @param $data
     * @return string
     */
    public static function transformGMapCoordintes($data)
    {
        return "<a target = '_blank' href = 'http://maps.google.com/maps?z=13&t=m&q=loc:" . $data->latitude . "+" . $data->gratitude . "'> " . trans('violation.see_on_go') . "</a>";
    }

    private function editeProject($data)
    {
        $string = '<div class="visible-md visible-lg hidden-sm hidden-xs">';
        $string .= '<a href="/project/' . $data->id . '/edit" class="btn btn-transparent btn-xs" tooltip-placement="top" tooltip="Edit"><i class="fa fa-pencil"></i></a>';
        $string .= '</div>';

        return $string;
    }

    private function deleteProject($data)
    {
        $string = '<div class="visible-md visible-lg hidden-sm hidden-xs">';
        $string .= '<a onclick=confirmDelete(' . $data->id . ') class="btn btn-transparent btn-xs tooltips" id="confirmDelete" tooltip-placement="top" tooltip="Remove"><i class="fa fa-times fa fa-white" style="color: #c82e29;"></i></a>';
        $string .= '</div>';

        return $string;
    }

    private function getProjectProgress($data)
    {
        $exe_tell_now = round($this->handleDevidedByZero($data->exp_rec_budg_dep,$data->expected_exe_in_year)*100);
        if (App::isLocale('en'))
        {
            return $exe_tell_now . " %";
        } else
        {
            return "<span style='direction: ltr !important;'>" . $exe_tell_now . "</span>" . " فیصد";
        }
    }

    private function handleDevidedByZero($val1,$val2){
        if ($val2 == 0){
            return 0;
        }else{
            return $val1 / $val2;
        }
    }
}
