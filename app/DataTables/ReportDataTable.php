<?php
namespace App\DataTables;
use App\Project;
use App\Report;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Services\DataTable;

class ReportDataTable extends DataTable
{

    public $id;

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        $posts = Report::where('project_id', $this->getId())->select('reports.*');

        return Datatables::of($posts)
            ->editColumn('date', function ($data)
            {
                return '<a href="/prjects/dailyReports/projects/' . $this->getId() . '/details/' . $data->id . '">' . Carbon::parse($data->date)->toFormattedDateString() . '</a>';
            })
            ->editColumn('time', function ($data)
            {
                return Carbon::parse($data->time)->format('l jS \\of F Y h:i:s A');
            })
            ->editColumn('status', function ($data)
            {
                return $this->getReportStatus($data);
            })
            ->editColumn('status_value', function ($data)
            {
                return $this->getRreportProgress($data);
            })
            ->addColumn('edite', function ($data)
            {
                return $this->editeReport($data);
            })
            ->addColumn('delete', function ($data)
            {
                return $this->deleteReport($data);
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $result = Project::findOrFail($this->getId());
        $query = new Report();
        if ($result->reports->count() > 0)
        {
            $query = $result->reports()->query();
        }

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->addAction(['width' => '80px'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            // add your columns
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reportdatatables_' . time();
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    private function getId()
    {
        return $this->id;
    }

    private function editeReport($data)
    {
        $string = '<div class="visible-md visible-lg hidden-sm hidden-xs">';
        $string .= '<a href="/project/'.$data->project_id.'/report/'.$data->id.'/edit" class="btn btn-transparent btn-xs" tooltip-placement="top" tooltip="Edit"><i class="fa fa-pencil"></i></a>';
        $string .= '</div>';
        return $string;
    }

    private function deleteReport($data)
    {
        $string = '<div class="visible-md visible-lg hidden-sm hidden-xs">';
        $string .= '<a onclick=confirmDelete(' . $data->id . ') class="btn btn-transparent btn-xs tooltips" id="confirmDelete" tooltip-placement="top" tooltip="Remove"><i class="fa fa-times fa fa-white" style="color: #c82e29;"></i></a>';
        $string .= '</div>';

        return $string;
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function getReportStatus($data)
    {
        switch ($data->status)
        {
            case 0:
                return trans('pmis.stoped');
            case 1:
                return trans('pmis.under_progress');
        }
    }

    private function getRreportProgress($data)
    {
        if ($data->status == 1)
        {
            if (App::isLocale('en'))
            {
                return $data->status_value . " %";
            } else
            {
                return "<span style='direction: ltr !important;'>" . $data->status_value . "</span>" . " فیصد";
            }
        } else
        {
            return $data->status_value;
        }
    }
}
