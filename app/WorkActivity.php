<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class WorkActivity extends Model
{

    /**
     * The database table name used by this model.
     * @var String
     */
    protected $table = 'work_activities';

    /**
     * Relationship to reports
     */
    public function reports()
    {
        return $this->belongsTo('App\Report');
    }

    /**
     * work defacts composed of many uploads
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activityUpload()
    {
        return $this->hasMany('App\WorkActivityUpload', 'work_activitie_id');
    }
}
