<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class BuildingEquipment extends Model
{

    /**
     * The database table name used by this model.
     * @var String
     */
    protected $table = 'building_equipments';

    /**
     * Relationship to reports
     */
    public function reports()
    {
        return $this->belongsTo('App\Report');
    }
}
