<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{

    /**
     * The database table name used by this model.
     * @var String
     */
    protected $table = 'uploads';
    public $timestamps = false;

    /**
     * Relationship to project
     */
    public function project()
    {
        return $this->belongsTo('App\Project', 'project_id');
    }
}
