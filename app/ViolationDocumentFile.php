<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViolationDocumentFile extends Model
{
    protected $table = 'violation_document_file';
      /**
     * Relationship to Violation Documents table
     */
    public function ViolationDocuments()
    {
        return $this->belongsTo('App\ViolationDocument');
    }

}
