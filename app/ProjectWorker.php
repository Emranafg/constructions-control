<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class ProjectWorker extends Model
{

    /**
     * The database table name used by this model.
     * @var String
     */
    protected $table = 'project_workers';

    /**
     * Relationship to reports
     */
    public function reports()
    {
        return $this->belongsTo('App\Report');
    }
}
