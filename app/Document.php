<?php

namespace App;
use App\Transformers\DocumentTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use Yajra\Datatables\Datatables;


class Document extends Model
{
    protected $table = "documents";
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function progress()
    {
        return $this->hasMany('App\Progress');
    }

    public function DocumentUpload()
    {
        return $this->hasMany('App\DocumentUpload', 'document_id');
    }


    public function getCustomeFilteredDataForDataTable($request)
    {
        $infraction = Document::select(['id','case','title','source','destination','start_date','expiration_date','progress','priority',
            'version' ])->get();

        return Datatables::of($infraction)
            ->editColumn('case', function ($data)
            {
                return '<a href="/document/details/' . $data->id . '">' . $data->case . "</a>";
            })
            ->editColumn('progress', function ($data)
            {
                return $this->getProgress($data);
            })
            ->editColumn('priority', function ($data)
            {
                return $this->getpriority($data);
            })
            ->editColumn('version', function ($data)
            {
                return $this->getversion($data);
            })
            ->editColumn('edit', function ($data)
            {
                return '<a href="/document/'.$data->id.'/edit"><i class = "icon edit"></i></a>';
            })
            ->editColumn('delete', function ($data)
            {
                return '<a onclick="return confirm(\'Are you sure to delete this document?\')" href="/document/'.$data->id.'/delete"><i class = "icon delete"></i></a>';
            })
//            ->editColumn('expiration_date', function ($data)
//            {
//                return $this->(a);
//            })
            ->filter(function ($instance) use ($request)
            {


                if ($request->has('progress'))
                {

                    $instance->collection = $instance->collection->filter(function ($row) use ($request)
                    {
                        return $row['progress'] == $request->get('progress') ? true : false;
                    });
                }
                if ($request->has('priority'))
                {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request)
                    {
                        return $row['priority'] == $request->get('priority') ? true : false;
                    });
                }
                if ($request->has('version'))
                {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request)
                    {
                        return $row['version'] == $request->get('version') ? true : false;
                    });
                }
//                if ($request->has('expiration_date'))
//                {
//                    $instance->Item = $instance->Item->filter(function ($row) use ($request)
//                    {
//                        return $row['version'] == $request->get('version') ? true : false;
//                    });
//                }



            })
            ->make(true);
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */

    private function getProgress($data)
    {
        switch ($data->progress)
        {
           case 1:
                return trans('pmis.start');
            case 2:
                return trans('pmis.under_progress');
            case 3:
                return trans('pmis.compleated');
        }
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function getpriority($data)
    {
        switch ($data->priority)
        {
            case 1:
                return trans('dms.Documents_action');
            case 2:
                return trans('dms.Public_petitions');
            case 3:
                return trans('dms.Instructions_mayor');
            case 4:
                return trans('dms.Supreme_meetings');
        }
    }


    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function getversion($data)
    {
        switch ($data->version)
        {
            case 1:
                return trans('dms.First');
            case 2:
                return trans('dms.Second');
            case 3:
                return trans('dms.Third');
            
        }
        
    }

    /**
     * @param $data
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */



    private function getTransformedProjects()
    {
        return fractal()
            ->collection($this->getDataForExcelExport())
            ->transformWith(new DocumentTransformer())
            ->toArray();
    }



    public function generatExcelReport()
    {
        Excel::create('KM-Project-report', function ($excel)
        {
            // here we are making all rows align center
            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            // here we are adding border to all rows
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
            $excel->getDefaultStyle()->applyFromArray($styleArray);
            // here we are making sheet

            $excel->sheet('KM-Project-report', function ($sheet)
            {
                //Array that replace non readable database columns with a readable columns name for excel sheet
                $data = $this->getLocalizedExcelSheetTitles();

                $sheet->setHeight(1, 35);


                //generating the sheet title from array
                $sheet->fromArray(array($data), null, 'A1', false, false);
                //generating excel sheet by help of Transformer class of Projec
                $sheet->cell('A1:O1', function ($cell)
                {
                    // manipulate the cell
                    $cell->setBackground('#C00000');
                    // Set with font color
                    $cell->setFontColor('#ffffff');
                    // Set font family
                    $cell->setFontFamily('Calibri');
                    // Set font size
                    $cell->setFontSize(16);
                    // Set font weight to bold
                    $cell->setFontWeight('bold');
                    //Set alignment to center
                    $cell->setAlignment('center');
                    //Set vertical alignment to middle
                    $cell->setValignment('center');
                    // Set all borders (top, right, bottom, left)
                    $cell->setBorder('solid', 'solid', 'solid', 'solid');


                });
                $transformedData = $this->getTransformedProjects();

                $sheet->fromArray($transformedData["data"], null, 'A1', false, false);
            });

        })->download('xls');
    }

    public function setDataForExcelExport($data)
    {
        $this->excelData = $data;
    }


    private function getLocalizedExcelSheetTitles()
    {
        if (App::isLocale('en'))
        {
            return [
                trans('id')
                ,trans('Title'), trans('Source'), trans('destination'),
                trans('Start Date'),trans('progress'), trans('priority'),
                trans('version'),trans('case')
            ];
        } else
        {
            return [
                trans('pmis.id'),
                trans('dms.Title'),trans('dms.Source'), trans('dms.Destination'),
                 trans('pmis.Start_Date'), trans('dms.progress'),
                  trans('dms.Priority'),trans('dms.Version'),trans('dms.Case')
            ];
        }
    }
    public function getDataForExcelExport()
    {
        return $this->excelData;
    }


    public function getCustomizedDataBaseOnUrlParameters($progress, $priority, $version,  $expiration_date)
    {
        $Varfields = [
            'progress'       => $progress,
            'priority' => $priority,
            'version'=> $version,
            'expiration_date'         => $expiration_date,
        ];

        $result = new self;

          foreach ($Varfields as $attr => $value)
        {
            if ( ! empty($value) && $value != 'unknown')
            {
                $result = $result->where($attr, "=", $value);
            }
        }

         $this->excelData = $result->get();

        return $this->generatExcelReport();
    }

}
