<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfractionUser extends Model
{
    protected $table = 'infraction_user';
    protected $fillable = [
        'infraction_id', 'user_id'
    ];

    public function infraction()
    {
        return $this->belongsTo('App\Infraction','infraction_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
