<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvioceFile extends Model
{
    protected  $table = 'actual_invoice_file';

    public  function getStatus()
    {
        return $this->belongsTo('App\ProjectStatus');
    }

}
