<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use Morilog\Jalali\jDate;
use App\Jobs\StatusSendEmailJob;
class StatusSendEmailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'status:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending email to project manager for updating project status.';
    private $project_status = null;
    private $today = null;
    private $seven_days_ago = null;
    private $three_days_ago = null;
    private $today_hijri = null;
    private $seven_days_ago_hijri = null;
    private $three_days_ago_hijri = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->today = Carbon::today()->toDateString();
        $this->seven_days_ago = Carbon::today()->addDays(7)->toDateString();
        $this->three_days_ago = Carbon::today()->addDays(3)->toDateString();
        $this->today_hijri = jDate::forge()->format('date');
        $this->seven_days_ago_hijri = jDate::forge()->reforge('+ 7 days')->format('date');
        $this->three_days_ago_hijri = jDate::forge()->reforge('+ 3 days')->format('date');
        $this->project_status = DB::table('projects')
            ->where('days_remain', $this->today)
            ->where('procurement_due_date', null)

            ->orWhere(function($query) {$query ->where('days_remain', $this->seven_days_ago) ->where('procurement_due_date', null); })
            ->orWhere(function($query) {$query ->where('days_remain', $this->three_days_ago) ->where('procurement_due_date', null); })

            ->orWhere(function($query) {$query ->where('days_remain', $this->today_hijri) ->where('procurement_due_date', null); })
            ->orWhere(function($query) {$query ->where('days_remain', $this->seven_days_ago_hijri) ->where('procurement_due_date', null); })
            ->orWhere(function($query) {$query ->where('days_remain', $this->three_days_ago_hijri) ->where('procurement_due_date', null); })

            ->orWhere(function($query) {$query ->where('procurement_due_date', $this->today) ->where('procurement_type', '!=', '4'); })
            ->orWhere(function($query) {$query ->where('procurement_due_date', $this->seven_days_ago) ->where('procurement_type', '!=', '4'); })
            ->orWhere(function($query) {$query ->where('procurement_due_date', $this->three_days_ago) ->where('procurement_type', '!=', '4'); })

            ->orWhere(function($query) {$query ->where('procurement_due_date', $this->today_hijri) ->where('procurement_type', '!=', '4'); })
            ->orWhere(function($query) {$query ->where('procurement_due_date', $this->seven_days_ago_hijri) ->where('procurement_type', '!=', '4'); })
            ->orWhere(function($query) {$query ->where('procurement_due_date', $this->three_days_ago_hijri) ->where('procurement_type', '!=', '4'); })

            ->join('project_user', 'project_user.project_id', '=', 'projects.id')
            ->join('users', 'users.id', '=', 'project_user.user_id')
            ->select('users.first_name', 'users.last_name', 'users.email', 'projects.id as project_id', 'projects.name as project_name', 'projects.days_remain', 'projects.procurement_due_date')
            ->get();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->project_status->each(function($data)
        {
            dispatch(new StatusSendEmailJob($data));
        });
    }
}
