<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use App\Jobs\MonthlySendEmailJob;
class MonthlySendEmailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monthly:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending email to project manager for filling actual amount of current month.';
    private $project_status = null;
    private $current_month = null;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->current_month = Carbon::today()->month;
        $this->project_status = DB::table('project_status')
            ->where('month_number', $this->current_month)
            ->where('actual_amount', '')
            ->join('project_user', 'project_user.project_id', '=', 'project_status.project_id')
            ->join('users', 'users.id', '=', 'project_user.user_id')
            ->join('projects', 'projects.id', '=', 'project_user.project_id')
            ->select('users.first_name', 'users.last_name', 'users.email', 'projects.id as project_id', 'projects.name as project_name')
            ->get();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->project_status->each(function($data)
        {
            dispatch(new MonthlySendEmailJob($data));
        });
    }
}
