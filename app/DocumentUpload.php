<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentUpload extends Model
{
    public function document()
    {
        return $this->belongsTo('App\document', 'document_id');
    }

}
