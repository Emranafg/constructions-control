<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupervisorProject extends Model
{
    protected $table = "supervisor_project";
    protected $fillable = ['supervisor_id', 'project_id'];
}
