<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
class User extends Authenticatable
{

    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'user_district'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
    * The projects that belong to the user.
    */
    public function projects()
    {
        return $this->belongsToMany('App\Project', 'project_user', 'user_id', 'project_id');
    }

    /*
    * The violations that belong to the user.
    */
    public function infractions()
    {
        return $this->belongsToMany('App\Infraction', 'infraction_user', 'user_id', 'infraction_id');
    }
}
