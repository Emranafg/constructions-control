<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPartialList extends Model
{
    protected $table = "project_partial_list";
    protected $fillable = ['name', 'type'];
}
