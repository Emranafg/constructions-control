-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 19, 2022 at 10:09 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cc.km`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_10_01_113408_create_projects_table', 1),
(4, '2016_10_02_113341_create_daliy_report_table', 1),
(5, '2016_10_03_123442_create_uploads_table', 1),
(6, '2016_10_03_123638_create_supervision_table', 1),
(7, '2016_10_03_123651_create_work_activity_table', 1),
(8, '2016_10_03_123735_create_safety_measures_table', 1),
(9, '2016_10_03_123750_create_bulding_material_testing_table', 1),
(10, '2016_10_03_123831_create_equipment_table', 1),
(11, '2016_10_03_123845_create_building_equipment_table', 1),
(12, '2016_10_05_053807_create_work_defacts_table', 1),
(13, '2016_10_05_054049_create_project_workers_table', 1),
(14, '2016_10_11_101538_crate_work_activity_uploads_table', 1),
(15, '2016_10_11_101552_crate_work_defacts_uploads_table', 1),
(16, '2016_10_18_052214_create_deficiency_table', 1),
(17, '2016_10_19_053230_create_deficiency_upload_table', 1),
(18, '2016_10_28_125409_create_infractions_table', 1),
(19, '2016_11_13_064354_create_project_partial_list_table', 1),
(20, '2016_11_13_170957_create_district_table', 1),
(21, '2016_11_14_152039_create_documents_table', 1),
(22, '2016_11_27_151329_create_violation_upload_table', 1),
(23, '2016_11_28_111508_create_case_table', 1),
(24, '2016_12_05_072342_create_document_uploads', 1),
(25, '2016_12_09_064133_create_supervisors_table', 1),
(26, '2016_12_13_170400_create_project_user_table', 1),
(27, '2016_12_15_102151_create_infractiont_user_table', 1),
(28, '2016_12_15_132709_create_permission_tables', 1),
(29, '2016_12_17_150843_create_material_test_uplaods_table', 1),
(30, '2017_02_18_032533_violation_type', 2),
(31, '2017_02_18_032609_violation_type_infraction', 2),
(32, '2017_02_28_035200_create_violation_document', 2),
(33, '2017_02_28_101846_create_violation_document_file', 2),
(34, '2017_09_27_112445_create_project_status_table', 1),
(35, '2017_10_06_190128_create_jobs_table', 1),
(36, '2017_10_06_190152_create_failed_jobs_table', 1),
(37, '2017_10_08_195008_create_actual_amount_invioce_file_table', 1),
(38, '2022_07_19_122358_add_district_to_users_table', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
