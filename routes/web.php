<?php

use App\Http\Controllers\BuildingsController;
use App\Http\Controllers\ParkingsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/ano', function ()
{
    return Hash::make('Reshad');
});

// Route::resource('Buildings', BuildingsController::class);
// Route::resource('Parkings', ParkingsController::class);
    //  Route::get('/Buildings', 'BuildingsController@index');
    //  Route::get('/building/create', 'BuildingsController@create');


    Route::get('/building/export/all/toExcel', 'BuildingsController@getExcel');
    Route::group(['middleware' => ['permission:building_create']], function ()
    {
        Route::get('/Buildings/create', 'BuildingsController@create');
    
        Route::post('/Buildings/store', 'BuildingsController@store');
        
    });
    Route::group(['middleware' => ['permission:parking_create']], function ()
    {
            
    Route::get('/Parkings/create', 'ParkingsController@create');
    Route::post('/Parkings/store', 'ParkingsController@store');
    });

    Route::group(['middleware' => ['permission:building_view']], function ()
    {
        
        Route::get('/Buildings', 'BuildingsController@index');
    });
Route::group(['middleware' => ['permission:building_edit']], function ()
{
    
    Route::get('/Buildings/{id}/edit', 'BuildingsController@edit');
});
Route::group(['middleware' => ['permission:parking_view']], function ()
{
   
    Route::get('/Parkings', 'ParkingsController@index');
});
Route::group(['middleware' => ['permission:parking_edit']], function ()
{
    
    Route::get('/Parkings/{id}/edit', 'ParkingsController@edit');

});
Route::group(['middleware' => ['permission:parking_delete']], function ()
{

    Route::get('/Parkings/delete/{id}', 'ParkingsController@destroy');
});


Route::group(['middleware' => ['auth']], function ()
{
    Route::get('/', 'DashboardController@index');
    Route::get('/locale/{lang}', 'LanguageController@index');

    // projects route
    Route::group(['middleware' => ['permission:view_project']], function ()
    {
        Route::get('/projects', 'ProjectController@index');
        Route::get('/searchprojects', 'ProjectController@searchProject');
        Route::get('/project/details/{id}', 'ProjectController@details');
        Route::get('/project/details/{id}/status/{status}', 'ProjectController@changeStatus')->middleware('assignedProject');
        Route::post('/project/details/{id}/status/{status}', 'ProjectController@storeStatus')->middleware('assignedProject');
        Route::get('/project/dropdown/{project_id}','ProjectController@dropdown');
        Route::post('/project/assign/user', 'ProjectController@assign');

        // upload actual amount invoice files
        Route::post('/actual/amount/invoicefile/upload','ProjectController@uploadInvoiceFile');
        Route::post('/actual/amount/invoicefile/upload/remove','ProjectController@removeInvoiceFile');

        // get the actual amount of the project with month number
        Route::post('/actual/amount','ProjectController@getActualAmountOfMonth');
        Route::post('/planned/amount','ProjectController@getPlannedAmountOfMonth');

        // update any month planned amount
        Route::post('/planned/amount/update','ProjectController@updatePlannedAmount');

        // project Budget Report
        Route::get('/export/project/budget/report', 'ProjectController@getProjectBudgetExcelFile');

        // this route is for ajax request  << script line 405
        Route::post('/project/details/{id}/status','ProjectController@updateStatus');

        // this route is for ajax request to save every month actual amount
        Route::post('/project/actualamount/update','ProjectController@updateActualAmount');


        Route::get('/projectdailyreports/project/{id}', 'ProjectController@dailyReport')->middleware('assignedProject');
        //not compleated yet
        Route::get('/prjects/dailyReports/{id}', 'ProjectController@getDailyReports');
        Route::get('/prjects/monthlyReports/{id}', 'ProjectController@getMpnthlyReports');
        Route::get('/prjects/dailyReports/projects/{proId}/details/{id}', 'ProjectController@reportDetails');
        Route::get('/prject/{p_id}/report/{r_id}/print', 'ProjectController@print');

        // can create project daily report
        Route::get('/project/{id}/report/create', 'ProjectController@createReport')->middleware('assignedProject');
        Route::post('/project/report/store', 'ProjectController@storeReport');

        Route::group(['middleware' => ['permission:view_project'||'role:admin']], function ()
        {
            //update report
            Route::post('/project/report/update', 'ProjectController@updateReport');
            Route::get('/project/{p_id}/report/{r_id}/edit', 'ProjectController@editReport');
        });

        Route::group(['middleware' => ['permission:delete_report'||'role:admin']], function ()
        {
            // delete daily report
            Route::get('/daily/report/delete/{id}', 'ProjectController@deleteDailyReport');
        });


    });

    Route::group(['middleware' => ['permission:create_report']], function ()
    {
        Route::get('/excel/reports', 'ProjectController@getReports');
        Route::get('/report/excel/export', 'ProjectController@getExcelFile');
        Route::get('/export/project/daily/report', 'ProjectController@getExcelFileForDailyReport');

        // generate excel report for project status
        Route::get('/export/project/status/report', 'ProjectController@getExcelFileForProjectStatus');

        Route::get('/report/excel/status/{status}/progress/{progress}/Popretor/{Poperetor}/value/{value}/dateType/{dateType}/Dateoperator/{dateOperator}/date/{date}', 'ProjectController@getCustomizeDataForDataTable');
        Route::get('/report/export/excel/status/{status}/progress/{progress}/Popretor/{Poperetor}/value/{value}/dateType/{dateType}/Dateoperator/{dateOperator}/date/{date}', 'ProjectController@getCustomizeDataForExcel');
    });

    Route::group(['middleware' => ['permission:assign_project']], function ()
    {
        // asign project to a user
        Route::post('/project/assign', 'ProjectController@assign');
        Route::get('/project/supervisor/autocomplete', 'ProjectController@supervisorAutocomplete');
    });

    Route::group(['middleware' => ['permission:create_project']], function ()
    {
        Route::get('/project/create', 'ProjectController@createProject');
        Route::post("/project/store", 'ProjectController@storeProject');
    });

    Route::group(['middleware' => ['permission:edit_project']], function ()
    {
        // has conflict but should be reviw this two route
        Route::get('/project/edit/{id}', 'ProjectController@editProject')->middleware('assignedProject');
        Route::post('/project/edit/update', 'ProjectController@update');

        Route::get('/project/{id}/edit', 'ProjectController@editProject')->middleware('assignedProject');
        Route::post('/project/update', 'ProjectController@updateProject');
    });

    Route::group(['middleware' => ['permission:delete_project']], function ()
    {
        Route::get('/projects/delete/{id}', 'ProjectController@deletProject')->middleware('assignedProject');
    });

    Route::post('/project/list/select', 'ProjectController@listProject');
    Route::get('/project/partial/list/{type}', 'ProjectController@partialList');
    Route::post('/project/partial/list/create', 'ProjectController@partialListCreate');

// ====================================================================================================================================================

    //Infraction routes start here
    Route::group(['middleware' => ['permission:view_violation']], function ()
    {
        Route::get('/km/infractions', 'InfractionController@index');
        // return google map
        Route::get('/km/infractions/map/{id?}','InfractionController@map');
        // violation document upload
        Route::post('/violation/details/document/upload','InfractionController@uploadDocuments');
        // violation document edit
        Route::post('/violation/details/document/edit','InfractionController@editDocuments');

        Route::get('/infraction/collection/custom-filter-data', 'InfractionController@getCustomFilterData');
        Route::get('/violation/details/violationId/{id}', 'InfractionController@viewViolationDetails');
        // For violation document delete
        Route::get('/infraction/document/delete/{id}', 'InfractionController@violationDocumentDelete');
        Route::get('/infraction/document/edit/{id}','InfractionController@violationDocumentEdit');

        // Document Delete
        Route::get('/violation/document/delete/{id}','InfractionController@DocumentDelete');






    });

    //create infraction 
    Route::group(['middleware' => ['permission:create_violation']], function ()
    {
        Route::get('/infraction/create', 'InfractionController@create');
        Route::post("/infraction/store", 'InfractionController@store');
    });
    //create building
    Route::group(['middleware' => ['permission:create_violation']], function ()
    {
        // Route::get('/building/create', 'BuildingController@create');
        Route::post("/infraction/store", 'InfractionController@store');
    });
    // delete violation
    Route::group(['middleware' => ['permission:delete_violation']], function ()
    {
        Route::get('/infraction/delete/{id}', 'InfractionController@deleteInfraction')->middleware('assignedInfraction');
    });
    //edite infraction
    Route::group(['middleware' => ['permission:edit_violation']], function ()
    {
        Route::get('/infraction/{id}/edit', 'InfractionController@edit')->middleware('assignedInfraction');
        Route::post('/infraction/update', 'InfractionController@update');

    });


    //report
    Route::group(['middleware' => ['permission:create_report']], function ()
    {
        Route::get('/km/violation/export/all/toExcel', 'InfractionController@getExcel');
        Route::get('/infraction/violation/excel/district/{district}/certification/{certification}/regionStatus/{regionStatus}/activityType/{activityType}/cunstractionSt/{cunstractionSt}/infractionsSt/{infractionsSt}/proceedings/{proceedings}/violationType/{violationType}', 'InfractionController@generateCustomizedExcelSheet');
    });

    //assign report
    Route::group(['middleware' => ['permission:assign_violation']], function ()
    {
        Route::post('/infraction/assign', 'InfractionController@assign');
     });
//    // Route::resource('Buildings', BuildingsController::class);
//     Route::get('/buildings', 'BuildingsController@index');
//     Route::get('/building/create', 'BuildingsController@create');


// ====================================================================================================================================================

    // document routes start here
    Route::group(['middleware' => ['permission:view_document']], function ()
    {
        Route::get('/documents', 'DocumentController@index');
        Route::get('/search/document', 'DocumentController@searchDocument');
        Route::get('/document/details/{id}', 'DocumentController@details');


        Route::get('/document/case/{id}/edit', 'DocumentController@editCase');
        Route::post('/document/case/update', 'DocumentController@updateCase');
        Route::get('/document/case/{id}/delete', 'DocumentController@deleteCase');

        Route::get('/document/{id}/case/create', 'DocumentController@createCase');
        Route::post('/document/case/store', 'DocumentController@storeCase');
    });

    Route::group(['middleware' => ['permission:edit_document']], function (){
        Route::get('/document/{id}/edit', 'DocumentController@editDocument');
        Route::post('/document/update', 'DocumentController@updateDocument');
    });

    Route::group(['middleware' => ['permission:create_document']], function (){
        Route::get('/document/create', 'DocumentController@createDocument');
        Route::post('/document/store', 'DocumentController@documentStore');
    });

    Route::group(['middleware' => ['permission:delete_document']], function (){
        Route::get('/document/{id}/delete', 'DocumentController@deleteDocument');
    });

    Route::group(['middleware' => ['permission:create_document']], function (){
        Route::get('/document/excel/export', 'DocumentController@getExcel');
        Route::get('/excel', 'DocumentController@getExcelFile');
        Route::get('/report/export/excel/progress/{progress}/priority/{priority}/version/{version}/expiration_date/{expiration_date}', 'DocumentController@getCustomizeDataForExcel');
    });

    // user management routes here
    Route::group(['middleware' => ['permission:create_user']], function ()
    {
        Route::get('/users', 'UserController@index');
        Route::get('/user/{id}/show', 'UserController@show');
        Route::get('/user/create', 'UserController@create');
        Route::post('/user/store', 'UserController@store');
        Route::get('/user/{id}/edit', 'UserController@edit');
        Route::post('/user/update', 'UserController@update');
        Route::get('/user/{id}/delete', 'UserController@destroy');
        Route::get('/user/{id}/permission', 'UserController@permission');

    });

    Route::get('/user/profile', 'UserController@profile');
    Route::post('/user/profile', 'UserController@profileUpdate');
    Route::post('/user/profile/reset/password', 'UserController@resetPassword');
    Route::get('/prject/{p_id}/report/{r_id}/print', 'ProjectController@printAsPdf');
    Route::post('/image/remove', 'ProjectController@remove');

    //uploading image to the report
    Route::post('/project/report/image/upload', 'ProjectController@upload');
    Route::post('/document/upload', 'DocumentController@upload');
    Route::post('/infraction/file/remove', 'InfractionController@removeImage')->name('infraction.img.remove');
    Route::post('/violation/document/upload', 'InfractionController@uploadFile');
    Route::post('/violation/document/remove', 'InfractionController@removeFile');
});

Route::get('public/reports/projects', 'PublicReportViewController@index');
Route::get('public/reports/violations', 'PublicReportViewController@showViolationGraph');
Route::get('public/reports/debatable', 'PublicReportViewController@getMultiFilterSelectData');

Auth::routes();
Route::any('/register',function(){
    return redirect('/login');
});
Route::any('/home', function ()
{
    return redirect('/');
});
Route::any('/testing', function ()
{
    return view("create");
});
