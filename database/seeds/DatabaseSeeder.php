<?php

use App\BuildingEquipment;
use App\BuildingMaterialTest;
use App\Deficiency;
use App\DeficiencyUpload;
use App\Equipment;
use App\Infraction;
use App\Project;
use App\ProjectWorker;
use App\Report;
use App\SafetyMeasur;
use App\Supervision;
use App\Supervisor;
use App\Upload;
use App\User;
use App\ViolationUpload;
use App\WorkActivity;
use App\WorkActivityUpload;
use App\WorkDefact;
use App\WorkDefactsUpload;
use App\District;
use App\SupervisorProject;
use Illuminate\Database\Seeder;
use App\Document;
use App\DocumentUpload;
use App\DocumentCase;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\MaterialTestUplaod;
use App\ViolationTypeInfraction;


class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         if (User::count() == 0) $this->call('UserSeeder');
         if (Project::count() == 0) $this->call('ProjectSeeder');
         if (Report::count() == 0) $this->call('ReportSeeder');
        if (Upload::count() == 0) $this->call('UploadSeeder');
         if (Supervision::count() == 0) $this->call('SupervisionSeeder');
         if (WorkActivity::count() == 0) $this->call('WorkActivitySeeder');
         if (SafetyMeasur::count() == 0) $this->call('SafetyMeasurSeeder');
         if (BuildingMaterialTest::count() == 0) $this->call('BuildingMaterialTestSeeder');
         if (Equipment::count() == 0) $this->call('EquipmentSeeder');
         if (BuildingEquipment::count() == 0) $this->call('BuildingEquipmentSeeder');
         if (WorkDefact::count() == 0) $this->call('WorkDefactSeeder');
         if (ProjectWorker::count() == 0) $this->call('ProjectWorkerSeeder');
         if (WorkActivityUpload::count() == 0) $this->call('WorkActivityUploadSeeder');
         if (WorkDefactsUpload::count() == 0) $this->call('WorkDefactUploadSeeder');
         if (Deficiency::count() == 0) $this->call('DeficencySeeder');
         if (DeficiencyUpload::count() == 0) $this->call('WorkDifictioncyUploadSeeder');
         if (Infraction::count() == 0) $this->call('InfractionSeeder');
         if (ViolationUpload::count() == 0) $this->call('InfractionUploadSeeder');
         if (District::count() == 0) $this->call('DistrictSeeder');
//        if (SupervisorProject::count() == 0) $this->call('SupervisorProjectSeeder');
         if (Document::count() == 0) $this->call('DocumentSeeder');
         if (DocumentUpload::count() == 0) $this->call('DocumentUploadsSeeder');
         if (DocumentCase::count() == 0) $this->call('CaseSeeder');
         if (Supervisor::count() == 0) $this->call('SupervisorSeeder');
         $this->call('ProjeUserPivot');
         $this->call('InfractionUserPivot');
         if (Role::count() == 0) $this->call('RoleSeeder');
         if (Permission::count() == 0) $this->call('PermissionSeeder');
         if (MaterialTestUplaod::count() == 0) $this->call('MaterialTestUplaodSeeder');
        if (ViolationTypeInfraction::count() == 0) $this->call('ViolationTypePivoteSeeder');


    }
}

class UserSeeder extends Seeder
{

    public function run()
    {
        factory('App\User', 100)->create();
    }

}

class ProjectSeeder extends Seeder
{

    public function run()
    {
        factory('App\Project', 100)->create();
    }

}

class ReportSeeder extends Seeder
{

    public function run()
    {
        factory('App\Report', 100)->create();
    }

}


class UploadSeeder extends Seeder
{

    public function run()
    {
        factory('App\Upload', 100)->create();
    }

}

class SupervisorSeeder extends Seeder
{

    public function run()
    {
        factory('App\Supervisor', 100)->create();
    }

}

class SupervisionSeeder extends Seeder
{

    public function run()
    {
        factory('App\Supervision', 100)->create();
    }

}

class WorkActivitySeeder extends Seeder
{

    public function run()
    {
        factory('App\WorkActivity', 100)->create();
    }

}

class SafetyMeasurSeeder extends Seeder
{

    public function run()
    {
        factory('App\SafetyMeasur', 100)->create();
    }

}

class BuildingMaterialTestSeeder extends Seeder
{

    public function run()
    {
        factory('App\BuildingMaterialTest', 100)->create();
    }

}

class EquipmentSeeder extends Seeder
{

    public function run()
    {
        factory('App\Equipment', 100)->create();
    }

}

class BuildingEquipmentSeeder extends Seeder
{

    public function run()
    {
        factory('App\BuildingEquipment', 100)->create();
    }

}

class WorkDefactSeeder extends Seeder
{

    public function run()
    {
        factory('App\WorkDefact', 100)->create();
    }

}

class ProjectWorkerSeeder extends Seeder
{

    public function run()
    {
        factory('App\ProjectWorker', 100)->create();
    }

}

class DeficencySeeder extends Seeder
{

    public function run()
    {
        factory('App\Deficiency', 100)->create();
    }

}


class WorkActivityUploadSeeder extends Seeder
{

    public function run()
    {
        factory('App\WorkActivityUpload', 100)->create();
    }

}

class WorkDefactUploadSeeder extends Seeder
{

    public function run()
    {
        factory('App\WorkDefactsUpload', 100)->create();
    }

}

class WorkDifictioncyUploadSeeder extends Seeder
{

    public function run()
    {
        factory('App\DeficiencyUpload', 100)->create();
    }

}

class InfractionSeeder extends Seeder
{

    public function run()
    {
        factory('App\Infraction', 100)->create();
    }

}

class InfractionUploadSeeder extends Seeder
{

    public function run()
    {
        factory('App\ViolationUpload', 100)->create();
    }
}

class SupervisorProjectSeeder extends Seeder
{

    public function run()
    {
        factory('App\SupervisorProject', 100)->create();
    }

}

class DistrictSeeder extends Seeder
{

    public function run()
    {
        District::create(['fa_name' => 'ناحیه اول', 'pa_name' => 'ناحیه اول', 'en_name' => 'First District']);
        District::create(['fa_name' => 'ناحیه دوم', 'pa_name' => 'ناحیه دوم', 'en_name' => 'Second District']);
        District::create(['fa_name' => 'ناحیه سوم', 'pa_name' => 'ناحیه سوم', 'en_name' => 'Third District']);
        District::create(['fa_name' => 'ناحیه چهارم', 'pa_name' => 'ناحیه چهارم', 'en_name' => 'Fourth District']);
        District::create(['fa_name' => 'ناحیه پنجم', 'pa_name' => 'ناحیه پنجم', 'en_name' => 'Fifth District']);
        District::create(['fa_name' => 'ناحیه ششم', 'pa_name' => 'ناحیه ششم', 'en_name' => 'Sixth District']);
        District::create(['fa_name' => 'ناحیه هفتم', 'pa_name' => 'ناحیه هفتم', 'en_name' => 'Seventh District']);
        District::create(['fa_name' => 'ناحیه هشتم', 'pa_name' => 'ناحیه هشتم', 'en_name' => 'Eighth District']);
        District::create(['fa_name' => 'ناحیه نهم', 'pa_name' => 'ناحیه نهم', 'en_name' => 'Ninth District']);
        District::create(['fa_name' => 'ناحیه دهم', 'pa_name' => 'ناحیه دهم', 'en_name' => 'Tenth District']);
        District::create(['fa_name' => 'ناحیه یازدهم', 'pa_name' => 'ناحیه یازدهم', 'en_name' => 'Eleventh District']);
        District::create(['fa_name' => 'ناحیه دوازدهم', 'pa_name' => 'ناحیه دوازدهم', 'en_name' => 'Twelfth District']);
        District::create(['fa_name' => 'ناحیه سیزدهم', 'pa_name' => 'ناحیه سیزدهم', 'en_name' => 'Thirteenth District']);
        District::create(['fa_name' => 'ناحیه چهاردهم', 'pa_name' => 'ناحیه چهاردهم', 'en_name' => 'Fourteenth District']);
        District::create(['fa_name' => 'ناحیه پاتزدهم', 'pa_name' => 'ناحیه پاتزدهم', 'en_name' => 'Fifteenth District']);
        District::create(['fa_name' => 'ناحیه شانزدهم', 'pa_name' => 'ناحیه شانزدهم', 'en_name' => 'Sixteenth District']);
        District::create(['fa_name' => 'ناحیه هفدهم', 'pa_name' => 'ناحیه هفدهم', 'en_name' => 'Seventeenth District']);
        District::create(['fa_name' => 'ناحیه هشدهم', 'pa_name' => 'ناحیه هشدهم', 'en_name' => 'Eighteenth District']);
        District::create(['fa_name' => 'ناحیه نزدهم', 'pa_name' => 'ناحیه نزدهم', 'en_name' => 'Nineteenth District']);
        District::create(['fa_name' => 'ناحیه بیستم', 'pa_name' => 'ناحیه بیستم', 'en_name' => 'Twentieth District']);
        District::create(['fa_name' => 'ناحیه بیست و یکم', 'pa_name' => 'ناحیه بیست و یکم', 'en_name' => 'Twenty First District']);
        District::create(['fa_name' => 'ناحیه بیست و دوم', 'pa_name' => 'ناحیه بیست و دوم', 'en_name' => 'Twenty Second District']);


    }

}
class DocumentSeeder extends Seeder
{

    public function run()
    {
        factory('App\Document', 100)->create();
    }

}
class DocumentUploadsSeeder extends Seeder
{

    public function run()
    {
        factory('App\DocumentUpload', 100)->create();
    }

}
class CaseSeeder extends Seeder
{

    public function run()
    {
        factory('App\DocumentCase', 100)->create();
    }
}

class ProjeUserPivot extends Seeder
{

    public function run()
    {

        for ($i = 0; $i < 100; $i ++)
        {
            if ($i == 0)
            {
                continue;
            }
            User::find(rand(1,100))->projects()->attach(rand(1,100));
        }
    }
}

class InfractionUserPivot extends Seeder
{

    public function run()
    {

        for ($i = 0; $i < 100; $i ++)
        {
            if ($i == 0)
            {
                continue;
            }
            User::find(rand(1,100))->infractions()->attach(rand(1,100));
        }
    }
}

class RoleSeeder extends Seeder
{

    public function run()
    {
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'user']);
        Role::create(['name' => 'supervisor']);
    }
}
class PermissionSeeder extends Seeder
{

    public function run()
    {
        Permission::create(['name' => 'create_project']);
        Permission::create(['name' => 'view_project']);
        Permission::create(['name' => 'edit_project']);
        Permission::create(['name' => 'delete_project']);
        Permission::create(['name' => 'assign_project']);
        Permission::create(['name' => 'create_report']);
        Permission::create(['name' => 'view_report']);
        Permission::create(['name' => 'edit_report']);
        Permission::create(['name' => 'delete_report']);
        Permission::create(['name' => 'create_violation']);
        Permission::create(['name' => 'view_violation']);
        Permission::create(['name' => 'edit_violation']);
        Permission::create(['name' => 'delete_violation']);
        Permission::create(['name' => 'assign_violation']);
        Permission::create(['name' => 'create_document']);
        Permission::create(['name' => 'view_document']);
        Permission::create(['name' => 'edit_document']);
        Permission::create(['name' => 'delete_document']);
        Permission::create(['name' => 'create_user']);
        Permission::create(['name' => 'view_user']);
        Permission::create(['name' => 'edit_user']);
        Permission::create(['name' => 'delete_user']);

    }
}

class MaterialTestUplaodSeeder extends Seeder
{

    public function run()
    {
        factory('App\MaterialTestUplaod', 100)->create();
    }

}
class ViolationTypePivoteSeeder extends Seeder
{

    public function run()
    {
        $infractions = Infraction::all();

        foreach ($infractions as $infraction)
        {

            ViolationTypeInfraction::create(['vt_id'=>$infraction->violation_type,'in_id'=>$infraction->id]);

        }


    }

}

