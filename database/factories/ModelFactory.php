<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker)
{
    static $password;

    return [
        'first_name'     => $faker->name,
        'last_name'      => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'phone'          => $faker->phoneNumber,
        'password'       => $password ? : $password = bcrypt('secret'),
        'profile'        => 'standard/assets/images/default-user.png',
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Project::class, function (Faker\Generator $faker)
{

    return [
        'name'              => $faker->name,
        'contractor'        => $faker->name,
        'contract'          => $faker->name,
        'owner'             => $faker->name,
        'area'              => $faker->address,
        'passway'           => $faker->address,
        'district'          => "First District",
        'status'            => $faker->boolean(70),
        'status_value'      => $faker->boolean(70) ? $faker->numberBetween($min = 1, $max = 100) : $faker->numberBetween($min = 1, $max = 100),
        'reasson_why_stop'  => $faker->sentence($nbWords = 15, $variableNbWords = true),
        'latitude'          => $faker->latitude,
        'gratitude'         => $faker->latitude,
        'contract_date'     => $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now'),
        'start_date'        => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
        'actual_date'       => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
        'planned_date'      => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
        'compilation_date'  => $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now'),
        'project_budget'    => $faker->numberBetween($min = 2000000, $max = 10000000),
        'addational_budget' => $faker->numberBetween($min = 2000000, $max = 10000000),
        'subcont_phone'     => $faker->phoneNumber,
        'subcont_email'     => $faker->email,

    ];
});

$factory->define(App\Report::class, function (Faker\Generator $faker)
{

    return [
        'project_id'    => $faker->numberBetween($min = 1, $max = 100),
        'supervisor_id' => $faker->numberBetween($min = 1, $max = 100),
        'status'        => $faker->boolean(70),
        'status_value'  => $faker->boolean(70) ? $faker->numberBetween($min = 1, $max = 100) : $faker->sentence(15),
        'district'      => $faker->address,
        'gozar'         => $faker->streetAddress,
        'date'          => $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now'),
        'time'          => $faker->time($format = 'H:i:s', $max = 'now'),
        'weather'       => $faker->word,
        'temprature'    => $faker->numberBetween($min = 0, $max = 55),
    ];
});


$factory->define(App\Upload::class, function (Faker\Generator $faker)
{

    return [
        'project_id' => $faker->numberBetween($min = 1, $max = 100),
        'name'       => $faker->name,
        'path'       => 'root',
    ];
});

$factory->define(App\Supervisor::class, function (Faker\Generator $faker)
{

    return [
        'project_id'    => $faker->numberBetween($min = 1, $max = 100),
        'infraction_id' => $faker->numberBetween($min = 1, $max = 100),
        'name'          => $faker->name,
        'email'         => $faker->email,
        'phone'         => $faker->phoneNumber,

    ];
});

$factory->define(App\Supervision::class, function (Faker\Generator $faker)
{

    return [
        'report_id'  => $faker->numberBetween($min = 1, $max = 100),
        'date'       => $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now'),
        'time'       => $faker->time($format = 'H:i:s', $max = 'now'),
        'weather'    => $faker->word,
        'temprature' => $faker->numberBetween($min = 1, $max = 60),

    ];
});

$factory->define(App\WorkActivity::class, function (Faker\Generator $faker)
{

    return [
        'report_id'   => $faker->numberBetween($min = 1, $max = 100),
        'type'        => $faker->word,
        'description' => $faker->sentence($nbWords = 15, $variableNbWords = true),
    ];
});

$factory->define(App\SafetyMeasur::class, function (Faker\Generator $faker)
{

    return [
        'report_id'                => $faker->numberBetween($min = 1, $max = 100),
        'eye_protection'           => $faker->boolean(70),
        'helmet'                   => $faker->boolean(70),
        'ear_protection'           => $faker->boolean(70),
        'respiratory_protection'   => $faker->boolean(70),
        'respiratory_protection'   => $faker->boolean(70),
        'safety_boots'             => $faker->boolean(70),
        'safety_gloves'            => $faker->boolean(70),
        'safety_uniforms'          => $faker->boolean(70),
        'safe_face_on'             => $faker->boolean(70),
        'tape_barriers'            => $faker->boolean(70),
        'symptoms_of_obstacles'    => $faker->boolean(70),
        'warning_signs'            => $faker->boolean(70),
        'mandatory_signs'          => $faker->boolean(70),
        'first_aid_sign'           => $faker->boolean(70),
        'risks_of_chemicals'       => $faker->boolean(70),
        'noise_pollution'          => $faker->boolean(70),
        'asphalting_disadvantages' => $faker->boolean(70),
        'concrete_disadvantages'   => $faker->boolean(70),
    ];
});

$factory->define(App\BuildingMaterialTest::class, function (Faker\Generator $faker)
{

    return [
        'report_id'   => $faker->numberBetween($min = 1, $max = 100),
        'type'        => $faker->word,
        'result'      => $faker->word,
        'description' => $faker->sentence($nbWords = 15, $variableNbWords = true),
    ];
});

$factory->define(App\Equipment::class, function (Faker\Generator $faker)
{

    return [
        'report_id' => $faker->numberBetween($min = 1, $max = 100),
        'type'      => $faker->word,
        'quantity'  => $faker->numberBetween($min = 1, $max = 100),
        'comment'   => $faker->sentence($nbWords = 15, $variableNbWords = true),
    ];
});

$factory->define(App\BuildingEquipment::class, function (Faker\Generator $faker)
{

    return [
        'report_id'   => $faker->numberBetween($min = 1, $max = 100),
        'type'        => $faker->word,
        'estim_value' => $faker->numberBetween($min = 1, $max = 100),
        'quality'     => $faker->word,
        'comment'     => $faker->sentence($nbWords = 15, $variableNbWords = true),
    ];
});

$factory->define(App\WorkDefact::class, function (Faker\Generator $faker)
{

    return [
        'report_id'   => $faker->numberBetween($min = 1, $max = 100),
        'type'        => $faker->word,
        'description' => $faker->sentence($nbWords = 15, $variableNbWords = true),
    ];
});

$factory->define(App\ProjectWorker::class, function (Faker\Generator $faker)
{

    return [
        'report_id'        => $faker->numberBetween($min = 1, $max = 100),
        'projectmanager'   => $faker->numberBetween($min = 10, $max = 30),
        'control_engineer' => $faker->numberBetween($min = 10, $max = 30),
        'safety_engineer'  => $faker->numberBetween($min = 10, $max = 30),
        'worker_leader'    => $faker->numberBetween($min = 10, $max = 30),
        'smart_worker'     => $faker->numberBetween($min = 10, $max = 30),
        'ordinary_worker'  => $faker->numberBetween($min = 10, $max = 30),
        'gard'             => $faker->numberBetween($min = 10, $max = 30),
        'visitor'          => $faker->numberBetween($min = 10, $max = 30),
        'other'            => $faker->numberBetween($min = 10, $max = 30),
    ];
});

$factory->define(App\Deficiency::class, function (Faker\Generator $faker)
{

    return [
        'project_id'     => $faker->numberBetween($min = 1, $max = 100),
        'type'           => $faker->word,
        'description'    => $faker->sentence($nbWords = 15, $variableNbWords = true),
        'issued_date'    => $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now'),
        'item_number'    => $faker->isbn10,
        'date_corrected' => $faker->dateTimeBetween($startDate = 'now', $endDate = '4 years'),
        'corrected'      => $faker->boolean(50),
    ];
});

$factory->define(App\WorkActivityUpload::class, function (Faker\Generator $faker)
{

    return [
        'work_activitie_id' => $faker->numberBetween($min = 1, $max = 100),
        'name'              => $faker->word,
        'path'              => 'standard/assets/images/Construction-2.jpg',
    ];
});
$factory->define(App\WorkDefactsUpload::class, function (Faker\Generator $faker)
{

    return [
        'work_defact_id' => $faker->numberBetween($min = 1, $max = 100),
        'name'           => $faker->word,
        'path'           => 'standard/assets/images/construc.jpg',

    ];
});

$factory->define(App\DeficiencyUpload::class, function (Faker\Generator $faker)
{

    return [
        'deficiency_id' => $faker->numberBetween($min = 1, $max = 100),
        'name'          => $faker->word,
        'path'          => 'standard/assets/images/construc.jpg',

    ];
});

$factory->define(App\Infraction::class, function (Faker\Generator $faker)
{

    return [
        'serial_number'           => $faker->isbn10,
        'name'                    => $faker->name,
        'father_name'             => $faker->name,
        'tazkira_number'          => $faker->isbn10,
        'job'                     => $faker->jobTitle,
        'latitude'                => $faker->latitude,
        'longitude'               => $faker->longitude,
        'property_area'           => $faker->numberBetween($min = 20, $max = 100),
        'district'                => $faker->numberBetween($min = 1, $max = 22),
        'street'                  => $faker->streetAddress,
        'property_number'         => $faker->numberBetween($min = 600, $max = 5000),
        'contruction_certificate' => $faker->boolean(70) ? 1 : 2,
        'comment'                 => $faker->sentence(20),
        'region_status'           => $faker->numberBetween($min = 1, $max = 3),
        'type_of_activity'        => $faker->numberBetween($min = 1, $max = 4),
        'status_of_cunstraction'  => $faker->numberBetween($min = 1, $max = 3),
        'start_date'              => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
        'monitor_date'            => $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now'),
        'proceedings'             => $faker->numberBetween($min = 1, $max = 8),
        'status_of_infractions'   => $faker->numberBetween($min = 1, $max = 5),
        'violation_type'          => $faker->numberBetween($min = 1, $max = 24),


    ];

});


$factory->define(App\ViolationUpload::class, function (Faker\Generator $faker)
{

    return [
        'infraction_id' => $faker->numberBetween($min = 1, $max = 100),
        'name'          => $faker->word,
        'path'          => 'standard/assets/images/construc.jpg',

    ];
});

$factory->define(App\SupervisorProject::class, function (Faker\Generator $faker)
{

    return [
        'supervisor_id' => $faker->numberBetween($min = 1, $max = 100),
        'project_id'    => $faker->numberBetween($min = 1, $max = 100),

    ];
});
$factory->define(App\Document::class, function (Faker\Generator $faker)
{

    return [
        'progress'        => $faker->numberBetween($min = 1, $max = 3),
        'title'           => $faker->name,
        'source'          => $faker->word,
        'destination'     => $faker->word,
        'start_date'      => $faker->dateTimeBetween($startDate = 'now', $endDate = '4 years'),
        'expiration_date' => $faker->dateTimeBetween($startDate = 'now', $endDate = '4 years'),
        'case'            => $faker->name,
        'version'         => $faker->numberBetween($min = 1, $max = 3),
        'category'        => $faker->numberBetween($min = 1, $max = 4),
        'priority'        => $faker->numberBetween($min = 1, $max = 4),
        'file'            => $faker->word,

    ];
});

$factory->define(App\DocumentUpload::class, function (Faker\Generator $faker)
{

    return [
        'document_id' => $faker->numberBetween($min = 1, $max = 100),
        'name'        => $faker->word,
        'path'        => 'standard/assets/images/Construction-2.jpg',
    ];
});
$factory->define(App\DocumentCase::class, function (Faker\Generator $faker)
{

    return [
        'documents_id' => $faker->numberBetween($min = 1, $max = 100),
        'file'         => $faker->word,
        'description'  => $faker->sentence($nbWords = 15, $variableNbWords = true),


    ];
});

$factory->define(App\MaterialTestUplaod::class, function (Faker\Generator $faker)
{

    return [
        'material_test_id' => $faker->numberBetween($min = 1, $max = 100),
        'name'           => $faker->word,
        'path'           => 'standard/assets/images/construc.jpg',
    ];
});
