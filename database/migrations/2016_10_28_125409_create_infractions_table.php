<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infractions', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('serial_number')->nullable();
            $table->string('name')->nullable();
            $table->string('father_name')->nullable();
            $table->string('tazkira_number')->nullable();
            $table->string('job')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('property_area')->nullable();
            $table->string('district')->nullable();
            $table->string('street')->nullable();
            $table->string('property_number')->nullable();
            $table->string('contruction_certificate')->nullable();
            $table->string('comment')->nullable();
            $table->string('region_status')->nullable();
            $table->string('type_of_activity')->nullable();
            $table->string('status_of_cunstraction')->nullable();
            $table->date('start_date')->nullable();
            $table->date('monitor_date')->nullable();
            $table->string('proceedings')->nullable();
            $table->string('status_of_infractions')->nullable();
            $table->string('violation_type')->nullable();
            $table->string('garbbing_area')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('infractions');
    }
}
