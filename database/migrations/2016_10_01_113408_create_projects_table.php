<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            // New Changes in KM
            $table->string('name');
            $table->integer('code')->nullable();
            $table->string('project_type')->nullable();
            $table->integer('available_fund')->nullable()->unsigned();
            $table->integer('expected_exe_in_year')->nullable();
            $table->integer('year')->nullable()->unsigned();
            $table->integer('exp_rec_budg_dep')->nullable()->unsigned();
            $table->date('complete_date')->nullable();
            $table->string('days_remain')->nullable();
            $table->string('percentage_remain')->nullable();
            $table->string('procurement_due_date')->nullable();
            $table->integer('procurement_type')->nullable();
            // ------
            $table->string('contractor')->nullable();
            $table->string('contract')->nullable();
            $table->string('subcont_phone')->nullable();
            $table->string('subcont_email')->nullable();
            $table->string('owner')->nullable();
            $table->string('area')->nullable();
            $table->string('passway')->nullable();
            $table->string('status')->nullable();
            $table->string('district')->nullable();
            $table->string('status_value')->nullable();
            $table->string('reasson_why_stop')->nullable();
            $table->string('latitude')->nullable();
            $table->string('gratitude')->nullable();
            $table->date('contract_date')->nullable();
            $table->date('start_date')->nullable();
            $table->date('actual_date')->nullable();
            $table->date('planned_date')->nullable();
            $table->date('compilation_date')->nullable();
            $table->string('project_budget')->nullable();
            $table->string('addational_budget')->nullable();
            $table->text('comment')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
