<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case', function (Blueprint $table){
            $table->increments('id');
            $table->integer('documents_id')->unsigned()->index();
            $table->foreign('documents_id')->references('id')->on('documents')->onDelete('cascade');
            $table->string('file')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('case');
    }
}
