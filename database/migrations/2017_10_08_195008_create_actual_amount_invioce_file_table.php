<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActualAmountInvioceFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actual_invoice_file', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_path');
            $table->integer('project_status_id')->unsigned();
            $table->foreign('project_status_id')->references('id')->on('project_status')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actual_invoice_file');
    }
}
