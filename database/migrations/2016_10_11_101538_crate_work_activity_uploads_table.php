<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateWorkActivityUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_activity_uploads', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('work_activitie_id')->unsigned()->index();
            $table->foreign('work_activitie_id')->references('id')->on('work_activities')->onDelete('cascade');
            $table->string('name')->nullable();
            $table->string('path')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('work_activity_uploads');
    }
}
