<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_workers', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('report_id')->unsigned()->index();
            $table->foreign('report_id')->references('id')->on('reports')->onDelete('cascade');
            $table->bigInteger('projectmanager')->nullable();
            $table->bigInteger('control_engineer')->nullable();
            $table->bigInteger('safety_engineer')->nullable();
            $table->bigInteger('worker_leader')->nullable();
            $table->bigInteger('smart_worker')->nullable();
            $table->bigInteger('ordinary_worker')->nullable();
            $table->bigInteger('gard')->nullable();
            $table->bigInteger('visitor')->nullable();
            $table->bigInteger('other')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_workers');
    }
}
