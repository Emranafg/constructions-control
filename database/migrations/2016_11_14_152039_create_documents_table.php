<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('progress');

            $table->string('title');
            $table->string('source');
            $table->string('destination');
            $table->date('start_date');
            $table->date('expiration_date');
            $table->string('case')->nullable();
            $table->string('category');
            $table->string('version');
            $table->string('priority');
            $table->string('file')->nullable();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
