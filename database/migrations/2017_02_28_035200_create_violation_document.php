<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViolationDocument extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('violation_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('infraction_id')->unsigned()->index();
            $table->foreign('infraction_id')->references('id')->on('infractions')->onDelete('cascade');
            $table->string('doc_number');
            $table->string('doc_name');
            $table->text('doc_description');
            $table->date('doc_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('violation_documents');
    }
}
