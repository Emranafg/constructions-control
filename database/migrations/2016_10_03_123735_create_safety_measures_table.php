<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafetyMeasuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('safety_measures', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('report_id')->unsigned()->index();
            $table->foreign('report_id')->references('id')->on('reports')->onDelete('cascade');
            $table->boolean('eye_protection')->nullable();
            $table->boolean('helmet')->nullable();
            $table->boolean('ear_protection')->nullable();
            $table->boolean('respiratory_protection')->nullable();
            $table->boolean('safety_boots')->nullable();
            $table->boolean('safety_gloves')->nullable();
            $table->boolean('safety_uniforms')->nullable();
            $table->boolean('safe_face_on')->nullable();
            $table->boolean('tape_barriers')->nullable();
            $table->boolean('symptoms_of_obstacles')->nullable();
            $table->boolean('warning_signs')->nullable();
            $table->boolean('mandatory_signs')->nullable();
            $table->boolean('first_aid_sign')->nullable();
            $table->boolean('risks_of_chemicals')->nullable();
            $table->boolean('noise_pollution')->nullable();
            $table->boolean('asphalting_disadvantages')->nullable();
            $table->boolean('concrete_disadvantages')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('safety_measures');
    }
}
