<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateWorkDefactsUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_defacts_uploads', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('work_defact_id')->unsigned()->index();
            $table->foreign('work_defact_id')->references('id')->on('work_defacts')->onDelete('cascade');
            $table->string('name')->nullable();
            $table->string('path')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('work_defacts_uploads');
    }
}
