var FormElements = function() {"use strict";

	//function to initiate jquery.maskedinput
	var maskInputHandler = function() {
		$.mask.definitions['~'] = '[+-]';
		$('.input-mask-date').mask('99/99/9999');
		$('.input-mask-phone').mask('(999) 999-9999');
		$('.input-mask-eyescript').mask('~9.99 ~9.99 999');
		$(".input-mask-product").mask("a*-999-a999", {
			placeholder: " ",
			completed: function() {
				alert("You typed the following: " + this.val());
			}
		});
	};
	//function to initiate bootstrap-touchspin
	var touchSpinHandler = function() {
		$("input.touchspin").TouchSpin({
			postfix: '%'
		});
		$("input.number").TouchSpin({
			initval: 1
		});

	};
	
	var autosizeHandler = function() {
		$('.autosize.area-animated').append("\n");
		autosize($('.autosize'));
	};
	var select2Handler = function() {
		$(".js-example-basic-single").select2();
		$(".js-example-basic-multiple").select2();
		$(".js-example-placeholder-single").select2({
			placeholder: "Select a state"
		});
		var data = [{
			id: 0,
			text: 'enhancement'
		}, {
			id: 1,
			text: 'bug'
		}, {
			id: 2,
			text: 'duplicate'
		}, {
			id: 3,
			text: 'invalid'
		}, {
			id: 4,
			text: 'wontfix'
		}];
		$(".js-example-data-array-selected").select2({
			data: data
		});
		$(".js-example-basic-hide-search").select2({
			minimumResultsForSearch: Infinity
		});
	};
	var datePickerHandler = function() {
		$('.datepicker').datepicker({
			autoclose: true,
			todayHighlight: true,
			format:  "yyyy-mm-dd",
		});
		$('.format-datepicker').datepicker({
			format:  "yyy-MM-dd",
			todayHighlight: true
		});
		
	};
	var timePickerHandler = function() {
		$('#timepicker-default').timepicker({timeFormat: 'HH:mm:ss'});
	};
	return {
		//main function to initiate template pages
		init: function() {
			maskInputHandler();
			touchSpinHandler();
			autosizeHandler();
			select2Handler();
			datePickerHandler();
			timePickerHandler();
		}
	};
}();
