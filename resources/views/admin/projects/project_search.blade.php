@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 2, 'subActive' => 1])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans('pmis.list_of_projects'), 'page' => trans('pmis.reports'), 'current' => trans('pmis.projects')])
@stop
@section('alert-message')
    @if(session('message_title'))
        <div role="alert" class="alert {{ session('message_class') }}">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
        </div>
    @endif
@stop
@section('main-content')
    <div class="container-fluid container-fullw bg-white">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading border-light">
                    <h4 class="panel-title">{{trans('pmis.projects')}}</h4>
                    <ul class="panel-heading-tabs border-light">
                        @if(Auth::User()->can('create_project'))
                        <li>
                            <div class="pull-right">
                                <div class="ui labeled button" tabindex="0" style="direction: ltr">
                                    <a href="/project/create" class="ui red button">
                                        <i class="plus icon"></i> {{ trans('pmis.creat_pro') }}
                                    </a>
                                    <a class="ui basic red left pointing label" style="border-radius: 0px !important">
                                        {{ $proCount }}
                                    </a>
                                </div>
                            </div>
                        </li>
                        @endif
                        <li class="panel-tools">
                            <a href="/export/project/budget/report" class="ui button">Export Report</a>
                        </li>
                        <li class="panel-tools">
                            <a data-original-title="{{trans('pmis.Refresh')}}" data-toggle="tooltip"
                               data-placement="top"
                               class="btn btn-transparent btn-sm panel-refresh" href="#"><i
                                        class="flaticon-business-2"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="margin-top-30"></div>
                        <div @if(App::isLocale('fa') || App::isLocale('pa'))
                             class="margin-right-10" @else class="margin-left-10" @endif >
                            <table class="ui celled table" id="users-table"
                                   style="width: 100%; text-align: center !important;">
                                <thead>
                                <tr>
                                    <th>{{trans('pmis.id')}}</th>
                                    <th>{{trans('pmis.name')}}</th>
                                    <th>{{trans('pmis.project_code')}}</th>
                                    <th>{{trans('pmis.available_fund')}}</th>
                                    <th>{{trans('pmis.owner')}}</th>
                                    <th>{{trans('pmis.status')}}</th>
                                    <th>{{trans('pmis.Progress')}}</th>
                                    {{--<th>{{trans('pmis.reports')}}</th>--}}
                                    @if(Auth::User()->can('assign_project'))
                                        <th>{{ trans('user.assign') }}</th>
                                    @endif
                                    @if(Auth::User()->can('edit_project'))
                                        <th>{{trans('pmis.edit')}}</th>
                                    @endif
                                    @if(Auth::User()->can('delete_project'))
                                        <th>{{trans('pmis.delete')}}</th>
                                    @endif
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="assign_project_modal" tabindex="-1" role="dialog" aria-labelledby="Assign Project Modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">{{trans('pmis.Adding_Item')}}</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="project-assign">
                        {!! csrf_field() !!}
                        <input type="hidden" name="project_id" value="" id="project_id">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="new_item">
                                Project Manager
                            </label>
                            <div class="col-md-8">
                                <select class="form-control" name="user_id" id="user_id">
                                        {{-- dropdown.blade.php is loading here by ajax--}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group container">
                            <div class="col-md-2 pull-right">
                                <button class="btn btn-o btn-primary" type="submit">
                                    {{trans('pmis.save')}}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script>
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        "language": {
            "lengthMenu": "{{trans('pmis.show')}} _MENU_{{trans('pmis.entries')}}",
            "zeroRecords": "{{trans('pmis.Nothing_found_sorry')}}",
            // "info": "{{trans('pmis.show')}}_PAGES_{{trans('pmis.of')}} _MAX_{{trans('pmis.entries')}}",
            "info": "{{trans('pmis.show')}} _START_ {{trans('pmis.of')}} _END_ {{trans('pmis.of')}} _TOTAL_ {{trans('pmis.entries')}}",//sInfoFiltered:"({{trans('pmis.show')}} _MAX_ total {{trans('pmis.entries')}})"
            "infoEmpty": "{{trans('pmis.No_records_available')}}",
            "infoFiltered": "({{trans('pmis.show')}} {{trans('pmis.of')}}_MAX_ {{trans('pmis.entrie')}})",
            "search": "{{trans('pmis.search')}}",
            paginate: {
                next: "{{trans('pmis.next')}}",
                previous: "{{trans('pmis.previous')}}"
            },
            // "next":"{{trans('pmis.search')}}"
        },
        'ajax': {
            'url': '/searchprojects'
        },
        columns: [
            {data: 'id', name: 'id', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'code', name: 'code'},
            {data: 'available_fund', name: 'available_fund'},
            {data: 'owner', name: 'owner'},
            {data: 'status', name: 'status'},
            {data: 'status_value', name: 'status_value'},
            @if(Auth::User()->can('assign_project'))
                {data: 'assign', name: 'assign', orderable: false, searchable: false},
            @endif
            @if(Auth::User()->can('edit_project'))
                {data: 'edite', name: 'edite', orderable: false, searchable: false},
            @endif
            @if(Auth::User()->can('delete_project'))
                {data: 'delete', name: 'delete', orderable: false, searchable: false}
            @endif
        ]
    });
    function confirmDelete(id) {
        swal({
            title: "{{trans('pmis.are_sure')}}",
            text: "{{trans('pmis.sw_sub_mess')}}",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "{{trans('pmis.ye_del')}}",
            cancelButtonText: "{{trans('pmis.cancel')}}",
            closeOnConfirm: false
        },
        function () {
            window.location = '/projects/delete/' + id;
            swal("{{trans('pmis.deleted')}}", "{{trans('pmis.conf_mess')}}", "success");
        });
    }
</script>
@endpush
