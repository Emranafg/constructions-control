@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 2, 'subActive' => 1])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => 'Tier II', 'page' => trans('pmis.reports'), 'current' => trans('pmis.projects')])
@stop

@section('main-content')
    <div class="container-fluid container-fullw bg-white">
        <h1>{{trans('pmis.monthly_report')}}</h1>
    </div>
@stop

@push('scripts')

@endpush