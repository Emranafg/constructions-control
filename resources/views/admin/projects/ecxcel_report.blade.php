@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 2, 'subActive' => 2])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans('pmis.reports'), 'page' => trans('pmis.projects'), 'current' => trans('pmis.reports')])
@stop
@section('main-content')
    <div class="panel panel-white no-radius margin-top-30">
        <form role="form" class="form-inline" id="search-form" method="POST">
            <div class="panel-heading border-light">
                <h4 class="panel-title">{{trans('pmis.Report_Generation_Panel')}} <span class="text-bold"></span></h4>
                <ul class="panel-heading-tabs border-light">
                    <li>
                        <div class="pull-right">
                            <div class="ui buttons">
                                <a id="export" class="ui button">{{trans('pmis.Export')}}</a>
                                <div class="or"></div>
                                <a class="ui button report" id="reload">{{trans('pmis.Filter')}}</a>
                            </div>
                        </div>
                    </li>
                    <li class="panel-tools">
                        <a href="#" class="btn btn-transparent btn-sm panel-refresh" data-placement="top"
                           data-toggle="tooltip" data-original-title="{{trans('pmis.Refresh')}}"><i
                                    class="flaticon-business-2"></i></a>
                    </li>
                </ul>
            </div>
            <div class="panel-body margin-top-10">
                <div class="row">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <select class="ui dropdown" name="status" id="status">
                                    <option value="">{{trans('pmis.status')}}</option>
                                    <option value="TRUE">{{trans('pmis.Ongoing')}}</option>
                                    <option value="FALSE">{{trans('pmis.Stopped')}}</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="ui dropdown" name="equality" id="">
                                    <option value="">
                                        <th>{{trans('pmis.Progress')}}</th>
                                    </option>

                                    <option value="1">{{trans('pmis.Progress')}}</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="ui dropdown" name="Popretor" id="Popretor">
                                    <option value="=">{{trans('pmis.Equal')}}</option>
                                    <option value=">=">>=</option>
                                    <option value="<="><=</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <div class="ui action input">
                                    <input placeholder="%" name="pValue" type="text" style="max-width: 95px !important;"
                                           id="pValue">
                                <span @if(App::isLocale('en')) class="ui teal right labeled icon button"
                                      @else class="ui teal left labeled icon button" @endif
                                      style="max-width: 100px; font-size: 10px">
                                    <i class="percent icon"></i>
                                    {{trans('pmis.Percentage')}}
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row margin-top-15">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <select class="ui dropdown" name="dateType" id="dateType">
                                    <option value="">{{trans('pmis.Chose_Date_Type')}}</option>
                                    <option value="contract_date">{{trans('pmis.Contract_Date')}}</option>
                                    <option value="start_date">{{trans('pmis.Start_Date')}}</option>
                                    <option value="compilation_date">{{trans('pmis.Completion_Date')}}</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="ui dropdown" name="dateoperator" id="dateoperator">
                                    <option value="=">{{trans('pmis.Equal')}}</option>
                                    <option value=">=">>=</option>
                                    <option value="<="><=</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <div class="ui fluid icon input" style="max-width: 198px !important;">
                                    <input id="datevalue" class="datepicker" data-date-format="yyyy-mm-dd"
                                           placeholder="{{trans('pmis.date')}}" type="text"
                                           style="max-width: 198px !important;">
                                    <i class="calendar icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row border-top margin-top-20">
                    <div class="margin-top-10"></div>
                    <div @if(App::isLocale('fa') || App::isLocale('pa'))
                         class="col-md-12 margin-right-15" @else class="col-md-12 margin-left-15" @endif >
                        <table class="ui celled table" id="users-table"
                               style="width: 100%; text-align: center !important;">
                            <thead>
                            <tr>
                                <th>{{trans('pmis.id')}}</th>
                                <th>{{trans('pmis.name')}}</th>
                                <th>{{trans('pmis.contractor')}}</th>
                                <th>{{trans('pmis.owner')}}</th>
                                <th>{{trans('pmis.location')}}</th>
                                <th>{{trans('pmis.status')}}</th>
                                <th>{{trans('pmis.Progress')}}</th>
                                <th>{{trans('pmis.Budget')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
@push('scripts')
<script>
    $('.ui.dropdown').dropdown();
    $('#export').click(function () {
        window.location = "/report/excel/export";
    });
    var status = 'unknown';
    var progress = 'unknown';
    var dateType = 'unknown';
    var dateValue = 'unknown';

    function initValues() {
        status = $('#status').val() ? $('#status').val() : 'unknown';
        progress = $('#pValue').val() ? $('#pValue').val() : 'unknown';
        dateType = $('#dateType').val() ? $('#dateType').val() : 'unknown';
        dateValue = $('#datevalue').val() ? $('#datevalue').val() : 'unknown';
    }

    function getUrl() {
        return '/report/excel/status/' + status + '/progress/willChange/Popretor/' + $('#Popretor').val() + '/value/'
                + progress + '/dateType/' + dateType + '/Dateoperator/' + $('#dateoperator').val() + '/date/' + dateValue;
    }
    var oTable = $('#users-table').DataTable({
        bFilter: false,
        processing: true,
        serverSide: true,
        "language": {
            "lengthMenu": "{{trans('pmis.show')}} _MENU_{{trans('pmis.entries')}}",
            "zeroRecords": "{{trans('pmis.Nothing_found_sorry')}}",
            // "info": "{{trans('pmis.show')}}_PAGES_{{trans('pmis.of')}} _MAX_{{trans('pmis.entries')}}",
            "info": "{{trans('pmis.show')}} _START_ {{trans('pmis.of')}} _END_ {{trans('pmis.of')}} _TOTAL_ {{trans('pmis.entries')}}",//sInfoFiltered:"({{trans('pmis.show')}} _MAX_ total {{trans('pmis.entries')}})"
            "infoEmpty": "{{trans('pmis.No_records_available')}}",
            "infoFiltered": "({{trans('pmis.show')}} {{trans('pmis.of')}}_MAX_ {{trans('pmis.entrie')}})",
            "search": "{{trans('pmis.search')}}",
            paginate: {

                next: "{{trans('pmis.next')}}",
                previous: "{{trans('pmis.previous')}}"
            },
            // "next":"{{trans('pmis.search')}}"
        },
        buttons: [
            'csv', 'excel', 'pdf', 'print'
        ],
        ajax: {
            url: getUrl()
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'contractor', name: 'contractor'},
            {data: 'owner', name: 'owner'},
            {data: 'area', name: 'area'},
            {data: 'status', name: 'status'},
            {data: 'status_value', name: 'status_value'},
            {data: 'project_budget', name: 'project_budget'},
        ]
    });

    function redrawTable(e) {
        initValues();
        oTable.ajax.url(getUrl());
        oTable.draw();
        e.preventDefault();
    }
    $('#status').change(function (e) {
        redrawTable(e);
    });
    $("#datevalue").change(function (e) {
        initValues();
        if (dateType === 'unknown') {
            swal({
                title: "Error!",
                text: "please first select date type !",
                type: "error",
                confirmButtonText: "close"
            });

            return;
        }
        redrawTable(e);
    });
    $("input[name='pValue']").change(function (e) {
        initValues();
        redrawTable(e)
    });
    $('#reload').click(function (e) {
        initValues();
        window.location = '/report/export/excel/status/' + status + '/progress/willChange/Popretor/' + $('#Popretor').val() + '/value/'
                + progress + '/dateType/' + dateType + '/Dateoperator/' + $('#dateoperator').val() + '/date/' + dateValue;
    });
</script>
@endpush
