
@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 2, 'subActive' => 1])
@stop
@section('page-title')
    @hasrole('admin')
        @include('partials.breadcrumb', ['pageTitle' => trans('pmis.Project_Details'), 'page' => trans('pmis.projects'), 'current' => trans('pmis.details')])
    @else
        @include('partials.breadcrumb', ['pageTitle' => trans('pmis.reports'), 'page' => trans('pmis.projects'), 'current' => trans('pmis.details')])
    @endhasrole
@stop
@section('alert-message')
    @if(session('message_title'))
        <div role="alert" class="alert {{ session('message_class') }}">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
        </div>
    @endif
@stop
@section('main-content')
    <div class="container-fluid container-fullw bg-white">
        @if(Auth::User()->can('view_project'))
        <div class="row">
            <div class="col-md-12">
                <div class="ui card card-full-width">
                    <div class="content card-header-backgrounded">
                        <div class="header">
                            <i class="flaticon-businessman-showing-a-project-sketch white-icon-card-style"
                               style="font-size: 25px !important;"></i>
                            <span class="white-ui-card-header-artical">{{$projectDetails->name}}</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6 padding-top-30">
                            <fieldset class="padding-top-15" style="min-height: 350px !important;">
                                <legend><i style="font-size: 18px" class="flaticon-icon-107807 icon-red"></i>
                                    <span style="">{{trans('pmis.Project_Details')}}</span>
                                </legend>
                                <div class="ui relaxed divided list">
                                    <div class="item">
                                        <i class="flaticon-instructor-lecture-with-sceen-projection-tool icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('project.project_name')}}</a>
                                            <div class="description">{{$projectDetails->name}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="barcode icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.project_code')}}</a>
                                            <div class="description">{{$projectDetails->code}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="grid layout icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.project_type')}}</a>
                                            <div class="description">
                                                @if($projectDetails->project_type == 1)
                                                    Private
                                                    @elseif($projectDetails->project_type == 2)
                                                    Kabul Municipality
                                                    @else
                                                    Fund
                                                    @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="dollar icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.fund_available_for_year')}}</a>
                                            <div class="description">AFN {{number_format($projectDetails->available_fund)}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="steam icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.expected_execution_in_year')}}</a>
                                            <div class="description">AFN {{number_format($projectDetails->expected_exe_in_year)}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="calendar icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.project_year')}}</a>
                                            <div class="description">{{$projectDetails->year}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="dollar icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.expense_record_budget_dep')}}</a>
                                            <div class="description">AFN {{number_format($projectDetails->exp_rec_budg_dep)}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="flaticon-give-money icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.Project_Budget')}}</a>
                                            <div class="description">
                                                AFN {{number_format($projectDetails->project_budget)}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="flaticon-money-bag-with-dollar-symbol icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.addational_budget')}}</a>
                                            <div class="description">
                                                AFN {{number_format($projectDetails->addational_budget)}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="flaticon-wireless-signal-interface-status-symbol-of-bars-group icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.Project_Status')}}</a>
                                                <p class="text-small">
                                                <div class="ui indicating progress" data-value="{{$status}}" data-total="100"
                                                     id="example4">
                                                    <div class="bar" style="height: 100%">
                                                        <div class="progress"
                                                             style="background-color: transparent !important;"></div>
                                                    </div>
                                                </div>
                                                </p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="flaticon-location-pin icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.Project_Location')}}</a>
                                            <div class="description">{{$projectDetails->area}}
                                                -{{$projectDetails->passway}}</div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <i class="flaticon-route-2 icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.Coordinates')}}</a>
                                            <div class="description">
                                                {!! $projectCoordinates !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="flaticon-calendar-3 icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.Contract_Date')}}</a>
                                            <div class="description">{{$projectDetails->contract_date}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="flaticon-calendar-with-a-clock-time-tools icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.actual_start_date')}}</a>
                                            <div class="description">{{$projectDetails->start_date}}</div>
                                        </div>
                                    </div>
                                    {{--<div class="item">--}}
                                        {{--<i class="flaticon-calendar-2 icon"></i>--}}
                                        {{--<div class="content">--}}
                                            {{--<a class="header">{{trans('pmis.Completion_Date')}}</a>--}}
                                            {{--<div class="description">{{$projectDetails->compilation_date}}</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="item">
                                        <i class="flaticon-calendar-3 icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.project_contract_comp_date')}}</a>
                                            <div class="description">{{$projectDetails->actual_date}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="flaticon-calendar-2 icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.completion_planned_date')}}</a>
                                            <div class="description">{{$projectDetails->planned_date}}</div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <i class="flaticon-calendar-2 icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.project_comment')}}</a>
                                            <div class="description">{{$projectDetails->comment}}</div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-md-6 padding-top-30">
                            <fieldset class="padding-top-15">
                                <legend><i style="font-size: 18px" class="flaticon-icon-107807 icon-red"></i>
                                    <span style="">{{trans('pmis.project_type')}}</span>
                                </legend>
                                <div class="ui relaxed divided list">
                                    <div class="item">
                                        <i class="flaticon-copyright-symbol icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.name')}}</a>
                                            <div class="description">{{$projectDetails->owner ? $projectDetails->owner : 'Unknown'}}</div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="padding-top-15">
                                <legend><i style="font-size: 18px" class="flaticon-icon-107807 icon-red"></i>
                                    <span style="">{{trans('pmis.project_status')}} </span>
                                </legend>
                                <div class="ui relaxed divided list">
                                    @if($projectDetails->status == 1)
                                    <div class="item">
                                        <div class="content">
                                            <a class="header">Design {{trans('pmis.days_remain')}}</a>
                                            <div class="description">{{$projectDetails->days_remain}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="content">
                                            <a class="header"> Design {{trans('pmis.percentage_remain')}}</a>
                                            <div class="description">{{$projectDetails->percentage_remain}} %</div>
                                        </div>
                                    </div>
                                        @elseif($projectDetails->status == 2)
                                    <div class="item">
                                        <div class="content">
                                            <a class="header">Procurement {{trans('pmis.days_remain')}}</a>
                                            <div class="description">{{$projectDetails->procurement_due_date}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="content">
                                            <a class="header">{{trans('pmis.procurement_phase')}}</a>
                                            <div class="description">
                                                @if($projectDetails->procurement_type == 1)
                                                    Bid Announcemenst
                                                    @elseif($projectDetails->procurement_type == 2)
                                                    Bid Opening
                                                    @elseif($projectDetails->procurement_type == 3)
                                                    Evaluation
                                                    @else
                                                    Contract
                                                    @endif
                                            </div>
                                        </div>
                                    </div>

                                    @elseif($projectDetails->status == 3)
                                        <div class="item">
                                            <div class="content">
                                                <a class="header">{{trans('pmis.project_status')}}</a>
                                                <div class="description">Construction</div>
                                            </div>
                                        </div>
                                        @elseif($projectDetails->status == 4)
                                        <div class="item">
                                            <div class="content">
                                                <a class="header">{{trans('pmis.complete_date')}}</a>
                                                <div class="description">{{$projectDetails->complete_date}}</div>
                                            </div>
                                        </div>
                                    @endif

                                </div>
                            </fieldset>
                            <fieldset class="padding-top-15" style="min-height: 250px !important;">
                                <legend><i style="font-size: 18px" class="flaticon-icon-107807 icon-red"></i>
                                    <span style="">{{trans('pmis.Supervisor_Details')}}</span>
                                </legend>
                                <div class="ui relaxed divided list">
                                    <div class="item">
                                        <i class="flaticon-project-manager icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.Supervisor_Name')}}</a>
                                            <div class="description">{{$projectSupervisor->first_name ? $projectSupervisor->first_name : 'Unknown'}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="mobile icon" style="font-size: 15px"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.Phone_Number')}}</a>
                                            <div class="description">{{$projectSupervisor->phone ? $projectSupervisor->phone : 'Unknown' }}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="mail icon" style="font-size: 13px"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.Email')}}</a>
                                            <div class="description">{{$projectSupervisor->email ? $projectSupervisor->email : 'Unknown' }}</div>
                                        </div>
                                    </div>

                                </div>
                            </fieldset>
                            <fieldset class="padding-top-15" style="min-height: 250px !important;">
                                <legend><i style="font-size: 15px" class="flaticon-icon-107807 icon-red"></i>
                                    <span style=""> {{trans('pmis.Contract_Details')}}</span>
                                </legend>
                                <div class="ui relaxed divided list">
                                    {{--<div class="item">--}}
                                        {{--<i class="flaticon-copyright-symbol icon"></i>--}}
                                        {{--<div class="content">--}}
                                            {{--<a class="header">{{trans('pmis.Project_owner')}}</a>--}}
                                            {{--<div class="description">{{$projectDetails->owner}}</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="item">
                                        <i class="flaticon-interface icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.Project_Contractor')}}</a>
                                            <div class="description">{{$projectDetails->contractor}}</div>
                                        </div>
                                    </div>
                                    {{--<div class="item">--}}
                                        {{--<i class="flaticon-handshake icon"></i>--}}
                                        {{--<div class="content">--}}
                                            {{--<a class="header">{{trans('pmis.Project_contract')}}</a>--}}
                                            {{--<div class="description">{{$projectDetails->contract}}</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="item">
                                        <i class="mobile icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.Phone_Number')}}</a>
                                            <div class="description">{{$projectDetails->subcont_phone}}</div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="mail icon"></i>
                                        <div class="content">
                                            <a class="header">{{trans('pmis.Email')}}</a>
                                            <div class="description">{{$projectDetails->subcont_email}}</div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if($projectDetails->status == 3)
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-12">
                    <div class="ui card card-full-width">
                        <div class="content card-header-backgrounded">
                            <div class="header">
                                <i class="flaticon-businessman-showing-a-project-sketch white-icon-card-style"
                                   style="font-size: 25px !important;"></i>
                                <span class="white-ui-card-header-artical">{{trans('pmis.planned_amount')}}</span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <table class="ui celled table striped  table-cyberaan
                                    @if(App::isLocale('en'))
                                        left aligned
                                        @else
                                        right aligned
                                        @endif ">
                                    <tr>
                                        <th>Id </th>
                                        <th> Month Name</th>
                                        <th>Planned Amount</th>
                                        <th>Percentage</th>
                                    </tr>

                                    <?php $id = 1;?>
                                    @foreach($projectDetails->getStatus as $projectStatus)

                                        <tr>
                                            <td>{{$id}}</td>

                                            <td>
                                                @if(Auth::User()->can('edit_plan'))
                                                <a onclick="getPlannedAmountModal({{$projectStatus->month_number}})">
                                                    <?php
                                                    $date = \Carbon\Carbon::parse('2017-'.$projectStatus->month_number.'-30');
                                                    echo $date->format('F');
                                                    ?>
                                                </a>
                                                    @else
                                                    <?php
                                                    $date = \Carbon\Carbon::parse('2017-'.$projectStatus->month_number.'-30');
                                                    echo $date->format('F');
                                                    ?>
                                                    @endif
                                            </td>
                                            <td>{{$projectStatus->planned_amount}}</td>
                                            <td>{{$projectStatus->planned_percentage}} %</td>
                                        </tr>
                                    <?php ++$id; ?>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px;">
            <div class="col-md-12">
                <div class="ui card card-full-width">
                    <div class="content card-header-backgrounded">
                        <div class="header">
                            <i class="flaticon-businessman-showing-a-project-sketch white-icon-card-style"
                               style="font-size: 25px !important;"></i>
                            <span class="white-ui-card-header-artical">{{trans('pmis.actual_amount_invoice')}}</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                            <div class="row">
                                <table class="ui celled table striped table-cyberaan
                                    @if(App::isLocale('en'))
                                        left aligned
                                        @else
                                        right aligned
                                        @endif
                                        "  style="border-radius: ">
                                    <tr>
                                        <th>Id </th>
                                        <th> Month Name</th>
                                        <th>Actual Amount</th>
                                        <th>Percentage</th>
                                        <th>Uploaded Data</th>
                                        <th>Document</th>
                                    </tr>
                                    <?php $id = 1;?>
                                    @foreach($projectDetails->getStatus as $projectStatus)
                                        <tr>
                                            <td>{{$id}}</td>
                                            <td>
                                                <a onclick="getActalAmountModal({{$projectStatus->month_number}})">
                                                <?php
                                                $date = \Carbon\Carbon::parse('2017-'.$projectStatus->month_number.'-30');
                                                echo $date->format('F');
                                                ?>
                                                </a>
                                            </td>
                                            <td>{{$projectStatus->actual_amount}}</td>
                                            <td>{{$projectStatus->actual_perccentage}} %</td>
                                            <td>{{$projectStatus->updated_at->toFormattedDateString()}}</td>
                                            <td>
                                                @foreach($projectStatus->getFiles as $files)
                                                    <a href="/{{$files->file_path}}" target="_blank"><i class="file icon"></i></a>
                                                @endforeach
                                            </td>
                                        </tr>
                                        <?php ++$id;?>
                                    @endforeach
                                    <tr>
                                        <td colspan="2" class="text-center"><strong>Total</strong></td>
                                        <td>
                                            <strong>{{$sumOfActualAmount}}</strong>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        <div class="row margin-top-15">
            <div class="col-md-12">
                <div class="ui card card-full-width">
                    <div class="content card-header-backgrounded">
                        <div class="header">
                            <i class="flaticon-monitor white-icon-card-style" style="font-size: 25px !important;"></i>
                            <span class="white-ui-card-header-artical">{{trans('pmis.reports')}}</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="container-fluid container-fullw bg-white">
                            <div class="panel panel-white">
                                <div class="panel-heading border-light">
                                    <h4 class="panel-title">{{trans('pmis.Daily_Report')}}</h4>
                                    <ul class="panel-heading-tabs border-light">
                                        <li>
                                            <div class="pull-right">
                                                <a href="/export/project/daily/report" class="ui button">{{ trans('pmis.Export') }}</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="pull-right">
                                                <div class="ui labeled button" tabindex="0" style="direction: ltr">
                                                    <a href="/project/{{ $projectDetails->id }}/report/create"
                                                       class="ui red button">
                                                        <i class="plus icon"></i> {{ trans('pmis.creat_daily_rep') }}
                                                    </a>
                                                    <a class="ui basic red left pointing label"
                                                       style="border-radius: 0px !important">
                                                        {{$daylyRepCount}}
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="panel-tools">
                                            <a data-original-title="{{trans('pmis.reload')}}" data-toggle="tooltip"
                                               data-placement="top"
                                               class="btn btn-transparent btn-sm panel-refresh" href="#"><i
                                                        class="flaticon-business-2"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="margin-top-30"></div>
                                        <div @if(App::isLocale('fa') || App::isLocale('pa'))
                                             class="margin-right-10" @else class="margin-left-10" @endif    >
                                            <table class="ui celled table" id="users-table"
                                                   style="width: 100%; text-align: center !important;">
                                                <thead>
                                                <tr>
                                                    <th>{{trans('pmis.id')}}</th>
                                                    <th>{{trans('pmis.Date')}}</th>
                                                    <th>{{trans('pmis.status')}}</th>
                                                    <th>{{trans('pmis.Progress')}}</th>
                                                    @if(Auth::user()->can('edit_report') || Auth::user()->hasrole('admin'))
                                                    <th>{{trans('pmis.edit')}}</th>
                                                    @endif
                                                    @if(Auth::user()->can('delete_report') || Auth::user()->hasrole('admin'))
                                                    <th>{{trans('pmis.delete')}}</th>
                                                        @endif
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="actual_amount_modal" tabindex="-1" role="dialog" aria-labelledby="Assign Project Modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">{{trans('pmis.project_status')}}</h4>
                </div>
                <div class="modal-body ">
                    <form id="actual_amount_form" action="{{action('ProjectController@updateActualAmount')}}"  method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="container-fluid container-fullw bg-white">
                            <div class="row">
                                <input type="hidden" id="month_number" name="month_number" >
                                <input type="hidden" id="available_fund" value="{{$projectDetails->available_fund}}">
                                <input type="hidden" id="project_id" name="project_id" value="{{$projectDetails->id}}">
                                <div class="col-md-6">
                                    <label>
                                        {{trans('pmis.actual_amount')}}
                                        <span class="symbol required" aria-required="true"></span>
                                    </label>
                                    <div class="form-group">
                                        <input placeholder="{{trans('pmis.actual_amount')}}" name="actual_amount" id="edit_project_actual_amount" class="form-control" type="text" >
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label>
                                        {{trans('pmis.actual_amount_percentage')}}
                                        <span class="symbol required" aria-required="true"></span>
                                    </label>
                                    <div class="form-group">
                                        <input  placeholder="" name="actual_amount_percentage" id="edit_project_actual_amount_percentage" class="form-control" type="text" readonly>
                                    </div>
                                </div>

                                <div class="invoice_file">
                                </div>
                                <div class="col-md-12">
                                    <div id="invoice_file" class="dropzone"></div>
                                </div>

                                <div class="form-group container" style="margin-top: 20px;">
                                    <div class="col-md-2 pull-right">
                                        <button class="btn btn-o btn-primary" type="submit">
                                            {{trans('pmis.save')}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade" id="planned_amount_modal" tabindex="-1" role="dialog" aria-labelledby="Assign Project Modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">{{trans('pmis.project_status')}}</h4>
                </div>
                <div class="modal-body ">
                    <form id="planned_amount_form" action="{{url('planned/amount/update')}}"  method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="container-fluid container-fullw bg-white">
                            <div class="row">
                                <input type="hidden" id="planned_month_number" name="month_number" >
                                <input type="hidden" id="available_fund" value="{{$projectDetails->available_fund}}">
                                <input type="hidden" id="project_id" name="project_id" value="{{$projectDetails->id}}">
                                <div class="col-md-6">
                                    <label>
                                        {{trans('pmis.planned_amount')}}
                                        <span class="symbol required" aria-required="true"></span>
                                    </label>
                                    <div class="form-group">
                                        <input placeholder="" name="planned_amount" id="edit_project_planned_amount" class="form-control" type="text" >
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label>
                                        {{trans('pmis.planned_amount_percentage')}}
                                        <span class="symbol required" aria-required="true"></span>
                                    </label>
                                    <div class="form-group">
                                        <input  placeholder="" name="planned_amount_percentage" id="edit_project_planned_amount_percentage" class="form-control" type="text" readonly>
                                    </div>
                                </div>


                                <div class="form-group container" style="margin-top: 20px;">
                                    <div class="col-md-2 pull-right">
                                        <button class="btn btn-o btn-primary" type="submit">
                                            {{trans('pmis.save')}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
@push('scripts')
<script>
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        "language": {
            "lengthMenu": "{{trans('pmis.show')}} _MENU_{{trans('pmis.entries')}}",
            "zeroRecords": "{{trans('pmis.Nothing_found_sorry')}}",
            // "info": "{{trans('pmis.show')}}_PAGES_{{trans('pmis.of')}} _MAX_{{trans('pmis.entries')}}",
            "info": "{{trans('pmis.show')}} _START_ {{trans('pmis.of')}} _END_ {{trans('pmis.of')}} _TOTAL_ {{trans('pmis.entries')}}",//sInfoFiltered:"({{trans('pmis.show')}} _MAX_ total {{trans('pmis.entries')}})"
            "infoEmpty": "{{trans('pmis.No_records_available')}}",
            "infoFiltered": "({{trans('pmis.show')}} {{trans('pmis.of')}}_MAX_ {{trans('pmis.entrie')}})",
            "search": "{{trans('pmis.search')}}",
            paginate: {
                next: "{{trans('pmis.next')}}",
                previous: "{{trans('pmis.previous')}}"
            },
            // "next":"{{trans('pmis.search')}}"
        },
        'ajax': {
            'url': '/projectdailyreports/project/' + '{{$projectDetails->id}}'
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'date', name: 'date'},
//            {data: 'time', name: 'time'},
//            {data: 'weather', name: 'weather'},
//            {data: 'temprature', name: 'temprature'},
            {data: 'status', name: 'status'},
            {data: 'status_value', name: 'status_value'},
            @if(Auth::user()->can('edit_report') || Auth::user()->hasrole('admin'))
            {data: 'edite', name: 'edite', orderable: false, searchable: false},
            @endif
                @if(Auth::user()->can('delete_report') || Auth::user()->hasrole('admin'))
            {data: 'delete', name: 'delete', orderable: false, searchable: false}
            @endif

        ]
    });
</script>
@if($projectDetails->status == true)
    <script>
        var percentage = {{$projectDetails->status_value}}
            $('#example4').progress({percent: percentage});
    </script>
@endif
<script>
    function confirmDelete(id) {
        swal({
            title: "{{trans('pmis.are_sure')}}",
            text: "{{trans('pmis.sw_sub_mess')}}",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "{{trans('pmis.ye_del')}}",
            cancelButtonText: "{{trans('pmis.cancel')}}",
            closeOnConfirm: false
        },
        function () {
            window.location = '/daily/report/delete/' + id;
            swal("{{trans('pmis.deleted')}}", "{{trans('pmis.conf_mess')}}", "success");
        });
    }
</script>
@endpush
