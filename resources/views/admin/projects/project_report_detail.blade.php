@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 2, 'subActive' => 1])
@stop
@section('page-title')
    <div class="row">
        <div class="col-sm-8">
            <h1 class="mainTitle">{{trans('pmis.Daily_Report')}}</h1>
            <small class="block" style="color: #8e8e93;">
                {{trans('pmis.Date')}}: {{$reportDetails->date}} | {{trans('pmis.time')}}: {{$reportDetails->time}}
            </small>
            </span>
        </div>
        <ol class="breadcrumb">
            <li>
                <span>{{trans('pmis.projects')}}</span>
            </li>
            <li class="active">
                <span>{{trans('pmis.Daily_Report')}}</span>
            </li>
        </ol>
    </div>
    <div class="pull-right">
        <div class="ui labeled button" tabindex="0" style="direction: ltr">
            <a href="/prject/{{ $reportDetails->project_id }}/report/{{ $reportDetails->id }}/print" class="ui red button">
                <i class="fa fa-file-pdf-o"></i>  &nbsp;{{ trans('pmis.Export') }}
            </a>
        </div>
    </div>
@stop
@section('alert-message')
    @if(session('message_title'))
        <div role="alert" class="alert {{ session('message_class') }}">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
        </div>
    @endif
@stop
@section('main-content')
    <div class="container-fluid container-fullw" style="background-color: #FAFAFA !important">
        <div class="row">
            <div class="col-sm-4">
                <div class="panel panel-white no-radius text-center" style="min-height: 197px">
                    <div class="panel-body">
                        <span class="fa-stack fa-2x">
                            <i class="cloud icon"></i>
                        </span>
                        <h2 class="StepTitle"> {{trans('pmis.Weather')}}</h2>
                        <p class="text-small">
                            {{trans('pmis.Date')}}: {{$reportDetails->date}} | {{trans('pmis.time')}}
                            : {{$reportDetails->time}} |
                            {{trans('pmis.Weather')}}: {{trans("pmis.$reportDetails->weather")}} |
                            {{trans('pmis.Temprature')}}: {{$reportDetails->temprature}}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-white no-radius text-center" style="min-height: 197px">
                    <div class="panel-body">
                        <span class="fa-stack fa-2x">
                            <i class="line chart icon"></i>
                        </span>
                        <h2 class="StepTitle">{{trans('pmis.Project_Status')}}</h2>
                        @if($reportDetails->status == true)
                            <p class="text-small">
                            <div class="ui indicating progress" data-value="1" data-total="100" id="example4">
                                <div class="bar" style="height: 100%">
                                    <div class="progress" style="background-color: transparent !important;"></div>
                                </div>
                            </div>
                            </p>
                        @endif
                        @if($reportDetails->status == false)
                            <p class="text-small">
                                {{trans('pmis.Stopped')}}

                                {{str_limit('Taki Manage Orders tool provides a view of all your orders mewr mamam fmadmf fmasdmf fdmasm.', 88, $end = '...')}}
                            </p>
                        @endif

                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-white no-radius text-center" style="min-height: 197px">
                    <div class="panel-body">
                        <span class="fa-stack fa-2x">
                            <i class="user icon"></i>
                        </span>
                        <h2 class="StepTitle">{{trans('pmis.Supervisor')}}</h2>
                        <p class="text-small">
                            {{trans('pmis.Supervisor_Name')}}: {{$supervisor->first_name}} {{$supervisor->last_name}} | {{trans('pmis.Phone_Number')}}
                            : {{$supervisor->phone}} |
                            {{trans('pmis.Email')}}: {{$supervisor->email}} |
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid container-fullw bg-white">
        <div class="tabbable">
            <ul class="nav nav-tabs" id="myTab1">
                <li class="active">
                    <a data-toggle="tab" href="#myTab1_example1" aria-expanded="true">
                        <i class="flaticon-people flt-icon-for-tab"></i>&nbsp;
                        {{trans('pmis.Ongoing_Activities')}}
                    </a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#myTab1_example2" aria-expanded="false">
                        <i class="flaticon-man flt-icon-for-tab"></i>
                        {{trans('pmis.Deficiencies')}}
                    </a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#myTab1_example3" aria-expanded="false">
                        <i class="flaticon-cone flt-icon-for-tab"></i>
                        {{trans('pmis.Site_Safety')}}
                    </a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#myTab1_example4" aria-expanded="false">
                        <i class="flaticon-science-2 flt-icon-for-tab"></i>
                        {{trans('pmis.Construction_Material_Tests')}}
                    </a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#myTab1_example5" aria-expanded="false">
                        <i class="flaticon-web flt-icon-for-tab"></i>
                        {{trans('pmis.Employees_area')}}
                    </a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#myTab1_example6" aria-expanded="false">
                        <i class="flaticon-tool-9 flt-icon-for-tab"></i>
                        {{trans('pmis.Equipment_area')}}
                    </a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#myTab1_example7" aria-expanded="false">
                        <i class="flaticon-wall flt-icon-for-tab"></i>
                        {{trans('pmis.Construction_Material_On_site')}}
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="myTab1_example1" class="tab-pane fade active in">
                    <div class="row">
                        <div class="col-md-12">
                            @foreach($workActivities as $activity)
                                <div class="panel-group accordion" id="accordion" style="margin-bottom: 7px">
                                    <div class="panel panel-white">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                               href="#{{$id = "list1" . rand()}}">
                                                <h3 style="color: #4183c4;"> {{ $activity->type }} </h3>
                                                <p>{{ $activity->description }} </p>
                                            </a>
                                        </div>
                                        <div id="{{ $id }}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    @foreach($activity->activityUpload as $image)
                                                        <div class="col-sm-6 col-md-3">
                                                            <a class="thumbnail" href="#">
                                                                <img alt="..." src="{{asset($image->path)}}">
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div id="myTab1_example2" class="tab-pane fade">
                    <div class="row">
                        <div class="col-md-12">
                            @foreach($workDefacts as $defact)
                                <div class="panel-group accordion" id="accordion" style="margin-bottom: 7px">
                                    <div class="panel panel-white">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                               href="#{{$id = "list1" . rand()}}">
                                                <h3 style="color: #4183c4;"> {{ $defact->type }} </h3>
                                                <p>{{ $defact->description }} </p>
                                            </a>
                                        </div>
                                        <div id="{{ $id }}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    @foreach($defact->defactsUpload as $image)
                                                        <div class="col-sm-6 col-md-3">
                                                            <a class="thumbnail" href="#">
                                                                <img alt="..." src="{{asset($image->path)}}">
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="cl-effect-13 pull-right">
                                <a href="" data-target=".bs-example-modal-lg" data-toggle="modal">
                                    {{trans('pmis.All_Deficiencies')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="myTab1_example3" class="tab-pane fade">
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset>
                                <legend> {{trans('pmis.safety_measure')}}</legend>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->eye_protection != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                <label for="checkbox11" class="margin-left-5">
                                                    {{trans('pmis.eye_protection')}}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->helmet != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                <label for="checkbox11" class="margin-left-5">
                                                    {{trans('pmis.helmet')}}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->ear_protection != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                <label for="checkbox11" class="margin-left-5">
                                                    {{trans('pmis.ear_protection')}}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->respiratory_protection != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                <label for="checkbox11" class="margin-left-5">
                                                    {{trans('pmis.respiratory_protection')}}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row margin-top-15">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->safety_boots != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                <label for="checkbox11" class="margin-left-5">
                                                    {{trans('pmis.safety_boots')}}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->safety_gloves != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                <label for="checkbox11" class="margin-left-5">
                                                    {{trans('pmis.safety_gloves')}}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->safety_uniforms != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                <label for="checkbox11" class="margin-left-5">
                                                    {{trans('pmis.safety_uniforms')}}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->safe_face_on != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                <label for="checkbox11" class="margin-left-5">
                                                    {{trans('pmis.safe_face_on')}}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row margin-top-15">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->tape_barriers != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                <label for="checkbox11" class="margin-left-5">
                                                    {{trans('pmis.tape_barriers')}}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset>
                                <legend> {{trans('pmis.sign')}}</legend>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->symptoms_of_obstacles != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                    <label for="checkbox11" class="margin-left-5">
                                                        {{trans('pmis.symptoms_of_obstacles')}}
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="">
                                                    <img height="20" width="20" src="{{ $safetyMeasures->warning_signs != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                    <label for="checkbox11" class="margin-left-5">
                                                        {{trans('pmis.warning_signs')}}
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="">
                                                    <img height="20" width="20" src="{{ $safetyMeasures->mandatory_signs != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                    <label for="checkbox11" class="margin-left-5">
                                                        {{trans('pmis.mandatory_signs')}}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row margin-top-15">
                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <div class="">
                                                    <img height="20" width="20" src="{{ $safetyMeasures->first_aid_sign != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                    <label for="checkbox11" class="margin-left-5">
                                                        {{trans('pmis.first_aid_sign')}}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset>
                                <legend>{{trans('pmis.Environmental_measures')}}</legend>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->risks_of_chemicals != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                <label for="checkbox11" class="margin-left-5">
                                                    {{trans('pmis.risks_of_chemicals')}}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->noise_pollution != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                <label for="checkbox11" class="margin-left-5">
                                                    {{trans('pmis.noise_pollution')}}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->asphalting_disadvantages != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                <label for="checkbox11" class="margin-left-5">
                                                    {{trans('pmis.asphalting_disadvantages')}}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="">
                                                <img height="20" width="20" src="{{ $safetyMeasures->concrete_disadvantages != 0 ?
                                                               asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                                                <label for="checkbox11" class="margin-left-5">
                                                    {{trans('pmis.concrete_disadvantages')}}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>

                <div id="myTab1_example4" class="tab-pane fade">
                    <div class="row">
                        <div class="col-md-12">
                            @foreach($buildingMaterialTests as $buildingMaterialTes)
                                <div class="panel-group accordion" id="accordion" style="margin-bottom: 7px">
                                    <div class="panel panel-white">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#{{$id = "list1" . rand()}}">
                                                <h3 style="color: #4183c4;"> {{ $buildingMaterialTes->type }}</h3>
                                                <p><span class="block">( {{trans("pmis.$buildingMaterialTes->result")}} )</span></br>{{ $buildingMaterialTes->description }} </p>
                                            </a>
                                        </div>
                                        <div id="{{ $id }}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    @foreach($buildingMaterialTes->uploads as $image)
                                                        <div class="col-sm-6 col-md-3">
                                                            <a class="thumbnail" href="#">
                                                                <img alt="..." src="{{asset($image->path)}}">
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div id="myTab1_example5" class="tab-pane fade">
                    <table class="ui fixed striped blue table">
                        <thead>
                        <tr>
                            <th><span class="t-rtl">{{trans('pmis.project_manaegr')}}</span></th>
                            <th><span class="t-rtl">{{trans('pmis.Quality_Engineer')}}</span></th>
                            <th><span class="t-rtl">{{trans('pmis.Safety_Engineer')}}</span></th>
                            <th><span class="t-rtl">{{trans('pmis.Foreman')}}</span></th>
                            <th><span class="t-rtl">{{trans('pmis.skilled_worker')}}</span></th>
                            <th><span class="t-rtl">{{trans('pmis.ordinary_workers')}}</span></th>
                            <th><span class="t-rtl">{{trans('pmis.escort')}}</span></th>
                            <th><span class="t-rtl">{{trans('pmis.visitor')}}</span></th>
                            <th><span class="t-rtl">{{trans('pmis.Other')}}</span></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($projectWorkers as $pworker)
                            <tr>
                                <td><span class="t-rtl">{{$pworker->projectmanager}}</span></td>
                                <td><span class="t-rtl">{{$pworker->control_engineer}}</span></td>
                                <td><span class="t-rtl">{{$pworker->safety_engineer}}</span></td>
                                <td><span class="t-rtl">{{$pworker->worker_leader}}</span></td>
                                <td><span class="t-rtl">{{$pworker->smart_worker}}</span></td>
                                <td><span class="t-rtl">{{$pworker->ordinary_worker}}</span></td>
                                <td><span class="t-rtl">{{$pworker->gard}}</span></td>
                                <td><span class="t-rtl">{{$pworker->visitor}}</span></td>
                                <td><span class="t-rtl">{{$pworker->other}}</span></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="myTab1_example6" class="tab-pane fade">
                    <table class="ui fixed striped blue table">
                        <thead>
                        <tr>
                            <th><span class="t-rtl">{{trans('pmis.type')}}</span></th>
                            <th><span class="t-rtl">{{trans('pmis.quality')}}</span></th>
                            <th><span class="t-rtl">{{trans('pmis.comment')}}</span></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($equipments as $equipment)
                            <tr>
                                <td><span class="t-rtl">{{$equipment->type}}</span></td>
                                <td><span class="t-rtl">{{$equipment->quantity}}</span></td>
                                <td><span class="t-rtl">{{$equipment->comment}}</span></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="myTab1_example7" class="tab-pane fade">
                    <table class="ui fixed striped blue table">
                        <thead>
                        <tr>
                            <th><span class="t-rtl">{{trans('pmis.type_material')}}</span></th>
                            <th><span class="t-rtl">{{trans('pmis.Estimated_value')}}</span></th>
                            <th><span class="t-rtl">{{trans('pmis.quality')}}</span></th>
                            <th><span class="t-rtl">{{trans('pmis.comment')}}</span></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($buildingEquipments as $equipment)
                            <tr>
                                <td><span class="t-rtl">{{$equipment->type}}</span></td>
                                <td><span class="t-rtl">{{$equipment->estim_value}}</span></td>
                                <td><span class="t-rtl">{{$equipment->quality}}</span></td>
                                <td><span class="t-rtl">{{$equipment->comment}}</span></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">All Deficiencies</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @foreach($projecDefacts as $projecDefact)
                            <div class="panel-group accordion" id="accordion" style="margin-bottom: 7px">
                                <div class="panel panel-white">
                                    <div class="panel-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                           href="#{{$id = "list1" . rand()}}">
                                            <h3 style="color: #4183c4;"> {{ $projecDefact->type }} </h3>
                                            <p>{{ $projecDefact->description }} </p>
                                        </a>
                                    </div>
                                    <div id="{{ $id }}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                @foreach($projecDefact->uploads as $image)
                                                    <div class="col-sm-6 col-md-3">
                                                        <a class="thumbnail" href="#">
                                                            <img alt="..." src="{{asset($image->path)}}">
                                                        </a>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <button class="btn btn-wide btn-success btn-squared pull-right"
                                                    type="button">
                                                Approve <i class="fa fa-angle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
@if($reportDetails->status == true)
    <script>
        var percentage = {{$reportDetails->status_value}}
            $('#example4').progress({percent: percentage});
    </script>
@endif
@endpush
