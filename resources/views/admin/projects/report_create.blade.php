@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 2, 'subActive' => 1])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans('pmis.Creating_report'), 'page' => 'Reports', 'current' => 'projects'])
@stop
@section('main-content')
    <form action="{{action('ProjectController@storeReport')}}" method="post" role="form" class="smart-wizard" id="form">
        {!! csrf_field() !!}
        <input type="hidden" name="project_id" value="{{ $project->id }}">
        <!-- start: WIZARD DEMO -->
        <div class="container-fluid container-fullw bg-white">
            <div class="row">
                <div class="col-md-12">
                    <!-- start: WIZARD FORM -->
                    <div id="wizard" class="swMain">
                        <!-- start: WIZARD SEPS -->
                        <ul>
                            <li>
                                <a href="#step-1">
                                    <div class="stepNumber">
                                        1
                                    </div>
                                    <span class="stepDesc"><small>{{trans('pmis.project_details')}}</small></span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-2">
                                    <div class="stepNumber">
                                        2
                                    </div>
                                    <span class="stepDesc"> <small>{{trans('pmis.supervision_char')}} </small></span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-3">
                                    <div class="stepNumber">
                                        3
                                    </div>
                                    <span class="stepDesc"> <small> {{trans('pmis.under_activities')}} </small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-4">
                                    <div class="stepNumber">
                                        4
                                    </div>
                                    <span class="stepDesc"> <small> {{trans('pmis.Deficiencies_work')}}</small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-5">
                                    <div class="stepNumber">
                                        5
                                    </div>
                                    <span class="stepDesc"> <small>{{trans('pmis.Safeguards_environmental')}} </small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-6">
                                    <div class="stepNumber">
                                        6
                                    </div>
                                    <span class="stepDesc"> <small>{{trans('pmis.Building_testing')}} </small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-7">
                                    <div class="stepNumber">
                                        7
                                    </div>
                                    <span class="stepDesc"> <small></small>{{trans('pmis.Employees_area')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-8">
                                    <div class="stepNumber">
                                        8
                                    </div>
                                    <span class="stepDesc"> <small> {{trans('pmis.Equipment_area')}}</small> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-9">
                                    <div class="stepNumber">
                                        9
                                    </div>
                                    <span class="stepDesc"> <small></small> {{trans('pmis.Construction_area')}}</span>
                                </a>
                            </li>
                        </ul>
                        <!-- end: WIZARD SEPS -->
                        <!-- start: WIZARD STEP 1 -->
                        <div id="step-1">
                            <fieldset>
                                <legend>
                                    {{trans('pmis.project_details')}}
                                </legend>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>
                                            {{trans('pmis.project_name')}} <span class="symbol"></span>
                                        </label>
                                        <div class="form-group">

                                            <input placeholder="" name="name" class="form-control" type="text" disabled
                                                   value="{{$project->name}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            {{trans('pmis.contractor')}} <span class="symbol"></span>
                                        </label>
                                        <div class="form-group">

                                            <input placeholder="" name="contractor" class="form-control" type="text"
                                                   disabled value="{{$project->contractor}}">
                                        </div>
                                    </div>


                                    <div class="col-md-4">
                                        <label>
                                            {{trans('pmis.contractual')}} <span class="symbol"></span>
                                        </label>
                                        <div class="form-group">
                                            <input placeholder="" name="contract" class="form-control" type="text"
                                                   disabled value="{{$project->contract}}">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label>
                                            {{trans('pmis.owner')}} <span class="symbol"></span>
                                        </label>
                                        <div class="form-group">
                                            <input placeholder="" name="owner" class="form-control" type="text" disabled
                                                   value="{{$project->owner}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            {{trans('pmis.location')}} <span class="symbol"></span>
                                        </label>
                                        <div class="form-group">
                                            <input placeholder="" name="location" class="form-control" type="text"
                                                   disabled value="{{$project->area}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            {{trans('pmis.district')}} <span class="symbol"></span>
                                        </label>
                                        <div class="form-group">
                                            <input placeholder="" name="district" class="form-control" type="text"
                                                   disabled value="{{$project->district}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            {{trans('pmis.transit')}} <span class="symbol"></span>
                                        </label>
                                        <div class="form-group">
                                            <input placeholder="" name="passway" class="form-control" type="text"
                                                   disabled value="{{$project->passway}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            <span class="symbol"></span>
                                            {{trans('pmis.contract_date')}} <span class="symbol"></span>
                                        </label>
                                        <div class="form-group">
                                            <input name="contract_date"
                                                   class="form-control" type="text" disabled
                                                   value="{{$project->contract_date}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            {{trans('pmis.Start_Date')}} <span class="symbol"></span>
                                        </label>
                                        <div class="form-group">
                                            <input name="start_date"
                                                   class="form-control" type="text" disabled
                                                   value="{{$project->start_date}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>
                                            {{trans('pmis.actual_date')}} <span class="symbol"></span>
                                        </label>
                                        <div class="form-group">
                                            <input name="start_date"
                                                   class="form-control" type="text" disabled
                                                   value="{{$project->actual_date}}">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label>
                                            {{trans('pmis.planned_date')}} <span class="symbol"></span>
                                        </label>
                                        <div class="form-group">
                                            <input name="start_date"
                                                   class="form-control" type="text" disabled
                                                   value="{{$project->planned_date}}">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label>
                                            {{trans('pmis.status')}} <span class="symbol"></span>
                                        </label>
                                        <div class="form-group">
                                            <div class="clip-radio radio-primary status">
                                                <input id="stop" name="status" value="stop" type="radio">
                                                <label for="stop">
                                                    {{trans('pmis.stopped')}}
                                                </label>
                                                <input id="progress" name="status" value="progress" checked="checked"
                                                       type="radio">
                                                <label for="progress">
                                                    {{trans('pmis.under_work')}}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group status-element">
                                            {{--<input id="demo1" type="text" value="{{$project->status_value}}"--}}
                                                   {{--name="status_value" touchspin data-min="{{$project->status_value}}"--}}
                                                   {{--data-max="100" class="touchspin">--}}
                                        <textarea class="form-control autosize area-animated hidden"
                                                  rows="7"
                                                  name="reason_why_stop">{{$project->reasson_why_stop}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group">
                                <button class="btn btn-primary btn-o next-step btn-wide pull-right">
                                    {{trans('pmis.next')}} <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </div>
                        <!-- end: WIZARD STEP 1 -->
                        <!-- start: WIZARD STEP 2 -->
                        <div id="step-2">
                            <fieldset>
                                <legend>
                                    {{trans('pmis.supervision_details')}}
                                </legend>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            {{trans('pmis.name')}}
                                        </label>
                                        <input class="form-control supervisor-name" placeholder="" type="text"
                                               name="name" disabled
                                               value="{{ $user->first_name }} {{ $user->last_name }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            {{trans('pmis.Phone_Number')}}
                                        </label>
                                        <input class="form-control supervisor-phone" placeholder="" type="text"
                                               name="phone" disabled value="{{ $user->phone }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>
                                            {{trans('pmis.Email')}}
                                        </label>
                                        <input class="form-control supervisor-email" placeholder="" type="text"
                                               name="email" disabled value="{{ $user->email }}">
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>
                                    {{trans('pmis.date_supervision')}}
                                </legend>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            {{trans('pmis.date')}}
                                        </label>
                                        <input class="form-control datepicker" type="text" name="supervision_date">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            {{trans('pmis.clock')}}
                                        </label>
                                        <input type="text" id="timepicker-default" class="form-control"
                                               name="supervision_time">
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>
                                    {{trans('pmis.climate_controle')}}
                                </legend>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            {{trans('pmis.climate')}}
                                        </label>
                                        <select class="form-control" name="weather">
                                            <option value="sunny">{{trans('pmis.sunny')}}</option>
                                            <option value="mostly_sunny">{{trans('pmis.mostly_sunny')}}</option>
                                            <option value="showers">{{trans('pmis.showers')}}</option>
                                            <option value="thunderstorms">{{trans('pmis.thunderstorms')}}</option>
                                            <option value="snowy">{{trans('pmis.snowy')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            {{trans('pmis.temperature')}}
                                        </label>
                                        <input class="form-control" placeholder="" type="text" name="temperature"
                                               value="55.96/19.43">
                                    </div>
                                </div>
                            </fieldset>
                            </fieldset>
                            <div class="form-group">
                                <button class="btn btn-primary btn-o back-step btn-wide pull-left" type="button">
                                    <i class="fa fa-circle-arrow-left"></i> {{trans('pmis.Back')}}
                                </button>
                                <button class="btn btn-primary btn-o next-step btn-wide pull-right" type="button">
                                    {{trans('pmis.next')}} <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </div>
                        <!-- end: WIZARD STEP 2 -->
                        <!-- start: WIZARD STEP 3 -->
                        <div id="step-3">
                            <fieldset>
                                <legend>
                                    {{trans('pmis.under_activities')}}
                                </legend>
                                <div class="panel panel-white">
                                    <div class="panel-body">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="form-field-select-1">
                                                    {{trans('pmis.Type_Activity')}}
                                                </label>
                                                <select class="form-control activity_list" name="activity_type[]"
                                                        onchange="projectPartialList(this.value,'activity');">
                                                    <option value="0"
                                                            selected="true">{{trans('pmis.select_activity')}}</option>
                                                    @foreach($activity_type as $list)
                                                        <option value="{{$list->name}}">{{$list->name}}</option>
                                                    @endforeach
                                                    <option value="other">&#xf067; {{trans('pmis.add_other')}}</option>
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="form-field-select-1">
                                                {{trans('pmis.Activity_description')}}
                                            </label>
                                            <textarea class="form-control autosize area-animated" rows="6"
                                                      name="activity_description[]"></textarea>
                                        </div>
                                    </div>
                                    <div class="activity_images">
                                    </div>

                                    <div class="panel-body with-image">
                                        <div id="activity_image_0" class="dropzone"></div>
                                    </div>

                                </div>
                                <button class="btn btn-primary pull-right btn-o btn-xs btn-add-new-activity"
                                        type="button">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </fieldset>
                            <div class="form-group">
                                <button class="btn btn-primary btn-o back-step btn-wide pull-left" type="button">
                                    <i class="fa fa-circle-arrow-left"></i> {{trans('pmis.Back')}}
                                </button>
                                <button class="btn btn-primary btn-o next-step btn-wide pull-right" type="button">
                                    {{trans('pmis.next')}} <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </div>
                        <!-- end: WIZARD STEP 3 -->
                        <!-- start: WIZARD STEP 4 -->
                        <div id="step-4">
                            <fieldset>
                                <legend>
                                    {{trans('pmis.Deficiencies_work')}}
                                </legend>
                                <div class="panel panel-white">

                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label>
                                                {{trans('pmis.Deficiencies')}}
                                            </label>
                                            <input class="form-control" placeholder="" type="text" name="defect_type[]">
                                        </div>
                                        <div class="form-group">
                                            <label for="form-field-select-1">
                                                {{trans('pmis.Deficiencies_description')}}
                                            </label>
                                            <textarea class="form-control autosize area-animated" rows="6"
                                                      name="defect_description[]"></textarea>
                                        </div>
                                    </div>
                                    <div class="defect_images">
                                    </div>
                                    <div class="panel-body with-image">
                                        <div id="defect_image_0" class="dropzone"></div>
                                    </div>
                                </div>
                                <button class="btn btn-primary pull-right btn-o btn-xs btn-add-new-defect"
                                        type="button">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </fieldset>
                            <div class="form-group">
                                <button class="btn btn-primary btn-o back-step btn-wide pull-left" type="button">
                                    <i class="fa fa-circle-arrow-left"></i> {{trans('pmis.Back')}}
                                </button>
                                <button class="btn btn-primary btn-o next-step btn-wide pull-right" type="button">
                                    {{trans('pmis.next')}} <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </div>
                        <!-- end: WIZARD STEP 4 -->
                        <!-- start: WIZARD STEP 5 -->
                        <div id="step-5">
                            <fieldset>
                                <legend>
                                    {{trans('pmis.Safeguards_environmental')}}
                                </legend>
                                <div class="panel panel-white">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend> {{trans('pmis.safety_measure')}}</legend>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox1"
                                                                           name="eye_protection">
                                                                    <label for="checkbox1">
                                                                        {{trans('pmis.eye_protection')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox2" name="helmet">
                                                                    <label for="checkbox2">
                                                                        {{trans('pmis.helmet')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox3"
                                                                           name="ear_protection">
                                                                    <label for="checkbox3">
                                                                        {{trans('pmis.ear_protection')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox4"
                                                                           name="respiratory_protection">
                                                                    <label for="checkbox4">
                                                                        {{trans('pmis.respiratory_protection')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox5"
                                                                           name="safety_boots">
                                                                    <label for="checkbox5">
                                                                        {{trans('pmis.safety_boots')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox6"
                                                                           name="safety_gloves">
                                                                    <label for="checkbox6">
                                                                        {{trans('pmis.safety_gloves')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox7"
                                                                           name="safety_uniforms">
                                                                    <label for="checkbox7">
                                                                        {{trans('pmis.safety_uniforms')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox8"
                                                                           name="safe_face_on">
                                                                    <label for="checkbox8">
                                                                        {{trans('pmis.safe_face_on')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox9"
                                                                           name="tape_barriers">
                                                                    <label for="checkbox9">
                                                                        {{trans('pmis.tape_barriers')}}
                                                                    </label>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox10"
                                                                           name="symptoms_of_obstacles">
                                                                    <label for="checkbox10">
                                                                        {{trans('pmis.symptoms_of_obstacles')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox11"
                                                                           name="warning_signs">
                                                                    <label for="checkbox11">
                                                                        {{trans('pmis.warning_signs')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox12"
                                                                           name="mandatory_signs">
                                                                    <label for="checkbox12">
                                                                        {{trans('pmis.mandatory_signs')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox13"
                                                                           name="first_aid_sign">
                                                                    <label for="checkbox13">
                                                                        {{trans('pmis.first_aid_sign')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <fieldset>
                                                    <legend> {{trans('pmis.Environmental_measures')}}</legend>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox14"
                                                                           name="risks_of_chemicals">
                                                                    <label for="checkbox14">
                                                                        {{trans('pmis.risks_of_chemicals')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox15"
                                                                           name="noise_pollution">
                                                                    <label for="checkbox15">
                                                                        {{trans('pmis.noise_pollution')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox16"
                                                                           name="asphalting_disadvantages">
                                                                    <label for="checkbox16">
                                                                        {{trans('pmis.asphalting_disadvantages')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkbox clip-check check-primary checkbox-inline">
                                                                    <input type="checkbox" id="checkbox17"
                                                                           name="concrete_disadvantages">
                                                                    <label for="checkbox17">
                                                                        {{trans('pmis.concrete_disadvantages')}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group">
                                <button class="btn btn-primary btn-o back-step btn-wide pull-left" type="button">
                                    <i class="fa fa-circle-arrow-left"></i> {{trans('pmis.Back')}}
                                </button>
                                <button class="btn btn-primary btn-o next-step btn-wide pull-right" type="button">
                                    {{trans('pmis.next')}} <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </div>
                        <!-- end: WIZARD STEP 5 -->
                        <!-- start: WIZARD STEP 6 -->
                        <div id="step-6">
                            <fieldset>
                                <legend>
                                    {{trans('pmis.Building_testing')}}

                                </legend>
                                <div class="panel panel-white">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="form-field-select-1">
                                                {{trans('pmis.name_test')}}
                                            </label>
                                            <select class="form-control material_test_list" name="material_test_type[]"
                                                    onchange="projectPartialList(this.value, 'material_test');">
                                                <option value="0" selected="true">Select Material Test Type</option>
                                                @foreach($material_test_type as $list)
                                                    <option value="{{$list->name}}">{{$list->name}}</option>
                                                @endforeach
                                                <option value="other">&#xf067; {{trans('pmis.add_other')}}</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label class="block">
                                                {{trans('pmis.test_result')}}
                                            </label>
                                            <div class="clip-radio radio-primary">
                                                <input type="radio" id="failed" name="material_test_result[]"
                                                       value="failed">
                                                <label for="failed">
                                                    {{trans('pmis.failed')}}
                                                </label>
                                                <input type="radio" id="passed" name="material_test_result[]"
                                                       value="passed" checked="checked">
                                                <label for="passed">
                                                    {{trans('pmis.passed')}}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="form-field-select-1">
                                                {{trans('pmis.Test_Description')}}
                                            </label>
                                            <textarea class="form-control autosize area-animated" rows="6"
                                                      name="material_test_description[]"></textarea>
                                        </div>
                                    </div>
                                    <div class="material_images">
                                    </div>
                                    <div class="panel-body with-image">
                                        <div id="material_image_0" class="dropzone"></div>
                                    </div>
                                </div>
                                <button class="btn btn-primary pull-right btn-o btn-xs btn-add-new-material"
                                        type="button">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </fieldset>
                            <div class="form-group">
                                <button class="btn btn-primary btn-o back-step btn-wide pull-left" type="button">
                                    <i class="fa fa-circle-arrow-left"></i> {{trans('pmis.Back')}}
                                </button>
                                <button class="btn btn-primary btn-o next-step btn-wide pull-right" type="button">
                                    {{trans('pmis.next')}} <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </div>
                        <!-- end: WIZARD STEP 6 -->
                        <!-- start: WIZARD STEP 7 -->
                        <div id="step-7">
                            <fieldset>
                                <legend>
                                    {{trans('pmis.Employees_area')}}
                                </legend>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>
                                                {{trans('pmis.project_manaegr')}}
                                            </label>
                                            <input name="project_manager" class="number">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">
                                                {{trans('pmis.Quality_Engineer')}}
                                            </label>
                                            <input name="control_manager" class="number">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>
                                                {{trans('pmis.Safety_Engineer')}}
                                            </label>
                                            <input name="safety_manager" class="number">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">
                                                {{trans('pmis.Foreman')}}
                                            </label>
                                            <input name="worker_leader" class="number">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>
                                                {{trans('pmis.skilled_worker')}}
                                            </label>
                                            <input name="smart_worker" class="number">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">
                                                {{trans('pmis.Unskilled_workers')}}
                                            </label>
                                            <input name="ordinary_worker" class="number">
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>
                                                {{trans('pmis.escort')}}
                                            </label>
                                            <input name="guard" class="number">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>
                                                {{trans('pmis.visitor')}}
                                            </label>
                                            <input name="visitor" class="number">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>
                                                {{trans('pmis.Other')}}
                                            </label>
                                            <input name="other" class="number">
                                        </div>
                                    </div>


                                </div>
                            </fieldset>
                            <div class="form-group">
                                <button class="btn btn-primary btn-o back-step btn-wide pull-left" type="button">
                                    <i class="fa fa-circle-arrow-left"></i> {{trans('pmis.Back')}}
                                </button>
                                <button class="btn btn-primary btn-o next-step btn-wide pull-right" type="button">
                                    {{trans('pmis.next')}} <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </div>
                        <!-- end: WIZARD STEP 7 -->
                        <!-- start: WIZARD STEP 8 -->
                        <div id="step-8">
                            <fieldset>
                                <legend>
                                    {{trans('pmis.Equipment_area')}}
                                </legend>
                                <div class="panel panel-white">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="form-field-select-1">
                                                        {{trans('pmis.type')}}
                                                    </label>
                                                    <select class="form-control equipment_list" name="equipment_type[]"
                                                            onchange="projectPartialList(this.value,'equipment');">
                                                        <option value="0" selected="true">Select Equipment Type</option>
                                                        @foreach($equipment_type as $list)
                                                            <option value="{{$list->name}}">{{$list->name}}</option>
                                                        @endforeach
                                                        <option value="other">
                                                            &#xf067; {{trans('pmis.add_other')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        {{trans('pmis.Number')}}
                                                    </label>
                                                    <input type="text" value="0" name="" class="form-control"
                                                           name="equipment_quantity[]">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="form-field-select-1">
                                                {{trans('pmis.considerations')}}
                                            </label>
                                            <textarea class="form-control autosize area-animated" rows="6"
                                                      name="equipment_description[]"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary pull-right btn-o btn-xs btn-add-new" type="button">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </fieldset>
                            <div class="form-group">
                                <button class="btn btn-primary btn-o back-step btn-wide pull-left" type="button">
                                    <i class="fa fa-circle-arrow-left"></i> {{trans('pmis.Back')}}
                                </button>
                                <button class="btn btn-primary btn-o next-step btn-wide pull-right" type="button">
                                    {{trans('pmis.next')}} <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </div>
                        <!-- end: WIZARD STEP 8 -->
                        <!-- start: WIZARD STEP 9 -->
                        <div id="step-9">
                            <fieldset>
                                <legend>
                                    {{trans('pmis.Construction_area')}}
                                </legend>
                                <div class="panel panel-white">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="form-field-select-1">
                                                        {{trans('pmis.type_material')}}
                                                    </label>
                                                    <select class="form-control building_equipment_list"
                                                            name="building_equipment_type[]"
                                                            onchange="projectPartialList(this.value,'building_equipment');">
                                                        <option value="0"
                                                                selected="true">{{trans('pmis.select_equipment_type')}}</option>
                                                        @foreach($building_equipment_type as $list)
                                                            <option value="{{$list->name}}">{{$list->name}}</option>
                                                        @endforeach
                                                        <option value="other">
                                                            &#xf067; {{trans('pmis.add_other')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label class="control-label">
                                                        {{trans('pmis.Estimated_value')}}
                                                    </label>
                                                    <input type="text" value="0" class="form-control"
                                                           name="building_equipment_quantity[]">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="form-field-select-1">
                                                        {{trans('pmis.quality')}}
                                                    </label>
                                                    <select class="form-control" name="building_equipment_quality[]">
                                                        <option value="excellent">{{trans('pmis.Excellent')}}</option>
                                                        <option value="good">{{trans('pmis.Good')}}</option>
                                                        <option value="fair">{{trans('pmis.Fair')}}</option>
                                                        <option value="bad">{{trans('pmis.Bad')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label class="control-label">
                                                {{trans('pmis.considerations')}}
                                            </label>
                                            <textarea class="form-control autosize area-animated" rows="6"
                                                      name="building_equipment_description[]"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary pull-right btn-o btn-xs btn-add-new" type="button">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </fieldset>
                            <div class="form-group">
                                <button class="btn btn-primary btn-o back-step btn-wide pull-left" type="button">
                                    <i class="fa fa-circle-arrow-left"></i> {{trans('pmis.Back')}}
                                </button>
                                <button type="submit" class="btn btn-wide btn-success btn-squared pull-right">

                                    {{trans('pmis.save')}} <i class="fa fa-angle-right"></i>
                                </button>
                            </div>
                        </div>
                        <!-- end: WIZARD STEP 9 -->
                    </div>
                    <!-- end: WIZARD FORM -->
                </div>
            </div>
        </div>
        <!-- start: WIZARD DEMO -->
    </form>
    <div class="modal fade" id="project_partial_list_modal" tabindex="-1" role="dialog"
         aria-labelledby="Project Partial List Modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">{{trans('pmis.Adding_Item')}}</h4>
                </div>
                <div class="modal-body">
                    <form action="" class="form-horizontal" id="project_partial_list_form">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="new_item">
                                {{trans('pmis.item_type')}}
                            </label>
                            <div class="col-md-8">
                                <input type="text" placeholder="{{trans('pmis.Adding_Item')}}" class="form-control"
                                       name="name">
                                <input type="hidden" name="type" value="">
                            </div>
                        </div>
                        <div class="form-group container">
                            <div class="col-md-2 pull-right">
                                <button class="btn btn-o btn-primary" type="button">
                                    {{trans('pmis.save')}}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('validator')
    {!! JsValidator::formRequest('App\Http\Requests\ReportRequest') !!}
@endsection
