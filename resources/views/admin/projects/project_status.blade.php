
@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 2, 'subActive' => 1])
@stop
@section('page-title')
    @hasrole('admin')
    @include('partials.breadcrumb', ['pageTitle' => trans('pmis.change_project_status'), 'page' => trans('pmis.projects'), 'current' => trans('pmis.details')])
    @else
        @include('partials.breadcrumb', ['pageTitle' => trans('pmis.reports'), 'page' => trans('pmis.projects'), 'current' => trans('pmis.details')])
        @endhasrole
@stop
@section('alert-message')
    @if(session('message_title'))
        <div role="alert" class="alert {{ session('message_class') }}">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
        </div>
    @endif
@stop
@section('main-content')
    <div class="container-fluid container-fullw bg-white">
        @if(Auth::User()->can('edit_project'))
        @if($isPlannedAmount)
                <div class="alert alert-danger">
                    The Planned Amount has been already entered!
                </div>
        @else
        <div class="row">
            <div class="col-md-12">
                <form action="/project/details/{{$project->id}}/status/{{$project->status}}" method="post" role="form" class="project-form"
                      id="form">
                    {!! csrf_field() !!}
                    <input type="hidden" value="{{$project->available_fund}}" id="available_fund">
                    <div class="container-fluid container-fullw bg-white">
                        <div class="row">
                            <fieldset>
                                <legend>
                                    {{trans('pmis.project_status')}}
                                </legend>
                                <div class="row">

                                    <div class="col-md-4">
                                        <label>
                                            {{trans('pmis.project_status')}} <span class="symbol required" aria-required="true"></span>
                                        </label>
                                        <div class="form-group">
                                            {{$status = ""}}
                                            @if($project->status == 1)
                                                <?php
                                                $status='Design'
                                                ?>
                                            @elseif($project->status == 2)
                                                <?php
                                                $status='Procurement'
                                                ?>
                                            @elseif($project->status == 3)
                                                <?php
                                                $status='Construction'
                                                ?>
                                            @elseif($project->status == 4)
                                                <?php
                                                $status='Completed'
                                                ?>
                                            @endif
                                            <input placeholder="" name="project_status" disabled value="{{$status}}" class="form-control" type="text">
                                        </div>
                                    </div>


                                    {{-- Show Des to Jun if project status is construction --}}
                                    @if($project_status == 3)
                                        <div class="col-md-12">
                                            <fieldset>
                                                <input type="hidden" value="{{$project->addational_budget}}" id="additional_budget">
                                                <legend>{{trans('pmis.january')}}, {{trans('pmis.february')}} , {{trans('pmis.march')}}</legend>
                                                {{--Junuary--}}
                                                <div class="col-md-4">
                                                        <h4 class="label-center">
                                                            {{trans('pmis.january')}}
                                                        </h4>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.planned_amount')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder=""  name="jan_planned_amount" class="form-control actual_amount_input" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.percentage')}}
                                                         <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="jan_percentage" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                {{--Febuary--}}
                                                <div class="col-md-4">

                                                    <h4 class="label-center">
                                                        {{trans('pmis.february')}}
                                                    </h4>

                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.planned_amount')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="feb_planned_amount" class="form-control actual_amount_input" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.percentage')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="feb_percentage" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                {{--March--}}
                                                <div class="col-md-4">

                                                    <h4 class="label-center">
                                                        {{trans('pmis.march')}}
                                                    </h4>

                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.planned_amount')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="mar_planned_amount" class="form-control actual_amount_input" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.percentage')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="mar_percentage" class="form-control" type="text">
                                                    </div>
                                                </div>
                                            </fieldset>

                                            <fieldset>
                                                <legend>{{trans('pmis.april')}}, {{trans('pmis.may')}} , {{trans('pmis.june')}}</legend>
                                                {{--april--}}
                                                <div class="col-md-4">

                                                    <h4 class="label-center">
                                                        {{trans('pmis.april')}}
                                                    </h4>

                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.planned_amount')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="apr_planned_amount" class="form-control actual_amount_input" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.percentage')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="apr_percentage" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                {{--may--}}
                                                <div class="col-md-4">

                                                    <h4 class="label-center">
                                                        {{trans('pmis.may')}}
                                                    </h4>

                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.planned_amount')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="may_planned_amount" class="form-control actual_amount_input" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.percentage')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="may_percentage" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                {{--june--}}
                                                <div class="col-md-4">
                                                    <h4 class="label-center">{{trans('pmis.june')}}</h4>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.planned_amount')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="jun_planned_amount" class="form-control actual_amount_input" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.percentage')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="jun_percentage" class="form-control" type="text">
                                                    </div>
                                                </div>

                                            </fieldset>

                                            <fieldset>
                                                <legend>{{trans('pmis.july')}}, {{trans('pmis.august')}} , {{trans('pmis.september')}}</legend>
                                                {{--july--}}
                                                <div class="col-md-4">

                                                    <h4 class="label-center">
                                                        {{trans('pmis.july')}}
                                                    </h4>

                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.planned_amount')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="jul_planned_amount" class="form-control actual_amount_input" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.percentage')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="jul_percentage" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                {{--august--}}
                                                <div class="col-md-4">

                                                    <h4 class="label-center">
                                                        {{trans('pmis.august')}}
                                                    </h4>

                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.planned_amount')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="aug_planned_amount" class="form-control actual_amount_input" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.percentage')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="aug_percentage" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                {{--september--}}
                                                <div class="col-md-4">

                                                    <h4 class="label-center">
                                                        {{trans('pmis.september')}}
                                                    </h4>

                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.planned_amount')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                        </label>
                                                        <div class="form-group">
                                                            <input placeholder="" name="sep_planned_amount" class="form-control actual_amount_input" type="text">
                                                        </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.percentage')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="sep_percentage" class="form-control" type="text">
                                                    </div>
                                                </div>

                                            </fieldset>

                                            <fieldset>
                                                <legend>{{trans('pmis.october')}}, {{trans('pmis.november')}} , {{trans('pmis.december')}}</legend>
                                                {{--october--}}
                                                <div class="col-md-4">

                                                    <h4 class="label-center">
                                                        {{trans('pmis.october')}}
                                                    </h4>

                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.planned_amount')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="oct_planned_amount" class="form-control actual_amount_input" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.percentage')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="oct_percentage" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                {{--november--}}
                                                <div class="col-md-4">

                                                    <h4 class="label-center">
                                                        {{trans('pmis.november')}}
                                                    </h4>

                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.planned_amount')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="nov_planned_amount" class="form-control actual_amount_input" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.percentage')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="nov_percentage" class="form-control" type="text">
                                                    </div>
                                                </div>

                                                {{--december--}}
                                                <div class="col-md-4">

                                                    <h4 class="label-center">
                                                        {{trans('pmis.december')}}
                                                    </h4>

                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.planned_amount')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="dec_planned_amount" class="form-control actual_amount_input" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>
                                                        {{trans('pmis.percentage')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="dec_percentage" class="form-control" type="text">
                                                    </div>
                                                </div>

                                            </fieldset>

                                        </div>
                                    @endif


                                </div>
                            </fieldset>
                            <div class="form-group">
                                <button class="btn btn-primary btn-o next-step btn-wide pull-right">
                                    {{trans('pmis.save')}} <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endif
    @endif
    </div>
@stop
