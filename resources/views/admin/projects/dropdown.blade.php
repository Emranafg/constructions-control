@foreach($users as $user)
    <option value="{{ $user->id }}"

            @foreach($projectManagers as $manager)
                @if($manager->id == $user->id)
            style="background-color:#DDD;" selected
                    @endif
                @endforeach

    >{{ $user->first_name }} {{ $user->last_name }}</option>
@endforeach