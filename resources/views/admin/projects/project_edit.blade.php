@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 2, 'subActive' => 1])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans('pmis.creat_pro'), 'page' => trans('pmis.reports'), 'current' => trans('pmis.projects')])
@stop
@section('alert-message')
    @if(session('message_title'))
        <div role="alert" class="alert {{ session('message_class') }}">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
        </div>
    @endif
@stop
@section('main-content')
    <form action="{{action('ProjectController@updateProject')}}" method="post" role="form" class="project-form"
          id="form">
        {!! csrf_field() !!}
        <input type="hidden" name="project_id" id="project_id" value="{{ $project->id }}">
        <div class="container-fluid container-fullw bg-white">
            <div class="row">
                <fieldset>
                    <legend>
                        {{trans('pmis.Project_Details')}}
                    </legend>
                    <div class="row">
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.project_name')}} <span class="symbol required"
                                                                     aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="name" class="form-control" type="text"
                                       value="{{ $project->name }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.project_code')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="code" class="form-control" value="{{ $project->code }}" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.project_type')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <select class="ui dropdown" name="project_type">
                                @if($project->project_type == 1)
                                    <option value="1">Private</option>
                                    <option value="2">Kabul Municipality</option>
                                    <option value="3">MoF</option>
                                @elseif($project->project_type == 2)
                                    <option value="2">Kabul Municipality</option>
                                    <option value="1">Private</option>
                                    <option value="3">MoF</option>
                                @else
                                    <option value="3">MoF</option>
                                    <option value="2">Kabul Municipality</option>
                                    <option value="1">Private</option>
                                    @endif
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.fund_available_for_year')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="available_fund" value="{{$project->available_fund}}" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.expected_execution_in_year')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="expected_exe_in_year" value="{{$project->expected_exe_in_year}}" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.project_year')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="year" value="{{$project->year}}" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.expense_record_budget_dep')}}
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="exp_rec_budg_dep" value="{{$project->exp_rec_budg_dep}}" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.contractor')}}
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="contractor" class="form-control" type="text"
                                       value="{{ $project->contractor }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.owner')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="owner" class="form-control" type="text"
                                       value="{{ $project->owner }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.location')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="area" class="form-control" type="text"
                                       value="{{ $project->area }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.transit')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="passway" class="form-control" type="text"
                                       value="{{ $project->passway }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.district')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <select class="ui dropdown" name="district">
                                <option value="{{ $project->district }}">{{ $project->district }}</option>
                                @foreach($district as $district)
                                    @if(App::isLocale('fa'))
                                        <option value="{{$district->id}}">{{$district->fa_name}}</option>
                                    @elseif(App::isLocale('pa'))
                                        <option value="{{$district->id}}">{{$district->pa_name}}</option>
                                    @else
                                        <option value="{{$district->id}}">{{$district->en_name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.contract_date')}} <span class="symbol required"
                                                                      aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input name="contract_date"
                                       class="form-control datepicker" type="text"
                                       value="{{ $project->contract_date }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.actual_start_date')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input name="start_date"
                                       class="form-control datepicker" type="text" value="{{ $project->start_date }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.project_contract_comp_date')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input name="actual_date" class="form-control datepicker" type="text" value="{{ $project->actual_date }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.completion_planned_date')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input name="planned_date"
                                       class="form-control datepicker" type="text" value="{{ $project->planned_date }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.Project_Budget')}} <span class="symbol required"
                                                                       aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="project_budget"
                                       class="form-control" type="text" value="{{ $project->project_budget }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.addational_budget')}} <span class="symbol required"
                                                                          aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="additional_budget" id="additional_budget"
                                       class="form-control" type="text" value="{{ $project->addational_budget }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.days_remain')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="days_remain" id="days_remain" value="{{$project->days_remain}}" class="form-control datepicker" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.percentage_remain')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <input placeholder="" name="percentage_remain" id="percentage_remain" value="{{$project->percentage_remain}}" class="form-control" type="text">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.project_status')}} <span class="symbol required"
                                                                          aria-required="true"></span>
                            </label>

                                <select class="ui dropdown" name="status" id="project_status">
                                    @if($project->status == 1)
                                        <option value="1">{{trans('pmis.design')}}</option>
                                        <option value="2">{{trans('pmis.procurement')}}</option>
                                        <option value="3">{{trans('pmis.construction')}}</option>
                                        <option value="4">{{trans('pmis.completed')}}</option>
                                    @elseif($project->status == 2)
                                        <option value="2">{{trans('pmis.procurement')}}</option>
                                        <option value="1">{{trans('pmis.design')}}</option>
                                        <option value="3">{{trans('pmis.construction')}}</option>
                                        <option value="4">{{trans('pmis.completed')}}</option>
                                    @elseif($project->status == 3)
                                        <option value="3">{{trans('pmis.construction')}}</option>
                                        <option value="1">{{trans('pmis.design')}}</option>
                                        <option value="2">{{trans('pmis.procurement')}}</option>
                                        <option value="4">{{trans('pmis.completed')}}</option>
                                    @else
                                        <option value="4">{{trans('pmis.completed')}}</option>
                                        <option value="1">{{trans('pmis.design')}}</option>
                                        <option value="2">{{trans('pmis.procurement')}}</option>
                                        <option value="3">{{trans('pmis.construction')}}</option>
                                    @endif

                                </select>
                        </div>
                        {{--@if($project->status == 3)--}}
                        {{--<div class="col-md-4">--}}
                            {{--<label>{{trans('pmis.actual_amount_for_month').$month_name}} </label>--}}
                            {{--<div class="form-group">--}}
                                {{--<a class="btn btn-primary" data-toggle="modal" style="border-radius: 0;" data-target="#actual_amount_modal">{{$month_name}} Actual Amount</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                            {{--@endif--}}
                    </div>


                    <fieldset>
                        <legend>{{trans('violation.latitude')}}</legend>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.degree')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="latitude_degree" class="form-control" type="text" value = "{{ $latitude_degree }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.minute')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="latitude_minute" class="form-control" type="text" value = "{{ $latitude_minute }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.second')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="latitude_second" class="form-control" type="text" value = "{{ $latitude_second }}">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>{{trans('violation.longitude')}}</legend>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.degree')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="longitude_degree" class="form-control" type="text" value = "{{ $longitude_degree }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.minute')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="longitude_minute" class="form-control" type="text" value = "{{ $longitude_minute }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.second')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="longitude_second" class="form-control" type="text" value = "{{ $longitude_second }}">
                            </div>
                        </div>
                    </fieldset>
                    <div class="col-md-12">
                        <label>
                            {{trans('pmis.project_comment')}}
                        </label>
                        <textarea class="form-control" rows="6" name="comment">{{$project->comment}}</textarea>
                    </div>
                </fieldset>
                <div class="form-group">
                    <button class="btn btn-primary btn-o next-step btn-wide pull-right">
                        {{trans('pmis.save')}} <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>
        </form>

        <div class="modal fade" id="project_status_modal" tabindex="-1" role="dialog" aria-labelledby="Assign Project Modal"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">{{trans('pmis.project_status')}}</h4>
                    </div>
                    <div class="modal-body ">
                        <form action="{{action('InfractionController@uploadDocuments')}}" method="post" id="form">
                            <div class="container-fluid container-fullw bg-white">
                                <div class="row">
                                    <fieldset>
                                        <legend>
                                            {{trans('pmis.project_status')}}
                                        </legend>
                                        <div class="row">
                                            <div id="design" hidden>
                                                <input type="hidden" name="project_status" id="" value="1">
                                                <div class="col-md-6">
                                                    <label>
                                                        {{trans('pmis.days_remain')}} <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="days_remain" id="days_remain" value="{{$project->days_remain}}" class="form-control date-picker" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>
                                                        {{trans('pmis.percentage_remain')}} <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <input placeholder="" name="percentage_remain" id="percentage_remain" value="{{$project->percentage_remain}}" class="form-control" type="text">
                                                    <div class="form-group">
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="procurement" hidden>
                                                <input type="hidden" name="project_status" id="" value="1">
                                                <div class="col-md-6">
                                                    <label>
                                                        {{trans('pmis.days_remain')}} <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder=""  id="procurement_due_date" value="{{$project->procurement_due_date}}" class="form-control date-picker" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>
                                                        {{trans('pmis.procurement_phase')}} <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <select class="ui dropdown" id="procurement_type" class="col-md-6">
                                                        @if($project->procurement_type == 1)
                                                        <option value="1">Bid Announcement</option>
                                                        <option value="2">Bid Opening</option>
                                                        <option value="3">Evaluation</option>
                                                        <option value="4">Contract</option>
                                                            @elseif($project->procurement_type == 2)
                                                            <option value="2">Bid Opening</option>
                                                            <option value="1">Bid Announcement</option>
                                                            <option value="3">Evaluation</option>
                                                            <option value="4">Contract</option>
                                                        @elseif($project->procurement_type == 3)
                                                            <option value="3">Evaluation</option>
                                                            <option value="2">Bid Opening</option>
                                                            <option value="1">Bid Announcement</option>
                                                            <option value="4">Contract</option>
                                                            @else
                                                            <option value="4">Contract</option>
                                                            <option value="2">Bid Opening</option>
                                                            <option value="1">Bid Announcement</option>
                                                            <option value="3">Evaluation</option>
                                                            @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="completed" hidden>
                                                <div class="col-md-6">
                                                    <label>
                                                        {{trans('pmis.completed')}} <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" id="completed_date" value="{{$project->completed_date}}" class="form-control datepicker" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="form-group container">
                                        <div class="col-md-2 pull-right">
                                            <button class="btn btn-o btn-primary" type="button"  onclick="updateStatus()">
                                                {{trans('pmis.save')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="modal fade" id="actual_amount_modal" tabindex="-1" role="dialog" aria-labelledby="Assign Project Modal"
                   aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">{{trans('pmis.project_status')}}</h4>
                    </div>
                    <div class="modal-body ">
                        <form id="actual_amount_form" action="{{action('ProjectController@updateActualAmount')}}"  method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="container-fluid container-fullw bg-white">
                                <div class="row">
                                    <input type="hidden" id="month_number" name="month_number" value="{{$month_number}}">
                                    <input type="hidden" id="month_number" name="project_id" value="{{$project->id}}">
                                    <div class="col-md-6">
                                        <label>
                                            {{trans('pmis.actual_amount')}}
                                            <span class="symbol required" aria-required="true"></span>
                                        </label>
                                        <div class="form-group">
                                            <input placeholder="actual_amount" name="actual_amount" id="edit_project_actual_amount" class="form-control" type="text" >
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label>
                                            {{trans('pmis.actual_amount_percentage')}}
                                            <span class="symbol required" aria-required="true"></span>
                                        </label>
                                        <div class="form-group">
                                            <input  placeholder="" name="actual_amount_percentage" id="edit_project_actual_amount_percentage" class="form-control" type="text" readonly>
                                        </div>
                                    </div>

                                    <div class="invoice_file">
                                    </div>
                                    <div class="col-md-12">
                                        <div id="invoice_file" class="dropzone"></div>
                                    </div>

                                    <div class="form-group container" style="margin-top: 20px;">
                                        <div class="col-md-2 pull-right">
                                            <button class="btn btn-o btn-primary" type="submit">
                                                {{trans('pmis.save')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>

        @stop
@push('scripts')
<script>
    $('.ui.dropdown').dropdown();
</script>
@endpush
@section('validator')
    {!! JsValidator::formRequest('App\Http\Requests\ProjectRequest') !!}
@endsection
