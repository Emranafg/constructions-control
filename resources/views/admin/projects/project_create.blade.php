@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 2, 'subActive' => 1])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans('pmis.creat_pro'), 'page' => trans('pmis.reports'), 'current' => trans('pmis.projects')])
@stop
@section('alert-message')
    @if(session('message_title'))
        <div role="alert" class="alert {{ session('message_class') }}">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
        </div>
    @endif
@stop
@section('main-content')
    <form action="{{action('ProjectController@storeProject')}}" method="post" role="form" class="project-form"
          id="form">
        {!! csrf_field() !!}
        <div class="container-fluid container-fullw bg-white">
            <div class="row">
                <fieldset>
                    <legend>
                        {{trans('pmis.Project_Details')}}
                    </legend>
                    <div class="row">
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.project_name')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="name" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.project_code')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="code" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.project_type')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <select class="ui dropdown" name="project_type">
                                <option value="1">Private</option>
                                <option value="2">Kabul Municipality</option>
                                <option value="3">MoF</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.fund_available_for_year')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="available_fund" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.expected_execution_in_year')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="expected_exe_in_year" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.project_year')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="year" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.expense_record_budget_dep')}}
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="exp_rec_budg_dep" class="form-control" type="text">
                            </div>
                        </div>


                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.contractor')}}
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="contractor" class="form-control" type="text">
                            </div>
                        </div>
                        {{-- <div class="col-md-4">
                            <label>
                                {{trans('pmis.contractual')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="contract" class="form-control" type="text">
                            </div>
                        </div> --}}
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.owner')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="owner" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.location')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="area" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.transit')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="passway" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.district')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <select class="ui dropdown" name="district">
                                @foreach($district as $district)
                                    @if(App::isLocale('fa'))
                                        <option value="{{$district->id}}">{{$district->fa_name}}</option>
                                    @elseif(App::isLocale('pa'))
                                        <option value="{{$district->id}}">{{$district->pa_name}}</option>
                                    @else
                                        <option value="{{$district->id}}">{{$district->en_name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.contract_date')}} <span class="symbol required"
                                                                      aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input name="contract_date"
                                       class="form-control datepicker" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.actual_start_date')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input name="start_date"
                                       class="form-control datepicker" type="text">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.project_contract_comp_date')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input name="actual_date"
                                       class="form-control datepicker" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.completion_planned_date')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input name="planned_date"
                                       class="form-control datepicker" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.Project_Budget')}} <span class="symbol required"
                                                                       aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="project_budget"
                                       class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.addational_budget')}} <span class="symbol required"
                                                                          aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="additional_budget"
                                       class="form-control" type="text">
                            </div>
                        </div>



                        {{--<div class="col-md-4">--}}
                            {{--<label>--}}
                                {{--{{trans('pmis.project_status')}} <span class="symbol required" aria-required="true"></span>--}}
                            {{--</label>--}}
                            {{--<select class="ui dropdown" name="status">--}}
                                {{--<option value="1">{{trans('pmis.design')}}</option>--}}
                                {{--<option value="2">{{trans('pmis.procurement')}}</option>--}}
                                {{--<option value="3">{{trans('pmis.construction')}}</option>--}}
                                {{--<option value="4">{{trans('pmis.completed')}}</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        
                        {{-- The defualt status of a project is design--}}

                        <input type="hidden" name="status" value="1">
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.days_remain')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="days_remain" id="days_remain" class="form-control datepicker" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.percentage_remain')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <input placeholder="" name="percentage_remain" id="percentage_remain" class="form-control" type="text">
                            <div class="form-group">
                            </div>
                        </div>

                    </div>
                    <fieldset>
                        <legend>{{trans('violation.latitude')}}</legend>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.degree')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="latitude_degree" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.minute')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="latitude_minute" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.second')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="latitude_second" class="form-control" type="text">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>{{trans('violation.longitude')}}</legend>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.degree')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="longitude_degree" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.minute')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="longitude_minute" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.second')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="longitude_second" class="form-control" type="text">
                            </div>
                        </div>
                    </fieldset>
                </fieldset>
                <div class="col-md-12">
                    <label>
                        {{trans('pmis.project_comment')}}
                        <span class="symbol required" aria-required="true"></span>
                    </label>
                    <div class="form-group">
                        <textarea class="form-control" rows="6" name="comment"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-o next-step btn-wide pull-right">
                        {{trans('pmis.save')}} <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
@stop
@push('scripts')
<script>
    $('.ui.dropdown').dropdown();
</script>
@endpush
@section('validator')
    {!! JsValidator::formRequest('App\Http\Requests\ProjectRequest') !!}
@endsection
