<!DOCTYPE html>
<html>
<head>
    {{-- <meta charset="utf-8"> --}}
    {{-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> --}}
    <title></title>
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> --}}
    {{-- <link rel="stylesheet" href="{{asset('standard/assets/css/print.css')}}"/> --}}
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        body {
            height: 842px;
            width: 595px;
            /* to centre page on screen*/
            margin-left: auto;
            margin-right: auto;
            font-size: 8px !important;
        }

        h5 {
            /*background-color: #eee;*/
            padding: 50px;
            margin: 150px;
        }
        .text-center{
            text-align: center;
        }
        table{
            border: 1px solid #999;
        }
        th, td{
            border: 1px solid #999;
        }
    </style>
</head>
<body>
    <div class="container">
        <h3 class="text-center">{{trans('pmis.Daily_Report')}}</h3>
    </div>
    <div class="container">
        <h5 class="panel-title">{{trans('pmis.details')}}</h5>
        <div class="panel-body">
            <table class="table profile__table" cellpadding="6">
                <tbody>
                <tr>
                    <th>{{trans('pmis.Date')}}</th>
                    <th>{{trans('pmis.time')}}</th>
                    <th>{{trans('pmis.Weather')}}</th>
                    <th>{{trans('pmis.Temprature')}}</th>
                    <th>{{trans('pmis.Project_Status')}}</th>
                </tr>
                <tr>
                    <td>{{$reportDetails->date}}</td>
                    <td>{{$reportDetails->time}}</td>
                    <td>{{$reportDetails->weather}}</td>
                    <td>{{$reportDetails->temprature}}</td>
                    <td>{{$reportDetails->status_value}} %</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="container">
        <h5 class="panel-title">{{trans('pmis.Supervisor')}}</h5>
        <div class="panel-body">
            <table class="table profile__table" cellpadding="6">
                <tbody>
                <tr>
                    <th>{{trans('pmis.Supervisor_Name')}}</th>
                    <th>{{trans('pmis.Email')}}</th>
                    <th>{{trans('pmis.Phone_Number')}}</th>
                </tr>
                <tr>
                    <td>{{$supervisor->first_name}} {{$supervisor->last_name}}</td>
                    <td>{{$supervisor->email}}</td>
                    <td>{{$supervisor->phone}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="container">
        <div class="panel-heading">
            <h5>{{trans('pmis.Ongoing_Activities')}}</h5>
        </div>
        <div class="panel-body">
            @foreach($workActivities as $activity)
                <h6> {{ $activity->type }} </h6>
                <p> {{ $activity->description }} </p>
            @endforeach
        </div>
    </div>
    <div class="container">
        <div class="panel-heading">
            <h5>{{trans('pmis.Deficiencies')}}</h5>
        </div>
        <div class="panel-body">
            @foreach($workDefacts as $defact)
                <h6> {{ $defact->type }} </h6>
                <p> {{ $defact->description }} </p>
            @endforeach
        </div>
    </div>
    <div class="container">
        <div class="panel-heading">
            <h5>{{trans('pmis.Construction_Material_Tests')}}</h5>
        </div>
        <div class="panel-body">
            @foreach($buildingMaterialTests as $buildingMaterialTes)
                <h6> {{ $buildingMaterialTes->type }} </h6>
                <small>{{ $buildingMaterialTes->result }}</small>
                <p> {{ $buildingMaterialTes->description }} </p>
            @endforeach
        </div>
    </div>
    <div class="container">
        <div class="panel-heading">
            <h5>{{trans('pmis.Equipment_area')}}</h5>
        </div>
        <div class="panel-body">
            @foreach($equipments as $equipment)
                <h6> {{ $equipment->type }} </h6>
                <small>{{ $equipment->quantity }}</small>
                <p> {{ $equipment->comment }} </p>
            @endforeach
        </div>
    </div>
    <div class="container">
        <div class="panel-heading">
            <h5 class="panel-title">{{trans('pmis.Site_Safety')}}</h5>
        </div>
        <div class="panel-body">
            <h6>{{trans('pmis.safety_measure')}}</h6>
            <table class="table profile__table" cellpadding="6">
                <tbody>
                <tr>
                    <th>{{ trans('pmis.eye_protection') }}</th>
                    <th>{{trans('pmis.helmet')}}</th>
                    <th>{{trans('pmis.ear_protection')}}</th>
                    <th>{{trans('pmis.respiratory_protection')}}</th>
                </tr>
                <tr>
                    <td>
                        <img height="10" width="10"
                             src="{{ $safetyMeasures->eye_protection != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                    </td>
                    <td>
                        <img height="10" width="10"
                             src="{{ $safetyMeasures->helmet != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                    </td>
                    <td>
                        <img height="10" width="10"
                             src="{{ $safetyMeasures->ear_protection != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                    </td>
                    <td>
                        <img height="10" width="10"
                             src="{{ $safetyMeasures->respiratory_protection != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                    </td>
                </tr>
                <tr>
                    <th>{{trans('pmis.safety_boots')}}</th>
                    <th>{{trans('pmis.safety_gloves')}}</th>
                    <th>{{trans('pmis.symptoms_of_obstacles')}}</th>
                    <th>{{trans('pmis.safe_face_on')}}</th>
                </tr>
                <tr>
                    <td>
                        <img height="10" width="10"
                             src="{{ $safetyMeasures->safety_boots != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                    </td>
                    <td>
                        <img height="10" width="10"
                             src="{{ $safetyMeasures->safety_gloves != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                    </td>
                    <td>
                        <img height="10" width="10"
                             src="{{ $safetyMeasures->symptoms_of_obstacles != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                    </td>
                    <td>
                        <img height="10" width="10"
                             src="{{ $safetyMeasures->safe_face_on != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="table profile__table" cellpadding="6">
                <tbody>
                <tr>
                    <th>{{trans('pmis.tape_barriers')}}</th>
                    <th>{{trans('pmis.safety_uniforms')}}</th>
                    <th>{{trans('pmis.warning_signs')}}</th>
                    <th>{{trans('pmis.mandatory_signs')}}</th>
                    <th>{{trans('pmis.first_aid_sign')}}</th>
                </tr>
                <tr>
                    <td>
                        <img height="10" width="10"
                             src="{{ $safetyMeasures->tape_barriers != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                    </td>
                    <td>
                        <img height="10" width="10"
                             src="{{ $safetyMeasures->safety_uniforms != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                    </td>
                    <td>
                        <img height="10" width="10"
                             src="{{ $safetyMeasures->warning_signs != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                    </td>
                    <td>
                        <img height="10" width="10"
                             src="{{ $safetyMeasures->mandatory_signs != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                    </td>
                    <td>
                        <img height="10" width="10"
                             src="{{ $safetyMeasures->first_aid_sign != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                    </td>
                </tr>
                </tbody>
            </table>
            <h6>{{trans('pmis.Environmental_measures')}}</h6>
            <table class="table profile__table" cellpadding="6">
                <tbody>
                    <tr>
                        <th>{{trans('pmis.risks_of_chemicals')}}</th>
                        <th>{{trans('pmis.noise_pollution')}}</th>
                        <th>{{trans('pmis.asphalting_disadvantages')}}</th>
                        <th>{{trans('pmis.concrete_disadvantages')}}</th>
                    </tr>
                    <tr>
                        <td><img height="10" width="10"
                                 src="{{ $safetyMeasures->risks_of_chemicals != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                        </td>
                        <td><img height="10" width="10"
                                 src="{{ $safetyMeasures->noise_pollution != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                        </td>
                        <td><img height="10" width="10"
                                 src="{{ $safetyMeasures->asphalting_disadvantages != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                        </td>
                        <td><img height="10" width="10"
                                 src="{{ $safetyMeasures->concrete_disadvantages != 0 ? asset('standard/assets/images/checked.png') : asset('standard/assets/images/cross.png') }}">
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>

    <div class="container">
        <div class="panel-heading">
            <h5 class="panel-title">{{trans('pmis.Employees_area')}}</h5>
        </div>
        <div class="panel-body">
            <table class="table profile__table" cellpadding="6">
                <tbody>
                <tr>
                    <th>{{trans('pmis.project_manaegr')}}</th>
                    <th>{{trans('pmis.Quality_Engineer')}}</th>
                    <th>{{trans('pmis.Safety_Engineer')}}</th>
                    <th>{{trans('pmis.Foreman')}}</th>
                </tr>
                <tr>
                    <td>{{ $projectWorker->projectmanager }}</td>
                    <td>{{ $projectWorker->control_engineer }}</td>
                    <td>{{ $projectWorker->safety_engineer }}</td>
                    <td>{{ $projectWorker->worker_leader }}</td>
                </tr>
                </tbody>
            </table>
            <table class="table profile__table" cellpadding="6">
                <tbody>
                <tr>
                    <th>{{trans('pmis.skilled_worker')}}</th>
                    <th>{{trans('pmis.ordinary_workers')}}</th>
                    <th>{{trans('pmis.escort')}}</th>
                    <th>{{trans('pmis.visitor')}}</th>
                    <th>{{trans('pmis.Other')}}</th>
                </tr>
                <tr>
                    <td>{{ $projectWorker->smart_worker }}</td>
                    <td>{{ $projectWorker->ordinary_worker }}</td>
                    <td>{{ $projectWorker->gard }}</td>
                    <td>{{ $projectWorker->visitor }}</td>
                    <td>{{ $projectWorker->other }}</td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="container">
        <div class="panel-heading">
            <h5 class="panel-title">{{trans('pmis.Construction_Material_On_site')}}</h5>
        </div>
        <div class="panel-body">
            @foreach($buildingEquipments as $equipment)
                <h6> {{trans('pmis.type_material')}}: {{ $equipment->type }} </h6>
                <h6> {{trans('pmis.Estimated_value')}}: {{ $equipment->estim_value }} </h6>
                <h6> {{trans('pmis.quality')}}: {{ $equipment->quality }} </h6>
                <p> {{trans('pmis.comment')}}: {{ $equipment->comment }} </p>
            @endforeach
        </div>
    </div>
</body>
</html>