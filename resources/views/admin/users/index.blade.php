@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 5])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' =>trans('user.list_users'), 'page' => trans('user.users'), 'current' => trans('user.all')])
@stop
@section('alert-message')
    @if(session('message_title'))
        <div role="alert" class="alert {{ session('message_class') }}">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
        </div>
    @endif
@stop
@section('style')
    <style media="screen">
        .select2-container {
            display: none;
        }
        .input-sm {
            height: 35px !important;
        }
    </style>
@stop
@section('main-content')
    <div class="container-fluid container-fullw bg-white">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-heading border-light">
                        <h4 class="panel-title">{{trans('user.users')}}</h4>
                        <ul class="panel-heading-tabs border-light">
                            <li>
                                <div class="pull-right">
                                    <div class="ui labeled button" tabindex="0" style="direction: ltr">
                                        <a href="/user/create" class="ui red button">
                                            <i class="plus icon"></i> {{trans('user.create_user')}}
                                        </a>
                                        <a class="ui basic red left pointing label"
                                           style="border-radius: 0px !important">
                                            {{ $users->count() }}
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="panel-tools">
                                <a data-original-title="{{trans('pmis.Refresh')}}" data-toggle="tooltip"
                                   data-placement="top"
                                   class="btn btn-transparent btn-sm panel-refresh" href="#"><i
                                            class="flaticon-business-2"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">

                        <div class="col-md-12">
                            <div class="margin-top-30"></div>
                            <div @if(App::isLocale('fa') || App::isLocale('pa'))
                                 class="margin-right-10" @else class="margin-left-10" @endif >
                                <table class="ui celled table" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{trans('user.first_name')}}</th>
                                            <th>{{trans('user.last_name')}}</th>
                                            <th>{{trans('pmis.Email')}}</th>
                                            <th>{{trans('pmis.Phone_Number')}}</th>
                                            <th>{{trans('user.role')}}</th>
                                            <th data-sortable="false">{{trans('user.permissions')}}</th>
                                            <th data-sortable="false">{{trans('pmis.edit')}}</th>
                                            <th data-sortable="false">{{trans('pmis.delete')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td>{{ $user->first_name }}</td>
                                            <td>{{ $user->last_name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->phone }}</td>
                                            <td>@if($user->roles()->count()) {{ $user->roles[0]['name'] }} @endif</td>
                                            <td class="td-center"><a href="/user/{{ $user->id }}/show"><i
                                                            class="icon eye"></i></a></td>
                                            <td class="td-center"><a href="/user/{{ $user->id }}/edit"><i
                                                            class="icon edit"></i></a></td>
                                            <td class="td-center"><a
                                                        onclick="return confirm('{{ trans("message.delete_message_user") }}')"
                                                        href="/user/{{ $user->id }}/delete"><i
                                                            class="icon delete red"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('scripts')
<script>
    $('document').ready(function () {
        var oTable = $('#sample_1').dataTable({
            "aoColumnDefs": [{
                "aTargets": [0]
            }],
            "language": {
                "lengthMenu": "{{trans('pmis.show')}} _MENU_{{trans('pmis.entries')}}",
                "zeroRecords": "{{trans('pmis.Nothing_found_sorry')}}",
                "info": "{{trans('pmis.show')}} _START_ {{trans('pmis.of')}} _END_ {{trans('pmis.of')}} _TOTAL_ {{trans('pmis.entries')}}",
                "infoEmpty": "{{trans('pmis.No_records_available')}}",
                "infoFiltered": "({{trans('pmis.show')}} {{trans('pmis.of')}}_MAX_ {{trans('pmis.entrie')}})",
                "search": "",
                paginate: {
                    next: "{{trans('pmis.next')}}",
                    previous: "{{trans('pmis.previous')}}"
                },
            },
            "aaSorting": [[1, 'asc']],
            "iDisplayLength": 10,
        });
        $('#sample_1_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "{{trans('pmis.search')}}");
        // modify table search input
        $('#sample_1_wrapper .dataTables_length select').addClass("m-wrap small");
        // modify table per page dropdown
        $('#sample_1_wrapper .dataTables_length select').select2();
        // initialzie select2 dropdown
        $('#sample_1_column_toggler input[type="checkbox"]').change(function () {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, ( bVis ? false : true));
        });
    });
</script>
@endpush