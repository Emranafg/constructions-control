@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 5])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => "User Details page", 'page' => "Users", 'current' => "Edit"])
@stop
@section('alert-message')
  @if(session('message_title'))
    <div role="alert" class="alert {{ session('message_class') }}">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
    </div>
  @endif
@stop
@section('style')
    <style media="screen">
        .select2-container {
            display: none;
        }
        .input-sm {
            height: 35px !important;
        }
    </style>
@stop
@section('main-content')
    <form action="{{action('UserController@update')}}" method="post" role="form"
          id="form">
        {!! csrf_field() !!}
        <input type="hidden" name="id" value="{{ $user->id }}">
        <div class="container-fluid container-fullw bg-white">
            <div class="row">
                <fieldset>
                    <legend>
                        {{ trans("user.user_details") }}
                    </legend>
                    <div class="row">
                        <div class="col-md-4">
                            <label>
                                {{trans('user.first_name')}} <span class="symbol"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="first_name" class="form-control" type="text"
                                       value="{{ $user->first_name }}" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('user.last_name')}} <span class="symbol"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="last_name" class="form-control" type="text"
                                       value="{{ $user->last_name }}" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.Email')}} <span class="symbol"></span>
                            </label>
                            <div class="form-group">

                                <input placeholder="" name="email" class="form-control" type="email"
                                       value="{{ $user->email }}" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.Phone_Number')}} <span class="symbol"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="phone" class="form-control" type="text"
                                       value="{{ $user->phone }}" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('user.role')}} <span class="symbol"></span>
                            </label>
                            <div class="form-group">
                                <input class="form-control" type="text"
                                       value="@if($user->roles()->count()) {{ $user->roles[0]['name'] }} @endif"
                                       disabled>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend> {{trans('user.permissions')}}</legend>
                    <div class="row">
                        <div class="col-md-12" id="permission">
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_project" id="checkbox1" name="permission[]"
                                           @if($user->hasPermissionTo('create_project')) checked @endif>
                                    <label for="checkbox1">
                                        <a href="/user/1/permission">{{trans('user.create_project')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_project" id="checkbox2" name="permission[]"
                                           @if($user->hasPermissionTo('view_project')) checked @endif>
                                    <label for="checkbox2">
                                        <a href="/user/2/permission">{{trans('user.view_project')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_project" id="checkbox3" name="permission[]"
                                           @if($user->hasPermissionTo('edit_project')) checked @endif>
                                    <label for="checkbox3">
                                        <a href="/user/3/permission">{{trans('user.edit_project')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_project" id="checkbox4" name="permission[]"
                                           @if($user->hasPermissionTo('delete_project')) checked @endif>
                                    <label for="checkbox4">
                                        <a href="/user/4/permission">{{trans('user.delete_project')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_report" id="checkbox6" name="permission[]"
                                           @if($user->hasPermissionTo('create_report')) checked @endif>
                                    <label for="checkbox6">
                                        <a href="/user/6/permission">{{trans('user.create_report')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_report" id="checkbox7" name="permission[]"
                                           @if($user->hasPermissionTo('view_report')) checked @endif>
                                    <label for="checkbox7">
                                        <a href="/user/7/permission">{{trans('user.view_report')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_report" id="checkbox8" name="permission[]"
                                           @if($user->hasPermissionTo('edit_report')) checked @endif>
                                    <label for="checkbox8">
                                        <a href="/user/8/permission">{{trans('user.edit_report')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_report" id="checkbox9" name="permission[]"
                                           @if($user->hasPermissionTo('delete_report')) checked @endif>
                                    <label for="checkbox9">
                                        <a href="/user/9/permission">{{trans('user.delete_report')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_violation" id="checkbox10" name="permission[]"
                                           @if($user->hasPermissionTo('create_violation')) checked @endif>
                                    <label for="checkbox10">
                                        <a href="/user/10/permission">{{trans('user.create_violation')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_violation" id="checkbox11" name="permission[]"
                                           @if($user->hasPermissionTo('view_violation')) checked @endif>
                                    <label for="checkbox11">
                                        <a href="/user/11/permission">{{trans('user.view_violation')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_violation" id="checkbox12" name="permission[]"
                                           @if($user->hasPermissionTo('edit_violation')) checked @endif>
                                    <label for="checkbox12">
                                        <a href="/user/12/permission">{{trans('user.edit_violation')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_violation" id="checkbox13" name="permission[]"
                                           @if($user->hasPermissionTo('delete_violation')) checked @endif>
                                    <label for="checkbox13">
                                        <a href="/user/13/permission">{{trans('user.delete_violation')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_document" id="checkbox15" name="permission[]"
                                           @if($user->hasPermissionTo('create_document')) checked @endif>
                                    <label for="checkbox15">
                                        <a href="/user/15/permission">{{trans('user.create_document')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_document" id="checkbox16" name="permission[]"
                                           @if($user->hasPermissionTo('view_document')) checked @endif>
                                    <label for="checkbox16">
                                        <a href="/user/16/permission">{{trans('user.view_document')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_document" id="checkbox17" name="permission[]"
                                           @if($user->hasPermissionTo('edit_document')) checked @endif>
                                    <label for="checkbox17">
                                        <a href="/user/17/permission">{{trans('user.edit_document')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_document" id="checkbox18" name="permission[]"
                                           @if($user->hasPermissionTo('delete_document')) checked @endif>
                                    <label for="checkbox18">
                                        <a href="/user/18/permission">{{trans('user.delete_document')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_user" id="checkbox19" name="permission[]"
                                           @if($user->hasPermissionTo('create_user')) checked @endif>
                                    <label for="checkbox19">
                                        <a href="/user/19/permission">{{trans('user.create_user')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_user" id="checkbox20" name="permission[]"
                                           @if($user->hasPermissionTo('view_user')) checked @endif>
                                    <label for="checkbox20">
                                        <a href="/user/20/permission">{{trans('user.view_user')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_user" id="checkbox21" name="permission[]"
                                           @if($user->hasPermissionTo('edit_user')) checked @endif>
                                    <label for="checkbox21">
                                        <a href="/user/21/permission">{{trans('user.edit_user')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_user" id="checkbox22" name="permission[]"
                                           @if($user->hasPermissionTo('delete_user')) checked @endif>
                                    <label for="checkbox22">
                                        <a href="/user/22/permission">{{trans('user.delete_user')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="assign_project" id="checkbox5" name="permission[]"
                                           @if($user->hasPermissionTo('assign_project')) checked @endif>
                                    <label for="checkbox5">
                                        <a href="/user/5/permission">{{trans('user.assign_project')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="assign_violation" id="checkbox14" name="permission[]"
                                           @if($user->hasPermissionTo('assign_violation')) checked @endif>
                                    <label for="checkbox14">
                                        <a href="/user/14/permission">{{trans('user.assign_violation')}}</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_plan" id="checkbox23" name="permission[]"
                                        @if($user->hasPermissionTo('edit_plan')) checked @endif>
                                    <label for="checkbox23">
                                        <a href="/user/23/permission">{{trans('user.edit_plan')}}</a>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-group">
                    <button class="btn btn-primary btn-o next-step btn-wide pull-right">
                        {{trans('pmis.save')}} <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
@stop
@push('scripts')
<script>
    $('.ui.dropdown').dropdown();
</script>
@endpush
