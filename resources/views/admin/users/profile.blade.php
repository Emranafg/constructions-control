@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 5])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => "Editing Profile", 'page' => "Users", 'current' => "Profile"])
@stop
@section('alert-message')
    @if(session('message_title'))
        <div role="alert" class="alert {{ session('message_class') }}">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
        </div>
    @endif
@stop
@section('style')
    <style media="screen">
        .select2-container {
            display: none;
        }
        .input-sm {
            height: 35px !important;
        }
    </style>
@stop
@section('main-content')
    <form action="{{action('UserController@profileUpdate')}}" method="post" role="form"
          id="form">
        {!! csrf_field() !!}
        <input type="hidden" name="id" value="{{ $user->id }}">
        <div class="container-fluid container-fullw bg-white">
            <div class="row">
                <fieldset>
                    <legend>
                        User Details
                    </legend>
                    <div class="row">
                        <div class="col-md-4">
                            <label>
                                First Name <span class="symbol"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="first_name" class="form-control" type="text"
                                       value="{{ $user->first_name }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                Last Name <span class="symbol"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="last_name" class="form-control" type="text"
                                       value="{{ $user->last_name }}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                Last Name <span class="symbol"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="last_name" class="form-control" type="text" value="{{ $user->last_name }}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                Email <span class="symbol"></span>
                            </label>
                            <div class="form-group">

                                <input placeholder="" name="email" class="form-control" type="email"
                                       value="{{ $user->email }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                Phone <span class="symbol"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="phone" class="form-control" type="text"
                                       value="{{ $user->phone }}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                                Password <span class="symbol"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="password" class="form-control" type="password"
                                       value="LFueXEhYe4eRmDJ" disabled>
                                <span class="change-password" id="change_password_profile">Change password</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                Role <span class="symbol"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder=""
                                       value="@if($user->roles()->count()) {{ $user->roles[0]['name'] }} @endif"
                                       class="form-control" type="text" disabled>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend> Permissions</legend>
                    <div class="row">
                        <div class="col-md-12" id="permission">
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_project" id="checkbox1" name="permission[]"
                                           disabled @if($user->hasPermissionTo('create_project')) checked @endif>
                                    <label for="checkbox1">
                                        create_project
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_project" id="checkbox2" name="permission[]"
                                           disabled @if($user->hasPermissionTo('view_project')) checked @endif>
                                    <label for="checkbox2">
                                        view_project
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_project" id="checkbox3" name="permission[]"
                                           disabled @if($user->hasPermissionTo('edit_project')) checked @endif>
                                    <label for="checkbox3">
                                        edit_project
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_project" id="checkbox4" name="permission[]"
                                           disabled @if($user->hasPermissionTo('delete_project')) checked @endif>
                                    <label for="checkbox4">
                                        delete_project
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_report" id="checkbox6" name="permission[]"
                                           disabled @if($user->hasPermissionTo('create_report')) checked @endif>
                                    <label for="checkbox6">
                                        create_report
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_report" id="checkbox7" name="permission[]"
                                           disabled @if($user->hasPermissionTo('view_report')) checked @endif>
                                    <label for="checkbox7">
                                        view_report
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_report" id="checkbox8" name="permission[]"
                                           disabled @if($user->hasPermissionTo('edit_report')) checked @endif>
                                    <label for="checkbox8">
                                        edit_report
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_report" id="checkbox9" name="permission[]"
                                           disabled @if($user->hasPermissionTo('delete_report')) checked @endif>
                                    <label for="checkbox9">
                                        delete_report
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_violation" id="checkbox10" name="permission[]"
                                           disabled @if($user->hasPermissionTo('create_violation')) checked @endif>
                                    <label for="checkbox10">
                                        create_violation
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_violation" id="checkbox11" name="permission[]"
                                           disabled @if($user->hasPermissionTo('view_violation')) checked @endif>
                                    <label for="checkbox11">
                                        view_violation
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_violation" id="checkbox12" name="permission[]"
                                           disabled @if($user->hasPermissionTo('edit_violation')) checked @endif>
                                    <label for="checkbox12">
                                        edit_violation
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_violation" id="checkbox13" name="permission[]"
                                           disabled @if($user->hasPermissionTo('delete_violation')) checked @endif>
                                    <label for="checkbox13">
                                        delete_violation
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_document" id="checkbox15" name="permission[]"
                                           disabled @if($user->hasPermissionTo('create_document')) checked @endif>
                                    <label for="checkbox15">
                                        create_document
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_document" id="checkbox16" name="permission[]"
                                           disabled @if($user->hasPermissionTo('view_document')) checked @endif>
                                    <label for="checkbox16">
                                        view_document
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_document" id="checkbox17" name="permission[]"
                                           disabled @if($user->hasPermissionTo('edit_document')) checked @endif>
                                    <label for="checkbox17">
                                        edit_document
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_document" id="checkbox18" name="permission[]"
                                           disabled @if($user->hasPermissionTo('delete_document')) checked @endif>
                                    <label for="checkbox18">
                                        delete_document
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_user" id="checkbox19" name="permission[]"
                                           disabled @if($user->hasPermissionTo('create_user')) checked @endif>
                                    <label for="checkbox19">
                                        create_user
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_user" id="checkbox20" name="permission[]"
                                           disabled @if($user->hasPermissionTo('view_user')) checked @endif>
                                    <label for="checkbox20">
                                        view_user
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_user" id="checkbox21" name="permission[]"
                                           disabled @if($user->hasPermissionTo('edit_user')) checked @endif>
                                    <label for="checkbox21">
                                        edit_user
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_user" id="checkbox22" name="permission[]"
                                           disabled @if($user->hasPermissionTo('delete_user')) checked @endif>
                                    <label for="checkbox22">
                                        delete_user
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="assign_project" id="checkbox5" name="permission[]"
                                           disabled @if($user->hasPermissionTo('assign_project')) checked @endif>
                                    <label for="checkbox5">
                                        assign_project
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="assign_violation" id="checkbox14" name="permission[]"
                                           disabled @if($user->hasPermissionTo('assign_violation')) checked @endif>
                                    <label for="checkbox14">
                                        assign_violation
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-group">
                    <button class="btn btn-primary btn-o next-step btn-wide pull-right">
                        {{trans('pmis.save')}} <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
    <div class="modal fade" id="change_password_modal" tabindex="-1" role="dialog"
         aria-labelledby="Change Password Modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Change Password</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="{{action('UserController@resetPassword')}}" method="post"
                          id="form">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label class="col-md-3 control-label">
                                Current Password
                            </label>
                            <div class="col-md-8">
                                <input type="password" class="form-control" name="current_password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">
                                New Password
                            </label>
                            <div class="col-md-8">
                                <input type="password" class="form-control" name="new_password">
                            </div>
                        </div>
                        <div class="form-group container">
                            <div class="col-md-2 pull-right">
                                <button class="btn btn-o btn-primary" type="submit">
                                    {{trans('pmis.save')}}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script>
    $('.ui.dropdown').dropdown();
</script>
@endpush
