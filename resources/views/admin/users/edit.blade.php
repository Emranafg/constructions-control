@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 5])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans("user.editing_user"), 'page' => trans('user.users'), 'current' => trans("pmis.edit")])
@stop
@section('alert-message')
    @if(session('message_title'))
        <div role="alert" class="alert {{ session('message_class') }}">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
        </div>
    @endif
@stop
@section('style')
    <style media="screen">
        .select2-container {
            display: none;
        }
        .input-sm {
            height: 35px !important;
        }
    </style>
@stop
@section('main-content')
    <form action="{{action('UserController@update')}}" method="post" role="form"
          id="form">
        {!! csrf_field() !!}
        <input type="hidden" name="id" value="{{ $user->id }}">
        <div class="container-fluid container-fullw bg-white">
            <div class="row">
                <fieldset>
                    <legend>
                        {{trans('user.user_details')}}
                    </legend>
                    <div class="row">
                        <div class="col-md-3">
                            <label>
                                {{trans('user.first_name')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="first_name" class="form-control" type="text"
                                       value="{{ $user->first_name }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label>
                                {{trans('user.last_name')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="last_name" class="form-control" type="text"
                                       value="{{ $user->last_name }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label>
                                {{trans('pmis.Email')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">

                                <input placeholder="" name="email" class="form-control" type="email"
                                       value="{{ $user->email }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>
                                {{trans('pmis.Phone_Number')}} <span class="symbol required"
                                                                     aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="phone" class="form-control" type="text"
                                       value="{{ $user->phone }}">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label>
                                {{trans('user.password')}} <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="password" class="form-control" type="password"
                                       value="LFueXEhYe4eRmDJ" disabled="true">
                                <span class="change-password" id="change_password_user">Change password</span>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <label>
                                {{trans('user.role')}} <span class="symbol"></span>
                            </label>
                            <select class="form-control ui dropdown" id="role" name="role" onchange="permissions(this.value)">
                                @if($user->hasRole('admin'))
                                    <option value="admin" selected>Admin</option>
                                    <option value="user">User</option>
                                    <option value="supervisor">Supervisor</option>
                                @elseif($user->hasRole('user'))
                                    <option value="user">User</option>
                                    <option value="supervisor">Supervisor</option>
                                    <option value="admin">Admin</option>
                                @else
                                    <option value="supervisor">Supervisor</option>
                                    <option value="user">User</option>
                                    <option value="admin">Admin</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>
                                {{trans('pmis.district')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <select class="form-control ui dropdown" name="user_district">
                                <option value="{{ $user->user_district }}"></option>
                                @foreach($district as $district)
                                    @if(App::isLocale('fa'))
                                        <option value="{{$district->id}}">{{$district->fa_name}}</option>
                                    @elseif(App::isLocale('pa'))
                                        <option value="{{$district->id}}">{{$district->pa_name}}</option>
                                    @else
                                        <option value="{{$district->id}}">{{$district->en_name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend> {{trans('user.permissions')}}
                        <span class="symbol required" aria-required="true"></span>
                        <span class="hint">{{ trans('user.at_least') }}</span>
                    </legend>
                    <div class="row">
                        <div class="col-md-12" id="permission">
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_project" id="checkbox1" name="permission[]"
                                           @if($user->hasPermissionTo('create_project')) checked @endif>
                                    <label for="checkbox1">
                                        {{trans('user.create_project')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_project" id="checkbox2" name="permission[]"
                                           @if($user->hasPermissionTo('view_project')) checked @endif>
                                    <label for="checkbox2">
                                        {{trans('user.view_project')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_project" id="checkbox3" name="permission[]"
                                           @if($user->hasPermissionTo('edit_project')) checked @endif>
                                    <label for="checkbox3">
                                        {{trans('user.edit_project')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_project" id="checkbox4" name="permission[]"
                                           @if($user->hasPermissionTo('delete_project')) checked @endif>
                                    <label for="checkbox4">
                                        {{trans('user.delete_project')}}
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_report" id="checkbox6" name="permission[]"
                                           @if($user->hasPermissionTo('create_report')) checked @endif>
                                    <label for="checkbox6">
                                        {{trans('user.create_report')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_report" id="checkbox7" name="permission[]"
                                           @if($user->hasPermissionTo('view_report')) checked @endif>
                                    <label for="checkbox7">
                                        {{trans('user.view_report')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_report" id="checkbox8" name="permission[]"
                                           @if($user->hasPermissionTo('edit_report')) checked @endif>
                                    <label for="checkbox8">
                                        {{trans('user.edit_report')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_report" id="checkbox9" name="permission[]"
                                           @if($user->hasPermissionTo('delete_report')) checked @endif>
                                    <label for="checkbox9">
                                        {{trans('user.delete_report')}}
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_violation" id="checkbox10" name="permission[]"
                                           @if($user->hasPermissionTo('create_violation')) checked @endif>
                                    <label for="checkbox10">
                                        {{trans('user.create_violation')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_violation" id="checkbox11" name="permission[]"
                                           @if($user->hasPermissionTo('view_violation')) checked @endif>
                                    <label for="checkbox11">
                                        {{trans('user.view_violation')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_violation" id="checkbox12" name="permission[]"
                                           @if($user->hasPermissionTo('edit_violation')) checked @endif>
                                    <label for="checkbox12">
                                        {{trans('user.edit_violation')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_violation" id="checkbox13" name="permission[]"
                                           @if($user->hasPermissionTo('delete_violation')) checked @endif>
                                    <label for="checkbox13">
                                        {{trans('user.delete_violation')}}
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_document" id="checkbox15" name="permission[]"
                                           @if($user->hasPermissionTo('create_document')) checked @endif>
                                    <label for="checkbox15">
                                        {{trans('user.create_document')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_document" id="checkbox16" name="permission[]"
                                           @if($user->hasPermissionTo('view_document')) checked @endif>
                                    <label for="checkbox16">
                                        {{trans('user.view_document')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_document" id="checkbox17" name="permission[]"
                                           @if($user->hasPermissionTo('edit_document')) checked @endif>
                                    <label for="checkbox17">
                                        {{trans('user.edit_document')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_document" id="checkbox18" name="permission[]"
                                           @if($user->hasPermissionTo('delete_document')) checked @endif>
                                    <label for="checkbox18">
                                        {{trans('user.delete_document')}}
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="create_user" id="checkbox19" name="permission[]"
                                           @if($user->hasPermissionTo('create_user')) checked @endif>
                                    <label for="checkbox19">
                                        {{trans('user.create_user')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="view_user" id="checkbox20" name="permission[]"
                                           @if($user->hasPermissionTo('view_user')) checked @endif>
                                    <label for="checkbox20">
                                        {{trans('user.view_user')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_user" id="checkbox21" name="permission[]"
                                           @if($user->hasPermissionTo('edit_user')) checked @endif>
                                    <label for="checkbox21">
                                        {{trans('user.edit_user')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="delete_user" id="checkbox22" name="permission[]"
                                           @if($user->hasPermissionTo('delete_user')) checked @endif>
                                    <label for="checkbox22">
                                        {{trans('user.delete_user')}}
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="assign_project" id="checkbox5" name="permission[]"
                                           @if($user->hasPermissionTo('assign_project')) checked @endif>
                                    <label for="checkbox5">
                                        {{trans('user.assign_project')}}
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="assign_violation" id="checkbox14" name="permission[]"
                                           @if($user->hasPermissionTo('assign_violation')) checked @endif>
                                    <label for="checkbox14">
                                        {{trans('user.assign_violation')}}
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="edit_plan" id="checkbox23" name="permission[]"
                                        @if($user->hasPermissionTo('edit_plan')) checked @endif>
                                    <label for="checkbox23">
                                        {{trans('user.edit_plan')}}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="building_view" id="checkbox24" name="permission[]"
                                    @if($user->hasPermissionTo('building_view')) checked @endif>
                                    <label for="checkbox24">
                                      bulding view
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="parking_delete" id="checkbox25" name="permission[]"
                                    @if($user->hasPermissionTo('parking_delete')) checked @endif>
                                    <label for="checkbox25">
                                    parking_delete
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="parking_edit" id="checkbox26" name="permission[]"
                                    @if($user->hasPermissionTo('parking_edit')) checked @endif>
                                    <label for="checkbox26">
                                    parking_edit
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="parking_view" id="checkbox27" name="permission[]"
                                    @if($user->hasPermissionTo('parking_view')) checked @endif>
                                    <label for="checkbox27">
                                    parking_view
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="building_delete" id="checkbox28" name="permission[]"
                                    @if($user->hasPermissionTo('building_delete')) checked @endif>
                                    <label for="checkbox28">
                                    building_delete
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="building_edit" id="checkbox29" name="permission[]"
                                    @if($user->hasPermissionTo('building_edit')) checked @endif>
                                    <label for="checkbox29">
                                    building_edit
                                    </label>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </fieldset>
                <div class="form-group">
                    <button class="btn btn-primary btn-o next-step btn-wide pull-right">
                        {{trans('pmis.save')}} <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
@stop

@push('scripts')
<script>
    $('.ui.dropdown').dropdown();
</script>
@endpush
@section('validator')
    {!! $validator !!}
@endsection
