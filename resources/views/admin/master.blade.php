<!DOCTYPE html>
<html lang="en">
<head>
    <title>Kabul Municipality Database</title>
    <meta charset="utf-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <meta content="{!! csrf_token() !!}" name="csrf_token"/>
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic"
          rel="stylesheet" type="text/css"/>

    <link href="{{asset('standard/assets/css/print.css')}}"/>
    <!-- end: GOOGLE FONTS -->
    @if(App::isLocale('en'))
    <link rel="stylesheet" href="{{asset('standard/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('standard/vendor/fontawesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('standard/vendor/themify-icons/themify-icons.min.css')}}">
    <link href="{{asset('standard/vendor/animate.css/animate.min.css')}}" rel="stylesheet" media="screen">
    <link href="{{asset('standard/vendor/perfect-scrollbar/perfect-scrollbar.min.css')}}" rel="stylesheet"
          media="screen">
    <link href="{{asset('standard/vendor/switchery/switchery.min.css')}}" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="{{asset('standard/assets/css/styles.css')}}">
    <link rel="stylesheet" href="{{asset('standard/assets/css/plugins.css')}}">
    <link rel="stylesheet" href="{{asset('standard/assets/css/themes/theme-1.css')}}" id="skin_color"/>
    <link rel="stylesheet" href="{{asset('vendor/semanticmaster/semantic.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/datatables2/css/dataTables.semanticui.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custome.css')}}">
    <link rel="stylesheet" href="{{asset('css/icon-font/flaticon.css')}}">

    <link href="{{asset('standard/vendor/switchery/switchery.min.css')}}" rel="stylesheet" media="screen">
    <link href="{{asset('standard/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet"
          media="screen">
    <link href="{{asset('standard/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}" rel="stylesheet"
          media="screen">
    <link href="{{asset('standard/vendor/bootstrap-timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet"
          media="screen">
    <link href="{{asset('standard/vendor/select2/select2.min.css')}}" rel="stylesheet" media="screen">

    <link href="{{asset('standard/vendor/select2/select2.min.css')}}" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="{{asset('vendor/dropzone/dropzone.css')}}" id="skin_color"/>
    @endif
    <!-- general css files -->
    <link rel="stylesheet" href="{{asset('vendor/semanticmaster/components/dropdown.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/semanticmaster/components/transition.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/semanticmaster/components/progress.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/semanticmaster/components/input.min.css')}}">
    <link rel="stylesheet" href="{{asset('standard/vendor/sweetalert/sweet-alert.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('standard/vendor/sweetalert/ie9.css')}}" media="screen">
    <!-- end of css files -->

    <!-- start: RTL main CSS -->
    @if(App::isLocale('fa') || App::isLocale('pa'))
        <link rel="stylesheet" href="{{asset('rtlversion/vendor/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('rtlversion/vendor/bootstrap/css/bootstrap-rtl.min.css')}}">
        <link rel="stylesheet" href="{{asset('rtlversion/vendor/fontawesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('rtlversion/vendor/themify-icons/themify-icons.min.css')}}">
        <link href="{{asset('rtlversion/vendor/animate.css/animate.min.css')}}" rel="stylesheet" media="screen">
        <link href="{{asset('rtlversion/vendor/perfect-scrollbar/perfect-scrollbar.min.css')}}" rel="stylesheet"
              media="screen">
        <link href="{{asset('rtlversion/vendor/switchery/switchery.min.css')}}" rel="stylesheet" media="screen">

        <link rel="stylesheet" href="{{asset('rtlversion/assets/css/styles.css')}}">
        <link rel="stylesheet" href="{{asset('rtlversion/assets/css/plugins.css')}}">
        <link rel="stylesheet" href="{{asset('rtlversion/assets/css/themes/theme-1.css')}}" id="skin_color"/>
        <link rel="stylesheet" href="{{asset('rtlversion/assets/css/rtl.css')}}"/>

        <link href="{{asset('standard/vendor/switchery/switchery.min.css')}}" rel="stylesheet" media="screen">
        <link href="{{asset('standard/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
              rel="stylesheet" media="screen">
        <link href="{{asset('standard/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}" rel="stylesheet"
              media="screen">
        <link href="{{asset('rtlversion/vendor/bootstrap-timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet"
              media="screen">

        <link href="{{asset('standard/vendor/select2/select2.min.css')}}" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="{{asset('vendor/dropzone/dropzone.css')}}" id="skin_color"/>

        <!-- Yajra dataTable css -->
        <link rel="stylesheet" href="{{asset('vendor/semanticmaster/semantic.min.css')}}">
        <link rel="stylesheet" href="{{asset('vendor/datatables2/css/dataTables.semanticui.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/custome.css')}}">
        <link rel="stylesheet" href="{{asset('css/icon-font/flaticon.css')}}">
        <link rel="stylesheet" href="{{asset('rtlversion/customertl.css')}}"/>
        @endif
                <!-- end: RTL main CSS -->
        <link rel="stylesheet" href="{{asset('css/style.css')}}">

        @yield('style')
</head>
<!-- end: HEAD -->
<body>
<div id="app">
    <!-- sidebar -->
    @yield('side-bar')
            <!-- / sidebar -->
    <div class="app-content">
        <!-- start: TOP NAVBAR -->
        <header class="navbar navbar-default navbar-static-top">
            <!-- start: NAVBAR HEADER -->
            <div class="navbar-header">
                <a href="#" class="sidebar-mobile-toggler pull-left hidden-md hidden-lg"
                   class="btn btn-navbar sidebar-toggle" data-toggle-class="app-slide-off" data-toggle-target="#app"
                   data-toggle-click-outside="#sidebar">
                    <i class="ti-align-justify"></i>
                </a>
                <a class="navbar-brand" href="#">
                    <img height="50" width="125" src="{{asset('standard/assets/images/logo.png')}}" alt="Municipality"/>
                </a>
                <a href="#" @if(App::isLocale('en')) class="sidebar-toggler pull-right visible-md visible-lg"
                   @else class="sidebar-toggler pull-left visible-md visible-lg" @endif
                   data-toggle-class="app-sidebar-closed" data-toggle-target="#app">
                    <i class="ti-align-justify"></i>
                </a>
                <a class="pull-right menu-toggler visible-xs-block" id="menu-toggler" data-toggle="collapse"
                   href=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="ti-view-grid"></i>
                </a>
            </div>
            <!-- end: NAVBAR HEADER -->
            <!-- start: NAVBAR COLLAPSE -->
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-right">
                    <li class="dropdown">
                        <a href class="dropdown-toggle" data-toggle="dropdown">
                            <i class="ti-world"></i> {{trans('pmis.LANGUAGE')}}
                        </a>
                        <ul role="menu" class="dropdown-menu dropdown-light fadeInUpShort">
                            <li>
                                <a href="/locale/en" class="menu-toggler">
                                    English
                                </a>
                            </li>
                            <li>
                                <a href="/locale/fa" class="menu-toggler">
                                    دری
                                </a>
                            </li>
                            <li>
                                <a href="/locale/pa" class="menu-toggler">
                                    پشتو
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- start: LANGUAGE SWITCHER -->
                    <!-- start: USER OPTIONS DROPDOWN -->
                    <li class="dropdown current-user">
                        <a href class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{asset('standard/assets/images/default-user.png')}}"> <span class="username">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }} <i class="ti-angle-down"></i></i></span>
                        </a>
                        <ul class="dropdown-menu dropdown-dark">
                            <li>
                                <a href="/user/profile">
                                    My Profile
                                </a>
                                <a href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    <!-- end: USER OPTIONS DROPDOWN -->
                </ul>
                <!-- start: MENU TOGGLER FOR MOBILE DEVICES -->
                <div class="close-handle visible-xs-block menu-toggler" data-toggle="collapse" href=".navbar-collapse">
                    <div class="arrow-left"></div>
                    <div class="arrow-right"></div>
                </div>
                <!-- end: MENU TOGGLER FOR MOBILE DEVICES -->
            </div>
            <a class="dropdown-off-sidebar sidebar-mobile-toggler hidden-md hidden-lg"
               data-toggle-class="app-offsidebar-open" data-toggle-target="#app"
               data-toggle-click-outside="#off-sidebar">
                &nbsp;
            </a>
            <!-- end: NAVBAR COLLAPSE -->
        </header>
        <!-- end: TOP NAVBAR -->
        <div class="main-content">
            <div class="wrap-content container" id="container">
                @yield('alert-message')
                        <!-- start: PAGE TITLE -->
                <section id="page-title">
                    @yield('page-title')
                </section>
                <!-- end: PAGE TITLE -->
                <!-- start: YOUR CONTENT HERE -->
                @yield('main-content')
                <!-- end: YOUR CONTENT HERE -->
            </div>
        </div>
    </div>
    <!-- start: FOOTER -->
    <footer>
        <div class="footer-inner">
            <div class="pull-left">
                &copy; <span class="current-year"></span><span class="text-bold text-uppercase"> <a href="http://km.gov.af" target="_blank">KM IT</a></span>.
                <span>All rights reserved</span>
            </div>
            <div class="pull-right">
                <span class="go-top"><i class="ti-angle-up"></i></span>
            </div>
        </div>
    </footer>
</div>
<!-- start: MAIN JAVASCRIPTS -->
<script src="{{asset('standard/vendor/jquery/jquery.min.js')}}"></script>
<!-- jQuery datatable -->
<script src="{{asset('vendor/datatables2/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables2/dataTables.semanticui.min.js')}}"></script>

<script src="{{asset('standard/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('standard/vendor/modernizr/modernizr.js')}}"></script>
<script src="{{asset('standard/vendor/jquery-cookie/jquery.cookie.js')}}"></script>
<script src="{{asset('standard/vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('standard/vendor/switchery/switchery.min.js')}}"></script>
<!-- end: MAIN JAVASCRIPTS -->
<script src="{{asset('standard/vendor/autosize/autosize.min.js')}}"></script>
<script src="{{asset('standard/vendor/maskedinput/jquery.maskedinput.min.js')}}"></script>
<script src="{{asset('standard/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
<script src="{{asset('standard/vendor/selectFx/classie.js')}}"></script>
<script src="{{asset('standard/vendor/selectFx/selectFx.js')}}"></script>
<script src="{{asset('standard/vendor/select2/select2.min.js')}}"></script>

<script src="{{asset('standard/vendor/jquery-smart-wizard/jquery.smartWizard.js')}}"></script>
<script src="{{asset('standard/assets/js/form-wizard.js')}}"></script>

<script src="{{asset('standard/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
@if(App::isLocale('en'))
    <script src="{{asset('standard/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>
@endif

<script src="{{asset('standard/vendor/jquery.sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('vendor/semanticmaster/components/transition.min.js')}}"></script>
<script src="{{asset('vendor/semanticmaster/components/progress.min.js')}}"></script>
<script src="{{asset('standard/vendor/sweetalert/sweet-alert.min.js')}}"></script>

@include('partials.theme')
        <!-- start: JavaScript Event Handlers for this page -->
<script src="{{asset('vendor/semanticmaster/components/dropdown.js')}}"></script>
<script src="{{asset('standard/assets/js/form-elements.js')}}"></script>
<script src="{{asset('vendor/dropzone/dropzone.js')}}"></script>

<script src="{{asset('vendor/dropzone/dropzone-config.js')}}"></script>
<script src="{{asset('standard/assets/js/index.js')}}"></script>

<script src="{{asset('standard/assets/js/table-data.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
@if(App::isLocale('fa') || App::isLocale('pa'))
    <script src="{{asset('rtlversion/vendor/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
    <script src="{{asset('standard/vendor/bootstrap-datepicker/bootstrap-datepicker.fa.js')}}"></script>
@endif
<script src="{{asset('vendor/jsvalidation/js/jsvalidation.min.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>
    <script>
    jQuery(document).ready(function () {
        Main.init();
        Index.init();
        FormElements.init();
        FormWizard.init();
    });
</script>
@stack('scripts')
@yield('validator')
</body>
</html>
