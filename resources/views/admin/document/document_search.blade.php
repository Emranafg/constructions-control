@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active'      => 4, 'subActive' => '1'])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans('dms.Documents'), 'page' => trans('pmis.reports'), 'current' => trans('dms.Documents')])
@stop
@section('alert-message')
    @if(session('message_title'))
        <div role="alert" class="alert {{ session('message_class') }}">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
        </div>
    @endif
@stop
@section('main-content')
    <div class="container-fluid container-fullw bg-white">
        <div class="container">
            <form role="form" class="form-inline" id="search-form" method="POST">
                <div class="row">
                    <div class="panel panel-white ">
                        <div class="panel-heading border-light">
                            <h4 class="panel-title">{{trans('dms.Documents')}}</h4>
                            <ul class="panel-heading-tabs border-light">
                                <li>
                                    <div class="pull-right">
                                        <div class="ui labeled button" tabindex="0" style="direction: ltr">
                                            <a href="/document/create" class="ui red button">
                                                <i class="plus icon"></i> {{trans('dms.create_document')}}
                                            </a>
                                            <a class="ui basic red left pointing label"
                                               style="border-radius: 0px !important">
                                                {{ $infCount }}
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="pull-right">
                                        <div class="btn-group" dropdown="">
                                            <div class="ui buttons">
                                               <!-- <a id="export" class="ui button">{{trans('pmis.Export')}}</a>
                                                <div class="or"></div>
                                                <a id="genarate" class="ui button report">{{trans('pmis.Filter')}} </a> -->
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="panel-tools">
                                    <a data-original-title="Refresh" data-toggle="tooltip" data-placement="top"
                                       class="btn btn-transparent btn-sm panel-refresh" href="#">
                                        <i class="flaticon-business-2">
                                        </i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                 <!--   <div class="col-md-3">
                                        <select class="ui dropdown" name="progress" id="progress">
                                            <option value="">{{trans('pmis.status')}} </option>
                                            <option value="1" id="search-form">{{trans('pmis.start')}}</option>
                                            <option value="2"
                                                    id="search-form">{{trans('pmis.under_progress')}} </option>
                                            <option value="3" id="search-form">{{trans('pmis.compleated')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="ui dropdown" name="priority" id="priority">
                                            <option value=""> priority</option>
                                            <option value="" hidden="">{{trans('dms.periority')}}</option>
                                            <option value="1">{{trans('dms.Documents_action')}}</option>
                                            <option value="2">{{trans('dms.Public_petitions')}}</option>
                                            <option value="3">{{trans('dms.Instructions_mayor')}}</option>
                                            <option value="4">{{trans('dms.Supreme_meetings')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="ui dropdown" name="version" id="version">
                                            <option value="">{{trans('dms.Version')}}</option>
                                            <option value="1">{{trans('dms.First')}}</option>
                                            <option value="2">{{trans('dms.Second')}}</option>
                                            <option value="3">{{trans('dms.Third')}}</option>
                                        </select>
                                    </div> 
                                    <div class="col-md-3">
                                        <div class="ui fluid icon input" style="max-width: 198px !important;">
                                            <input id="expiration_date" name="expiration_date" class="datepicker"
                                                   data-date-format="yyyy-mm-dd"
                                                   placeholder="Search a very wide input..." type="text"
                                                   style="max-width: 198px !important;">
                                            <i class="calendar icon"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>  -->
                            <div class="row margin-top-30 border-top">
                                <div class="margin-top-10"></div>
                                <div @if(App::isLocale('fa') || App::isLocale('pa'))
                                     class="col-md-12 margin-right-15" @else class="col-md-12 margin-left-15" @endif >
                                    <table class="ui celled table" id="users-table"
                                           style="width: 100%; text-align: center !important;">
                                        <thead>
                                        <tr>
                                            <th>{{trans('pmis.id')}}</th>
                                            <th>{{trans('dms.Case')}}</th>
                                            <th>{{trans('dms.Title')}}</th>
                                            <th>{{trans('dms.Source')}}</th>
                                            
                                            <th>{{trans('dms.Start_Datee')}}</th>
                                       
                                            <th>{{trans('pmis.edit')}}</th>
                                            <th>{{trans('pmis.delete')}}</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@push('scripts')
<script>
    var oTable = $('#users-table').DataTable({
        bFilter: false,
        processing: true,
        serverSide: true,
        "language": {
            "lengthMenu": "{{trans('pmis.show')}} _MENU_{{trans('pmis.entries')}}",
            "zeroRecords": "{{trans('pmis.Nothing_found_sorry')}}",
            // "info": "{{trans('pmis.show')}}_PAGES_{{trans('pmis.of')}} _MAX_{{trans('pmis.entries')}}",
            "info": "{{trans('pmis.show')}} _START_ {{trans('pmis.of')}} _END_ {{trans('pmis.of')}} _TOTAL_ {{trans('pmis.entries')}}",//sInfoFiltered:"({{trans('pmis.show')}} _MAX_ total {{trans('pmis.entries')}})"
            "infoEmpty": "{{trans('pmis.No_records_available')}}",
            "infoFiltered": "({{trans('pmis.show')}} {{trans('pmis.of')}}_MAX_ {{trans('pmis.entries')}})",
            "search": "{{trans('pmis.search')}}",
            paginate: {

                next: "{{trans('pmis.next')}}",
                previous: "{{trans('pmis.previous')}}"
            },
            // "next":"{{trans('pmis.search')}}"
        },
        ajax: {
            url: '/search/document',
            data: function (d) {
                d.progress = $('select[name=progress]').val();
                d.priority = $('select[name=priority]').val();
                d.version = $('select[name=version]').val();
                d.expiration_date = $('select[name=expiration_date]').val();
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'case', name: 'case'},
            {data: 'title', name: 'title'},
            {data: 'source', name: 'source'},
           
            {data: 'start_date', name: 'start_date'},
        
            {data: 'edit', name: 'edit'},
            {data: 'delete', name: 'delete'},
        ]
    });
    $('#progress, #priority, #version, #expiration_date').change(function (e) {
        oTable.draw();
        e.preventDefault();
    });
    var progress = 'unknown';
    var priority = 'unknown';
    var version = 'unknown';
    var expiration_date = 'unknown';

    function initValues() {
        progress = $('#progress').val() ? $('#progress').val() : 'unknown';
        priority = $('#priority').val() ? $('#priority').val() : 'unknown';
        version = $('#version').val() ? $('#version').val() : 'unknown';
        expiration_date = $('#expiration_date').val() ? $('#expiration_date').val() : 'unknown';
    }

    function getUrl() {
        return '/report/export/excel/progress/' + progress + '/priority/'
                + priority + '/version/' + version + '/expiration_date/' + expiration_date;
    }
    $("#genarate").click(function () {
        initValues();
        alert(getUrl());
        window.location = getUrl()
    });
    $('#export').click(function () {
        window.location = "/excel";
    });
    $('.ui.dropdown')
            .dropdown()
    ;
</script>
@endpush
