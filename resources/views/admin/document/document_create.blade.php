@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 4, 'subActive' => '1'])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans('dms.Document_information'), 'page' => trans('pmis.reports'), 'current' => trans('pmis.projects')])
@stop
@section('main-content')

    <form method="post" action="{{action('DocumentController@documentStore')}}" class="document" id="form">
        {!! csrf_field() !!}
        <div class="container-fluid container-fullw bg-white">
            <div class="row">
                <fieldset>
                    <legend>{{trans('dms.insert_here')}}</legend>
                    <div class="col-md-4">
                        <label>
                            {{trans('dms.Title')}} <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="  " class="form-control" name="title" type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('dms.Source')}} <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="  " class="form-control" name="source" type="text">
                        </div>
                    </div>
                <!--<div class="col-md-4">
                        <label>
                            {{trans('dms.Destination')}} <span class="symbol "></span>
                        </label>
                        <div class="form-group">
                            </label>
                            <input placeholder="  " class="form-control" name="destination"
                                   type="text">
                        </div>   -->
                    </div>  
                   <div class="col-md-4">
                        <label>
                            {{trans('dms.Start_Datee')}} <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="start_date"
                                   class="form-control datepicker" type="text">
                        </div>
                    </div>
                   <div class="col-md-4">
                        <label>
                            {{trans('dms.expiration_datee')}} <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="expiration_date"
                                   class="form-control datepicker" type="text">
                        </div>
                    </div>
               <!--     <div class="col-md-4">
                        <label>
                            {{trans('dms.category')}} <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="category">
                            <option value="1">{{trans('dms.Archive')}}</option>
                            <option value="2">{{trans('dms.Tahrir_File')}}</option>
                            <option value="3">{{trans('dms.Documents')}}</option>
                            <option value="4">{{trans('dms.Petitions')}}</option>
                        </select>
                    </div> -->
              <!--      <div class="col-md-4">
                        <label>
                            {{trans('dms.periority')}} <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="priority">
                            <option value="1">{{trans('dms.Documents_action')}}</option>
                            <option value="2">{{trans('dms.Public_petitions')}}</option>
                            <option value="3">{{trans('dms.Instructions_mayor')}}</option>
                            <option value="4">{{trans('dms.Supreme_meetings')}}</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('dms.progress')}} <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="progress">
                            <option value="1">{{trans('pmis.start')}}</option>
                            <option value="2">{{trans('pmis.under_progress')}}</option>
                            <option value="3">{{trans('pmis.compleated')}}</option>

                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('dms.version')}} <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="version">
                            <option value="1">{{trans('dms.First')}}</option>
                            <option value="2">{{trans('dms.Second')}}</option>
                            <option value="3">{{trans('dms.Third')}}</option>
                            <option value="4">{{trans('dms.Fourth')}}</option>
                        </select>
                    </div> -->
                    <div class="col-md-12">
                        <label>
                            {{trans('dms.Case')}} <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <textarea name="case" rows="10" cols="90"></textarea>
                        </div>
                    </div> 
                    <div class="document_file">
                    </div>
                  <!--  <div class="panel-body col-md-12">
                        <div id="document_file" class="dropzone"></div>
                    </div>-->
                </fieldset>
                <div class="form-group">
                    <button class="btn btn-primary btn-o next-step btn-wide pull-right">
                        {{trans('dms.save')}} <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
@stop
@push('scripts')
<script>
    $('.ui.dropdown').dropdown();
</script>
<script>
  // Dropzone has been added as a global variable.
  const dropzone = new Dropzone("div.dropzone", { url: "/DocumentController" });
</script>
@endpush
@section('validator')
    {!! JsValidator::formRequest('App\Http\Requests\DocumentRequest') !!}
@endsection
