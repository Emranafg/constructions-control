@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 4, 'subActive' => 1])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => 'Project Details', 'page' => trans('dms.Documents'), 'current' => trans('pmis.details')])
@stop
@section('alert-message')
    @if(session('message_title'))
        <div role="alert" class="alert {{ session('message_class') }}" xmlns="http://www.w3.org/1999/html">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
        </div>
    @endif
@stop
@section('main-content')
    <div class="container-fluid container-fullw bg-white">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12 padding-top-30">
                    <div class="ui card card-full-width">

                        <div class="panel panel-white">
                            <div class="panel-heading border-light">
                                <h4 class="panel-title">{{trans('dms.Case')}}</h4>
                                <ul class="panel-heading-tabs border-light">
                                    <li>
                                        <div class="pull-right">
                                            <div class="ui labeled button" tabindex="0" style="direction: ltr">
                                                <a href="/document/{{ $documentDetails->id }}/case/create"
                                                   class="ui red button">
                                                    <i class="plus icon"></i> {{ trans('dms.add_case') }}
                                                </a>
                                                <a class="ui basic red left pointing label"
                                                   style="border-radius: 0px !important">

                                                </a>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="col-md-6 padding-top-30">
                                    <fieldset class="padding-top-15" style="min-height: 250px !important;">
                                        <legend><i style="font-size: 18px" class="flaticon-icon-107807 icon-red"></i>
                                            <span style="">{{trans('dms.document_details')}}</span>
                                        </legend>
                                        <div class="ui relaxed divided list">
                                            <div class="item">

                                                <div class="content">
                                                    <a class="header">{{trans('dms.Title')}}</a>
                                                    <div class="description">{{$documentDetails->title}}</div>
                                                </div>
                                            </div>
                                            <div class="item">

                                                <div class="content">
                                                    <a class="header">{{trans('dms.Source')}}</a>
                                                    <div class="description">{{$documentDetails->source}}</div>
                                                </div>
                                            </div>
                                         <!--   <div class="item">

                                                <div class="content">
                                                    <a class="header">{{trans('dms.Destination')}}</a>
                                                    <div class="description">{{$documentDetails->destination}}</div>
                                                </div>
                                            </div> -->
                                        <!--    <div class="item"> 

                                                <div class="content">
                                                    <a class="header">{{trans('dms.category')}}</a>
                                                    <div class="description">{{$documentDetails->category}}</div>
                                                </div>
                                            </div> -->
                                            <div class="item">

                                                <div class="content">
                                                    <a class="header">{{trans('dms.Start_Datee')}}</a>
                                                    <div class="description">{{$documentDetails->start_date}}</div>
                                                </div>
                                            </div>
                                            <div class="item">

                                                <div class="content">
                                                    <a class="header">{{trans('dms.End_Datee')}}</a>
                                                    <div class="description">{{$documentDetails->expiration_date}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-6 padding-top-30">
                                    <fieldset class="padding-top-15" style="min-height: 250px !important;">
                                        <legend><i style="font-size: 18px" class="flaticon-icon-107807 icon-red"></i>
                                            <span style="">{{trans('dms.Case')}}</span>
                                        </legend>
                                        <div class="ui relaxed divided list">
                                            <div class="ui relaxed divided list">
                                                <div class="">
                                                    <div class="content">
                                                        <a class="header" href="/{{$documentDetails->file}}"> <span class="description">{{$documentDetails->case}} </span></a>
                                                        <hr>
                                                        @foreach($documentDetails->DocumentUpload as $document)
                                                            <a class="header" href="/{{$document->path}}"> <span
                                                                        class="description">{{$document->name}} </span></a>
                                                            <hr>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
