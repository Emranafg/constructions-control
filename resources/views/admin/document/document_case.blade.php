@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 4, 'subActive' => '1'])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => 'Creating new case', 'page' => 'Reports', 'current' => 'projects'])
@stop
@section('main-content')
    <form method="post" action="{{action('DocumentController@storeCase')}}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <fieldset>
            <input type="hidden" name="document_id" value="{{ $id }}">
            <div class="col-md-12">
                <label>
                    <span class="symbol">{{trans('dms.Case')}}</span>
                </label>
                <div class="form-group">
                    <textarea name="case" rows="7"></textarea>
                </div>
            </div>
            <div class="document_file">
            </div>
            <div class="panel-body col-md-12">
               <input type="file" multiple id="gallery-photo-add" name="images[]">
            </div>
        </fieldset>
        <div class="form-group">
            <button class="btn btn-primary btn-o next-step btn-wide pull-right">
                {{trans('dms.save')}} <i class="fa fa-arrow-circle-right"></i>
            </button>
        </div>
    <form>
	
@stop
