<!DOCTYPE html>
<html dir=
        {{-- change the direction if App local is Fa or pa  --}}
        @if(App::isLocale('fa'))
                   {{"rtl"}}                                 
        @elseif(App::isLocale('pa'))
                    {{"rtl"}} 
        @else
                    {{"ltr"}} 
        @endif>
<head>
    <title>Kabul Municipality Database - Google Map</title>
    <style type="text/css">
        body{margin:0;}
        #button{
            position: absolute;
            top: 20px;
            z-index: 100;
            width: 200px;
            margin: 10px;
        }
        /* select Style */
        select{
          padding: 5px;
          color: #333;
          font-size: 15px;
        }
        table {
          border-collapse: collapse;
        }
        table,tr,td{
          border:1px solid black;
        }
        td{
          padding: 2px;
        }
    </style>
</head>
<body>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDMTpTAZvgjLsSkL73ihlxnhFMLZbjfR0U"></script>
    <script type="text/javascript">
      // set the Map variable
      var map;
      var infowindow = new google.maps.InfoWindow();
      function initialize() {
        var markers = [];
        var marker;
        var locations = [];
        
        locations = <?php print($locations) ?>;
        var mapOptions = {
            center: new google.maps.LatLng(34.53757777777778, 69.16273055555556),
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: true,
            mapTypeControl: false
        };
        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        var contentString = [];
      // return all Markers 
      for (var i = 0; i < locations.length; i++) {
                var location = locations[i];
                myLatLng = new google.maps.LatLng(location.latitude, location.longitude);
                var marker = new google.maps.Marker({
                          map: map,
                          position: myLatLng
                      });
                marker.setMap(map);
                markers.push(marker);
                contentString[i] = "<div style='width:300px; text-align: center;  padding:0'>&nbsp;&nbsp;&nbsp;&nbsp;<table style='width:100%;' ><tr><td>{{trans('violation.name')}}</td><td>"+location.name+"</td></tr><tr><td>{{trans('violation.father_name')}}</td><td>"+location.father_name+"</td></tr><tr><td>{{trans('violation.district')}}</td><td>"+location.district+"</td></tr><tr><td>{{trans('violation.street')}}</td><td>"+location.street+"</td></tr><tr><td>{{trans('violation.property_area')}}</td><td>"+location.property_area+"</td></tr><tr><td>{{trans('violation.property_number')}}</td><td>"+location.property_number+"</td></tr><tr><td>{{trans('violation.contruction_certificate')}}</td><td>"+location.contruction_certificate+"</td></tr></table></div>";

                marker.infowindow = new google.maps.InfoWindow({
                    // set the content string for markers 
                    content: contentString[i]
                        });
                          
                          //add click event
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                      return function() {
                          infowindow.setContent(contentString[i]);
                          infowindow.open(map, marker);
                      }
                })(marker, i));
        } // end of for loop
          };

      // initializes the Google Map
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <div id="button"> 
      <label>
    <select onchange="change();" id="one"
    {{-- change the direction of select if local is Fa pa  --}}
    @if(App::isLocale('en'))                       
          {{'ltr'}}                     
    @else
           {{'trl'}}                             
    @endif
    >
        <option value="all">
                  @if(App::isLocale('fa'))
                           انتخاب ناحیه                            
                  @elseif(App::isLocale('pa'))
                           انتخاب ناحیه                            
                  @else
                           Select District                             
                  @endif
        </option>
         <option value="0"
         {{-- show the selected option --}}
           @if($sid == 0)
                     selected=""
                     @endif

         >
                  @if(App::isLocale('fa'))
                   همه                             
                    @elseif(App::isLocale('pa'))
                           همه                           
                    @else
                           All                            
                    @endif
        </option>
                  @foreach($district as $districts)
                  @if(App::isLocale('fa'))
                      <option class="item" value="{{$districts->id}}"
                       @if($districts->id == $sid)
                     selected=""
                     @endif
                      >
                        {{$districts->fa_name}}
                      </option>
                  @elseif(App::isLocale('pa'))
                     <option class="item" value="{{$districts->id}}" 
                      @if($districts->id == $sid)
                     selected=""
                     @endif 
                     >
                        {{$districts->pa_name}}
                      </option>
                  @else
                     <option class="item" value="{{$districts->id}}"
                     @if($districts->id == $sid)
                     selected=""
                     @endif 
                     >
                        {{$districts->en_name}}
                      </option>
                  @endif
              @endforeach        
    </select>

    </div>
  <div id="map_canvas" style="height: 97vh;"></div>
  <script type="text/javascript">

  // one change of selection direct to the map route
    function change(){
      var seletedvalue = document.getElementById('one').value;
      window.location.href = '/km/infractions/map/'+seletedvalue;
    }
  </script>
</body>
</html>

