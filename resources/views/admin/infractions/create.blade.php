@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 3])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans('violation.Creating_Infraction'), 'page' => trans('violation.Violation'), 'current' => trans('pmis.projects')])
@stop
@section('style')
    <style media="screen">
        .ui.dropdown {
            margin-bottom: 11px;
        }
        .infraction-img img{
          max-width: 150px;
          margin: 5px;
        }
        .infraction-img{
          margin: 0 auto;
          overflow: hidden;
          position: relative;
          height: 150px;
        }
    </style>
@stop
@section('main-content')
<script>
  let districts =  {!! json_encode($district) !!};
  function guzar(){
        let chosen_district = document.getElementById('district').value;
    

        let guzar_S  = document.getElementById('Guzar')
        guzar_S.innerHTML = ""
        console.log(districts[chosen_district].no_guzar);
        let dump = document.createElement('option');
        guzar_S.appendChild(dump)
    
    for (var i = 1 ; i<= districts[chosen_district].no_guzar;i++) {
    let option = document.createElement('option');
    pad = "00"
   let value = (pad+i).slice(-pad.length)
    option.value = value;
    option.innerHTML = "Guzar "+value;
    guzar_S.appendChild(option)
}
}



function block_ch(){
    let block_S  = document.getElementById('Block')
    block_S.innerHTML = ""
    let dump = document.createElement('option');
    block_S.appendChild(dump)
    for (var i = 1 ; i<=140;i++) {
    let option = document.createElement('option');
    pad = "000"
   let value = (pad+i).slice(-pad.length)
    option.value = value;
    option.innerHTML = "Block "+value;
    block_S.appendChild(option)
}
   console.log("clicked")
}

window.onload = function() {
    guzar()
};


</script>
<link href="{{ asset('/summernote/summernote.css') }}" rel="stylesheet">
<script src="{{ asset('/summernote/bootstrap.min.js') }}"></script>
<script src="{{ asset('/summernote/summernote.min.js') }}">
</script>

<script>
  let districts =  {!! json_encode($district) !!};
  function guzar(){
        let chosen_district = document.getElementById('district').value;
        chosen_district--

        let guzar_S  = document.getElementById('Guzar')
        guzar_S.innerHTML = ""
        console.log(districts[chosen_district].no_guzar);
        let dump = document.createElement('option');
        guzar_S.appendChild(dump)
    
    for (var i = 1 ; i<= districts[chosen_district].no_guzar;i++) {
    let option = document.createElement('option');
    pad = "00"
   let value = (pad+i).slice(-pad.length)
    option.value = value;
    option.innerHTML = "Guzar "+value;
    guzar_S.appendChild(option)
}
}



function block_ch(){
    let block_S  = document.getElementById('Block')
    block_S.innerHTML = ""
    let dump = document.createElement('option');
    block_S.appendChild(dump)
    for (var i = 1 ; i<=140;i++) {
    let option = document.createElement('option');
    pad = "000"
   let value = (pad+i).slice(-pad.length)
    option.value = value;
    option.innerHTML = "Block "+value;
    block_S.appendChild(option)
}
   console.log("clicked")
}

window.onload = function() {
    guzar()
};


</script>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul class="list-group">
                @foreach($errors->all() as $error)
                    <li class="list-group-item text-danger">
                        {{ $error }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{action('InfractionController@store')}}" method="post" role="form" class="smart-wizard" enctype="multipart/form-data"  id="form">
        {!! csrf_field() !!}
        <div class="container-fluid container-fullw bg-white">
            <div class="row">
                <fieldset>
                    <legend>
                        {{trans('pmis.supervision_details')}}
                    </legend>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                {{ trans("pmis.name") }}
                            </label>
                            <input class="form-control supervisor-name" placeholder="" type="text" disabled
                                   value="{{ $user->first_name }} {{ $user->last_name }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                {{trans('pmis.Phone_Number')}}
                            </label>
                            <input class="form-control supervisor-phone" placeholder="" type="text" disabled
                                   value="{{ $user->phone }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                {{trans('pmis.Email')}}
                            </label>
                            <input class="form-control supervisor-email" placeholder="" type="text" disabled
                                   value="{{ $user->email }}">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>{{trans('violation.property_owner')}}</legend>

                    <div class="col-md-4">
                        <label>
                            {{trans('violation.name')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="name" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.father_name')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="father_name" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            اسم پدر کلان 
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="G_father_name" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.tazkira_number')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="tazkira_number" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.contact')}}
                            {{-- <span class="symbol required" aria-required="true"></span> --}}
                        </label>
                        <div class="form-group">
                            <input type="hidden" name="street" id="" value="0">
                            <input placeholder="" name="contact" class="form-control" type="text">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>{{trans('violation.property_location')}}</legend>
                    <fieldset>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.district')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            @if(!Auth::user()->hasRole('admin')) 
                                <input type="hidden" value="{{ $user->user_district }}" name="district">
                                 @endif
                            <select class="ui dropdown" name="district" id="district" onchange="guzar()" @if(!Auth::user()->hasRole('admin')) disabled @endif >
                                
                              <option value="{{ $user->user_district }}"></option>
                                @foreach($district as $district)
                                    @if(App::isLocale('fa'))
                                        <option value="{{$district->id}}">{{$district->fa_name}}</option>
                                    @elseif(App::isLocale('pa'))
                                        <option value="{{$district->id}}">{{$district->pa_name}}</option>
                                    @else
                                        <option value="{{$district->id}}">{{$district->en_name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                  
                        <div class="col-md-4">
                            <label>
                            {{trans('violation.guzar')}}  
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="ui dropdown selection" tabindex="0">
                                <select name="Guzar" id="Guzar" onchange="block_ch();">
                                <option value="1">.</option>
                                </select>
                            <i class="dropdown icon"></i>
                            <div class="text"></div>
                            <div class="menu" tabindex="-1">
                                <div class="item" data-value="1"></div>
                            </div>
                        </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                            {{trans('violation.block')}}  
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="ui dropdown selection" tabindex="0">
                                <select name="Block" id="Block">
                                <option value="1"></option>
                                </select>
                            <i class="dropdown icon"></i>
                            <div class="text"></div>
                            <div class="menu" tabindex="-1">
                                <div class="item" data-value="1"></div>
                            </div>
                        </div>
                        </div>

                        <div class="col-md-3">
                            <label>
                            {{trans('violation.parcel')}}  
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group" tabindex="0">
                            <input placeholder="" name="Parcel" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label>
                            {{trans('violation.unit')}}  
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group" tabindex="0">
                            <input placeholder="" name="Unit" class="form-control" type="text">
                            </div>
                        </div>
                        
                        <br>
                        <div class="row">
                        <div class="col-md-6">
                            <label>
                                {{trans('violation.property_number')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="property_number" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                        <label>
                            {{trans('violation.serial_number')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="serial_number" class="form-control" type="text">
                        </div>
                    </div>
                    </div>
                    </fieldset>
                    <fieldset>
                        <legend>{{trans('violation.latitude')}}</legend>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.degree')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="latitude_degree" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.minute')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="latitude_minute" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.second')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="latitude_second" class="form-control" type="text">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>{{trans('violation.longitude')}}</legend>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.degree')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="longitude_degree" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.minute')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="longitude_minute" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.second')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="longitude_second" class="form-control" type="text">
                            </div>
                        </div>
                    </fieldset>
                </fieldset>
                <fieldset>
                    <legend>{{trans('violation.Violation_Details')}}</legend>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.Construction_Permit')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="contruction_certificate">
                            <option value="1">{{trans('violation.Yes')}}</option>
                            <option value="2">{{trans('violation.No')}}</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.Category_of_Land')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="region_status">
                            <option value="1">{{trans('violation.Planned')}}</option>
                            <option value="2">{{trans('violation.Unplanned')}}</option>
                            <option value="3">{{trans('violation.Irregular')}}</option>
                            <option value="4">{{trans('violation.bare_land')}}</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.type_of_activity')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="type_of_activity">
                            <option value="1">{{trans('violation.Residential')}}</option>
                            <option value="2">{{trans('violation.commercial')}}</option>
                            <option value="3">{{trans('violation.Industrial')}}</option>
                            <option value="4">{{trans('violation.service')}}</option>
                            <option value="5">{{trans('violation.mixed_res')}}</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.Type_of_Violation')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select name="violation_type[]" multiple="" class="ui fluid dropdown">
                        @foreach($violationType as $violationType)
                                    @if(App::isLocale('fa'))
                                        <option value="{{$violationType->id}}">{{$violationType->fa_type}}</option>
                                    @elseif(App::isLocale('pa'))
                                        <option value="{{$violationType->id}}">{{$violationType->pa_type}}</option>
                                    @else
                                        <option value="{{$violationType->id}}">{{$violationType->en_type}}</option>
                                    @endif
                                @endforeach
                         </select>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.garbbing_land_area')}}
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="garbbing_area" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.Status_of_Violations')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="status_of_infractions">
                            <option value="1">{{ trans('violation.ongoing') }}</option>
                            <option value="2">{{ trans('violation.stopped') }}</option>
                            <option value="3">{{ trans('violation.completed') }}</option>
                            <option value="4">{{ trans('violation.demolished') }}</option>
                            <option value="5">{{ trans('violation.resolved') }}</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.Status_of_Construction')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="status_of_cunstraction">
                            <option value="1">{{trans('violation.ongoing')}}</option>
                            <option value="2">{{trans('violation.stopped')}}</option>
                            <option value="3">{{trans('violation.completed')}}</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.start_date')}}
                            {{-- <span class="symbol required" aria-required="true"></span> --}}
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="start_date" class="form-control datepicker" type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.monitor_date')}}
                            {{-- <span class="symbol required" aria-required="true"></span> --}}
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="monitor_date" class="form-control datepicker"
                                   type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.proceedings')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="proceedings" id="proceedings">
                            @for($i = 1; $i <= 7; $i++)
                                <option value="{{$i}}">{{trans('violation.proceeding_' . $i)}}</option>
                            @endfor
                        </select>
                    </div>
                    </fieldset>
                <fieldset>
                    <br>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.Molkeyat_Document')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="Molkeyat">
                            <option value="1">{{trans('violation.Yes')}}</option>
                            <option value="2">{{trans('violation.No')}}</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label>
                            {{trans('violation.Molkeyat_Image')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <input type="file" multiple id="gallery-photo-add" name="Molkyat_doc">
                        <div class="gallery infraction-img"></div>

                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <label for="role_name">اظهار نظر<span class="text-danger"> * </span></label>
                      <textarea id="summernote" name="comment" required ></textarea>
                    </div>

                    <div class="col-md-12">
                        <label>
                        عکس تخلف
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <input type="file" multiple id="galler-add" name="images[]">
                        <div class="gallery infraction-img"></div>

                    </div>
                

                <div class="form-group">
                    <button class="btn btn-primary btn-o next-step btn-wide pull-right">
                        {{trans('pmis.save')}} <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
                </fieldset>
            </div>
        </div>
    </form>
@stop
@push('scripts')
<link href="{{ asset('/summernote/summernote.css') }}" rel="stylesheet">
<script src="{{ asset('/summernote/summernote.min.js') }}">
</script>
<script>
    $('.ui.dropdown').dropdown();
    $(function() {
      // Multiple images preview in browser
      var imagesPreview = function(input, placeToInsertImagePreview) {

          if (input.files) {
              var filesAmount = input.files.length;

              for (i = 0; i < filesAmount; i++) {
                  var reader = new FileReader();

                  reader.onload = function(event) {
                      $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                  }

                  reader.readAsDataURL(input.files[i]);
              }
          }

      };

      $('#gallery-photo-add').on('change', function() {
          imagesPreview(this, 'div.gallery');
      });
  });
</script>
<script>
  $(document).ready(function() {
      $('#summernote').summernote({
        placeholder: 'دیتای اعلانات شاروالی کابل',
        tabsize: 2,
        height: 300
      });

  });
</script>
@endpush
@section('validator')
    {!! JsValidator::formRequest('App\Http\Requests\ViolationRequest') !!}
@endsection
