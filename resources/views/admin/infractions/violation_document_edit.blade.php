@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 3])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans('violation.violation_documents'), 'page' => trans('violation.Violation'), 'current' => trans('pmis.details')])
@stop
@section('main-content')
<form action="{{action('InfractionController@editDocuments')}}" method="post" role="form" class="smart-wizard"
      id="form">
    {!! csrf_field() !!}
    <div class="container-fluid container-fullw bg-white">
        <div class="row">
            <fieldset>
                <legend>
                    {{trans('violation.violation_documents_edit')}}
                </legend>
                @foreach($violationDoument as $violationDouments)
                <input type="hidden" name="doc_id" value="{{$violationDouments->id}}">
                <div class="row">
                <div class="col-md-4">
                    <label>
                        {{ trans("violation.violation_doc_number") }}
                    </label>
                    <div class="form-group">
                        <input class="form-control" placeholder="" type="text" value="{{$violationDouments->doc_number}}" name="doc_number">
                    </div>
                </div>
                <div class="col-md-4">
                    <label>
                        {{ trans("violation.violation_doc_name") }}
                    </label>
                    <div class="form-group">
                        <input class="form-control" placeholder="" type="text" value="{{$violationDouments->doc_name}}" name="doc_name">
                    </div>
                </div>
                <div class="col-md-4">
                    <label>
                        {{ trans("violation.violation_upload_date") }}
                    </label>
                    <div class="form-group">
                        <input class="form-control datepicker" placeholder="" type="text" value="{{$violationDouments->doc_date}}" name="doc_date">
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-8">
                    <label>
                        {{ trans("violation.violation_doc_decription") }}
                    </label>
                    <div class="form-group">
                        <textarea rows="5" name="doc_description">{{$violationDouments->doc_description}}</textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <table class="ui compact table
                        @if(App::isLocale('en'))                             
                            left aligned
                        @else
                            right aligned
                        @endif
                        " style="margin-top: 23px;">
                            <tr>
                                <td>{{trans('violation.violation_file')}}</td>
                                <td>{{trans('pmis.delete')}}</td>
                            </tr>
                                @foreach($documentpaths as $documentpath)
                                <tr>
                                <td class="top aligned">
                                    <a href="{{asset($documentpath->doc_path)}}" target="_blank">
                                    <i class="file text icon">
                                    </i>
                                    </a>
                                </td>
                                <td> 
                                 <a onclick="confirmDocumentDelation({{$documentpath->id}})"> 
                                    <i class="delete icon" style="color: #c82e29;"></i>
                                 </a> 
                                </td>
                            </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
                </div>
                <div class="col-md-12">
                    <div class="violation_document_file">
                    </div>
                    <div class="panel-body col-md-12">
                        <div id="violation_document_file_edit" class="dropzone"></div>
                    </div>
                </div>
                @endforeach
                </fieldset>
                <div class="form-group 
                    @if(App::isLocale('en'))                             
                        pull-right
                    @else
                        pull-left
                    @endif
                ">
                    <button class="btn btn-primary btn-o next-step btn-wide" style="border-radius: 0">
                        {{trans('pmis.save')}}
                    </button>
                </div>
            
        </div>
    </div>
</form>
@stop
@push('scripts')
<script>
function confirmDocumentDelation(id) {
        swal({
                    title: "{{trans('pmis.are_sure')}}",
                    text: "{{trans('pmis.sw_sub_mess')}}",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('pmis.ye_del')}}",
                    cancelButtonText: "{{trans('pmis.cancel')}}",
                    closeOnConfirm: false
                },
                function () {
                    window.location = '/violation/document/delete/' + id;
                    swal("{{trans('pmis.deleted')}}", "{{trans('pmis.conf_mess')}}", "success");
                });
    }


// edit rout
    function editroute(id){
         window.location = '/infraction/document/edit/' + id;
    }
</script>
@endpush
@section('validator')
    {!! JsValidator::formRequest('App\Http\Requests\ViolationFileRequest') !!}
@endsection