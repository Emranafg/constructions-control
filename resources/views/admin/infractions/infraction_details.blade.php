@extends('admin.master')
@section('side-bar')
@include('partials.side_bar', ['active' => 3])
@stop
@section('page-title')
@include('partials.breadcrumb', ['pageTitle' => trans('violation.Violation_Details'), 'page' => trans('violation.Violation'), 'current' => trans('pmis.details')])
@stop
@section('style')
<style>
  .card-full-width{
    width: 100% !important;
  }
  .infraction-img img{
    max-width: 150px;
    margin: 5px;
  }
  .infraction-img{
    margin: 0 auto;
    overflow: hidden;
    position: relative;
    height: 150px;
  }
</style>
<link rel="stylesheet" href="{{asset('css/style.css')}}">
<link href="{{ asset('vendor/photoswipe/photoswipe.css?v=4.1.2-1.0.4') }}" rel="stylesheet" />
<link href="{{ asset('vendor/photoswipe/default-skin/default-skin.css?v=4.1.2-1.0.4') }}" rel="stylesheet" />
@stop
@section('main-content')
<div class="container-fluid container-fullw bg-white">
    <div class="row">
        <div class="col-md-180">
            <div class="ui card card-full-width">
                <div class="content card-header-backgrounded">
                    <div class="header">
                        <i class="flaticon-caution white-icon-card-style" style="font-size: 25px !important;"></i>
                        <span class="white-ui-card-header-artical">{{trans('violation.Violation')}}</span>
                    </div>
                </div>
                <div class="col-md-30">
                    <div class="col-md-120 padding-top-90">
                        <fieldset class="padding-top-15" style="min-height: 350px !important;">
                            <legend><i style="font-size: 18px" class="flaticon-icon-107807 icon-red"></i>
                                <span style="">{{trans('violation.Violation_Details')}} {{$infraction->latitude}}</span>
                            </legend>
                            <div class="ui relaxed divided list">
                                <div class="item">
                                    <i class="user icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.name')}}</a>
                                        <div class="description">{{$infraction->name}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="user icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.father_name')}}</a>
                                        <div class="description">{{$infraction->father_name}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-contract icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.serial_number')}}</a>
                                        <div class="description">{{$infraction->tazkira_number}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="travel icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.job')}}</a>
                                        <div class="description">{{ $infraction->job }}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-route-2 icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.Violation_Coordinates')}}</a>
                                        <div class="description">
                                            {!! $coordinates !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-location-pointer icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.property_area')}}</a>
                                        <div class="description">{{$infraction->property_area}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-map icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.Address')}}</a>
                                        <div class="description">{{$infraction->district}}
                                        , {{$infraction->street}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-budget-2 icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.property_number')}}</a>
                                        <div class="description">{{$infraction->property_number}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-diploma-3 icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.contruction_certificate')}}</a>
                                        <div class="description">{{$infraction->contruction_certificate}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-calendar-with-a-clock-time-tools icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.start_date')}}</a>
                                        <div class="description">{{$infraction->start_date}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-calendar-2 icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.monitor_date')}}</a>
                                        <div class="description">{{$infraction->monitor_date}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-drawing-architecture-project-of-a-house icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.comment')}}</a>
                                        <div class="description">{{$infraction->comment}}</div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
					</div>


                    <div class="col-md-120 padding-top-30">
                        <fieldset class="padding-top-15" style="min-height: 250px !important;">
                            <legend><i style="font-size: 18px" class="flaticon-icon-107807 icon-red"></i>
                                <span style="">{{trans('violation.Supervisor_Details')}}</span>
                            </legend>
                            <div class="ui relaxed divided list">
                                <div class="item">
                                    <i class="flaticon-project-manager icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.Supervisor_Name')}}</a>
                                        <div class="description">{{$supervisor->first_name ? $supervisor->first_name : 'Unknown'}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="mobile icon" style="font-size: 15px"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.Phone_Number')}}</a>
                                        <div class="description">{{$supervisor->phone ? $supervisor->phone : 'Unknown' }}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="mail icon" style="font-size: 13px"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.Email')}}</a>
                                        <div class="description">{{$supervisor->email ? $supervisor->email : 'Unknown' }}</div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="padding-top-15" style="min-height: 250px !important;">
                            <legend><i style="font-size: 15px" class="flaticon-icon-107807 icon-red"></i>
                                <span style="">{{trans('violation.Violation_Oppreations')}}</span>
                            </legend>
                            <div class="ui relaxed divided list">
                                <div class="item">
                                    <i class="flaticon-road icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.Type_of_Violation')}}</a>
                                        <div class="description">
                                            @foreach($infvt as $infvts)
                                            @if(App::isLocale('fa'))
                                            <ul>
                                                <li>   {{$infvts->fa_type}}</li>
                                            </ul>
                                            @elseif(App::isLocale('pa'))
                                            <ul>
                                                <li>   {{$infvts->pa_type}}</li>
                                            </ul>
                                            @else
                                            <ul>
                                                <li>   {{$infvts->en_type}}</li>
                                            </ul>
                                            @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-business-presentation icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.garbbing_land_area')}}</a>
                                        <div class="description">{{$infraction->garbbing_area}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-business-presentation icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.Category_of_Land')}}</a>
                                        <div class="description">{{$infraction->region_status}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-people icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.Type_Of_Activity')}}</a>
                                        <div class="description">{{$infraction->type_of_activity}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-laboratory-microscope icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.Construction_Status')}}</a>
                                        <div class="description">{{$infraction->status_of_cunstraction}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-people-12 icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.proceedings')}}</a>
                                        <div class="description">{{$infraction->proceedings}}</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="flaticon-searcher icon"></i>
                                    <div class="content">
                                        <a class="header">{{trans('violation.Violation_Status')}}</a>
                                        <div class="description">{{$infraction->status_of_infractions}}</div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                {{-- Violation Documents --}}
                <div class="col-md-12">
                    <div class="col-md-12">
                        <fieldset class="padding-top-15" style="min-height: 50px !important;">
                            <legend><i style="font-size: 18px" class="flaticon-icon-107807 icon-red"></i>
                                <span style="">{{trans('violation.violation_documents')}}</span>
                            </legend>
                            <div class="row">
                                <table class="ui celled table
                                    @if(App::isLocale('en'))
                                    left aligned
                                    @else
                                    right aligned
                                    @endif
                                    " >
                                    <tr>
                                        <th>{{trans('violation.violation_doc_number')}} </th>
                                        <th> {{trans('violation.violation_doc_name')}}</th>
                                        <th>{{trans('violation.violation_doc_decription')}}</th>
                                        <th>{{trans('violation.violation_upload_date')}}</th>
                                        <th>{{trans('violation.violation_file')}}</th>
                                        <th>{{trans('pmis.delete')}}</th>
                                        <th>{{trans('pmis.edit')}}</th>
                                    </tr>
                                    @foreach($violationDouments as $violationDoument)
                                    <tr>
                                        <td>{{$violationDoument->doc_number}}</td>
                                        <td>{{$violationDoument->doc_name}}</td>
                                        <td>{{$violationDoument->doc_description}}</td>
                                        <td>{{$violationDoument->doc_date}}</td>
                                        <td>
                                            @foreach($violationDoument->ViolationFiles()->get() as $documentpath)
                                            <a href="{{asset($documentpath->doc_path)}}" target="_blank">
                                                <i class="file text icon">

                                                </i>
                                            </a>
                                            @endforeach
                                        </td>
                                        <td>
                                            <a onclick="confirmDocumentDelation({{$violationDoument->id}})">
                                                <i class="delete icon" style="color: #c82e29;"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a onclick="editroute({{$violationDoument->id}});" >
                                                <i class="edit icon" style="color: #007AFF "></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                                <div class="ui right floated small primary labeled icon button" id="attach_documents" data-toggle="modal" data-target="#upload_document_modal">
                                    <i class="attach icon"></i> <span >{{trans('violation.violation_attach')}}</span>
                                </div>

                            </div>
                        </fieldset>
                    </div>
                </div>
                {{-- document uploading modal  --}}
                <div class="modal fade bs-example-modal-lg" id="upload_document_modal" tabindex="-1" role="dialog"
                    aria-labelledby="Assign Infraction Modal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content ui card card-full-width">
                            <div class="modal-header" style="background-color: #007AFF">
                                <button type="button" class="close"  style="color: white;" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel" style="color: white;"> {{trans('violation.violation_documents')}}</h4>
                            </div>
                            <div class="modal-body ">
                                <form action="{{action('InfractionController@uploadDocuments')}}" method="post" id="form" enctype="multipart/form-data">
                                    <div class="container-fluid container-fullw bg-white" style="padding-bottom: 15px">
                                        <div class="row">
                                            <fieldset id="documentfieldset">
                                                <legend>
                                                    {{trans('violation.violation_new_file')}}
                                                </legend>
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="infraction_id" value="{{$id}}">
                                                <div class="col-md-6">
                                                    <label>
                                                        {{trans('violation.violation_doc_number')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="doc_number" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>
                                                        {{trans('violation.violation_doc_name')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="doc_name" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>
                                                        {{trans('violation.violation_upload_date')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <input placeholder="" name="doc_date" class="form-control datepicker" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>
                                                        {{trans('violation.violation_doc_decription')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="form-group">
                                                        <textarea rows="3" class="form-control" name="doc_description"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>
                                                        {{trans('violation.violation_file')}}
                                                        <span class="symbol required" aria-required="true"></span>
                                                    </label>
                                                    <div class="violation_document_file">
                                                    </div>
                                                    <div class="panel-body">

													                            <input type="file" multiple id="gallery-photo-add" name="images[]">
                                                      <div class="gallery infraction-img"></div>

                                                    </div>
                                                    <div class="col-md-3 pull-right">
                                                        <button class="btn btn-o btn-primary btn-block" style="border-radius: 0" type="submit">
                                                        {{trans('pmis.save')}}
                                                        </button>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- document  Modal end --}}
                <div class="col-md-12">
                    <div class="col-md-12">
                        <fieldset class="padding-top-15" style="min-height: 350px !important;">
                            <legend><i style="font-size: 18px" class="flaticon-icon-107807 icon-red"></i>
                                <span style="">{{trans('violation.Violation_Images')}}</span>
                            </legend>
                            <div class="row demo-gallery" id="demo-test-gallery">
                                @foreach($infractionUploads as $infractionUpload)
                                <a href="{{asset($infractionUpload->path)}}" data-size="1600x1000" data-med="{{asset($infractionUpload->path)}}" data-med-size="800x500" class="thumbnail col-sm-6 col-md-4">
                                    <img src="{{asset($infractionUpload->path)}}" alt="" />
                                <figure>{{$infractionUpload->created_at->toFormattedDateString()}}</figure>
                            </a>
                            @endforeach
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- photoswipe gallery --}}
<div id="gallery" class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip">
                    <!-- <a href="#" class="pswp__share--facebook"></a>
                    <a href="#" class="pswp__share--twitter"></a>
                    <a href="#" class="pswp__share--pinterest"></a>
                    <a href="#" download class="pswp__share--download"></a> -->
                </div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"> </div>
            </div>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script src="{{asset('vendor/photoswipe/photoswipe.min.js')}}"></script>
<script src="{{asset('vendor/photoswipe/photoswipe-ui-default.min.js')}}"></script>
<script src="{{asset('vendor/photoswipe/script.js')}}"></script>
<script>
function confirmDocumentDelation(id) {
swal({
title: "{{trans('pmis.are_sure')}}",
text: "{{trans('pmis.sw_sub_mess')}}",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#DD6B55",
confirmButtonText: "{{trans('pmis.ye_del')}}",
cancelButtonText: "{{trans('pmis.cancel')}}",
closeOnConfirm: false
},
function () {
window.location = '/infraction/document/delete/' + id;
swal("{{trans('pmis.deleted')}}", "{{trans('pmis.conf_mess')}}", "success");
});
}
// edit rout
function editroute(id){
window.location = '/infraction/document/edit/' + id;
}
</script>
<script>
    $('.ui.dropdown').dropdown();
    $(function() {
      // Multiple images preview in browser
      var imagesPreview = function(input, placeToInsertImagePreview) {

          if (input.files) {
              var filesAmount = input.files.length;

              for (i = 0; i < filesAmount; i++) {
                  var reader = new FileReader();

                  reader.onload = function(event) {
                      $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                  }

                  reader.readAsDataURL(input.files[i]);
              }
          }

      };

      $('#gallery-photo-add').on('change', function() {
          imagesPreview(this, 'div.gallery');
      });
  });
</script>
@endpush
@section('validator')
{!! JsValidator::formRequest('App\Http\Requests\ViolationFileRequest') !!}
@endsection
