@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 3])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans('violation.Creating_Infraction'), 'page' => trans('violation.Violation'), 'current' => trans('pmis.projects')])
@stop
@section('style')
    <style media="screen">
        .ui.dropdown {
            margin-bottom: 11px;
        }
        .infraction-img img, .div-img{
          max-width: 150px;
          margin: 5px;
          float: right;
          position: relative;

        }
        .infraction-img{
          margin: 0 auto;
          overflow: hidden;
          position: relative;
          height: 150px;
        }
        .img-icon{
          position: absolute;
          z-index: 1;
          top:5px;
          right:5px;
          padding: 10px;
          background-color: #fff;
          border-radius: 10px 0px 10px 10px;
        }
    </style>
@stop
@section('main-content')
    <form action="{{action('InfractionController@update')}}" method="post" role="form" class="smart-wizard"
          id="form" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="hidden" name="infraction_id" value="{{ $infraction->id }}">
        <div class="container-fluid container-fullw bg-white">
            <div class="row">
                <fieldset>
                    <legend>
                        {{trans('pmis.supervision_details')}}
                    </legend>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                Name
                            </label>
                            <input class="form-control supervisor-name" placeholder="" type="text" disabled
                                   value="{{ $infraction_user->user->first_name }} {{ $infraction_user->user->last_name }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                {{trans('pmis.Phone_Number')}}
                            </label>
                            <input class="form-control supervisor-phone" placeholder="" type="text" disabled
                                   value="{{ $infraction_user->user->phone }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                {{trans('pmis.Email')}}
                            </label>
                            <input class="form-control supervisor-email" placeholder="" type="text" disabled
                                   value="{{ $infraction_user->user->email }}">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>{{trans('violation.property_owner')}}</legend>

                    <div class="col-md-4">
                        <label>
                            {{trans('violation.name')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="name" class="form-control" type="text"
                                   value="{{ $infraction->name }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.father_name')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="father_name" class="form-control" type="text"
                                   value="{{ $infraction->father_name }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.tazkira_number')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="tazkira_number" class="form-control" type="text"
                                   value="{{ $infraction->tazkira_number }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.job')}}
                            {{-- <span class="symbol required" aria-required="true"></span> --}}
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="job" class="form-control" type="text"
                                   value="{{ $infraction->job }}">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>{{trans('violation.property_location')}}</legend>
                    <fieldset>
                        <div class="col-md-4">
                            <label>
                                {{trans('pmis.district')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <select class="ui dropdown" name="district" @if(!Auth::user()->hasRole('admin')) disabled @endif>
                              @if(Auth::user()->hasRole('admin'))
                                <option value="{{ $infraction->district }}"></option>
                              @else
                                <option value="{{ Auth::user()->user_district }}"></option>
                              @endif
                                @foreach($district as $district)
                                    @if(App::isLocale('fa'))
                                        <option value="{{$district->id}}">{{$district->fa_name}}</option>
                                    @elseif(App::isLocale('pa'))
                                        <option value="{{$district->id}}">{{$district->pa_name}}</option>
                                    @else
                                        <option value="{{$district->id}}">{{$district->en_name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.street')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="street" class="form-control" type="text"
                                       value="{{ $infraction->street }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.property_area')}}
                                {{-- <span class="symbol required" aria-required="true"></span> --}}
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="property_area" class="form-control" type="text"
                                       value="{{ $infraction->property_area }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.property_number')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="property_number" class="form-control" type="text"
                                       value="{{ $infraction->property_number }}">
                            </div>
                        </div>
                         <div class="col-md-4">
                        <label>
                            {{trans('violation.serial_number')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="serial_number" class="form-control" type="text"
                                   value="{{ $infraction->serial_number }}">
                        </div>
                    </div>
                    </fieldset>
                    <fieldset>
                        <legend>{{trans('violation.latitude')}}</legend>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.degree')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="latitude_degree" class="form-control" type="text" value = "{{ $latitude_degree }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.minute')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="latitude_minute" class="form-control" type="text" value = "{{ $latitude_minute }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.second')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="latitude_second" class="form-control" type="text" value = "{{ $latitude_second }}">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>{{trans('violation.longitude')}}</legend>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.degree')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="longitude_degree" class="form-control" type="text" value = "{{ $longitude_degree }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.minute')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="longitude_minute" class="form-control" type="text" value = "{{ $longitude_minute }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>
                                {{trans('violation.second')}}
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="longitude_second" class="form-control" type="text" value = "{{ $longitude_second }}">
                            </div>
                        </div>
                    </fieldset>
                </fieldset>
                <fieldset>
                    <legend>{{trans('violation.Violation_Details')}}</legend>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.Construction_Permit')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="contruction_certificate">
                            @if($infraction->contruction_certificate == 1)
                                <option value="1">{{trans('violation.Yes')}}</option>
                                <option value="2">{{trans('violation.No')}}</option>
                            @else
                                <option value="2">{{trans('violation.No')}}</option>
                                <option value="1">{{trans('violation.Yes')}}</option>
                            @endif
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.Category_of_Land')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="region_status">
                            @if($infraction->region_status == 1)
                                <option value="1">{{trans('violation.Planned')}}</option>
                                <option value="2">{{trans('violation.Unplanned')}}</option>
                                <option value="3">{{trans('violation.Irregular')}}</option>
                                <option value="4">{{trans('violation.bare_land')}}</option>
                            @elseif($infraction->region_status == 2)
                                <option value="2">{{trans('violation.Unplanned')}}</option>
                                <option value="3">{{trans('violation.Irregular')}}</option>
                                <option value="1">{{trans('violation.Planned')}}</option>
                                <option value="4">{{trans('violation.bare_land')}}</option>
                            @elseif($infraction->region_status == 3)
                                <option value="3">{{trans('violation.Irregular')}}</option>
                                <option value="1">{{trans('violation.Planned')}}</option>
                                <option value="2">{{trans('violation.Unplanned')}}</option>
                                <option value="4">{{trans('violation.bare_land')}}</option>
                            @else
                                <option value="4">{{trans('violation.bare_land')}}</option>
                                <option value="1">{{trans('violation.Planned')}}</option>
                                <option value="2">{{trans('violation.Unplanned')}}</option>
                                <option value="3">{{trans('violation.Irregular')}}</option>
                            @endif
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.type_of_activity')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="type_of_activity">
                            @if($infraction->type_of_activity == 1)
                                <option value="1">{{trans('violation.Residential')}}</option>
                                <option value="2">{{trans('violation.commercial')}}</option>
                                <option value="3">{{trans('violation.Industrial')}}</option>
                                <option value="4">{{trans('violation.service')}}</option>
                                <option value="5">{{trans('violation.mixed_res')}}</option>
                            @elseif($infraction->type_of_activity == 2)
                                <option value="2">{{trans('violation.commercial')}}</option>
                                <option value="1">{{trans('violation.Residential')}}</option>
                                <option value="3">{{trans('violation.Industrial')}}</option>
                                <option value="4">{{trans('violation.service')}}</option>
                                <option value="5">{{trans('violation.mixed_res')}}</option>
                            @elseif($infraction->type_of_activity == 3)
                                <option value="3">{{trans('violation.Industrial')}}</option>
                                <option value="1">{{trans('violation.Residential')}}</option>
                                <option value="2">{{trans('violation.commercial')}}</option>
                                <option value="4">{{trans('violation.service')}}</option>
                                <option value="5">{{trans('violation.mixed_res')}}</option>
                            @elseif($infraction->type_of_activity == 4)
                                <option value="4">{{trans('violation.service')}}</option>
                                <option value="1">{{trans('violation.Residential')}}</option>
                                <option value="2">{{trans('violation.commercial')}}</option>
                                <option value="3">{{trans('violation.Industrial')}}</option>
                                <option value="5">{{trans('violation.mixed_res')}}</option>
                            @else
                                <option value="5">{{trans('violation.mixed_res')}}</option>
                                <option value="1">{{trans('violation.Residential')}}</option>
                                <option value="2">{{trans('violation.commercial')}}</option>
                                <option value="3">{{trans('violation.Industrial')}}</option>
                                <option value="4">{{trans('violation.service')}}</option>
                            @endif
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.Type_of_Violation')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" multiple="" name="violation_type[]">
                            @foreach($violationType as $violationTypes)
                            {{$count = count($infvt)}}
                                    @if(App::isLocale('fa'))
                                        <option value="{{$violationTypes->id}}"
                                                @if($count !=0)
                                            @foreach($infvt as $infvts)
                                                @if($infvts->vt_id == $violationTypes->id )
                                                selected=""
                                                @endif
                                                @endforeach

                                        @endif
                                        >{{$violationTypes->fa_type}}</option>
                                    @elseif(App::isLocale('pa'))
                                        <option value="{{$violationTypes->id}}"
                                                @if($count !=0)
                                            @foreach($infvt as $infvts)
                                                @if($infvts->vt_id == $violationTypes->id )
                                                selected=""
                                                @endif
                                                @endforeach
                                        @endif
                                        >
                                        {{$violationTypes->pa_type}}
                                        </option>
                                    @else
                                        <option value="{{$violationTypes->id}}"

                                        @if($count !=0)
                                            @foreach($infvt as $infvts)
                                                @if($infvts->vt_id == $violationTypes->id )
                                                selected=""
                                                @endif
                                                @endforeach

                                        @endif
                                        >
                                        {{$violationTypes->en_type}}
                                        </option>


                                    @endif
                                @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label >
                            {{trans('violation.garbbing_land_area')}}
                        </label>
                        <div class="form-group">
                        <input placeholder="" name="start_date" class="form-control datepicker" type="text"
                               value="{{ $infraction->garbbing_area }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.Status_of_Violations')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="status_of_infractions">
                            @if($infraction->status_of_infractions == 1)
                                <option value="1">{{ trans('violation.ongoing') }}</option>
                                <option value="2">{{ trans('violation.stopped') }}</option>
                                <option value="3">{{ trans('violation.completed') }}</option>
                                <option value="4">{{ trans('violation.demolished') }}</option>
                                <option value="5">{{ trans('violation.resolved') }}</option>
                            @elseif($infraction->status_of_infractions == 2)
                                <option value="2">{{ trans('violation.stopped') }}</option>
                                <option value="1">{{ trans('violation.ongoing') }}</option>
                                <option value="3">{{ trans('violation.completed') }}</option>
                                <option value="4">{{ trans('violation.demolished') }}</option>
                                <option value="5">{{ trans('violation.resolved') }}</option>
                            @elseif($infraction->status_of_infractions == 3)
                                <option value="3">{{ trans('violation.completed') }}</option>
                                <option value="1">{{ trans('violation.ongoing') }}</option>
                                <option value="2">{{ trans('violation.stopped') }}</option>
                                <option value="4">{{ trans('violation.demolished') }}</option>
                                <option value="5">{{ trans('violation.resolved') }}</option>
                            @elseif($infraction->status_of_infractions == 4)
                                <option value="4">{{ trans('violation.demolished') }}</option>
                                <option value="1">{{ trans('violation.ongoing') }}</option>
                                <option value="2">{{ trans('violation.stopped') }}</option>
                                <option value="3">{{ trans('violation.completed') }}</option>
                                <option value="5">{{ trans('violation.resolved') }}</option>
                            @else
                                <option value="5">{{ trans('violation.resolved') }}</option>
                                <option value="1">{{ trans('violation.ongoing') }}</option>
                                <option value="2">{{ trans('violation.stopped') }}</option>
                                <option value="3">{{ trans('violation.completed') }}</option>
                                <option value="4">{{ trans('violation.demolished') }}</option>
                            @endif
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.Status_of_Construction')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="status_of_cunstraction">
                            @if($infraction->status_of_cunstraction == 1)
                                <option value="1">{{trans('violation.ongoing')}}</option>
                                <option value="2">{{trans('violation.stopped')}}</option>
                                <option value="3">{{trans('violation.completed')}}</option>
                            @elseif($infraction->status_of_cunstraction == 2)
                                <option value="2">{{trans('violation.stopped')}}</option>
                                <option value="1">{{trans('violation.ongoing')}}</option>
                                <option value="3">{{trans('violation.completed')}}</option>
                            @else
                                <option value="3">{{trans('violation.completed')}}</option>
                                <option value="1">{{trans('violation.ongoing')}}</option>
                                <option value="2">{{trans('violation.stopped')}}</option>
                            @endif
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.start_date')}}
                            {{-- <span class="symbol required" aria-required="true"></span> --}}
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="start_date" class="form-control datepicker" type="text"
                                   value="{{ $infraction->start_date }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.monitor_date')}}
                            {{-- <span class="symbol required" aria-required="true"></span> --}}
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="monitor_date" class="form-control datepicker" type="text"
                                   value="{{ $infraction->monitor_date }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            {{trans('violation.proceedings')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <select class="ui dropdown" name="proceedings" id="proceedings">
                            <option value="{{$infraction->proceedings}}">{{trans('violation.proceeding_' . $infraction->proceedings)}}
                            @for($i = 1; $i <= 7; $i++)
                                <option value="{{$i}}">{{trans('violation.proceeding_' . $i)}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="col-md-12">
                        <label>
                            {{trans('violation.comment')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <textarea name="comment" rows="7">{{ $infraction->comment }}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>
                            {{trans('violation.image')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <input type="file" multiple id="gallery-photo-add" name="images[]">
                        <div class="gallery infraction-img">
                          @foreach($infractionUploads as $image)
                          <div class="div-img id{{ $image->id }}">
                            <i class="fa fa-trash text-red img-icon" onclick="removeImg('{{ $image->path }}', '{{ $image->id }}')"></i>
                            <img src="/{{ $image->path }}">
                          </div>
                          @endforeach
                        </div>

                    </div>
                </fieldset>

                <div class="form-group">
                    <button class="btn btn-primary btn-o next-step btn-wide pull-right">
                        {{trans('pmis.save')}} <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
@stop

@push('scripts')
<script>
    $('.ui.dropdown').dropdown();
    $(function() {
      // Multiple images preview in browser
      var imagesPreview = function(input, placeToInsertImagePreview) {

          if (input.files) {
              var filesAmount = input.files.length;

              for (i = 0; i < filesAmount; i++) {
                  var reader = new FileReader();

                  reader.onload = function(event) {
                      $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                  }

                  reader.readAsDataURL(input.files[i]);
              }
          }

      };

      $('#gallery-photo-add').on('change', function() {
          imagesPreview(this, 'div.gallery');
      });
  });
  function removeImg(path,id){
    let text = "Are you sure to delete the file?";
    let selector = this.parent;
    if (confirm(text) == true) {

      $.post("{{route('infraction.img.remove')}}",{ 'path': path, '_token':'{{csrf_token()}}'} , function(data, status){
        if(status == 'success'){
          $('.id'+id).hide();

        }

      });
    }

  }
</script>
@endpush
@section('validator')
    {!! JsValidator::formRequest('App\Http\Requests\ViolationRequest') !!}
@endsection
