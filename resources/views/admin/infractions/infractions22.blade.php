@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 3])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans('violation.List_Of_Violations'), 'page' => trans('violation.Violation'), 'current' => trans('pmis.reports')])
@stop
@section('alert-message')
    @if(session('message_title'))
        <div role="alert" class="alert {{ session('message_class') }}">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
        </div>
    @endif
@stop
@section('main-content')
    <div class="container-fluid container-fullw bg-white">
        <div class="container">
            <form role="form" class="form-inline" id="search-form" method="POST">
                <div class="row">
                    <div class="panel panel-white">
                        <div class="panel-heading border-light">
                            <h4 class="panel-title">{{trans('violation.Reports')}}</h4>
                            <ul class="panel-heading-tabs border-light">
                                @if(Auth::User()->can('create_violation'))
                                    <li>
                                        <div class="pull-right">
                                            <div class="ui labeled button" tabindex="0" style="direction: ltr">
                                                <a href="/infraction/create" class="ui red button">
                                                    <i class="plus icon"></i> {{ trans('pmis.creat_infraction') }}
                                                </a>
                                                <a class="ui basic red left pointing label" style="border-radius: 0px !important">
                                                    {{ $infCount }}
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                @endif

                                @if(Auth::User()->can('create_report'))
                                    <li>
                                        <div class="pull-right">
                                            <div class="btn-group" dropdown="">
                                                <div class="ui buttons">
                                                    <a id="exportall"
                                                       class="ui button">{{trans('violation.Export')}}</a>
                                                    <div class="or"></div>
                                                    <a id="export"
                                                       class="ui button report">{{trans('violation.Filter')}} </a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endif
                                <li class="panel-tools">
                                    <a data-original-title="{{trans('pmis.See_on_Map')}}" data-toggle="tooltip"
                                       data-placement="top"
                                       class="btn btn-sm" href="/km/infractions/map/all" target="_blank">
                                        <i class="large marker icon"></i>
                                       </a>
                                </li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <select class="ui dropdown" name="district" id="district">
                                            <option value="">{{trans('violation.district')}}</option>
                                            @foreach($district as $district)
                                                @if(App::isLocale('fa'))
                                                    <option value="{{$district->id}}">{{$district->fa_name}}</option>
                                                @elseif(App::isLocale('pa'))
                                                    <option value="{{$district->id}}">{{$district->pa_name}}</option>
                                                @else
                                                    <option value="{{$district->id}}">{{$district->en_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="ui dropdown" name="contruction_certificate" id="permits">
                                            <option value="">{{trans('violation.Construction_Permit')}}</option>
                                            <option value="1">{{trans('violation.Yes')}}</option>
                                            <option value="2">{{trans('violation.No')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="ui dropdown" name="region_status" id="region_status">
                                            <option value="">{{trans('violation.Category_of_Land')}}</option>
                                            <option value="1">{{trans('violation.Planned')}}</option>
                                            <option value="2">{{trans('violation.Unplanned')}}</option>
                                            <option value="3">{{trans('violation.Irregular')}}</option>
                                            <option value="4">{{trans('violation.bare_land')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="ui dropdown" name="type_of_activity"
                                                id="activity_type">
                                            <option value="">{{trans('violation.type_of_activity')}}</option>
                                            <option value="1">{{trans('violation.Residential')}}</option>
                                            <option value="2">{{trans('violation.commercial')}}</option>
                                            <option value="3">{{trans('violation.Industrial')}}</option>
                                            <option value="4">{{trans('violation.service')}}</option>
                                            <option value="5">{{trans('violation.mixed_res')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3  margin-top-15">
                                        <select class="ui dropdown" name="status_of_cunstraction"
                                                id="cunstraction_status">
                                            <option value="">{{trans('violation.Status_of_Construction')}}</option>
                                            <option value="1">{{trans('violation.ongoing')}}</option>
                                            <option value="2">{{trans('violation.stopped')}}</option>
                                            <option value="3">{{trans('violation.completed')}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3  margin-top-15">
                                        <select class="ui dropdown" name="status_of_infractions" id="violation_status">
                                            <option value="">{{ trans('violation.Status_of_Violations') }}</option>
                                            <option value="1">{{ trans('violation.ongoing') }}</option>
                                            <option value="2">{{ trans('violation.stopped') }}</option>
                                            <option value="3">{{ trans('violation.completed') }}</option>
                                            <option value="4">{{ trans('violation.demolished') }}</option>
                                            <option value="5">{{ trans('violation.resolved') }}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3  margin-top-15">
                                        <select class="ui dropdown" name="proceedings" id="proceedings">
                                            <option value="">{{ trans('violation.proceedings') }}</option>
                                            @for($i = 1; $i <= 7; $i++)
                                                <option value="{{$i}}">{{trans('violation.proceeding_' . $i)}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-3  margin-top-15">
                                        <select class="ui dropdown" name="violation_type" id="violation_type">
                                            <option value="">{{ trans('violation.Type_of_Violation') }}</option>
                                            @foreach($violationType as $violationTypes)
                                                @if(App::isLocale('fa'))
                                                    <option value="{{$violationTypes->id}}">{{$violationTypes->fa_type}}</option>
                                                @elseif(App::isLocale('pa'))
                                                    <option value="{{$violationTypes->id}}">{{$violationTypes->pa_type}}</option>
                                                @else
                                                    <option value="{{$violationTypes->id}}">{{$violationTypes->en_type}}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row margin-top-30 border-top">
                                <div class="margin-top-10"></div>
                                <div @if(App::isLocale('fa') || App::isLocale('pa'))
                                     class="col-md-12 margin-right-15" @else class="col-md-12 margin-left-15" @endif >
                                    <table class="ui celled table" id="users-table"
                                           style="width: 100%; text-align: center !important;">
                                        <thead>
                                        <tr>
                                            <th>{{trans('violation.id')}}</th>
                                            <th>{{trans('violation.name')}}</th>
                                            <th>{{trans('violation.coordinate')}}</th>
                                            <th>{{trans('violation.district')}}</th>
                                            <th>{{trans('violation.Construction_Permit')}}</th>
                                            <th>{{trans('violation.type_of_activity')}}</th>
                                            {{--<th>{{trans('violation.proceedings')}}</th>--}}
                                            <th>{{trans('violation.Status_of_Construction')}}</th>
                                            <th>{{trans('violation.Status_of_Violations')}}</th>
                                            {{--<th>{{trans('violation.Violation_Type')}}</th>--}}
                                            @if(Auth::User()->can('assign_violation'))
                                                <th>{{ trans('user.assign') }}</th>
                                            @endif
                                            @if(Auth::User()->can('edit_violation'))
                                                <th>{{trans('pmis.edit')}}</th>
                                            @endif
                                            @if(Auth::User()->can('delete_violation'))
                                                <th>{{trans('pmis.delete')}}</th>
                                            @endif
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="container">
        </div>
    </div>
    <div class="modal fade" id="assign_infraction_modal" tabindex="-1" role="dialog"
         aria-labelledby="Assign Infraction Modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Assign Infraction</h4>
                </div>
                <div class="modal-body">
                    <form action="{{action('InfractionController@assign')}}" method="post" class="form-horizontal">
                        {!! csrf_field() !!}
                        <input type="hidden" name="infraction_id" value="">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="new_item">
                                Select Supervisor
                            </label>
                            <div class="col-md-8">
                                <select class="form-control" name="user_id">
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->first_name }} {{ $user->last_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group container">
                            <div class="col-md-2 pull-right">
                                <button class="btn btn-o btn-primary" type="submit">
                                    {{trans('pmis.save')}}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
@push('scripts')
<script>
    var oTable = $('#users-table').DataTable({
        bFilter: false,
        processing: true,
        serverSide: true,
        "language": {
            "lengthMenu": "{{trans('pmis.show')}} _MENU_{{trans('pmis.entries')}}",
            "zeroRecords": "{{trans('pmis.Nothing_found_sorry')}}",
            // "info": "{{trans('pmis.show')}}_PAGES_{{trans('pmis.of')}} _MAX_{{trans('pmis.entries')}}",
            "info": "{{trans('pmis.show')}} _START_ {{trans('pmis.to')}} _END_ {{trans('pmis.of')}} _TOTAL_ {{trans('pmis.entries')}}",
            "infoEmpty": "{{trans('pmis.No_records_available')}}",
            "infoFiltered": "({{trans('pmis.show')}} {{trans('pmis.of')}}_MAX_ {{trans('pmis.entrie')}})",
            "search": "{{trans('pmis.search')}}",
            paginate: {
                next: "{{trans('pmis.next')}}",
                previous: "{{trans('pmis.previous')}}"
            },
            // "next":"{{trans('pmis.search')}}"
        },
        buttons: [
            'csv', 'excel', 'pdf', 'print'
        ],
        ajax: {
            url: '/infraction/collection/custom-filter-data',
            data: function (d) {
                d.district = $('select[name=district]').val();
                d.contruction_certificate = $('select[name=contruction_certificate]').val();
                d.region_status = $('select[name=region_status]').val();
                d.type_of_activity = $('select[name=type_of_activity]').val();
                d.status_of_cunstraction = $('select[name=status_of_cunstraction]').val();
                d.status_of_infractions = $('select[name=status_of_infractions]').val();
                d.proceedings = $('select[name=proceedings]').val();
                d.violation_type = $('select[name=violation_type]').val();
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'latitude', name: 'latitude'},
            {data: 'district', name: 'district'},
            {data: 'contruction_certificate', name: 'contruction_certificate'},
            {data: 'type_of_activity', name: 'type_of_activity'},
//            {data: 'proceedings', name: 'proceedings'},
            {data: 'status_of_cunstraction', name: 'status_of_cunstraction'},
            {data: 'status_of_infractions', name: 'status_of_infractions'},
//            {data: 'violation_type', name: 'violation_type'},
            @if(Auth::User()->can('assign_violation'))
                {data: 'assign', name: 'assign', orderable: false, searchable: false},
            @endif
            @if(Auth::User()->can('edit_violation'))
                {data: 'action', name: 'action', orderable: false, searchable: false},
            @endif
            @if(Auth::User()->can('delete_violation'))
                {data: 'delete', name: 'delete', orderable: false, searchable: false}
            @endif
        ]
    });

    $('#permits, #district, #region_status, #activity_type, #cunstraction_status, #violation_status, #proceedings, #violation_type').change(function (e) {
        oTable.draw();
        e.preventDefault();
    });
    var district = 'unknown';
    var certification = 'unknown';
    var regionStatus = 'unknown';
    var activityType = 'unknown';
    var cunstractionSt = 'unknown';
    var infractionsSt = 'unknown';
    var proceedings = 'unknown';
    var violationType = 'unknown';

    function initValues() {
        district = $('#district').val() ? $('#district').val() : 'unknown';
        certification = $('#permits').val() ? $('#permits').val() : 'unknown';
        regionStatus = $('#region_status').val() ? $('#region_status').val() : 'unknown';
        activityType = $('#activity_type').val() ? $('#activity_type').val() : 'unknown';
        cunstractionSt = $('#cunstraction_status').val() ? $('#cunstraction_status').val() : 'unknown';
        infractionsSt = $('#violation_status').val() ? $('#violation_status').val() : 'unknown';
        proceedings = $('#proceedings').val() ? $('#proceedings').val() : 'unknown';
        violationType = $('#violation_type').val() ? $('#violation_type').val() : 'unknown';

    }
    function getUrl() {
        return '/infraction/violation/excel/district/' + district + '/certification/' + certification + '/regionStatus/' + regionStatus + '/activityType/'
                + activityType + '/cunstractionSt/' + cunstractionSt + '/infractionsSt/' + infractionsSt +
                '/proceedings/' + proceedings + '/violationType/' + violationType;
    }
    // generate excel report base on dropdown selections
    $("#export").click(function () {
        initValues();
        window.location = getUrl();
    })
    $("#exportall").click(function () {
        window.location = '/km/violation/export/all/toExcel'
    });
    $('.ui.dropdown').dropdown();
    function confirmDelete(id) {
        swal({
                    title: "{{trans('pmis.are_sure')}}",
                    text: "{{trans('pmis.sw_sub_mess')}}",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('pmis.ye_del')}}",
                    cancelButtonText: "{{trans('pmis.cancel')}}",
                    closeOnConfirm: false
                },
                function () {
                    window.location = '/infraction/delete/' + id;
                    swal("{{trans('pmis.deleted')}}", "{{trans('pmis.conf_mess')}}", "success");
                });
    }
</script>
@endpush
