@extends('admin.admin_panel')
@section('side-bar')
    @include('partials.side_bar', ['active' => 1, 'subActive' => null])
@stop

@section('page-level-css')
    <style>
        .highcharts-text-outline{
            stroke: none;
            fill: #000;
        }
    </style>
    @endsection

@section('main-content')

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>  
    <div id="container" class="wrap-content container">
        <!-- project detail and chart section starts here -->
        <section id="page-title" class="padding-top-15 padding-bottom-15">
            <div class="row">
                <div class="col-sm-7">
                    <h1 class="mainTitle">{{trans('pmis.Dashboard')}}</h1>
                    <span class="mainDescription">{{trans('pmis.overview_stats')}}</span>
                </div>
            </div>
        </section>
        <!-- project detail and chart section ends here -->
        <!-- project detail and route section starts here -->
     <!-- <div class="container-fluid container-fullw bg-white">
            <div class="row">
                <div class="col-sm-4">
                    <div class="panel panel-white no-radius text-center">
                        <div class="panel-body">
                    <span class="fa-stack fa-2x">
                        <i class="desktop icon"></i>
                    </span>
                            <h2 class="StepTitle">{{trans('pmis.manage_projects')}}</h2>
                            <p class="links cl-effect-1">
                                <a href="/projects">
                                    {{trans('pmis.Go_on')}}
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-white no-radius text-center">
                        <div class="panel-body">
                    <span class="fa-stack fa-2x">
                        <i class="fa fa-user-secret"></i>
                    </span>
                            <h2 class="StepTitle">{{trans('pmis.manage_violations')}}</h2>
                            <p class="cl-effect-1">
                                <a href="/km/infractions">
                                    {{trans('pmis.Go_on')}}
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-white no-radius text-center">
                        <div class="panel-body">
                    <span class="fa-stack fa-2x">
                        <i class="icon line chart"></i>
                    </span>
                            <h2 class="StepTitle">{{trans('pmis.manage_reports')}}</h2>
                            <p class="links cl-effect-1">
                                <a href="/excel/reports">
                                    {{trans('pmis.Go_on')}}
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- project detail and route section ends here -->
        <!-- project and violation linearchart section starts here -->
       <div class="container-fluid container-fullw padding-bottom-10">
            <div class="row">
              <div class="col-sm-12">
                    <div class="row">
					 <div id="violationBarCh" style="min-width: 510px; max-width: 1200px;
                                                height: 400px; margin: 0 auto; direction: ltr !important;"></div>


                        <!-- linar chart content start here -->
                      <!--  <div class="col-md-12 col-lg-12">
                            <div class="panel panel-white no-radius" id="visits">
                                <div class="panel-heading border-light">
                                    <h4 class="panel-title"> {{trans('pmis.project_overview')}} </h4>
                                    <ul class="panel-heading-tabs border-light">
                                        <li class="panel-tools">
                                            <a data-original-title="Reload" data-toggle="tooltip" data-placement="top"
                                               class="btn btn-transparent btn-sm panel-refresh" href="#"><i
                                                        class="flaticon-monitor"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div collapse="visits" class="panel-wrapper">
                                    <div class="panel-body">
                                        <div class="col-md-5">
                                            <div class="ui relaxed divided list">
                                                <div class="item">

                                                    <div class="content">
                                                        <a class="header">{{trans('pmis.fund_available_for_year')}}</a>
                                                        <div class="description">AFN {{number_format($available_fund,2)}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--<div class="ui relaxed divided list">--}}
                                                {{--<div class="item">--}}
                                                    {{--<div class="content">--}}
                                                        {{--<a class="header">{{trans('pmis.project_weightage')}}</a>--}}
                                                        {{--<div class="description"> {{$project_weightage}} %</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            <div class="ui relaxed divided list">
                                                <div class="item">

                                                    <div class="content">
                                                        <a class="header">{{trans('pmis.expected_execution_in_year')}}</a>
                                                        <div class="description">AFN {{number_format($expected_exe_in_year,2)}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ui relaxed divided list">
                                                <div class="item">

                                                    <div class="content">
                                                        <a class="header">{{trans('pmis.expected_exe_in_year')}}</a>
                                                        <div class="description">{{$exp_weightage_to_total_budget}} %</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ui relaxed divided list">
                                                <div class="item">

                                                    <div class="content">
                                                        <a class="header">{{trans('pmis.exp_rec_budg_dep')}}</a>
                                                        <div class="description">AFN {{number_format($exp_rec_budg_dep,2)}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ui relaxed divided list">
                                                <div class="item">

                                                    <div class="content">
                                                        <a class="header">{{trans('pmis.exe_tell_now')}}</a>
                                                        <div class="description">{{$executed_tell_now}} %</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="proj_pie" class="col-md-7"
                                             style="min-width: 310px; height: 400px; margin: 0 auto; direction: ltr !important;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- linar chart content ends here -->
                    </div>
                </div>
            </div>
        </div>





        <!-- project and violation linearchart section starts here -->
        <div class="container-fluid container-fullw padding-bottom-10" style="display:none">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="panel panel-white no-radius" id="visits">

                                <div collapse="visits" class="panel-wrapper">
                                    <div class="panel-body">
									 <div id="containerrtr"
                                             style="min-width: 310px; height: 400px; margin: 0 auto; direction: ltr !important;"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div style="height: 700px;width: 800px;;">
    <h1>building chart</h1>
    <canvas id="buildChart" style="background-color: rgb(245, 245, 245);" ></canvas>
    
    </div>
    <div style="height: 700px;width: 800px;">
      <h1>Parking chart</h1>
      <canvas id="parkChart" style="background-color: rgb(245, 245, 245);" ></canvas>
      
      </div>
        <!-- projects chart section starts here -->
        <div class="container-fluid container-fullw padding-bottom-10" style="display:none">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <!-- linar chart content start here -->
                      <div class="col-md-7 col-lg-7">
                            <div class="panel panel-white no-radius" id="visitss">

                                <div collapse="visits" class="panel-wrapper" >


									<div id="ProjectPiePerc"
                                       style="display:none" >
										 </div>
                                        <div id="projectBarcontainer" ></div>

                                </div>
                            </div>
                        </div>
                        <!-- linar chart content ends here -->
                        <!-- pie count chart content ends here -->
                        <div class="col-md-5 col-lg-5">
                            <div class="panel panel-white no-radius">
                             <!--   <div class="panel-heading border-light">
                                    <h4 class="panel-title"> {{ trans('pmis.pro_sta') }} </h4>
                                </div> -->
                                <div class="panel-body">

                                </div>
                            </div>
                        </div>
                        <!-- pie count chart content ends here -->
                    </div>
                </div>
            </div>
        </div>
        <!-- projects chart section starts here -->
        <!-- projects chart section starts here Districts -->
        <div class="container-fluid container-fullw padding-bottom-10" style="display:none">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <!-- Bar chart content start here -->
                        <div class="col-md-12 col-lg-12">
                          <div class="panel panel-white no-radius" id="visits">

                                <div collapse="visits" class="panel-wrapper">
                                    <div class="panel-body">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Bar chart content ends here -->
                    </div>
                </div>
            </div>
        </div>
        <!-- projects chart section starts here -->

        <!-- Infraction type of activity and region status chart section starts here -->
        <div class="container-fluid container-fullw padding-bottom-10">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <!-- linar chart content start here -->
                      <!--  <div class="col-md-6 col-lg-6">
                            <div class="panel panel-white no-radius" id="visits">
                                <div class="panel-heading border-light">
                                    <h4 class="panel-title"> {{trans('pmis.projects_violation')}} </h4>
                                    <ul class="panel-heading-tabs border-light">
                                        <li class="panel-tools">
                                            <a data-original-title="Reload" data-toggle="tooltip" data-placement="top"
                                               class="btn btn-transparent btn-sm panel-refresh" href="#"><i
                                                        class="flaticon-monitor"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div collapse="visits" class="panel-wrapper">
                                    <div class="panel-body">
                                        <div id="regionStatus"
                                             style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto; direction: ltr !important;">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- linar chart content ends here -->
                        <!-- pie count chart content ends here -->
                       <!-- <div class="col-md-6 col-lg-6">
                            <div class="panel panel-white no-radius">
                                <div class="panel-heading border-light">
                                    <h4 class="panel-title"> {{ trans('pmis.projects_violation') }} </h4>
                                </div>
                                <div class="panel-body">
                                    <div id="typeActivityPie"
                                         style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto; direction: ltr !important;"></div>
                                </div>
                            </div>
                        </div>
                        <!-- pie count chart content ends here -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Infraction type of activity and region status chart section starts here -->

        <!-- Infraction type of activity and region status chart section starts here -->
      <!--  <div class="container-fluid container-fullw padding-bottom-10">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <!-- linar chart content start here -->
                      <!--  <div class="col-md-6 col-lg-6">
                            <div class="panel panel-white no-radius" id="visits">
                                <div class="panel-heading border-light">
                                    <h4 class="panel-title"> {{trans('pmis.projects_violation')}} </h4>
                                    <ul class="panel-heading-tabs border-light">
                                        <li class="panel-tools">
                                            <a data-original-title="Reload" data-toggle="tooltip" data-placement="top"
                                               class="btn btn-transparent btn-sm panel-refresh" href="#"><i
                                                        class="flaticon-monitor"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div collapse="visits" class="panel-wrapper">
                                    <div class="panel-body">
                                        <div id="constStatus"
                                             style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto; direction: ltr !important;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- linar chart content ends here -->
                        <!-- pie count chart content ends here -->
                     <!--   <div class="col-md-6 col-lg-6">
                            <div class="panel panel-white no-radius">
                                <div class="panel-heading border-light">
                                    <h4 class="panel-title"> {{ trans('pmis.projects_violation') }} </h4>
                                </div>
                                <div class="panel-body">
                                    <div id="constPermit"
                                         style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto; direction: ltr !important;"></div>
                                </div>
                            </div>
                        </div>
                        <!-- pie count chart content ends here -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Infraction type of activity and region status chart section starts here -->
    </div>
    <script>
        
    let buildvalues = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    let parkvalues = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    

    let buildata = {!! json_encode($building)!!}
    let parkdata = {!! json_encode($parking)!!}

    for (i=0;i<buildata.length;i++)
 {
let district_nu  = Number(buildata[i].District)
     
     console.log(district_nu)
     district_nu--
     buildvalues[district_nu] = buildata[i].total
     console.log(buildvalues)
}
for (i=0;i<parkdata.length;i++)
 {
let district_nu  = Number(parkdata[i].District)
     
     console.log(district_nu)
     district_nu--
     parkvalues[district_nu] = parkdata[i].total
     console.log(parkvalues)
}
        const ctx = document.getElementById('buildChart');
        const ctx2 = document.getElementById('parkChart');
        new Chart(ctx, {
      
      data: {
        labels: ['District 1','District 2','District 3','District 4','District 5','District 6','District 7','District 8','District 9','District 10','District 11','District 12','District 13','District 14','District 15','District 16','District 17','District 18','District 19','District 20','District 21','District 22' ],
        datasets: [
        {
            type: 'line',
            label: 'Line chart display',
            data: buildvalues,
            backgroundColor: 'black',

          borderColor: 'gray',

        },
            {
            type: 'bar',
          label: 'bar chart display',
          data: buildvalues,
          borderColor: '#121111',
          backgroundColor: '#689975',
          borderWidth: 1,
        }
    ]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    },
    );
    
    new Chart(ctx2, {
      
      data: {
        labels: ['District 1','District 2','District 3','District 4','District 5','District 6','District 7','District 8','District 9','District 10','District 11','District 12','District 13','District 14','District 15','District 16','District 17','District 18','District 19','District 20','District 21','District 22' ],
        datasets: [
        {
            type: 'line',
            label: 'Line chart display',
            data: parkvalues,
          

        },
            {
            type: 'bar',
          label: 'bar chart display',
          data: parkvalues,
          borderWidth: 1,
        }
    ]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    },
    );
    
    </script>
@stop


@push('scripts')
<script src="{{asset('standard/vendor/Chart.js/Chart.min.js')}}"></script>
<script src="{{asset('standard/vendor/jquery.sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('vendor/highcharts/highcharts.js')}}"></script>
<script src="{{asset('vendor/highcharts/highcharts-3d.js')}}"></script>

@if(App::isLocale('fa') || App::isLocale('pa'))
    <script src="{{asset('vendor/highcharts/exporting_fa.js')}}"></script>
@else
    <script src="{{asset('vendor/highcharts/exporting.js')}}"></script>
@endif
{!! $projectpiePercantage !!}
{!! $linearChartScript !!}
{{--{!! $pieCountChart !!}--}}
{!! $barChartBaseOnPercantage !!}
{!! $distVioBchar !!}
{!! $typeOfActChart !!}
{!! $regionstatus !!}
{!! $statusOfConstraction !!}
{!! $constructionPermit !!}
{!! $projectStatusChart !!}

@endpush
