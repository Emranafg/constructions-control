



<!-- Content area -->
<script>
  let districts =  {!! json_encode($district) !!};
  function guzar(){
        let chosen_district = document.getElementById('district').value;
        chosen_district--

        let guzar_S  = document.getElementById('Guzar')
        guzar_S.innerHTML = ""
        console.log(districts[chosen_district].no_guzar);
        let dump = document.createElement('option');
        guzar_S.appendChild(dump)
    
    for (var i = 1 ; i<= districts[chosen_district].no_guzar;i++) {
    let option = document.createElement('option');
    pad = "00"
   let value = (pad+i).slice(-pad.length)
    option.value = value;
    option.innerHTML = "Guzar "+value;
    guzar_S.appendChild(option)
}
}



function block_ch(){
    let block_S  = document.getElementById('Block')
    block_S.innerHTML = ""
    let dump = document.createElement('option');
    block_S.appendChild(dump)
    for (var i = 1 ; i<=140;i++) {
    let option = document.createElement('option');
    pad = "000"
   let value = (pad+i).slice(-pad.length)
    option.value = value;
    option.innerHTML = "Block "+value;
    block_S.appendChild(option)
}
   console.log("clicked")
}




</script>
<div class="content d-flex justify-content-center align-items-center">
   

    <!-- Registration form -->
    <form action="{{route('property.store')}}" class="flex-fill" method="POST" enctype="multipart/form-data">
        @csrf
        
        <div class="row">
         
           
            <div class="col-lg-12">
                <div class="card mb-0">

                    <div class="card-body">
                        
                        <div class="text-center mb-3">
                            <i class="icon-plus3 icon-2x text-success border-success border-3 rounded-round p-3 mb-3 mt-1"></i>
                            <h5 class="mb-0">{{__('general.add_new_parcel')}}</h5>
                            <span class="d-block text-muted">{{__('general.all_field_required')}}</span>
                        </div>

                        <legend class="font-weight-semibold text-uppercase font-size-sm">
                            <i class="icon-reading mr-2"></i>
                            {{__('general.owners_details')}}
                            <a class="float-right text-default" data-toggle="collapse" data-target="#demo2">
                                <i class="icon-circle-down2"></i>
                            </a>
                        </legend>
                        <div class="row">
                            <div class="col-md-4">
                                
                        <label>{{__('general.Name')}} </label>
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="text" class="form-control" placeholder="{{__('general.Name')}}" name="name" value="{{ old('name') }}">
                                    <div class="form-control-feedback">
                                        <i class="icon-user-tie text-muted"></i>
                                    </div>
                                    @error('name')
                                    <span style="color: red;">*  {{ $message }} </span>
                                @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label>{{__('general.Father_Name')}} </label>
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    
                                    <input type="text" class="form-control" placeholder="{{__('general.Father_Name')}}" name="father_name" value="{{ old('father_name') }}">
                                    <div class="form-control-feedback">
                                        <i class="icon-user-check text-muted"></i>
                                    </div>
                                    @error('father_name')
                                    <span style="color: red;">*  {{ $message }} </span>
                                @enderror
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <label>{{__('general.Grand_Father_Name')}} </label>
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="text" class="form-control" placeholder="{{__('general.Grand_Father_Name')}}" name="Grand_father_name" value="{{ old('Grand_father_name') }}">
                                    <div class="form-control-feedback">
                                        <i class="icon-user-check text-muted"></i>
                                    </div>
                                    @error('Grand_father_name')
                                    <span style="color: red;">*  {{ $message }} </span>
                                @enderror
                                </div>
                            </div>
                        </div>

                      

                        <div class="row">
                            
                            <div class="col-md-4">
                                <label>{{__('general.Tazkira')}} </label>
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="text" class="form-control" placeholder="{{__('general.Tazkira')}}" name="tazkira" value="{{ old('tazkira') }}">
                                    <div class="form-control-feedback">
                                        <i class="icon-vcard text-muted"></i>
                                    </div>
                                    @error('tazkira')
                                    <span style="color: red;">*  {{ $message }} </span>
                                @enderror
                                </div>
                            </div>
                             
                

                            <div class="col-md-4">
                                <label>{{__('general.Phone_nu')}} </label>
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">+93</span>
                                        </span>
                                        <input type="text" class="form-control" placeholder="{{__('general.Phone_nu')}}" name="contact_number" value="{{ old('contact_number') }}">
                                        @error('contact_number')
                                        <span style="color: red;">*  {{ $message }} </span>
                                    @enderror
                                    </div>
                           
                                     
                                
                                    
                                    <div class="form-control-feedback">
                                        <i class="icon-phone2 text-muted"></i>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                <label>{{__('general.job')}} </label>
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="text" class="form-control" placeholder="{{__('general.job')}}" name="job" value="{{ old('job') }}">
                                    <div class="form-control-feedback">
                                        <i class="icon-vcard text-muted"></i>
                                    </div>
                                    @error('job')
                                    <span style="color: red;">*  {{ $message }} </span>
                                @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>{{__('general.current_adress')}} </label>
                                <div class="form-group form-group-feedback form-group-feedback-right">

                                    <input type="text" class="form-control" placeholder="{{__('general.current_adress')}}" name="current_address" value="{{ old('current_address') }}">

                                    <div class="form-control-feedback">
                                        <i class="icon-vcard text-muted"></i>
                                    </div>
                                    @error('current_address')
                                    <span style="color: red;">*  {{ $message }} </span>
                                @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>{{__('general.Permenant_address')}} </label>
                                <div class="form-group form-group-feedback form-group-feedback-right">

                                    <input type="text" class="form-control" placeholder="{{__('general.Permenant_address')}}" name="permenant_address" value="{{ old('permenant_address') }}">

                                    <div class="form-control-feedback">
                                        <i class="icon-vcard text-muted"></i>
                                    </div>
                                    @error('permenant_address')
                                    <span style="color: red;">*  {{ $message }} </span>
                                @enderror
                                </div>
                            </div>
                        


                            
                            <div class="form-group row">
                                <div class="col-sm-6">
                                   <b>{{__('general.Property_Owner_Image')}}</b>
                                  
                                    <input type="file" class="file-input form-control-lg" name="Owner_image_p" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
                                </div>
                                <div class="col-sm-6">
                                   <b>{{__('general.Property_Owner_Tazkira')}}</b> 
                                  
                                    <input type="file" class="file-input form-control-lg" name="Owner_tazkira_p" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
                                </div> 
                                {{-- Parcel Sketch:
                                <input type="file" class="file-input form-control-lg" name="image" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
                                             --}}
                                 
                            </div>
                            



                        </div>

                        
                        <br><br>
                
                  
                       
                        <legend class="font-weight-semibold text-uppercase font-size-sm">
                            <i class="icon-reading mr-2"></i>
                            {{__('general.parcel_location')}}
                            <a class="float-right text-default" data-toggle="collapse" data-target="#demo2">
                                <i class="icon-circle-down2"></i>
                            </a>
                        </legend>
                       
                 
                         <div class="row">
                    <div class="form-group col-sm-3">
                        <label>{{__('general.District')}} </label>
                        <select data-placeholder="Select District" class="form-control select" name="district"  data-fouc id="district" onchange="guzar();">
                       <option value=""></option>
                            <optgroup>
                                @foreach ($district as $item)
                                <option value="{{$item->district_no}}">District {{$item->district_no}} </option> 
                                @endforeach
                              
                              
                             
                            </optgroup>
                        </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <label>{{__('general.Guzar')}} </label>
                        <select data-placeholder="Select Guzar" class="form-control select" name="Guzar" id="Guzar" data-fouc onchange="block_ch();">
                            <option value=""></option>
                            <optgroup>
                        
                                                          
                            </optgroup>
                        </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <label>{{__('general.Block')}} </label>
                        <select data-placeholder="{{__('general.Block')}}" class="form-control select"  id="Block" name="block" data-fouc>
                            <option value=""> </option> 
                            <optgroup>

                              
                                <option value="1">{{__('general.Block1')}}</option>
                                <option value="2">{{__('general.Block2')}}</option>
                                <option value="3">{{__('general.Block3')}}</option>
                                <option value="4">{{__('general.Block4')}}</option>
                                <option value="5">{{__('general.Block5')}}</option>
                                <option value="6">{{__('general.Block6')}}</option>
                                <option value="7">{{__('general.Block7')}}</option>
                                <option value="8">{{__('general.Block8')}}</option>
                                <option value="9">{{__('general.Block9')}}</option>
                             
                            </optgroup>
                        </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <label>{{__('general.Parcel')}} </label>
                        <input type="text" class="form-control" placeholder="{{__('general.Parcel_No')}}" name="parcel">
                     
                    </div>
                    <div class="col-md-3">
                        <label>{{__('general.unit_no')}} </label>
                        <div class="form-group form-group-feedback form-group-feedback-right">
                            <input type="text" class="form-control" placeholder="{{__('general.unit_no')}}" name="unit_no" value="{{ old('unit_no') }}">
                            <div class="form-control-feedback">
                                <i class="icon-vcard text-muted"></i>
                            </div>
                            @error('unit_no')
                            <span style="color: red;">*  {{ $message }} </span>
                        @enderror
                        </div>
                        
                        
                   </div>
                   <div class="col-md-3">
                    <label>{{__('general.house_no')}} </label>
                    <div class="form-group form-group-feedback form-group-feedback-right">
                        <input type="text" class="form-control" placeholder="{{__('general.house_no')}}" name="house_nu" value="{{ old('house_nu') }}">
                        <div class="form-control-feedback">
                            <i class="icon-vcard text-muted"></i>
                        </div>
                        @error('tazkira')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    
                    
               </div>
               <div class="col-md-3">
                <label>{{__('general.Longitude')}} </label>
                <div class="form-group form-group-feedback form-group-feedback-right">
                    <input type="text" class="form-control" placeholder="{{__('general.Longitude')}}" name="Longitude" value="{{ old('Longitude') }}">
                    <div class="form-control-feedback">
                        <i class="icon-vcard text-muted"></i>
                    </div>
                    @error('tazkira')
                    <span style="color: red;">*  {{ $message }} </span>
                @enderror
                </div>
                
                
           </div>
           <div class="col-md-3">
            <label>{{__('general.Latitude')}} </label>
            <div class="form-group form-group-feedback form-group-feedback-right">
                <input type="text" class="form-control" placeholder="{{__('general.Latitude')}}" name="Latitude" value="{{ old('Latitude') }}">
                <div class="form-control-feedback">
                    <i class="icon-vcard text-muted"></i>
                </div>
                @error('tazkira')
                <span style="color: red;">*  {{ $message }} </span>
            @enderror
            </div>
            
            
       </div>
                         </div>
                         <br><br><br>
                   <legend class="font-weight-semibold text-uppercase font-size-sm">
                    <i class="icon-reading mr-2"></i>
                    {{__('general.parcel_boundaries')}}
                    <a class="float-right text-default" data-toggle="collapse" data-target="#demo2">
                        <i class="icon-circle-down2"></i>
                    </a>
                </legend>
                <div class="row">
                    <div class="form-group col-sm-3">
                        <label>{{__('general.North')}}</label>
                        <input type="text" class="form-control" placeholder="{{__('general.North')}}" name="North" value="{{ old('vote') }}">
                        @error('vote')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    <div class="form-group col-sm-3">
                        <label>{{__('general.South')}}</label>
                        <input type="text" class="form-control" placeholder="{{__('general.South')}}" name="South" value="{{ old('vote') }}">
                        @error('vote')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    <div class="form-group col-sm-3">
                        <label>{{__('general.East')}}</label>
                        <input type="text" class="form-control" placeholder="{{__('general.East')}}" name="East" value="{{ old('vote') }}">
                        @error('vote')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    <div class="form-group col-sm-3">

                        <label>{{__('general.West')}}</label>
                        <input type="text" class="form-control" placeholder="{{__('general.West')}}" name="West" value="{{ old('vote') }}">
                        @error('vote')

                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    <div class="form-group col-sm-3">


                        <label>{{__('general.document_no')}}</label>
                        <input type="text" class="form-control" placeholder="{{__('general.document_no')}}" name="doc_nu" value="{{ old('vote') }}">
                        @error('vote')

                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    <div class="form-group col-sm-3">
                        <label>{{__('general.document_date')}} </label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar22"></i></span>
                            </span>
                            <input type="text" class="form-control jalali-date" value="Document Date" name="document_date" value="{{ old('document_date') }}">
                    
                        </div>
                   
                    </div>
                    <div class="form-group col-sm-3">
                        <label>{{__('general.parcel_type')}} </label>

                        <select data-placeholder="{{__('general.parcel_type')}}" class="form-control select" name="parcel_type"  data-fouc id="district">

                       <option value=""></option>
                            <optgroup>
                                <option value="رهایشی">رهایشی</option>
                                <option value="تجارتی">تجارتی</option>
                                <option value="مختلط(رهایشی/تجارتی)">مختلط(رهایشی/تجارتی)</option>
                                <option value="تولیدی">تولیدی</option>
                                <option value="دولتی">دولتی</option>
                                <option value="زراعتی">زراعتی</option>
                                <option value="نمره های خالی">نمره های خالی</option>
                                <option value="نوعیت  ملکیت - دیگر(مشخص) ">نوعیت  ملکیت - دیگر(مشخص) </option>
                              <option value="نوعیت ملکیت - دیگر(نامشخص)">نوعیت ملکیت - دیگر(نامشخص)</option>
                              
                             
                            </optgroup>
                        </select>
                        @error('parcel_type')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    <div class="form-group col-sm-3">
                        <label>{{__('general.building_type')}} </label>

                        <select data-placeholder="{{__('general.building_type')}}" class="form-control select" name="building_type"  data-fouc id="district">

                       <option value=""></option>
                            <optgroup>
                                <option value="انفرادی">انفرادی</option>
                                <option value="اشتراکی">اشتراکی</option>
                            
                            </optgroup>
                        </select>
                        @error('building_type')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    <div class="form-group col-sm-3">
                        <label>{{__('general.Area_status')}} </label>

                        <select data-placeholder="{{__('general.Area_status')}}" class="form-control select" name="Area_status"  data-fouc id="district">

                       <option value=""></option>
                            <optgroup>
                                <option value="Planned">Planned</option>
                                <option value="UnPlanned">UnPlanned</option>
                            
                              
                              
                             
                            </optgroup>
                        </select>
                        @error('Area_status')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                 <br>
                    <div class="form-group row">
                        <div class="col-sm-6">
                           <b> {{__('general.Parcel_Image')}}</b>
                          
                            <input type="file" class="file-input form-control-lg" name="Parcel_image_p" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
                        </div>
                        <div class="col-sm-6">
                           <b>{{__('general.sketch')}}</b> 
                          
                            <input type="file" class="file-input form-control-lg" name="Parcel_sketch_p" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
                        </div> 
                        {{-- Parcel Sketch:
                        <input type="file" class="file-input form-control-lg" name="image" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
                                     --}}
                         
                    </div>
                    </div>

<br><br><br><br>
                        {{-- <div class="form-group row">
                            <label class="col-lg-2 col-form-label font-weight-semibold">Single file upload:</label>
                            <div class="col-lg-10">
                                <input type="file" class="file-input form-control-lg" name="z_image" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
										
                                <span class="form-text text-muted">Uploaded pic will be shown here.</span>
                                @error('z_image')
                                <span style="color: red;">*  {{ $message }} </span>
                            @enderror
                            </div>
                        </div> --}}
                        <button type="submit" class="btn bg-info-400 btn-labeled btn-labeled-right"><b><i class="icon-plus3"></i></b>{{__('general.Parcel_Save')}}</button>
                    </div>
                </div>
            </div>
        </div>
  
    </form>

    <!-- /registration form -->

</div>
<!-- /content area -->
<script>
            $(document).ready(function() {
  $(".jalali-date").pDatepicker({
    initialValueType: "gregorian",
    format: "YYYY/MM/DD",
    onSelect: "year"
  });
});
</script>
