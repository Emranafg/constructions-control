<html lang="en">
<head>
    <title>Kabul Municipality Database</title>
    <meta charset="utf-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <meta content="{!! csrf_token() !!}" name="csrf_token"/>
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic"
          rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="{{asset('rtlversion/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('rtlversion/vendor/bootstrap/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('rtlversion/vendor/fontawesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('rtlversion/vendor/themify-icons/themify-icons.min.css')}}">
    <link href="{{asset('rtlversion/vendor/animate.css/animate.min.css')}}" rel="stylesheet" media="screen">
    <link href="{{asset('rtlversion/vendor/perfect-scrollbar/perfect-scrollbar.min.css')}}" rel="stylesheet"
          media="screen">
    <link href="{{asset('rtlversion/vendor/switchery/switchery.min.css')}}" rel="stylesheet" media="screen">

    <link rel="stylesheet" href="{{asset('rtlversion/assets/css/styles.css')}}">
    <link rel="stylesheet" href="{{asset('rtlversion/assets/css/plugins.css')}}">
    <link rel="stylesheet" href="{{asset('rtlversion/assets/css/themes/theme-1.css')}}" id="skin_color"/>
    <link rel="stylesheet" href="{{asset('rtlversion/assets/css/rtl.css')}}"/>

    <link href="{{asset('standard/vendor/switchery/switchery.min.css')}}" rel="stylesheet" media="screen">
    <link href="{{asset('standard/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
          rel="stylesheet" media="screen">
    <link href="{{asset('standard/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}" rel="stylesheet"
          media="screen">
    <link href="{{asset('rtlversion/vendor/bootstrap-timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet"
          media="screen">

    <link href="{{asset('standard/vendor/select2/select2.min.css')}}" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="{{asset('vendor/dropzone/dropzone.css')}}" id="skin_color"/>

    <!-- Yajra dataTable css -->
    <link rel="stylesheet" href="{{asset('vendor/semanticmaster/semantic.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/datatables2/css/dataTables.semanticui.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custome.css')}}">
    <link rel="stylesheet" href="{{asset('css/icon-font/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('rtlversion/customertl.css')}}"/>
    <style>
        tr {
            text-align: right !important;
        }
        .ui.selection.dropdown {
            text-align: right !important;
        }
        .ui.selection.dropdown .menu>.item {
            text-align: right !important;
        }

    </style>
    <!-- end: RTL main CSS -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    @yield('style')
</head>
<!-- end: HEAD -->
<body>
<nav class="navbar navbar-default" style="position: fixed !important; top: 0 !important;">
    <div class="container-fluid pub-nav" style="text-align: center">
        <a class="center" href="#" style="margin-top: 18px;">
            <img src="/standard/assets/images/logo_dr.png">
        </a>
    </div>
</nav>
<div id="app">
    <!-- sidebar -->
    <!-- start: TOP NAVBAR -->
    <!-- end: TOP NAVBAR -->
    <div class="margin-40">
        <!-- projects chart section starts here -->
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <!-- Bar chart content start here -->
                    <div class="col-md-12 col-lg-12">
                        <div class="panel panel-white no-radius" id="visits">
                            <div class="panel-heading border-light">
                                <h4 class="panel-title"> {{trans('pmis.violation')}} </h4>
                                <ul class="panel-heading-tabs border-light">
                                    <li class="panel-tools">
                                        <a data-original-title="{{trans('pmis.Refresh')}}" data-toggle="tooltip"
                                           data-placement="top"
                                           class="btn btn-transparent btn-sm panel-refresh" href="#"><i
                                                    class="fa fa-refresh"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div collapse="visits" class="panel-wrapper">
                                <div class="panel-body">
                                    <div id="violationBarCh" style="min-width: 810px; max-width: 1200px;
                                                height: 600px; margin: 0 auto; direction: ltr !important;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Bar chart content ends here -->
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="border-top" style="height: 65px !important; border-top: 1px solid rgba(0, 0, 0, 0.07); direction: ltr;">
        <div class="pull-left padding-top-20 padding-left-10" style="">
            &copy; <span class="current-year"></span>
            <span class="text-bold text-uppercase"> <a href="http://km.gov.af" target="_blank">KM</a></span>.
            <span>All rights reserved</span>
            <span>powered by <a href="https://km.gov.af" target="_blank">KM IT</a></span>
        </div>
    </div>
</footer>
<!-- start: MAIN JAVASCRIPTS -->
<script src="{{asset('standard/vendor/jquery/jquery.min.js')}}"></script>
<!-- jQuery datatable -->
<script src="{{asset('vendor/datatables2/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables2/dataTables.semanticui.min.js')}}"></script>

<script src="{{asset('standard/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('standard/vendor/modernizr/modernizr.js')}}"></script>
<script src="{{asset('standard/vendor/jquery-cookie/jquery.cookie.js')}}"></script>
<script src="{{asset('standard/vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('standard/vendor/switchery/switchery.min.js')}}"></script>
<!-- end: MAIN JAVASCRIPTS -->
<script src="{{asset('standard/vendor/autosize/autosize.min.js')}}"></script>
<script src="{{asset('standard/vendor/maskedinput/jquery.maskedinput.min.js')}}"></script>
<script src="{{asset('standard/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
<script src="{{asset('standard/vendor/selectFx/classie.js')}}"></script>
<script src="{{asset('standard/vendor/selectFx/selectFx.js')}}"></script>
<script src="{{asset('standard/vendor/select2/select2.min.js')}}"></script>

<script src="{{asset('standard/vendor/jquery-smart-wizard/jquery.smartWizard.js')}}"></script>
<script src="{{asset('standard/assets/js/form-wizard.js')}}"></script>

<script src="{{asset('standard/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
@if(App::isLocale('en'))
    <script src="{{asset('standard/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>
@endif

<script src="{{asset('standard/vendor/jquery.sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('vendor/semanticmaster/components/transition.min.js')}}"></script>
<script src="{{asset('vendor/semanticmaster/components/progress.min.js')}}"></script>
<script src="{{asset('standard/vendor/sweetalert/sweet-alert.min.js')}}"></script>

@include('partials.theme')
<!-- start: JavaScript Event Handlers for this page -->
<script src="{{asset('vendor/semanticmaster/components/dropdown.js')}}"></script>
<script src="{{asset('standard/assets/js/form-elements.js')}}"></script>
<script src="{{asset('vendor/dropzone/dropzone.js')}}"></script>
<script src="{{asset('standard/assets/js/index.js')}}"></script>

<script src="{{asset('standard/assets/js/table-data.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
@if(App::isLocale('fa') || App::isLocale('pa'))
    <script src="{{asset('rtlversion/vendor/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
    <script src="{{asset('standard/vendor/bootstrap-datepicker/bootstrap-datepicker.fa.js')}}"></script>
@endif
<script src="{{asset('vendor/jsvalidation/js/jsvalidation.min.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>
<script>
    jQuery(document).ready(function () {
        Main.init();
        Index.init();
        FormElements.init();
        FormWizard.init();
    });
</script>
<!-- the pie chart section -->
<script src="{{asset('standard/vendor/Chart.js/Chart.min.js')}}"></script>
<script src="{{asset('standard/vendor/jquery.sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('vendor/highcharts/highcharts.js')}}"></script>
<script src="{{asset('vendor/highcharts/highcharts-3d.js')}}"></script>
{!! $distVioBchar !!}
</body>
</html>
</h1>
