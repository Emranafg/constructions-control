<html lang="en">
<head>
    <title>Kabul Municipality Database</title>
    <meta charset="utf-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <meta content="{!! csrf_token() !!}" name="csrf_token"/>
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic"
          rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="{{asset('rtlversion/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('rtlversion/vendor/bootstrap/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('rtlversion/vendor/fontawesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('rtlversion/vendor/themify-icons/themify-icons.min.css')}}">
    <link href="{{asset('rtlversion/vendor/animate.css/animate.min.css')}}" rel="stylesheet" media="screen">
    <link href="{{asset('rtlversion/vendor/perfect-scrollbar/perfect-scrollbar.min.css')}}" rel="stylesheet"
          media="screen">
    <link href="{{asset('rtlversion/vendor/switchery/switchery.min.css')}}" rel="stylesheet" media="screen">

    <link rel="stylesheet" href="{{asset('rtlversion/assets/css/styles.css')}}">
    <link rel="stylesheet" href="{{asset('rtlversion/assets/css/plugins.css')}}">
    <link rel="stylesheet" href="{{asset('rtlversion/assets/css/themes/theme-1.css')}}" id="skin_color"/>
    <link rel="stylesheet" href="{{asset('rtlversion/assets/css/rtl.css')}}"/>

    <link href="{{asset('standard/vendor/switchery/switchery.min.css')}}" rel="stylesheet" media="screen">
    <link href="{{asset('standard/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
          rel="stylesheet" media="screen">
    <link href="{{asset('standard/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}" rel="stylesheet"
          media="screen">
    <link href="{{asset('rtlversion/vendor/bootstrap-timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet"
          media="screen">

    <link href="{{asset('standard/vendor/select2/select2.min.css')}}" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="{{asset('vendor/dropzone/dropzone.css')}}" id="skin_color"/>

    <!-- Yajra dataTable css -->
    <link rel="stylesheet" href="{{asset('vendor/semanticmaster/semantic.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/datatables2/css/dataTables.semanticui.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custome.css')}}">
    <link rel="stylesheet" href="{{asset('css/icon-font/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('rtlversion/customertl.css')}}"/>
    <style>
        tr {
            text-align: right !important;
        }
        .ui.selection.dropdown {
            text-align: right !important;
        }
        .ui.selection.dropdown .menu>.item {
            text-align: right !important;
        }

    </style>
    <!-- end: RTL main CSS -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    @yield('style')
</head>
<!-- end: HEAD -->
<body>
<nav class="navbar navbar-default" style="position: fixed !important; top: 0 !important;">
    <div class="container-fluid pub-nav">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4"></div>
            <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4">
                <a class="center" href="#" style="margin-top: 18px;">
                    <img src="/standard/assets/images/logo_dr.png">
                </a>  
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4">
                <a href="http://km.cyberaan.com" class="btn btn-link pull-left" style="color: white; margin-top: 20px;">ورود به سیستم</a>
            </div>
        </div>
    </div>

</nav>
<div id="app">
    <!-- sidebar -->
    <!-- start: TOP NAVBAR -->
    <!-- end: TOP NAVBAR -->
    <div class="margin-40">
        <!-- projects chart section starts here -->
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <!-- Bar chart content start here -->
                    <div class="col-md-12 col-lg-12">
                        <div class="panel panel-white no-radius margin-top-30">
                            <form role="form" class="form-inline" id="search-form" method="POST">
                                <div class="panel-heading border-light">
                                    <h4 class="panel-title">{{trans('pmis.km_project_list')}} <span
                                                class="text-bold"></span></h4>
                                    <ul class="panel-heading-tabs border-light">
                                        <li class="panel-tools">
                                            <a href="#" class="btn btn-transparent btn-sm panel-refresh"
                                               onclick="reloadDataTable()"
                                               data-placement="top"
                                               data-toggle="tooltip"
                                               data-original-title="{{trans('pmis.Refresh')}}"><i
                                                        class="fa fa-refresh"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="panel-body margin-top-10">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <select class="ui dropdown" name="status" id="district">
                                                    <option value="">{{trans('pmis.district')}}</option>
                                                    <option value="all">{{trans('pmis.all')}}</option>
                                                    @foreach($district as $district)
                                                        <option value="{{$district->id}}">{{$district->fa_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <select class="ui dropdown" name="status" id="status_n">
                                                    <option value="">{{trans('pmis.project_status_n')}}</option>
                                                    <option value="all">{{trans('pmis.all')}}</option>
                                                    <option value="1">{{trans('pmis.design')}}</option>
                                                    <option value="2">{{trans('pmis.procurement')}}</option>
                                                    <option value="3">{{trans('pmis.construction')}}</option>
                                                    <option value="4">{{trans('pmis.completed')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row border-top margin-top-20">
                                        <div class="margin-top-10"></div>
                                        <div @if(App::isLocale('fa') || App::isLocale('pa'))
                                             class="col-md-12 margin-right-15"
                                             @else class="col-md-12 margin-left-15" @endif >
                                            <table class="ui celled table" id="users-table"
                                                   style="width: 100%; text-align: center !important;">
                                                <thead>
                                                <tr>
                                                    <th>{{trans('pmis.id')}}</th>
                                                    <th>{{trans('pmis.p_name')}}</th>
                                                    <th>{{trans('pmis.district')}}</th>
                                                    <th>{{trans('pmis.project_status_n')}}</th>
                                                    {{--<th>{{trans('pmis.Project_Status')}}</th>--}}
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Bar chart content ends here -->
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="border-top" style="height: 65px !important; border-top: 1px solid rgba(0, 0, 0, 0.07); direction: ltr;">
        <div class="pull-left padding-top-20 padding-left-10" style="">
            &copy; <span class="current-year"></span>
            <span class="text-bold text-uppercase"> <a href="http://km.gov.af" target="_blank">KM</a></span>.
            <span>All rights reserved</span>
            <span>powered by <a href="https://km.gov.af" target="_blank">KM IT</a></span>
        </div>
    </div>
</footer>
<!-- start: MAIN JAVASCRIPTS -->
<script src="{{asset('standard/vendor/jquery/jquery.min.js')}}"></script>
<!-- jQuery datatable -->
<script src="{{asset('vendor/datatables2/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables2/dataTables.semanticui.min.js')}}"></script>

<script src="{{asset('standard/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('standard/vendor/modernizr/modernizr.js')}}"></script>
<script src="{{asset('standard/vendor/jquery-cookie/jquery.cookie.js')}}"></script>
<script src="{{asset('standard/vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('standard/vendor/switchery/switchery.min.js')}}"></script>
<!-- end: MAIN JAVASCRIPTS -->
<script src="{{asset('standard/vendor/autosize/autosize.min.js')}}"></script>
<script src="{{asset('standard/vendor/maskedinput/jquery.maskedinput.min.js')}}"></script>
<script src="{{asset('standard/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
<script src="{{asset('standard/vendor/selectFx/classie.js')}}"></script>
<script src="{{asset('standard/vendor/selectFx/selectFx.js')}}"></script>
<script src="{{asset('standard/vendor/select2/select2.min.js')}}"></script>

<script src="{{asset('standard/vendor/jquery-smart-wizard/jquery.smartWizard.js')}}"></script>
<script src="{{asset('standard/assets/js/form-wizard.js')}}"></script>

<script src="{{asset('standard/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
@if(App::isLocale('en'))
    <script src="{{asset('standard/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>
@endif

<script src="{{asset('standard/vendor/jquery.sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('vendor/semanticmaster/components/transition.min.js')}}"></script>
<script src="{{asset('vendor/semanticmaster/components/progress.min.js')}}"></script>
<script src="{{asset('standard/vendor/sweetalert/sweet-alert.min.js')}}"></script>

@include('partials.theme')
<!-- start: JavaScript Event Handlers for this page -->
<script src="{{asset('vendor/semanticmaster/components/dropdown.js')}}"></script>
<script src="{{asset('standard/assets/js/form-elements.js')}}"></script>
<script src="{{asset('vendor/dropzone/dropzone.js')}}"></script>
<script src="{{asset('standard/assets/js/index.js')}}"></script>

<script src="{{asset('standard/assets/js/table-data.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
@if(App::isLocale('fa') || App::isLocale('pa'))
    <script src="{{asset('rtlversion/vendor/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
    <script src="{{asset('standard/vendor/bootstrap-datepicker/bootstrap-datepicker.fa.js')}}"></script>
@endif
<script src="{{asset('vendor/jsvalidation/js/jsvalidation.min.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>
<script>
    jQuery(document).ready(function () {
        Main.init();
        Index.init();
        FormElements.init();
        FormWizard.init();
    });
</script>
<!-- the pie chart section -->
<script src="{{asset('standard/vendor/Chart.js/Chart.min.js')}}"></script>
<script src="{{asset('standard/vendor/jquery.sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('vendor/highcharts/highcharts.js')}}"></script>
<script src="{{asset('vendor/highcharts/highcharts-3d.js')}}"></script>

<script type="application/javascript">
    $('.ui.dropdown').dropdown();
</script>

<script type="application/javascript">

    var status = 'unknown';
    var name = 'unknown';
    var district = 'unknown';
    var progress = 'unknown';

    function initValues() {
        district = $('#district').val() ? $('#district').val() : 'unknown';
        status = $('#status_n').val() ? $('#status_n').val() : 'unknown';
        name = $('#name').val() ? $('#name').val() : 'unknown';
        dateValue = $('#datevalue').val() ? $('#datevalue').val() : 'unknown';
    }


    var oTable = $('#users-table').DataTable({
        bFilter: false,
        processing: true,
        serverSide: true,
        "language": {
            "lengthMenu": "{{trans('pmis.show')}} _MENU_{{trans('pmis.entries')}}",
            "zeroRecords": "{{trans('pmis.Nothing_found_sorry')}}",
            // "info": "{{trans('pmis.show')}}_PAGES_{{trans('pmis.of')}} _MAX_{{trans('pmis.entries')}}",
            "info": "{{trans('pmis.show')}} _START_ {{trans('pmis.of')}} _END_ {{trans('pmis.of')}} _TOTAL_ {{trans('pmis.entries')}}",//sInfoFiltered:"({{trans('pmis.show')}} _MAX_ total {{trans('pmis.entries')}})"
            "infoEmpty": "{{trans('pmis.No_records_available')}}",
            "infoFiltered": "({{trans('pmis.show')}} {{trans('pmis.of')}}_MAX_ {{trans('pmis.entrie')}})",
            "search": "{{trans('pmis.search')}}",
            'sProcessing':"{{trans('pmis.processing')}}",
            paginate: {

                next: "{{trans('pmis.next')}}",
                previous: "{{trans('pmis.previous')}}"
            },
            // "next":"{{trans('pmis.search')}}"
        },
        buttons: [
            'csv', 'excel', 'pdf', 'print'
        ],
        ajax: {
            url: '/public/reports/debatable'
        },

        ajax: {
            url: '/public/reports/debatable',
            data: function (d) {
                d.name = $('#name').val();
                d.district = $('#district').val();
                d.status_n = $('#status_n').val()
            }
        },
        columns: [
            {data: 'DT_Row_Index', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'district', name: 'district'},
            {data: 'status', name: 'status'},
//            {data: 'project_status_percentage', name: 'project_status_percentage'},
        ]
    });

    function redrawTable(e) {
        oTable.draw();
        e.preventDefault();
    }

    $('#district').change(function (e) {
        redrawTable(e);
    });

    $('#status_n').change(function (e) {
        redrawTable(e);
    });

    function reloadDataTable() {
        console.log($('#users-table').DataTable().url());
    }

</script>


</body>
</html>
</h1>
