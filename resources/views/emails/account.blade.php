@extends('beautymail::templates.minty')
@section('content')
    @include('beautymail::templates.minty.contentStart')
        <tr>
            <td class="title">
                Welcome {{ $user->first_name }} {{ $user->last_name }}
            </td>
        </tr>
        <tr>
            <td width="100%" height="10"></td>
        </tr>
        <tr>
            <td class="paragraph">
                Your account has been created successfully.
            </td>
        </tr>
        <tr>
            <td width="100%" height="25"></td>
        </tr>
        <tr>
            <td class="title">
                Your Account Details
            </td>
        </tr>
        <tr>
            <td width="100%" height="10"></td>
        </tr>
        <tr>
            <td class="paragraph">First Name: {{ $user->first_name }}</td>
        </tr>
        <tr>
            <td class="paragraph">Last Name: {{ $user->last_name }}</td>
        </tr>
        <tr>
            <td class="paragraph">Phone: {{ $user->phone }}</td>
        </tr>
        <tr>
            <td class="paragraph">Email: {{ $user->email }}</td>
        </tr>
        <tr>
            <td class="paragraph">Password: {{ $password }}</td>
        </tr>
        <tr>
            <td width="100%" height="25"></td>
        </tr>
        <tr>
            <td>
                @include('beautymail::templates.minty.button', ['text' => 'Sign in', 'link' => 'http://km.cyberaan.com/login'])
            </td>
        </tr>
        <tr>
            <td width="100%" height="25"></td>
        </tr>
    @include('beautymail::templates.minty.contentEnd')
@stop