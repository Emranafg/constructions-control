<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <style type="text/css" media="screen">
        body {
            font-family: 'Source Sans Pro', 'Helvetica Neue', Arial, sans-serif;
            color: #34495e;
            -webkit-font-smoothing: antialiased;
            line-height: 1.6em;
        }
        p {
            margin: 0;
        }
        .notice {
            position: relative;
            margin: 1em;
            background: #F9F9F9;
            padding: 1em 1em 1em 2em;
            border-left: 4px solid #DDD;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.125);
        }
        .warning {
            border-color: #FFDC00;
            width: 730px;
            margin: 0 auto;
        }
        img{
            position: absolute;
            top: -5px;
            right: 10px;
        }

        </style>
    </head>
    <body>
        <div class="notice warning">
            <div style = "width: 500px;">
                <strong>Dear {{ $data->first_name }} {{ $data->last_name }}, </strong>
                <small class="text-light">Project Manager of </small>
                <strong>{{ $data->project_name }}</strong>
                <p>It is a gentle reminder that please update the progress of the project for the <b>{{ date("F") }}</b> and  attach the related invoices.</p>
                <a href = '{{ url("http://km.cyberaan.com/project/details/$data->project_id") }}' class="btn btn-xs btn-default"> Update </a>
            </div>
            <img src="http://km.cyberaan.com/standard/assets/images/logo.png" alt="">
        </div>
    </body>
</html>