@extends('admin.master2')
@section('side-bar')
    @include('partials.side_bar', ['active' => 3])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans('violation.list_of_parkings'), 'page' => trans('violation.parkings'), 'current' => trans('pmis.reports')])
@stop
@section('alert-message')
    @if(session('message_title'))
        <div role="alert" class="alert {{ session('message_class') }}">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>{{ session('message_title') }}</strong> {{ session('message_description') }}
        </div>
    @endif
@stop
@section('main-content')

<script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />
  
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/buttons/2.3.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

<script src="https://cdn.datatables.net/buttons/2.3.6/js/buttons.html5.min.js"></script>

<div class="row">

</div>

    <div class="container-fluid container-fullw bg-white">
        <div class="container">
            <form role="form" class="form-inline" id="search-form" method="POST">
                <div class="row">
                    <div class="panel panel-white">
                        <div class="panel-heading border-light">
                            <h4 class="panel-title">{{trans('violation.Reports')}}</h4>
                            <ul class="panel-heading-tabs border-light">
                                @if(Auth::User()->can('parking_create'))
                                    <li>
                                        <div class="pull-right">
                                            <div class="ui labeled button" tabindex="0" style="direction: ltr">
                                                <a href="/Parkings/create" class="ui red button">
                                                    <i class="plus icon"></i> {{ trans('violation.create_parking') }}
                                                </a>
                                                <a class="ui basic red left pointing label"
                                                   style="border-radius: 0px !important">
                                                 
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                @endif

                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                          
                                    </div>
                                    
                                   
                                </div>
                            </div>
                            <div class="row margin-top-30 border-top">
                                <div class="margin-top-10"></div>
                                <div @if(App::isLocale('fa') || App::isLocale('pa'))
                                     class="col-md-12 margin-right-15" @else class="col-md-12 margin-left-15" @endif >
                                    <table class="ui celled table" id="example"
                                           style="width: 100%; text-align: center !important;">
                                        <thead>
                                        <tr>
                                            <th>{{trans('violation.name')}}</th>
                                            <th>{{trans('violation.father_name')}}</th>
                                            <th>{{trans('violation.contact')}}</th>
                                            <th>{{trans('violation.coordinate')}}</th>
                                            <th>{{trans('violation.district')}}</th>
                                            <th>{{trans('violation.Status_of_Construction')}}</th>
                                            <th>{{trans('violation.parcel')}}</th>
                                            <th>{{trans('violation.options')}}</th>
                                            {{--<th>{{trans('violation.proceedings')}}</th>--}}
                                            
                                        </tr>
</thead>
                                        @foreach($parking as $item)
                                        <tr>
                                            <td>name</td>
                                            <td>father name</td>
                                            <td>contact</td> 
                                            <td>coordinate</td>
                                            <td>district</td>
                                           
                                            <td>parcel</td>
                                 <td>{{$item->District}}-{{$item->Guzar}}-{{$item->Block}}-{{$item->Parcel}}-{{$item->Unit}}</td>
                                      <td><i class="icon edit"></i>
                                      <a href="/Parkings/delete/{{$item->id}}">
                                      <i class="fa fa-times fa fa-white" style="color: #c82e29;"></i>
                                        </a>
                                    </td>  
                                     
                                        </tr>
                                        @endforeach
                                      
                                     
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="container">
        </div>
    </div>
    <div class="modal fade" id="assign_infraction_modal" tabindex="-1" role="dialog"
         aria-labelledby="Assign Infraction Modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Assign Infraction</h4>
                </div>
                <div class="modal-body">
                    <form action="{{action('InfractionController@assign')}}" method="post" class="form-horizontal">
                        {!! csrf_field() !!}
                        <input type="hidden" name="infraction_id" value="">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="new_item">
                                Select Supervisor
                            </label>
                            <div class="col-md-8">
                                <select class="form-control" name="user_id">
                                
                                </select>
                            </div>
                        </div>
                        <div class="form-group container">
                            <div class="col-md-2 pull-right">
                                <button class="btn btn-o btn-primary" type="submit">
                                    {{trans('pmis.save')}}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <script>
     $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'excel'
        ]
    } );
} );
    </script>
    
@stop
