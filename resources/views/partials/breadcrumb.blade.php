<div class="row">
    <div class="col-sm-8">
        <h1 class="mainTitle">{{$pageTitle}}</h1>
    </div>
    <ol class="breadcrumb">
        <li>
            <span>{{$page}}</span>
        </li>
        <li class="active">
            <span>{{$current}}</span>
        </li>
    </ol>
</div>