sidebar -->
<div class="sidebar app-aside" id="sidebar">
    <div class="sidebar-container perfect-scrollbar">
        <nav>
            <!-- start: SEARCH FORM -->

            <!-- end: SEARCH FORM -->
            <!-- start: MAIN NAVIGATION MENU -->
            <div class="navbar-title">
                <span>{{trans('pmis.Kabul_Municipality')}}</span>
            </div>
            <ul class="main-navigation-menu">
                <li @if($active == 1) class="active" @endif>
                    <a href="/">
                        <div class="item-content">
                            <div class="item-media">
                                <i class="ti-home"></i>
                            </div>
                            <div class="item-inner">
                                <span class="title"> {{trans('pmis.Dashboard')}} </span>
                            </div>
                        </div>
                    </a>
                </li>
                @if(Auth::User()->can('view_project'))
                    <li @if($active == 2) class="active open" @endif>
                        <a href="javascript:void(0)">
                            <div class="item-content">
                                <div class="item-media">
                                    <i class="flaticon-line"></i>
                                </div>
                                <div class="item-inner">
                                    <span class="title"> {{trans('pmis.PMIS')}} </span><i class="icon-arrow"></i>
                                </div>
                            </div>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="/projects">
                                    <span class="title"> {{trans('pmis.projects')}} </span>
                                </a>
                            </li>
                            @if(Auth::User()->can('create_report'))
                            <li @if($active == 2 && $subActive == 2) class="active" @endif>
                                <a href="/excel/reports">
                                    <span class="title"> {{trans('pmis.Excel_Reports')}}</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                @endif
                @if(Auth::User()->can('view_violation'))
                    <li @if($active == 4) class="active" @endif>
                        <a href="/km/infractions">
                            <div class="item-content">
                                <div class="item-media">
                                    <i class="flaticon-man"></i>
                                </div>
                                <div class="item-inner">
                                    <span class="title"> {{trans('violation.Violation')}} </span>
                                </div>
                            </div>
                        </a>
                    </li>
                @endif
                @if(Auth::User()->can('building_view'))
                    <li @if($active == 5) class="active" @endif>
                        <a href="/Buildings">
                            <div class="item-content">
                                <div class="item-media">
                                    <i class="flaticon-man"></i>
                                </div>
                                <div class="item-inner">
                                    <span class="title"> {{trans('violation.buldings')}} </span>
                                </div>
                            </div>
                        </a>
                    </li>
                @endif
                @if(Auth::User()->can('view_violation'))
                    <li @if($active == 6) class="active" @endif>
                        <a href="/Parkings">
                            <div class="item-content">
                                <div class="item-media">
                                    <i class="flaticon-man"></i>
                                </div>
                                <div class="item-inner">
                                    <span class="title"> {{trans('violation.parkings')}} </span>
                                </div>
                            </div>
                        </a>
                    </li>
                @endif
                @if(Auth::User()->can('view_violation'))
                    <li @if($active == 7) class="active" @endif>
                        <a href="#">
                            <div class="item-content">
                                <div class="item-media">
                                <i class="icon parking open"></i>
                                </div>
                                <div class="item-inner">
                                    <span class="title"> {{trans('violation.Reports')}} </span>
                                </div>
                            </div>
                        </a>
                    </li>
                @endif
                @if(Auth::User()->can('view_document'))
                    <li @if($active == 8) class="active" @endif>
                        <a href="/documents">
                            <div class="item-content">
                                <div class="item-media">
                                    <i class="icon folder open"></i>
                                </div>
                                <div class="item-inner">
                                    <span class="title"> {{trans('pmis.Document')}} </span>
                                </div>
                            </div>
                        </a>
                    </li>
                @endif
                @if(Auth::User()->can('create_user'))
                    <li @if($active == 9) class="active" @endif>
                        <a href="/users">
                            <div class="item-content">
                                <div class="item-media">
                                    <i class="icon users"></i>
                                </div>
                                <div class="item-inner">
                                    <span class="title"> {{ trans('user.users') }} </span>
                                </div>
                            </div>
                        </a>
                    </li>
                @endif
            </ul>
            <!-- end: MAIN NAVIGATION MENU -->
        </nav>
    </div>
</div>
<!-- / sidebar
