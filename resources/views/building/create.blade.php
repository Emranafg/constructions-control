@extends('admin.master')
@section('side-bar')
    @include('partials.side_bar', ['active' => 3])
@stop
@section('page-title')
    @include('partials.breadcrumb', ['pageTitle' => trans('violation.create_new_building'), 'page' => trans('violation.buldings'), 'current' => trans('violation.Reports')])
@stop
@section('style')
    <style media="screen">
        .ui.dropdown {
            margin-bottom: 11px;
        }
        .infraction-img img{
          max-width: 150px;
          margin: 5px;
        }
        .infraction-img{
          margin: 0 auto;
          overflow: hidden;
          position: relative;
          height: 150px;
        }
    </style>
@stop
@section('main-content')
<link href="{{ asset('/summernote/summernote.css') }}" rel="stylesheet">
<script src="{{ asset('/summernote/bootstrap.min.js') }}"></script>
<script src="{{ asset('/summernote/summernote.min.js') }}">
</script>

<script>
  let districts =  {!! json_encode($district) !!};
  function guzar(){
        let chosen_district = document.getElementById('district').value;
    

        let guzar_S  = document.getElementById('Guzar')
        guzar_S.innerHTML = ""
        console.log(districts[chosen_district].no_guzar);
        let dump = document.createElement('option');
        guzar_S.appendChild(dump)
    
    for (var i = 1 ; i<= districts[chosen_district].no_guzar;i++) {
    let option = document.createElement('option');
    pad = "00"
   let value = (pad+i).slice(-pad.length)
    option.value = value;
    option.innerHTML = "Guzar "+value;
    guzar_S.appendChild(option)
}
}



function block_ch(){
    let block_S  = document.getElementById('Block')
    block_S.innerHTML = ""
    let dump = document.createElement('option');
    block_S.appendChild(dump)
    for (var i = 1 ; i<=140;i++) {
    let option = document.createElement('option');
    pad = "000"
   let value = (pad+i).slice(-pad.length)
    option.value = value;
    option.innerHTML = "Block "+value;
    block_S.appendChild(option)
}
   console.log("clicked")
}

window.onload = function() {
    guzar()
};


</script>

<div class="container-fluid container-fullw bg-white">
            <div class="row">
                <fieldset>
                    <legend>
                        جزییات نظارت کننده
                    </legend>
                    <form action="{{action('BuildingsController@store')}}" method="post" enctype="multipart/form-data"  id="form">
                    {!! csrf_field() !!}
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                اسم
                            </label>
                            <input class="form-control supervisor-name" placeholder="" type="text" disabled="" value="حبیب احمدزی">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                شماره تماس 
                            </label>
                            <input class="form-control supervisor-phone" placeholder="" type="text" disabled="" value="0773040738">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                ایمیل 
                            </label>
                            <input class="form-control supervisor-email" placeholder="" type="text" disabled="" value="gulhabib82@gmail.test">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>جزئیات مالک جایداد</legend>

                    <div class="col-md-4">
                        <label>
                            اسم
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="name" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            اسم پدر 
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="father_name" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            اسم پدر کلان 
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="G_father_name" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            شماره تذکره
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="Tazkira" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>
                        شماره تلیفون
                        <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="form-group">
                            <input placeholder="" name="Contact" class="form-control" type="text">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>جزئیات موقعیت جایداد</legend>
                    <fieldset>
                    <div class="col-md-4">
                            <label>
                            {{trans('violation.district')}} 
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="ui dropdown selection"  tabindex="0">
                                <select name="district" id="district" onchange="guzar()">
                                
                                <option value="1"></option>
                                <option value="1">ناحیه اول</option>
                                <option value="2">ناحیه دوم</option>
                                <option value="3">ناحیه سوم</option>
                                <option value="4">ناحیه چهارم</option>
                                <option value="5">ناحیه پنجم</option>
                                <option value="6">ناحیه ششم</option>
                                <option value="7">ناحیه هفتم</option>
                                <option value="8">ناحیه هشتم</option>
                                <option value="9">ناحیه نهم</option>
                                <option value="10">ناحیه دهم</option>
                                <option value="11">ناحیه یازدهم</option>
                                <option value="12">ناحیه دوازدهم</option>
                                <option value="13">ناحیه سیزدهم</option>
                                <option value="14">ناحیه چهاردهم</option>
                                <option value="15">ناحیه پاتزدهم</option>
                                <option value="16">ناحیه شانزدهم</option>
                                <option value="17">ناحیه هفدهم</option>
                                <option value="18">ناحیه هشدهم</option>
                                <option value="19">ناحیه نزدهم</option>
                                <option value="20">ناحیه بیستم</option>
                                <option value="21">ناحیه بیست و یکم</option>
                                <option value="22">ناحیه بیست و دوم</option>
                                <option value="23">سایر نواحی</option>
                            </select><i class="dropdown icon"></i>
                            <div class="text">ناحیه اول</div>
                            <div class="menu" tabindex="-1">
                                <div class="item" data-value="1"></div>
                                <div class="item active selected" data-value="1">ناحیه اول</div>
                                <div class="item" data-value="2">ناحیه دوم</div>
                                <div class="item" data-value="3">ناحیه سوم</div>
                                <div class="item" data-value="4">ناحیه چهارم</div>
                                <div class="item" data-value="5">ناحیه پنجم</div>
                                <div class="item" data-value="6">ناحیه ششم</div>
                                <div class="item" data-value="7">ناحیه هفتم</div>
                                <div class="item" data-value="8">ناحیه هشتم</div>
                                <div class="item" data-value="9">ناحیه نهم</div>
                                <div class="item" data-value="10">ناحیه دهم</div>
                                <div class="item" data-value="11">ناحیه یازدهم</div>
                                <div class="item" data-value="12">ناحیه دوازدهم</div>
                                <div class="item" data-value="13">ناحیه سیزدهم</div>
                                <div class="item" data-value="14">ناحیه چهاردهم</div>
                                <div class="item" data-value="15">ناحیه پاتزدهم</div>
                                <div class="item" data-value="16">ناحیه شانزدهم</div>
                                <div class="item" data-value="17">ناحیه هفدهم</div>
                                <div class="item" data-value="18">ناحیه هشدهم</div>
                                <div class="item" data-value="19">ناحیه نزدهم</div>
                                <div class="item" data-value="20">ناحیه بیستم</div>
                                <div class="item" data-value="21">ناحیه بیست و یکم</div>
                                <div class="item" data-value="22">ناحیه بیست و دوم</div>
                                <div class="item" data-value="23">سایر نواحی</div></div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                            {{trans('violation.guzar')}}  
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="ui dropdown selection" tabindex="0">
                                <select name="Guzar" id="Guzar" onchange="block_ch();">
                                <option value="1">.</option>
                                </select>
                            <i class="dropdown icon"></i>
                            <div class="text"></div>
                            <div class="menu" tabindex="-1">
                                <div class="item" data-value="1"></div>
                            </div>
                        </div>
                        </div>

                        <div class="col-md-4">
                            <label>
                            {{trans('violation.block')}}  
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="ui dropdown selection" tabindex="0">
                                <select name="Block" id="Block">
                                <option value="1"></option>
                                </select>
                            <i class="dropdown icon"></i>
                            <div class="text"></div>
                            <div class="menu" tabindex="-1">
                                <div class="item" data-value="1"></div>
                            </div>
                        </div>
                        </div>

                        <div class="col-md-3">
                            <label>
                            {{trans('violation.parcel')}}  
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group" tabindex="0">
                            <input placeholder="" name="Parcel" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label>
                            {{trans('violation.unit')}}  
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group" tabindex="0">
                            <input placeholder="" name="Unit" class="form-control" type="text">
                            </div>
                        </div>
                        

                    </fieldset>
                    <fieldset>
                        <legend>کوردینات ساختمان</legend>
                        <div class="col-md-6">
                            <label>
                                عرض البلد
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="Latitude" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>
                                طول البلد
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group">
                                <input placeholder="" name="Longtitude" class="form-control" type="text">
                            </div>
                        </div>
                    </fieldset>
                    
                        
                        <fieldset>
                <br>
                    
                    <div class="col-md-4">
                        <label>
                        {{trans('violation.Construction_Permit')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="ui dropdown selection" tabindex="0"><select name="contruction_certificate">
                            <option value="1">بلی</option>
                            <option value="2">نخیر</option>
                        </select><i class="dropdown icon"></i><div class="text">بلی</div><div class="menu" tabindex="-1"><div class="item active selected" data-value="1">بلی</div><div class="item" data-value="2">نخیر</div></div></div>
                    </div>
                    <div class="col-md-4">
                        <label>
                        {{trans('violation.Category_of_Land')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="ui dropdown selection" tabindex="0"><select name="land_catagory">
                            <option value="1">پلانی</option>
                            <option value="2">غیر پلانی</option>
                            <option value="3">خودسر</option>
                            <option value="4">زمین سفید</option>
                        </select><i class="dropdown icon"></i><div class="text">پلانی</div><div class="menu" tabindex="-1"><div class="item active selected" data-value="1">پلانی</div><div class="item" data-value="2">غیر پلانی</div><div class="item" data-value="3">خودسر</div><div class="item" data-value="4">زمین سفید</div></div></div>
                    </div>
                    <div class="col-md-4">
                        <label>
                            نوع فعالیت
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="ui dropdown selection" tabindex="0"><select name="land_type">
                            <option value="1">رهایشی</option>
                            <option value="2">تجارتی</option>
                            <option value="3">صنعتی</option>
                            <option value="4">خدماتی/دولتی</option>
                            <option value="5">مختلط (تجارتی / رهایشی)</option>
                        </select><i class="dropdown icon"></i><div class="text">رهایشی</div><div class="menu" tabindex="-1"><div class="item active selected" data-value="1">رهایشی</div><div class="item" data-value="2">تجارتی</div><div class="item" data-value="3">صنعتی</div><div class="item" data-value="4">خدماتی/دولتی</div><div class="item" data-value="5">مختلط (تجارتی / رهایشی)</div></div></div>
                    </div>
                
                    <br>
                    <div class="col-md-3">
                            <label>
                            {{trans('violation.floors')}}  
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group" tabindex="0">
                            <input placeholder="" name="floor" class="form-control" type="text">
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <label>
                            {{trans('violation.building_volume')}}  
                                <span class="symbol required" aria-required="true"></span>
                            </label>
                            <div class="form-group" tabindex="0">
                            <input placeholder="" name="Building_Volume" class="form-control" type="text">
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                        <label>
                        {{trans('violation.Area_size')}}                             
                        <span class="symbol required" aria-required="true"></span>
                        </label>
                        <input placeholder="" name="Area_size" class="form-control" type="text">
                    </div>
                    </fieldset>
                    <fieldset>
                        <br>
                        <div class="col-md-4">
                        <label>
                        {{trans('violation.Construction_Permit')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <div class="ui dropdown selection" tabindex="0"><select name="molkiyat">
                            <option value="1">بلی</option>
                            <option value="2">نخیر</option>
                        </select><i class="dropdown icon"></i><div class="text">بلی</div><div class="menu" tabindex="-1"><div class="item active selected" data-value="1">بلی</div><div class="item" data-value="2">نخیر</div></div></div>
                    </div>
                    <div class="col-md-6">
                        <label>
                        {{trans('violation.Molkeyat_Image')}}
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <input type="file" multiple id="gallery-photo-add" name="Molkyat_doc">
                        <div class="gallery infraction-img"></div>
                    </div>
                    <div class="col-md-9">
                    <div class="form-group">
                      <label for="role_name">اظهار نظر<span class="text-danger"> * </span></label>
                      <textarea id="summernote" name="Comment" required></textarea>

                    </div>
                    </div>
                    
                    <div class="col-md-6">
                        <label>
                            سکیچ ساختمان
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <input type="file" multiple="" id="gallery-photo-add" name="building_sketch">
                        <div class="gallery infraction-img"></div>

                    </div>
                    <div class="col-md-6">
                        <label>
                           عکس ساختمان
                            <span class="symbol required" aria-required="true"></span>
                        </label>
                        <input type="file" multiple="" id="gallery-photo-add" name="building_image">
                        <div class="gallery infraction-img"></div>

                    </div>
                

                <div class="form-group">
                    <button class="btn btn-primary btn-o next-step btn-wide pull-right">
                        ذخیره <i class="fa fa-arrow-circle-right"></i>
                    </button>
        </form>
                </div>
            </div>
        </div>
    </fieldset>
       
    <!-- </form> -->
@stop
@push('scripts')
<link href="{{ asset('/summernote/summernote.css') }}" rel="stylesheet">
<script src="{{ asset('/summernote/summernote.min.js') }}">
</script>
<script>
    $('.ui.dropdown').dropdown();
    $(function() {
      // Multiple images preview in browser
      var imagesPreview = function(input, placeToInsertImagePreview) {

          if (input.files) {
              var filesAmount = input.files.length;

              for (i = 0; i < filesAmount; i++) {
                  var reader = new FileReader();

                  reader.onload = function(event) {
                      $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                  }

                  reader.readAsDataURL(input.files[i]);
              }
          }

      };

      $('#gallery-photo-add').on('change', function() {
          imagesPreview(this, 'div.gallery');
      });
  });
</script>
<script>
  $(document).ready(function() {
      $('#summernote').summernote({
        placeholder: 'دیتای اعلانات شاروالی کابل',
        tabsize: 2,
        height: 300
      });

  });
</script>
@endpush
@section('validator')
    {!! JsValidator::formRequest('App\Http\Requests\ViolationRequest') !!}
@endsection
