<!DOCTYPE html>
<html lang="en">
<head>
    <title>Kabul Municipality Database</title>
    <meta charset="utf-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- end: META -->
    <!-- start: GOOGLE FONTS -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic"
          rel="stylesheet" type="text/css"/>
    <!-- end: GOOGLE FONTS -->
    <!-- start: MAIN CSS -->
    <link rel="stylesheet" href="{{asset('standard/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('standard/vendor/fontawesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('standard/vendor/themify-icons/themify-icons.min.css')}}">
    <link rel="stylesheet" href="{{asset('standard/vendor/animate.css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('standard/vendor/perfect-scrollbar/perfect-scrollbar.min.css')}}">
    <link rel="stylesheet" href="{{asset('standard/vendor/switchery/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('standard/assets/css/styles.css')}}">
    <link rel="stylesheet" href="{{asset('standard/assets/css/plugins.css')}}">
    <link rel="stylesheet" href="{{asset('standard/assets/css/themes/theme-1.css')}}">
    <style media="screen">
        .main-login .logo {
            text-align: center;
        }

        .btn-primary, fieldset, .box-login {
            border-radius: 0 !important;
        }
    </style>
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body class="login">
		<!-- start: LOGIN -->
		<div class="row">
			<div class="main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
				<div class="logo margin-top-30">
					<img src="{{asset('standard/assets/images/logo.png')}}" alt="Municipality"/>
                    <h4>Kabul Municipality Database</h4>
				</div>
				<!-- start: LOGIN BOX -->
				<div class="box-login">
					<form class="form-login" method="POST" action="{{ url('/login') }}" id = "form">
                        {{ csrf_field() }}
						<fieldset>
							<legend>
								Sign in to your account
							</legend>
							<p>
								Please enter your email and password to log in.
							</p>
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<span class="input-icon">
									<input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required autofocus placeholder="Email">
									<i class="fa fa-user"></i> </span>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-actions form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        						<span class="input-icon">
        							<input type="password" class="form-control password" id="password" name="password"
                                           placeholder="Password" required>
        							<i class="fa fa-lock"></i>
        							{{-- <a class="forgot" href="/">
                                        I forgot my password
                                    </a> --}}
                                </span>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-actions">
                                {{-- <div class="checkbox clip-check check-primary">
                                    <input type="checkbox" id="remember" name="remember">
                                    <label for="remember">
                                        Keep me signed in
                                    </label>
                                </div> --}}
                                <button type="submit" class="btn btn-primary pull-right">
                                    Login <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                            {{-- <div class="new-account">
                                Don't have an account yet?
                                <a href="login_registration.html">
                                    Create an account
                                </a>
                            </div> --}}
                        </fieldset>
                    </form>
                    <!-- start: COPYRIGHT -->
                    <div class="copyright">
                        &copy; <span class="current-year"></span><a target="_blank" href="https://km.gov.af"><span
                                    class="text-bold text-uppercase"> KM IT</span></a>. <span>All rights reserved</span>
                    </div>
                    <!-- end: COPYRIGHT -->
                </div>
            <!-- end: LOGIN BOX -->
            </div>
        </div>
        <!-- end: LOGIN -->
        <!-- start: MAIN JAVASCRIPTS -->
        <script src="{{asset('standard/vendor/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('standard/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('vendor/jsvalidation/js/jsvalidation.min.js')}}"></script>

        {!! JsValidator::formRequest('App\Http\Requests\LoginRequest') !!}
    </body>
<!-- end: BODY -->
</html>
