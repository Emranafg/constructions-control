<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Message  Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during displaying alert message for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    /*
    |--------------------------------------------------------------------------
    | Description of abbreviations
    |--------------------------------------------------------------------------
    |
    | p => project
    | r => Report
    | i => Infraction
    | d => Document
    | c => Case
    |
    */


    'p_success_title' => 'Well done!',
    'p_success_desc' => 'You have successfully created a new project.',

    'r_success_title' => 'Well done!',
    'r_success_desc' => 'You have successfully created a new project.',

    'i_success_title' => 'Well done!',
    'i_success_desc' => 'You have successfully created a new project.',

    'd_success_title' => 'Well done!',
    'd_success_desc' => 'You have successfully created a new project.',

    'c_success_title' => 'Well done!',
    'c_success_desc' => 'You have successfully created a new project.',


];
