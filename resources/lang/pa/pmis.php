<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'yes'                      => 'Yes',
    'no'                       => 'No',
    'planned'                  => 'Planned',
    'nplanned'                 => 'Non Planned',
    'overconf'                 => 'Overconfident',
    'residential'              => 'Residential',
    'commercial'               => 'Commercial',
    'industrial'               => 'Industrial',
    'services'                 => 'Services',
    'und_construction'         => 'Under Construction',
    'stoped'                   => 'Stoped',
    'under_progress'           => 'Under Progress',
    'compleated'               => 'Compleated',
    'eye_protection'           => 'Eyes Protection',
    'helmet'                   => 'Helmet',
    'ear_protection'           => 'Ear Protection',
    'respiratory_protection'   => 'Respiratory Protection',
    'safety_boots'             => 'Safety Boots',
    'safety_gloves'            => 'Safety Gloves',
    'safety_uniforms'          => 'Safety Uniforms',
    'safe_face_on'             => 'Safe Face On',
    'tape_barriers'            => 'Tape Barriers',
    'warning_signs'            => 'Warning Signs',
    'mandatory_signs'          => 'Mandatory Signs',
    'first_aid_sign'           => 'First Aid Sign',
    'risks_of_chemicals'       => 'Risks Of Chemicals',
    'symptoms_of_obstacles'    => 'Symptoms Of Obstacles',
    'noise_pollution'          => 'Noise Pollution',
    'asphalting_disadvantages' => 'Asphalting Disadvantages',
    'concrete_disadvantages'   => 'Concrete Disadvantages',
    // excel files
    'p_name'                   => 'Project Name',
    'contract'                 => 'Contract',
    'contractor'               => 'Contractor',
    'owner'                    => 'Owner',
    'area'                     => 'Area',
    'passway'                  => 'Passway',
    'status'                   => 'Status',
    'status_value'             => 'Value',
    'latitude'                 => 'Latitude',
    'longitude'                => 'Longitude',
    'contract_date'            => 'Contract Date',
    'start_date'               => 'Start Date',
    'compilation_date'         => 'Compilation Date',
    'project_budget'           => 'Project Budget',
    'addational_budget'        => 'Project Addation Budget',
    // new changes in creating project
    'project_code'                  => 'Project Code',
    'fund_available_for_year'      => 'Fund Available For The Project',
    'expected_execution_in_year'    => 'Expected Execution In Year',
    'expense_record_budget_dep'    => 'Expense Record with Budget Department',
    'project_status'                => 'Project Status',
    'project_comment'               => 'Project Comment',
    'project_type'                  => 'Project_type',
    'processing'       => 'در حال اجرا...'


];