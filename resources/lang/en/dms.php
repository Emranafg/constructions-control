<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    */
    'Title'                      => 'Title',
    'Source'                     => 'Order',
    'Destination'                 => 'Destination',
    'Version'                     => 'Version',
    'Case'                        => 'Result',
    'Priority'                    => 'Priority',
     
    'End_Date'                    => 'End Date',
    'progress'                    =>'Progress',
    'Document_information'        => 'Document information',
    'insert_here'                 => 'Insert Document Inforamtion here',
    'category'                    => 'Category',
    'Archive'                     => 'Archive',
    'Tahrir_File'                 => 'Edit File',
    'Documents'                   => 'Documents',
    'Petitions'                   => 'Request',
    'periority'                   => 'Periority',
    'Documents_action'            => 'Urgent Documents',
    'Public_petitions'            => 'Citizen Request',
    'Instructions_mayor'          => 'The Mayor Decree',
    'Supreme_meetings'            => 'Administrative Council meetings',
    'version'                     => 'Version',
    'First'                       => 'First',
    'Second'                      => 'Second',
    'Third'                       => 'Third',
    'Fourth'                      => 'Fourth',
    'Drop_files'                  => 'Drop files',
    'save'                        => 'Save',
    'Document_related_case'       =>'Document related case',
    'document_details'            => 'Document Details',
    'create_new_case'             =>'Create New Case',
    'add_case'                      => 'Add new Case',
	'create_document'              =>'Create',
	
];
