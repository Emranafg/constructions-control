<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Message  Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during displaying alert message for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    /*
    |--------------------------------------------------------------------------
    | Description of abbreviations
    |--------------------------------------------------------------------------
    |
    | p => project
    | r => Report
    | i => Infraction
    | d => Document
    | c => Case
    |
    */

    'p_c_success_title' => 'Well done!',
    'p_c_success_desc' => 'You have successfully created a new Project.',

    'p_u_success_title' => 'Well done!',
    'p_u_success_desc' => 'You have successfully updated the Project.',

    'p_d_success_title' => 'Well done!',
    'p_d_success_desc' => 'You have successfully deleted the Project.',

    'p_a_success_title' => 'Well done!',
    'p_a_success_desc' => 'You have successfully Assigned the Project.',

    'r_c_success_title' => 'Well done!',
    'r_c_success_desc' => 'You have successfully created a new Report.',

    'r_u_success_title' => 'Well done!',
    'r_u_success_desc' => 'You have successfully updated the Report.',

    'r_d_success_title' => 'Well done!',
    'r_d_success_desc' => 'You have successfully deleted the Report.',

    'i_c_success_title' => 'Well done!',
    'i_c_success_desc' => 'You have successfully created a new Infraction.',

    'i_u_success_title' => 'Well done!',
    'i_u_success_desc' => 'You have successfully updated the Infraction.',

    'i_d_success_title' => 'Well done!',
    'i_d_success_desc' => 'You have successfully deleted the Infraction.',

    'd_c_success_title' => 'Well done!',
    'd_c_success_desc' => 'You have successfully created a new Document.',

    'd_u_success_title' => 'Well done!',
    'd_u_success_desc' => 'You have successfully updated the Document.',

    'd_d_success_title' => 'Well done!',
    'd_d_success_desc' => 'You have successfully deleted the Document.',

    'c_c_success_title' => 'Well done!',
    'c_c_success_desc' => 'You have successfully created a new Document Case.',

    'c_u_success_title' => 'Well done!',
    'c_u_success_desc' => 'You have successfully updated the Document Case.',

    'c_d_success_title' => 'Well done!',
    'c_d_success_desc' => 'You have successfully deleted the Document Case.',

    'u_c_success_title' => 'Well done!',
    'u_c_success_desc' => 'You have successfully created a new User.',

    'u_u_success_title' => 'Well done!',
    'u_u_success_desc' => 'You have successfully updated the User.',

    'u_d_success_title' => 'Well done!',
    'u_d_success_desc' => 'You have successfully deleted the User.',

    'profile_u_success_title' => 'Well done!',
    'profile_u_success_desc' => 'You have successfully updated your Profile.',

    'query_error_title' => 'Whoops!',
    'query_error_desc' => 'An Error Occurred.',

    'delete_message_project'  => 'Are you sure to delete this Project?',
    'delete_message_report'  => 'Are you sure to delete this Report?',
    'delet_message_violation' => 'Are you sure to delete this Violation?',
    'delet_message_document' =>  'Are you sure to delete this Document?',
    'delete_message_case'  => 'Are you sure to delete this Case?',
    'delete_message_user'  => 'Are you sure to delete this User?',

    'i_a_success_title' => 'Well done!',
    'i_a_success_desc' => 'You have successfully Assigned the Violation.',
];
