<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    */
    'Title'                       => 'عنوان',
    'Source'                      => 'منبع',
    'Destination'                 => 'مقصد',
    'Version'                     => 'نسخه',
    'Case'                        => 'موضوعات جلسه',
    'Priority'                    => 'اولویت',
    'Start_Date'                  => '',
    'End_Date'                    => 'تاریخ ختم',
    'progress'                    =>'جریان',
    'Document_information'        => 'تصمیمات جلسه مقام شاروالی کابل',
    'insert_here'                 => 'قرار دادن اطلاعات سند در اینجا',
    'category'                    => 'کتگوری',
    'Archive'                     => 'ارشيف',
    'Tahrir_File'                 => 'تحریرات',
    'Documents'                   => 'اسناد',
    'Petitions'                   => 'عرایض',
    'periority'                   => 'اولويت',
    'Documents_action'            => 'اقدامات عاجل',
    'Public_petitions'            => 'عرایض مردم',
    'Instructions_mayor'          => 'هدایات شاروال',
    'Supreme_meetings'            => 'جلسات شورای عالی',
    'First'                       => 'اول',
    'Second'                      => 'دوم',
    'Third'                       => 'سوم',
    'Fourth'                      => 'چهادرم',
    'Drop_files'                  => 'افزودن فایل ها',
    'save'                        => 'ذخیره',
    'Document_related_case'        =>'لست اسناد',
    'document_details'          => 'جزئیات اسناد',
    'Kabul_Municipality'            =>'شاروالی کابل',
    'Title'                 => 'عنوان',
    'Source'                => 'هدایت جلسه',
    'Destination'           => 'فیصله جلسه',
    'Version'               => 'نسخه',
    'Priority'              => 'اولویت',
    'End_Date'              => 'تاریخ ختم',
    'progress'              => 'جریان',
    'Document_information'  => 'تصمیمات جلسه مقام شاروالی کابل در مورد کنترول ساختمان ها',
    'insert_here'           => 'قرار دادن فیصله جلسه کنترول ساختمان ها',
    'category'              => 'کتگوری',
    'Archive'               => 'ارشيف',
    'Tahrir_File'           => 'ویرایش سند',
    'Documents'             => 'تصمیمات جلسه مقام شاروالی کابل در مورد کنترول ساختمان ها',
    'Petitions'             => 'عرایضه',
    'periority'             => 'اولويت',
    'Documents_action'      => 'اقدامات عاجل',
    'Public_petitions'      => 'عرایض مردم',
    'Instructions_mayor'    => 'احکام مقام ',
    'Supreme_meetings'      => 'موضوع دیگر',
    'version'               => 'نمونه',
    'First'                 => 'هدایت اول',
    'Second'                => 'هدایت دوم',
    'Third'                 => 'هدایت سوم',
    'Fourth'                => 'اخطاریه',
    'Drop_files'            => 'افزودن فایل ها',
    'save'                  => 'ذخیره',
    'Document_related_case' => 'دوسیه های مربوط فیصله جلسه',
    'document_details'      => 'جزئیات فیصله',
    'Kabul_Municipality'    => 'شاروالی کابل',
    'create_new_case'       =>'اینجا دوسیه جدید',
    'add_case'              => 'اضافه مکتوب ',
	
	'Start_Datee'           =>'تاریخ جلسه',
	'create_document'       =>'ایجاد',
	'expiration_datee'      => 'تاریخ ختم جلسه',
];
