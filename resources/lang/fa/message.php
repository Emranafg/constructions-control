<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Message  Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during displaying alert message for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    /*
    |--------------------------------------------------------------------------
    | Description of abbreviations
    |--------------------------------------------------------------------------
    |
    | p => project
    | r => Report
    | i => Infraction
    | d => Document
    | c => Case
    |
    */

    'p_c_success_title' => 'بسیار خوب!',
    'p_c_success_desc' => 'شما موفقانه پروژه  جدید را ایجاد نمودید.',

    'p_u_success_title' => 'بسیار خوب!',
    'p_u_success_desc' => 'شما موفقانه پروژه را اپدیت نمودید.',

    'p_d_success_title' => 'بسیار خوب!',
    'p_d_success_desc' => 'شما موفقانه پروژه را حذف نمودید.',

    'p_a_success_title' => 'بسیار خوب!',
    'p_a_success_desc' => 'شما موفقانه پروژه را اختصاص نمودید.',

    'r_c_success_title' => 'بسیار خوب!',
    'r_c_success_desc' => 'شما موفقانه گزارش جدید را ایجاد نمودید.',

    'r_u_success_title' => 'بسیار خوب!',
    'r_u_success_desc' => 'شما موفقانه گزارش را اپدیت نمودید',

    'r_d_success_title' => 'بسیار خوب!',
    'r_d_success_desc' => 'شما موفقانه گزارش را حذف نمودید.',

    'i_c_success_title' => 'بسیار خوب!',
    'i_c_success_desc' => 'شما موفقانه تخلف جدید را ایجاد نمودید.',

    'i_u_success_title' => 'بسیار خوب!',
    'i_u_success_desc' => 'شما موفقانه تخلف را اپدیت نمودید',

    'i_d_success_title' => 'بسیار خوب!',
    'i_d_success_desc' => 'شما موفقانه تخلف را حذف نمودید.',

    'd_c_success_title' => 'بسیار خوب!',
    'd_c_success_desc' => 'شما موفقانه سند جدید را ایجاد نمودید.',

    'd_u_success_title' => 'بسیار خوب!',
    'd_u_success_desc' => 'شما موفقانه سند را اپدیت نمودید.',

    'd_d_success_title' => 'بسیار خوب!',
    'd_d_success_desc' => 'شما موفقانه سند را حذف نمودید.',

    'c_c_success_title' => 'بسیار خوب!',
    'c_c_success_desc' => 'شما موفقانه قضیه جدید را ایجاد نمودید',

    'c_u_success_title' => 'بسیار خوب!',
    'c_u_success_desc' => 'شما موفقانه قضیه را اپدیت نمودید.',

    'c_d_success_title' => 'بسیار خوب!',
    'c_d_success_desc' => 'شما موفقانه قضیه را حذف نمودید.',

    'u_c_success_title' => 'بسیار خوب!',
    'u_c_success_desc' => 'شما موفقانه استفاده کننده جدید را ایجاد نمودید',

    'u_u_success_title' => 'بسیار خوب!',
    'u_u_success_desc' => 'شما موفقانه استفاده کننده را اپدیت نمودید.',

    'u_d_success_title' => 'بسیار خوب!',
    'u_d_success_desc' => 'شما موفقانه استفاده کننده را حذف نمودید.',

    'profile_u_success_title' => 'بسیار خوب!',
    'profile_u_success_desc' => 'شما موفقانه پروفایل را اپدیت نمودید.',

    'delet_message_document' =>  'آیا مطمئن هستید که این سند حذف شود؟',
    'delet_message_violation' => 'آیا مطمئن هستید که این تخلف حذف شود؟',
    'delete_message_user' => 'آیا مطمئن هستید که این استفاده کننده حذف شود؟',
    'delete_message_report' => 'آیا مطمئن هستید که این گزارش حذف شود؟',
    'delete_message_project' => 'آیا مطمئن هستید که این پروژه حذف شود؟',
    'delete_message_case' => 'آیا مطمئن هستید که این  قضیه حذف شود؟',

    'i_a_success_title' => 'بسیار خوب!',
    'i_a_success_desc' => 'شما موفقانه تخلف را اختصاص دادید.',
];