<?php



return [



    /*

    |--------------------------------------------------------------------------

    | Validation Language Lines

    |--------------------------------------------------------------------------

    |

    | The following language lines contain the default error messages used by

    | the validator class. Some of these rules have multiple versions such

    | as the size rules. Feel free to tweak each of these messages here.

    |

    */



    'accepted'             => 'The :attribute must be accepted.',

    'active_url'           => 'The :attribute is not a valid URL.',

    'after'                => ' :attribute باید بعد از :date باشد.',

    'alpha'                => 'فیلد :attribute تنها شامل حروف باشد.',

    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',

    'alpha_num'            => 'فیلد:attribute تنها شامل حروف و اعداد باشد.',

    'array'                => 'The :attribute must be an array.',

    'before'               => 'The :attribute must be a date before :date.',

    'between'              => [

        'numeric' => 'The :attribute must be between :min and :max.',

        'file'    => 'The :attribute must be between :min and :max kilobytes.',

        'string'  => 'The :attribute must be between :min and :max characters.',

        'array'   => 'The :attribute must have between :min and :max items.',

    ],

    'boolean'              => 'The :attribute field must be true or false.',

    'confirmed'            => 'فیلد تکرار :attribute مطابقت ندارد.',

    'date'                 => 'فیلد:attribute یک تاریخ درست نیست.',

    'date_format'          => 'The :attribute does not match the format :format.',

    'different'            => 'The :attribute and :other must be different.',

    'digits'               => 'فیلد:attribute باید :digits digits.',

    'digits_between'       => 'The :attribute must be between :min and :max digits.',

    'dimensions'           => 'The :attribute has invalid image dimensions.',

    'distinct'             => 'The :attribute field has a duplicate value.',

    'email'                => 'فیلد:attribute باید یک ایمیل ادرس درست باشد.',
    'exists'               => 'The selected :attribute is invalid.',

    'file'                 => 'The :attribute must be a file.',

    'filled'               => 'The :attribute field is required.',

    'image'                => 'The :attribute must be an image.',

    'in'                   => 'The selected :attribute is invalid.',

    'in_array'             => 'The :attribute field does not exist in :other.',

    'integer'              => 'The :attribute must be an integer.',

    'ip'                   => 'The :attribute must be a valid IP address.',

    'json'                 => 'The :attribute must be a valid JSON string.',

    'max'                  => [

        'numeric' => 'The :attribute may not be greater than :max.',

        'file'    => 'The :attribute may not be greater than :max kilobytes.',

        'string'  => 'The :attribute may not be greater than :max characters.',

        'array'   => 'The :attribute may not have more than :max items.',

    ],

    'mimes'                => 'The :attribute must be a file of type: :values.',

    'mimetypes'            => 'The :attribute must be a file of type: :values.',

    'min'                  => [

        'numeric' => 'The :attribute must be at least :min.',

        'file'    => 'The :attribute must be at least :min kilobytes.',

        'string'  => ':attribute حداقل :min حرف باشد.',

        'array'   => 'The :attribute must have at least :min items.',

    ],

    'not_in'               => 'The selected :attribute is invalid.',

    'numeric'              => 'فیلد :attribute  باید نمبر باشد. (....1234)',

    'present'              => 'The :attribute field must be present.',

    'regex'                => 'The :attribute format is invalid.',

    'required'             => 'فیلد :attribute ضروری است.',

    'required_if'          => 'The :attribute field is required when :other is :value.',

    'required_unless'      => 'The :attribute field is required unless :other is in :values.',

    'required_with'        => 'The :attribute field is required when :values is present.',

    'required_with_all'    => 'The :attribute field is required when :values is present.',

    'required_without'     => 'The :attribute field is required when :values is not present.',

    'required_without_all' => 'The :attribute field is required when none of :values are present.',

    'same'                 => 'The :attribute and :other must match.',

    'size'                 => [

        'numeric' => 'The :attribute must be :size.',

        'file'    => 'The :attribute must be :size kilobytes.',

        'string'  => 'The :attribute must be :size characters.',

        'array'   => 'The :attribute must contain :size items.',

    ],

    'string'               => 'فیلد :attribute باید متن باشد.',

    'timezone'             => 'The :attribute must be a valid zone.',

    'unique'               => '÷:attribute قبلا گرفته شده.',

    'uploaded'             => 'The :attribute failed to upload.',

    'url'                  => 'The :attribute format is invalid.',

    "alpha_spaces"         => "فیلد:attribute تنها شامل حروف و فاص باشد.",

    "strong_password"      => "رمز عبور باید ترکیبی از حروف بزرگ، حروف کوچک و اعداد باشد.",



    /*

    |--------------------------------------------------------------------------

    | Custom Validation Language Lines

    |--------------------------------------------------------------------------

    |

    | Here you may specify custom validation messages for attributes using the

    | convention "attribute.rule" to name the lines. This makes it quick to

    | specify a specific custom language line for a given attribute rule.

    |

    */



    'custom' => [

        'attribute-name' => [

            'rule-name' => 'custom-message',

        ],

    ],



    /*

    |--------------------------------------------------------------------------

    | Custom Validation Attributes

    |--------------------------------------------------------------------------

    |

    | The following language lines are used to swap attribute place-holders

    | with something more reader friendly such as E-Mail Address instead

    | of "email". This simply helps us make messages a little cleaner.

    |

    */

    'attributes' => ['first_name' => 'اسم', 'last_name' =>'تخلص', 'email' =>'ایمیل', 'phone' => 'نمبر تيلفون', 'password' =>'رمز عبور' ,'name' => 'اسم', 'contractor' =>'فرار داد دهنده', 'contract' =>'قراردادی', 'owner' => 'مالک', 'area' =>'ساحه' , 'district' => 'ناحیه', 'passway' =>'گذر', 'contract_date' =>'تاریخ عقد قرار داد', 'start_date' => 'تاریخ شروع', 'actual_date' =>'تاریخ اصلی' ,'planned_date' => 'تاریخ پلان شده', 'project_budget' =>'بودیجه پروژه', 'additional_budget' =>'بودیجه اضافی', 'latitude' => 'طول البلد', 'longitude' =>'عرض البلد' ,'supervision_date' => 'تاریخ نظارت', 'supervision_time' =>'زمان بررسی', 'weather' =>'آب وهوا', 'temperature' => 'درجه حرارت', 'project_manager' =>'مدیر پروژه' ,'control_manager' => 'انجنیر کنترول کیفیت', 'safety_manager' =>'انجنیر ایمنی', 'worker_leader' =>'سر کارگر', 'smart_worker' => 'کارگر ماهر', 'ordinary_worker' =>'کارگر غیر ماهر' , 'guard' => 'گارد محافظ', 'visitor' =>'بازدید کنندگان', 'serial_number' =>'نمبر مسلسل', 'father_name' => 'اسم پدر', 'tazkira_number' =>'شماره تذکره' ,'job' => 'وظیفه', 'property_area' =>'ساحه','street' => 'سرک', 'property_number' =>'نمبر ملکیت' , 'contruction_certificate' => 'مجوز ساختمانی', 'region_status' =>'وضعیت منطقه', 'type_of_activity' =>'نوع فعالیت ها', 'status_of_cunstraction' => 'وضعیت ساخت وساز', 'status_of_infractions' =>'وضعیت تخلفات' , 'violation_type' => 'نوع تخلف', 'monitor_date' =>'تاریخ مانیتورینگ', 'proceedings' =>'اجرائات', 'comment' => 'اظهار نظر','title' =>'عنوان', 'source' =>'منبع', 'destination' => 'مقصد','expiration_date' => 'تاریخ انقضا', 'category' =>'کتگوری', 'priority' =>'اولویت', 'progress' => 'جریان', 'version' =>'نمونه', 'case' => 'قضیه', 'latitude_degree' => 'درجه', 'latitude_minute' => 'دقیقه', 'latitude_second' => 'ثانیه', 'longitude_degree' => 'درجه', 'longitude_minute' => 'دقیقه', 'longitude_second' => 'ثانیه'],



];